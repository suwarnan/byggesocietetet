tinyMCE.init({
// language : "da",
    mode : "specific_textareas",
    editor_deselector : "noEditor",
    width: "700",
    height: "350",
    editor_deselector : "noMCE",
    theme : "advanced",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    remove_trailing_nbsp : true,
    //content_css: "/media/layout.css",

    force_p_newlines : true,
    force_br_newlines : false,
//    convert_newlines_to_brs : false,
    remove_linebreaks : false,
    forced_root_block : '',

    paste_auto_cleanup_on_paste : true,

    theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,strikethrough,separator,hr,bullist,numlist,separator,undo,redo,separator,link,unlink,separator,code",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
	    //    theme_advanced_blockformats : "p,h2,blockquote",
    auto_cleanup_word : true,
    plugins : "paste,save,iespell,preview,contextmenu",
    plugin_insertdate_dateFormat : "%d-%m-%Y",
    plugin_insertdate_timeFormat : "%H:%M:%S",
//    extended_valid_elements : "a[name|target|href|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]"
    valid_elements : "p,a[name|target|href|title],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],br,b,strong,table,tr,td,th,ul,li,h1,h2,h3,h4"
});

