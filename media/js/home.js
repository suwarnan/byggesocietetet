$(function() {
    if ($('.slideshow .images img').length > 1) {
        $('.slideshow').hover(
            function() { $('.controls').fadeIn(); },
            function() { $('.controls').fadeOut(); }
        );
        $('.images').cycle({
            fx: 'fade',
            speed: '750', 
            timeout: 10000, 
            pause: 0,
            next: '.controls .next', 
            prev: '.controls .prev'
        });

        var resumeImage = $('.controls .pause').attr("name"),
            pauseImage = $('.controls .pause').attr("src");
        $('.controls .pause').toggle(function() {
            $('.images').cycle('pause');
            $(this).attr({ src: resumeImage });        
        }, function() {
            $('.images').cycle('resume');
            $(this).attr({ src: pauseImage });
        });
    }
});
