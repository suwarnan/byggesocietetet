from django.contrib.auth.models import User, check_password
from winkas.models import Member

class WinkasAuthBackend(object):
    # FIXME: hvad er det?
    supports_object_permissions = True
    supports_anonymous_user = True
    supports_inactive_user = True

    def authenticate(self, username=None, password=None):
        try:
            member = Member.objects.get(username=username, password=password)
            return User.objects.get_or_create(username="winkasdummyuser")[0]
        except Member.DoesNotExist:
            return None 

    def get_user(self, user_id):
        """ Get a User object from the user_id. """
        try:
            return User.objects.get(username="winkasdummyuser")
        except User.DoesNotExist:
            return None
