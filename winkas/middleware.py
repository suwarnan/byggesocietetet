from django.db.utils import DatabaseError
from django.http import HttpResponse
from django.shortcuts import render_to_response
from main.helpers import get_context
from mysql.connector import errors as mysql_errors

class WinkasExceptionMiddleware(object):
    def process_exception(self, request, exception):
        if (type(exception) == DatabaseError or "DatabaseError" in exception.message) and "bygge" in exception.message or "MySQL" in unicode(exception):
            return render_to_response("main/winkas-error.html", {
                }, context_instance=get_context(request))
