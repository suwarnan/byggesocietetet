# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Member'
        db.create_table('medlemp', (
            ('id', self.gf('django.db.models.fields.AutoField')(unique=True, primary_key=True, db_column='Felt00')),
            ('company', self.gf('django.db.models.fields.CharField')(max_length=35, db_column='Felt01', blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=40, db_column='Felt02', blank=True)),
            ('zipcode', self.gf('django.db.models.fields.CharField')(max_length=10, db_column='Felt03', blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=25, db_column='Felt04', blank=True)),
            ('mobile', self.gf('django.db.models.fields.CharField')(max_length=16, db_column='Felt05', blank=True)),
            ('ean', self.gf('django.db.models.fields.CharField')(max_length=13, db_column='Felt06', blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=16, db_column='Felt07', blank=True)),
            ('comment', self.gf('django.db.models.fields.TextField')(db_column='Felt08', blank=True)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=12, db_column='Felt09', blank=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=50, db_column='Felt10', blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=25, db_column='Felt11', blank=True)),
            ('community', self.gf('django.db.models.fields.CharField')(max_length=25, db_column='Felt12', blank=True)),
            ('catalog_permissions', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='Felt13', blank=True)),
            ('username', self.gf('django.db.models.fields.CharField')(max_length=25, db_column='Felt14', blank=True)),
            ('created', self.gf('django.db.models.fields.DateField')(null=True, db_column='Felt15', blank=True)),
            ('updated', self.gf('django.db.models.fields.DateField')(null=True, db_column='Felt16', blank=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=25, db_column='Felt17', blank=True)),
            ('half_yearly_fee', self.gf('django.db.models.fields.FloatField')(null=True, db_column='Felt18', blank=True)),
            ('member_type', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='Felt19', blank=True)),
            ('payed', self.gf('django.db.models.fields.CharField')(max_length=15, db_column='Felt20', blank=True)),
            ('committee', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='Felt21', blank=True)),
            ('industry', self.gf('django.db.models.fields.CharField')(max_length=18, db_column='Felt22', blank=True)),
            ('resigned', self.gf('django.db.models.fields.DateField')(null=True, db_column='Felt23', blank=True)),
            ('active', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='Felt24', blank=True)),
            ('birthday', self.gf('django.db.models.fields.DateField')(null=True, db_column='Felt25', blank=True)),
            ('requisition_number', self.gf('django.db.models.fields.CharField')(max_length=35, db_column='Felt26', blank=True)),
        ))
        db.send_create_signal('winkas', ['Member'])

        # Adding model 'Felter'
        db.create_table('felter', (
            ('wpmodule', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='WPModule', blank=True)),
            ('feltnavn', self.gf('django.db.models.fields.CharField')(unique=True, max_length=24, primary_key=True, db_column='FeltNavn')),
            ('feltetiket', self.gf('django.db.models.fields.CharField')(max_length=35, db_column='FeltEtiket', blank=True)),
            ('intfelttype', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='intFeltType', blank=True)),
            ('reftype', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='RefType', blank=True)),
            ('feltsize', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='FeltSize', blank=True)),
            ('displaywidth', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='DisplayWidth', blank=True)),
            ('displayheight', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='DisplayHeight', blank=True)),
            ('taborder', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='TabOrder', blank=True)),
            ('fleft', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='FLeft', blank=True)),
            ('ftop', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='FTop', blank=True)),
            ('fpage', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='FPage', blank=True)),
            ('propflag', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='PropFlag', blank=True)),
            ('frequired', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='FRequired', blank=True)),
            ('freadonly', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='FReadOnly', blank=True)),
            ('ftabstop', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='FTabstop', blank=True)),
            ('fobs', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='FObs', blank=True)),
            ('fnonduplicates', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='FNonDuplicates', blank=True)),
            ('fnonsuplicates', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='FNonSuplicates', blank=True)),
            ('beregning', self.gf('django.db.models.fields.CharField')(max_length=128, db_column='Beregning', blank=True)),
            ('filnavn', self.gf('django.db.models.fields.CharField')(max_length=24, db_column='FilNavn', blank=True)),
            ('inet', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='INet', blank=True)),
            ('webshopfelt', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='Webshopfelt', blank=True)),
            ('fcodefield', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='FCodeField', blank=True)),
            ('fshowfield', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='FShowField', blank=True)),
            ('historik', self.gf('django.db.models.fields.CharField')(max_length=1, db_column='Historik', blank=True)),
        ))
        db.send_create_signal('winkas', ['Felter'])


    def backwards(self, orm):
        # Deleting model 'Member'
        db.delete_table('medlemp')

        # Deleting model 'Felter'
        db.delete_table('felter')


    models = {
        'winkas.felter': {
            'Meta': {'object_name': 'Felter', 'db_table': "'felter'"},
            'beregning': ('django.db.models.fields.CharField', [], {'max_length': '128', 'db_column': "'Beregning'", 'blank': 'True'}),
            'displayheight': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'DisplayHeight'", 'blank': 'True'}),
            'displaywidth': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'DisplayWidth'", 'blank': 'True'}),
            'fcodefield': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'FCodeField'", 'blank': 'True'}),
            'feltetiket': ('django.db.models.fields.CharField', [], {'max_length': '35', 'db_column': "'FeltEtiket'", 'blank': 'True'}),
            'feltnavn': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '24', 'primary_key': 'True', 'db_column': "'FeltNavn'"}),
            'feltsize': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'FeltSize'", 'blank': 'True'}),
            'filnavn': ('django.db.models.fields.CharField', [], {'max_length': '24', 'db_column': "'FilNavn'", 'blank': 'True'}),
            'fleft': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'FLeft'", 'blank': 'True'}),
            'fnonduplicates': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'FNonDuplicates'", 'blank': 'True'}),
            'fnonsuplicates': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'FNonSuplicates'", 'blank': 'True'}),
            'fobs': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'FObs'", 'blank': 'True'}),
            'fpage': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'FPage'", 'blank': 'True'}),
            'freadonly': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'FReadOnly'", 'blank': 'True'}),
            'frequired': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'FRequired'", 'blank': 'True'}),
            'fshowfield': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'FShowField'", 'blank': 'True'}),
            'ftabstop': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'FTabstop'", 'blank': 'True'}),
            'ftop': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'FTop'", 'blank': 'True'}),
            'historik': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'Historik'", 'blank': 'True'}),
            'inet': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'INet'", 'blank': 'True'}),
            'intfelttype': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'intFeltType'", 'blank': 'True'}),
            'propflag': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'PropFlag'", 'blank': 'True'}),
            'reftype': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'RefType'", 'blank': 'True'}),
            'taborder': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'TabOrder'", 'blank': 'True'}),
            'webshopfelt': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'Webshopfelt'", 'blank': 'True'}),
            'wpmodule': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'WPModule'", 'blank': 'True'})
        },
        'winkas.member': {
            'Meta': {'ordering': "('company',)", 'object_name': 'Member', 'db_table': "'medlemp'"},
            'active': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_column': "'Felt24'", 'blank': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '40', 'db_column': "'Felt02'", 'blank': 'True'}),
            'birthday': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'Felt25'", 'blank': 'True'}),
            'catalog_permissions': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'Felt13'", 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '25', 'db_column': "'Felt04'", 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'db_column': "'Felt08'", 'blank': 'True'}),
            'committee': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'Felt21'", 'blank': 'True'}),
            'community': ('django.db.models.fields.CharField', [], {'max_length': '25', 'db_column': "'Felt12'", 'blank': 'True'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '35', 'db_column': "'Felt01'", 'blank': 'True'}),
            'created': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'Felt15'", 'blank': 'True'}),
            'ean': ('django.db.models.fields.CharField', [], {'max_length': '13', 'db_column': "'Felt06'", 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_column': "'Felt10'", 'blank': 'True'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '12', 'db_column': "'Felt09'", 'blank': 'True'}),
            'half_yearly_fee': ('django.db.models.fields.FloatField', [], {'null': 'True', 'db_column': "'Felt18'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'unique': 'True', 'primary_key': 'True', 'db_column': "'Felt00'"}),
            'industry': ('django.db.models.fields.CharField', [], {'max_length': '18', 'db_column': "'Felt22'", 'blank': 'True'}),
            'member_type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'Felt19'", 'blank': 'True'}),
            'mobile': ('django.db.models.fields.CharField', [], {'max_length': '16', 'db_column': "'Felt05'", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25', 'db_column': "'Felt11'", 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '25', 'db_column': "'Felt17'", 'blank': 'True'}),
            'payed': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_column': "'Felt20'", 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '16', 'db_column': "'Felt07'", 'blank': 'True'}),
            'requisition_number': ('django.db.models.fields.CharField', [], {'max_length': '35', 'db_column': "'Felt26'", 'blank': 'True'}),
            'resigned': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'Felt23'", 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_column': "'Felt16'", 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '25', 'db_column': "'Felt14'", 'blank': 'True'}),
            'zipcode': ('django.db.models.fields.CharField', [], {'max_length': '10', 'db_column': "'Felt03'", 'blank': 'True'})
        }
    }

    complete_apps = ['winkas']