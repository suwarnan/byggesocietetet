# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models

class Abbudligning(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='Kontonr', blank=True) # Field name made lowercase.
    udligndato = models.DateTimeField(null=True, db_column='Udligndato', blank=True) # Field name made lowercase.
    betaling = models.FloatField(null=True, db_column='Betaling', blank=True) # Field name made lowercase.
    aabentbelob = models.FloatField(null=True, db_column='Aabentbelob', blank=True) # Field name made lowercase.
    obs = models.CharField(max_length=1, db_column='Obs', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'abbudligning'

class Adgang(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    id = models.CharField(max_length=10, db_column='ID', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=30, db_column='Navn', blank=True) # Field name made lowercase.
    fpassword = models.CharField(max_length=16, db_column='FPassWord', blank=True) # Field name made lowercase.
    super = models.CharField(max_length=1, db_column='Super', blank=True) # Field name made lowercase.
    finans = models.CharField(max_length=1, db_column='Finans', blank=True) # Field name made lowercase.
    medlemvis = models.CharField(max_length=1, db_column='MedlemVis', blank=True) # Field name made lowercase.
    medlemvisside2 = models.CharField(max_length=1, db_column='MedlemVisSide2', blank=True) # Field name made lowercase.
    medlemret = models.CharField(max_length=1, db_column='MedlemRet', blank=True) # Field name made lowercase.
    export = models.CharField(max_length=1, db_column='Export', blank=True) # Field name made lowercase.
    kunegnekunder = models.CharField(max_length=1, db_column='KunEgneKunder', blank=True) # Field name made lowercase.
    retfeltermedlem = models.CharField(max_length=1, db_column='RetFelterMedlem', blank=True) # Field name made lowercase.
    ejtilpasning = models.IntegerField(null=True, db_column='EjTilpasning', blank=True) # Field name made lowercase.
    moduler = models.FloatField(null=True, db_column='Moduler', blank=True) # Field name made lowercase.
    timetowait = models.IntegerField(null=True, db_column='TimeToWait', blank=True) # Field name made lowercase.
    medarb = models.IntegerField(null=True, db_column='MedArb', blank=True) # Field name made lowercase.
    retwebshop = models.IntegerField(null=True, db_column='RetWebShop', blank=True) # Field name made lowercase.
    koncernafd = models.IntegerField(null=True, db_column='KoncernAfd', blank=True) # Field name made lowercase.
    kreditfaktura = models.CharField(max_length=1, db_column='KreditFaktura', blank=True) # Field name made lowercase.
    bogforfaktura = models.CharField(max_length=1, db_column='BogforFaktura', blank=True) # Field name made lowercase.
    kobsfaktura = models.CharField(max_length=1, db_column='KobsFaktura', blank=True) # Field name made lowercase.
    smartfaktilpas = models.CharField(max_length=1, db_column='SmartfakTilpas', blank=True) # Field name made lowercase.
    sfvkarttilpas = models.CharField(max_length=1, db_column='SFVKartTilpas', blank=True) # Field name made lowercase.
    sfvkartregistrering = models.CharField(max_length=1, db_column='SFVKartRegistrering', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'adgang'

class Afdeling(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    formuekonto = models.IntegerField(null=True, db_column='FormueKonto', blank=True) # Field name made lowercase.
    resultatkonto = models.IntegerField(null=True, db_column='ResultatKonto', blank=True) # Field name made lowercase.
    budget = models.IntegerField(null=True, db_column='Budget', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'afdeling'

class Almopkrav(models.Model):
    funique = models.IntegerField(unique=True, db_column='FUnique') # Field name made lowercase.
    payunique = models.IntegerField(db_column='PAYUnique') # Field name made lowercase.
    sumkontonr = models.IntegerField(db_column='SumKontoNr') # Field name made lowercase.
    kontonr = models.IntegerField(db_column='KontoNr') # Field name made lowercase.
    autonr = models.IntegerField(db_column='AutoNr') # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=30, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    belob = models.FloatField(null=True, db_column='Belob', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    kontingent = models.IntegerField(null=True, blank=True)
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'almopkrav'

class Amter(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.IntegerField(null=True, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=30, db_column='Navn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'amter'

class Anledning(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'anledning'

class Arbkort(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    debnavn = models.CharField(max_length=40, db_column='DebNavn', blank=True) # Field name made lowercase.
    bettype = models.IntegerField(null=True, db_column='BetType', blank=True) # Field name made lowercase.
    sfhunique = models.IntegerField(null=True, db_column='SFHUnique', blank=True) # Field name made lowercase.
    ordrenr = models.IntegerField(null=True, db_column='OrdreNr', blank=True) # Field name made lowercase.
    egenunique = models.IntegerField(null=True, db_column='EgenUnique', blank=True) # Field name made lowercase.
    oprettet = models.DateTimeField(null=True, db_column='Oprettet', blank=True) # Field name made lowercase.
    afsluttet = models.DateTimeField(null=True, db_column='Afsluttet', blank=True) # Field name made lowercase.
    oprettid = models.DateTimeField(null=True, db_column='OpretTid', blank=True) # Field name made lowercase.
    afsluttid = models.DateTimeField(null=True, db_column='AfslutTid', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    rma = models.CharField(max_length=40, db_column='RMA', blank=True) # Field name made lowercase.
    rmasendt = models.DateTimeField(null=True, db_column='RMASendt', blank=True) # Field name made lowercase.
    mobil = models.CharField(max_length=16, db_column='Mobil', blank=True) # Field name made lowercase.
    rekvnr = models.CharField(max_length=50, db_column='Rekvnr', blank=True) # Field name made lowercase.
    modtagetaf = models.CharField(max_length=50, db_column='Modtagetaf', blank=True) # Field name made lowercase.
    opretdato = models.DateTimeField(null=True, db_column='Opretdato', blank=True) # Field name made lowercase.
    montoer = models.CharField(max_length=50, db_column='Montoer', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arbkort'

class Arblinie(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    farbunique = models.IntegerField(null=True, db_column='FArbUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=72, db_column='VareTekst', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    pris = models.FloatField(null=True, db_column='Pris', blank=True) # Field name made lowercase.
    salgseimoms = models.IntegerField(null=True, db_column='SalgsEIMoms', blank=True) # Field name made lowercase.
    bogfort = models.CharField(max_length=1, db_column='Bogfort', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    garankred = models.IntegerField(null=True, db_column='GaranKred', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arblinie'

class Arbsetup(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    arbkort = models.CharField(max_length=12, db_column='ArbKort', blank=True) # Field name made lowercase.
    showprint = models.CharField(max_length=1, db_column='ShowPrint', blank=True) # Field name made lowercase.
    autoorder = models.CharField(max_length=1, db_column='AutoOrder', blank=True) # Field name made lowercase.
    defstatus = models.IntegerField(null=True, db_column='DefStatus', blank=True) # Field name made lowercase.
    fakgaran = models.CharField(max_length=1, db_column='FakGaran', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arbsetup'

class Arrangement(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    antalv = models.IntegerField(null=True, db_column='AntalV', blank=True) # Field name made lowercase.
    antalb = models.IntegerField(null=True, db_column='AntalB', blank=True) # Field name made lowercase.
    oprettet = models.DateTimeField(null=True, db_column='Oprettet', blank=True) # Field name made lowercase.
    bekraft = models.DateTimeField(null=True, db_column='Bekraft', blank=True) # Field name made lowercase.
    start = models.DateTimeField(null=True, db_column='Start', blank=True) # Field name made lowercase.
    slut = models.DateTimeField(null=True, db_column='Slut', blank=True) # Field name made lowercase.
    starttid = models.DateTimeField(null=True, db_column='StartTid', blank=True) # Field name made lowercase.
    sluttid = models.DateTimeField(null=True, db_column='SlutTid', blank=True) # Field name made lowercase.
    anledning = models.IntegerField(null=True, db_column='Anledning', blank=True) # Field name made lowercase.
    fremsendt = models.CharField(max_length=1, db_column='Fremsendt', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    printstatus = models.IntegerField(null=True, db_column='PrintStatus', blank=True) # Field name made lowercase.
    bettype = models.IntegerField(null=True, db_column='Bettype', blank=True) # Field name made lowercase.
    sfhunique = models.IntegerField(null=True, db_column='SFHUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arrangement'

class Arrdato(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    farrunique = models.IntegerField(null=True, db_column='FArrUnique', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arrdato'

class Arrfac(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    farrunique = models.IntegerField(null=True, db_column='FArrUnique', blank=True) # Field name made lowercase.
    ffacunique = models.IntegerField(null=True, db_column='FFacUnique', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=40, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arrfac'

class Arrlinie(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    farrunique = models.IntegerField(null=True, db_column='FArrUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=72, db_column='VareTekst', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    pris = models.FloatField(null=True, db_column='Pris', blank=True) # Field name made lowercase.
    bogfort = models.CharField(max_length=1, db_column='Bogfort', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arrlinie'

class Arrress(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    farrunique = models.IntegerField(null=True, db_column='FArrUnique', blank=True) # Field name made lowercase.
    fressunique = models.IntegerField(null=True, db_column='FRessUnique', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arrress'

class Arrsetup(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    design = models.CharField(max_length=12, db_column='Design', blank=True) # Field name made lowercase.
    showprint = models.CharField(max_length=1, db_column='ShowPrint', blank=True) # Field name made lowercase.
    defstatus = models.IntegerField(null=True, db_column='DefStatus', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arrsetup'

class Arter(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    tspligtig = models.IntegerField(null=True, db_column='TSPligtig', blank=True) # Field name made lowercase.
    betaling = models.IntegerField(null=True, db_column='Betaling', blank=True) # Field name made lowercase.
    eksternart = models.CharField(max_length=2, db_column='EksternArt', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=64, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'arter'

class Avery(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    a_label = models.CharField(max_length=30, db_column='A_label', blank=True) # Field name made lowercase.
    a_type = models.CharField(max_length=30, db_column='A_Type', blank=True) # Field name made lowercase.
    e_type = models.CharField(max_length=30, db_column='E_type', blank=True) # Field name made lowercase.
    s_margen = models.FloatField(null=True, db_column='S_margen', blank=True) # Field name made lowercase.
    t_margen = models.FloatField(null=True, db_column='T_margen', blank=True) # Field name made lowercase.
    l_pitch = models.FloatField(null=True, db_column='L_Pitch', blank=True) # Field name made lowercase.
    v_pitch = models.FloatField(null=True, db_column='V_Pitch', blank=True) # Field name made lowercase.
    e_hojde = models.FloatField(null=True, db_column='E_hojde', blank=True) # Field name made lowercase.
    e_brede = models.FloatField(null=True, db_column='E_brede', blank=True) # Field name made lowercase.
    a_henad = models.FloatField(null=True, db_column='A_henad', blank=True) # Field name made lowercase.
    a_nedad = models.FloatField(null=True, db_column='A_nedad', blank=True) # Field name made lowercase.
    side = models.CharField(max_length=10, blank=True)
    class Meta:
        db_table = 'avery'

class Bankkontogrupper(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    regnr = models.IntegerField(null=True, db_column='RegNr', blank=True) # Field name made lowercase.
    bankkonto = models.CharField(max_length=10, db_column='BankKonto', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    koncern = models.IntegerField(null=True, db_column='Koncern', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bankkontogrupper'

class Banklog(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    logdato = models.DateTimeField(null=True, db_column='LogDato', blank=True) # Field name made lowercase.
    logtekst = models.CharField(max_length=20, db_column='LogTekst', blank=True) # Field name made lowercase.
    indbetalingsdato = models.DateTimeField(null=True, db_column='Indbetalingsdato', blank=True) # Field name made lowercase.
    kreditorid = models.CharField(max_length=19, db_column='KreditorId', blank=True) # Field name made lowercase.
    betalerid = models.CharField(max_length=19, db_column='BetalerId', blank=True) # Field name made lowercase.
    beloeb = models.FloatField(null=True, db_column='Beloeb', blank=True) # Field name made lowercase.
    gebyrtype = models.CharField(max_length=2, db_column='GebyrType', blank=True) # Field name made lowercase.
    gebyrbeloeb = models.FloatField(null=True, db_column='GebyrBeloeb', blank=True) # Field name made lowercase.
    transkode = models.CharField(max_length=4, db_column='TransKode', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'banklog'

class Berotype(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'berotype'

class Besth(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fralager = models.IntegerField(null=True, db_column='FraLager', blank=True) # Field name made lowercase.
    tillager = models.IntegerField(null=True, db_column='TilLager', blank=True) # Field name made lowercase.
    medarb = models.IntegerField(null=True, db_column='Medarb', blank=True) # Field name made lowercase.
    oprettet = models.DateTimeField(null=True, db_column='Oprettet', blank=True) # Field name made lowercase.
    levering = models.DateTimeField(null=True, db_column='Levering', blank=True) # Field name made lowercase.
    behandlet = models.IntegerField(null=True, db_column='Behandlet', blank=True) # Field name made lowercase.
    note = models.TextField(db_column='Note', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'besth'

class Bestl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    besthfunique = models.IntegerField(null=True, db_column='BestHFUnique', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=64, db_column='VareTekst', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    salgsenhed = models.CharField(max_length=12, db_column='SalgsEnhed', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    godkendtantal = models.FloatField(null=True, db_column='GodkendtAntal', blank=True) # Field name made lowercase.
    medarb = models.IntegerField(null=True, db_column='Medarb', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bestl'

class Betaling(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=10, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=40, db_column='Navn', blank=True) # Field name made lowercase.
    aktivkonto = models.IntegerField(null=True, db_column='AktivKonto', blank=True) # Field name made lowercase.
    gebyrsats = models.FloatField(null=True, db_column='GebyrSats', blank=True) # Field name made lowercase.
    gebyrkonto = models.IntegerField(null=True, db_column='GebyrKonto', blank=True) # Field name made lowercase.
    prefix = models.CharField(max_length=12, db_column='Prefix', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'betaling'

class Betform(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    atype = models.IntegerField(null=True, db_column='AType', blank=True) # Field name made lowercase.
    akontant = models.IntegerField(null=True, db_column='AKontant', blank=True) # Field name made lowercase.
    apbs = models.IntegerField(null=True, db_column='APbs', blank=True) # Field name made lowercase.
    cash = models.FloatField(null=True, db_column='Cash', blank=True) # Field name made lowercase.
    aafr = models.IntegerField(null=True, db_column='AAfr', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'betform'

class Boadic(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fbmunique = models.IntegerField(null=True, db_column='FBMUnique', blank=True) # Field name made lowercase.
    fname = models.CharField(max_length=32, db_column='FName', blank=True) # Field name made lowercase.
    startfelt = models.CharField(max_length=32, db_column='StartFelt', blank=True) # Field name made lowercase.
    fautonumber = models.IntegerField(null=True, db_column='FAutoNumber', blank=True) # Field name made lowercase.
    kontonrfra = models.IntegerField(null=True, db_column='KontoNrFra', blank=True) # Field name made lowercase.
    kontonrtil = models.IntegerField(null=True, db_column='KontoNrTil', blank=True) # Field name made lowercase.
    ffields = models.TextField(db_column='FFields', blank=True) # Field name made lowercase.
    fnote = models.TextField(db_column='FNote', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'boadic'

class Boadicstatus(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    boafunique = models.IntegerField(null=True, db_column='BoaFUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    user = models.CharField(max_length=32, db_column='User', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'boadicstatus'

class Boamodule(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fname = models.CharField(max_length=32, db_column='FName', blank=True) # Field name made lowercase.
    fobject = models.TextField(db_column='FObject', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'boamodule'

class Boaopkravs(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    wpmodule = models.IntegerField(null=True, db_column='WPModule', blank=True) # Field name made lowercase.
    udsnit = models.IntegerField(null=True, db_column='Udsnit', blank=True) # Field name made lowercase.
    specmoms = models.IntegerField(null=True, db_column='SpecMoms', blank=True) # Field name made lowercase.
    specperiode = models.IntegerField(null=True, db_column='SpecPeriode', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    ffields = models.CharField(max_length=255, db_column='FFields', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'boaopkravs'

class Boaopkrlin(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fhunique = models.IntegerField(null=True, db_column='FHUnique', blank=True) # Field name made lowercase.
    ftype = models.IntegerField(null=True, db_column='FType', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    specadvis = models.CharField(max_length=1, db_column='SpecAdvis', blank=True) # Field name made lowercase.
    posttekst = models.CharField(max_length=50, db_column='PostTekst', blank=True) # Field name made lowercase.
    fastbelob = models.FloatField(null=True, db_column='FastBelob', blank=True) # Field name made lowercase.
    brugfastbelob = models.CharField(max_length=1, db_column='BrugFastBelob', blank=True) # Field name made lowercase.
    tekstindex = models.IntegerField(null=True, db_column='TekstIndex', blank=True) # Field name made lowercase.
    momsindex = models.IntegerField(null=True, db_column='MomsIndex', blank=True) # Field name made lowercase.
    engangsvarer = models.CharField(max_length=24, db_column='EngangsVarer', blank=True) # Field name made lowercase.
    valutafelt = models.CharField(max_length=24, db_column='ValutaFelt', blank=True) # Field name made lowercase.
    tekstfelt = models.CharField(max_length=24, db_column='TekstFelt', blank=True) # Field name made lowercase.
    konteringsfelt = models.CharField(max_length=24, db_column='KonteringsFelt', blank=True) # Field name made lowercase.
    artfelt = models.CharField(max_length=24, db_column='ArtFelt', blank=True) # Field name made lowercase.
    projektfelt = models.CharField(max_length=24, db_column='ProjektFelt', blank=True) # Field name made lowercase.
    afdelingfelt = models.CharField(max_length=24, db_column='AfdelingFelt', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'boaopkrlin'

class Bogf1(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    modkonto = models.IntegerField(null=True, db_column='ModKonto', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    bilagnr = models.IntegerField(null=True, db_column='BilagNr', blank=True) # Field name made lowercase.
    kontraktnr = models.CharField(max_length=15, db_column='KontraktNr', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    debet = models.FloatField(null=True, db_column='Debet', blank=True) # Field name made lowercase.
    kredit = models.FloatField(null=True, db_column='Kredit', blank=True) # Field name made lowercase.
    omregnet = models.FloatField(null=True, db_column='Omregnet', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    ocrident = models.CharField(max_length=15, db_column='OCRIdent', blank=True) # Field name made lowercase.
    opkrkode = models.IntegerField(null=True, db_column='OpkrKode', blank=True) # Field name made lowercase.
    perkode = models.IntegerField(null=True, db_column='PerKode', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    funcref = models.IntegerField(null=True, db_column='FuncRef', blank=True) # Field name made lowercase.
    functype = models.IntegerField(null=True, db_column='FuncType', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    finadim6 = models.IntegerField(null=True, db_column='FinaDim6', blank=True) # Field name made lowercase.
    krebetalunique = models.IntegerField(null=True, db_column='KreBetalUnique', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    udligningskode = models.IntegerField(null=True, db_column='UdligningsKode', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    rettetaf = models.CharField(max_length=10, db_column='RettetAf', blank=True) # Field name made lowercase.
    dimension = models.IntegerField(null=True, db_column='Dimension', blank=True) # Field name made lowercase.
    dimfunique = models.IntegerField(null=True, db_column='DimFUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bogf1'

class Bogf2(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    modkonto = models.IntegerField(null=True, db_column='ModKonto', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    bilagnr = models.IntegerField(null=True, db_column='BilagNr', blank=True) # Field name made lowercase.
    kontraktnr = models.CharField(max_length=15, db_column='KontraktNr', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    debet = models.FloatField(null=True, db_column='Debet', blank=True) # Field name made lowercase.
    kredit = models.FloatField(null=True, db_column='Kredit', blank=True) # Field name made lowercase.
    omregnet = models.FloatField(null=True, db_column='Omregnet', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    ocrident = models.CharField(max_length=15, db_column='OCRIdent', blank=True) # Field name made lowercase.
    opkrkode = models.IntegerField(null=True, db_column='OpkrKode', blank=True) # Field name made lowercase.
    perkode = models.IntegerField(null=True, db_column='PerKode', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    funcref = models.IntegerField(null=True, db_column='FuncRef', blank=True) # Field name made lowercase.
    functype = models.IntegerField(null=True, db_column='FuncType', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    finadim6 = models.IntegerField(null=True, db_column='FinaDim6', blank=True) # Field name made lowercase.
    krebetalunique = models.IntegerField(null=True, db_column='KreBetalUnique', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    udligningskode = models.IntegerField(null=True, db_column='UdligningsKode', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    rettetaf = models.CharField(max_length=10, db_column='RettetAf', blank=True) # Field name made lowercase.
    dimension = models.IntegerField(null=True, db_column='Dimension', blank=True) # Field name made lowercase.
    dimfunique = models.IntegerField(null=True, db_column='DimFUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bogf2'

class Bogfa(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    modkonto = models.IntegerField(null=True, db_column='ModKonto', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    bilagnr = models.IntegerField(null=True, db_column='BilagNr', blank=True) # Field name made lowercase.
    kontraktnr = models.CharField(max_length=15, db_column='KontraktNr', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    debet = models.FloatField(null=True, db_column='Debet', blank=True) # Field name made lowercase.
    kredit = models.FloatField(null=True, db_column='Kredit', blank=True) # Field name made lowercase.
    omregnet = models.FloatField(null=True, db_column='Omregnet', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    ocrident = models.CharField(max_length=15, db_column='OCRIdent', blank=True) # Field name made lowercase.
    opkrkode = models.IntegerField(null=True, db_column='OpkrKode', blank=True) # Field name made lowercase.
    perkode = models.IntegerField(null=True, db_column='PerKode', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    funcref = models.IntegerField(null=True, db_column='FuncRef', blank=True) # Field name made lowercase.
    functype = models.IntegerField(null=True, db_column='FuncType', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    finadim6 = models.IntegerField(null=True, db_column='FinaDim6', blank=True) # Field name made lowercase.
    krebetalunique = models.IntegerField(null=True, db_column='KreBetalUnique', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    udligningskode = models.IntegerField(null=True, db_column='UdligningsKode', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    rettetaf = models.CharField(max_length=10, db_column='RettetAf', blank=True) # Field name made lowercase.
    dimension = models.IntegerField(null=True, db_column='Dimension', blank=True) # Field name made lowercase.
    dimfunique = models.IntegerField(null=True, db_column='DimFUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bogfa'

class Bogfa2(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    periode = models.IntegerField(null=True, db_column='Periode', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    primo = models.FloatField(null=True, db_column='Primo', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    samlekonto = models.IntegerField(null=True, db_column='SamleKonto', blank=True) # Field name made lowercase.
    koncern = models.IntegerField(null=True, db_column='Koncern', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bogfa2'

class Bogfb(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    modkonto = models.IntegerField(null=True, db_column='ModKonto', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    bilagnr = models.IntegerField(null=True, db_column='BilagNr', blank=True) # Field name made lowercase.
    kontraktnr = models.CharField(max_length=15, db_column='KontraktNr', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    debet = models.FloatField(null=True, db_column='Debet', blank=True) # Field name made lowercase.
    kredit = models.FloatField(null=True, db_column='Kredit', blank=True) # Field name made lowercase.
    omregnet = models.FloatField(null=True, db_column='Omregnet', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    ocrident = models.CharField(max_length=15, db_column='OCRIdent', blank=True) # Field name made lowercase.
    opkrkode = models.IntegerField(null=True, db_column='OpkrKode', blank=True) # Field name made lowercase.
    perkode = models.IntegerField(null=True, db_column='PerKode', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    funcref = models.IntegerField(null=True, db_column='FuncRef', blank=True) # Field name made lowercase.
    functype = models.IntegerField(null=True, db_column='FuncType', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    finadim6 = models.IntegerField(null=True, db_column='FinaDim6', blank=True) # Field name made lowercase.
    krebetalunique = models.IntegerField(null=True, db_column='KreBetalUnique', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    udligningskode = models.IntegerField(null=True, db_column='UdligningsKode', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    rettetaf = models.CharField(max_length=10, db_column='RettetAf', blank=True) # Field name made lowercase.
    dimension = models.IntegerField(null=True, db_column='Dimension', blank=True) # Field name made lowercase.
    dimfunique = models.IntegerField(null=True, db_column='DimFUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bogfb'

class Bogfe(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    modkonto = models.IntegerField(null=True, db_column='ModKonto', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    bilagnr = models.IntegerField(null=True, db_column='BilagNr', blank=True) # Field name made lowercase.
    kontraktnr = models.CharField(max_length=15, db_column='KontraktNr', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    debet = models.FloatField(null=True, db_column='Debet', blank=True) # Field name made lowercase.
    kredit = models.FloatField(null=True, db_column='Kredit', blank=True) # Field name made lowercase.
    omregnet = models.FloatField(null=True, db_column='Omregnet', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    ocrident = models.CharField(max_length=15, db_column='OCRIdent', blank=True) # Field name made lowercase.
    opkrkode = models.IntegerField(null=True, db_column='OpkrKode', blank=True) # Field name made lowercase.
    perkode = models.IntegerField(null=True, db_column='PerKode', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    funcref = models.IntegerField(null=True, db_column='FuncRef', blank=True) # Field name made lowercase.
    functype = models.IntegerField(null=True, db_column='FuncType', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    finadim6 = models.IntegerField(null=True, db_column='FinaDim6', blank=True) # Field name made lowercase.
    krebetalunique = models.IntegerField(null=True, db_column='KreBetalUnique', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    udligningskode = models.IntegerField(null=True, db_column='UdligningsKode', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    rettetaf = models.CharField(max_length=10, db_column='RettetAf', blank=True) # Field name made lowercase.
    dimension = models.IntegerField(null=True, db_column='Dimension', blank=True) # Field name made lowercase.
    dimfunique = models.IntegerField(null=True, db_column='DimFUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bogfe'

class Bogfg(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    modkonto = models.IntegerField(null=True, db_column='ModKonto', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    bilagnr = models.IntegerField(null=True, db_column='BilagNr', blank=True) # Field name made lowercase.
    kontraktnr = models.CharField(max_length=15, db_column='KontraktNr', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    debet = models.FloatField(null=True, db_column='Debet', blank=True) # Field name made lowercase.
    kredit = models.FloatField(null=True, db_column='Kredit', blank=True) # Field name made lowercase.
    omregnet = models.FloatField(null=True, db_column='Omregnet', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    ocrident = models.CharField(max_length=15, db_column='OCRIdent', blank=True) # Field name made lowercase.
    opkrkode = models.IntegerField(null=True, db_column='OpkrKode', blank=True) # Field name made lowercase.
    perkode = models.IntegerField(null=True, db_column='PerKode', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    funcref = models.IntegerField(null=True, db_column='FuncRef', blank=True) # Field name made lowercase.
    functype = models.IntegerField(null=True, db_column='FuncType', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    finadim6 = models.IntegerField(null=True, db_column='FinaDim6', blank=True) # Field name made lowercase.
    krebetalunique = models.IntegerField(null=True, db_column='KreBetalUnique', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    udligningskode = models.IntegerField(null=True, db_column='UdligningsKode', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    rettetaf = models.CharField(max_length=10, db_column='RettetAf', blank=True) # Field name made lowercase.
    dimension = models.IntegerField(null=True, db_column='Dimension', blank=True) # Field name made lowercase.
    dimfunique = models.IntegerField(null=True, db_column='DimFUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bogfg'

class Bogfg2(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    reskonto = models.IntegerField(null=True, db_column='ResKonto', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    bilagnr = models.IntegerField(null=True, db_column='BilagNr', blank=True) # Field name made lowercase.
    kontraktnr = models.CharField(max_length=15, db_column='KontraktNr', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    debet = models.FloatField(null=True, db_column='Debet', blank=True) # Field name made lowercase.
    kredit = models.FloatField(null=True, db_column='Kredit', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    finadim6 = models.IntegerField(null=True, db_column='FinaDim6', blank=True) # Field name made lowercase.
    koncern = models.IntegerField(null=True, db_column='Koncern', blank=True) # Field name made lowercase.
    kladdenr = models.IntegerField(null=True, db_column='KladdeNr', blank=True) # Field name made lowercase.
    ocrident = models.CharField(max_length=15, db_column='OCRIdent', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    mk = models.IntegerField(null=True, db_column='MK', blank=True) # Field name made lowercase.
    valutafunique = models.IntegerField(null=True, db_column='ValutaFUnique', blank=True) # Field name made lowercase.
    valuta = models.FloatField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    udligningskode = models.IntegerField(null=True, db_column='UdligningsKode', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    kludlignet = models.FloatField(null=True, db_column='KLUdlignet', blank=True) # Field name made lowercase.
    opkrkode = models.IntegerField(null=True, db_column='OpkrKode', blank=True) # Field name made lowercase.
    perkode = models.IntegerField(null=True, db_column='PerKode', blank=True) # Field name made lowercase.
    journalnr = models.IntegerField(null=True, db_column='JournalNr', blank=True) # Field name made lowercase.
    rettetaf = models.CharField(max_length=10, db_column='RettetAf', blank=True) # Field name made lowercase.
    moms = models.FloatField(null=True, db_column='Moms', blank=True) # Field name made lowercase.
    ekspdato = models.DateTimeField(null=True, db_column='EkspDato', blank=True) # Field name made lowercase.
    dimension = models.IntegerField(null=True, db_column='Dimension', blank=True) # Field name made lowercase.
    dimfunique = models.IntegerField(null=True, db_column='DimFUnique', blank=True) # Field name made lowercase.
    krebetalunique = models.IntegerField(null=True, db_column='KreBetalUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bogfg2'

class Bonkart(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    bonnr = models.IntegerField(null=True, db_column='BonNr', blank=True) # Field name made lowercase.
    kortnr = models.IntegerField(null=True, db_column='KortNr', blank=True) # Field name made lowercase.
    korttype = models.IntegerField(null=True, db_column='KortType', blank=True) # Field name made lowercase.
    ekspedient = models.IntegerField(null=True, db_column='Ekspedient', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    kundenr = models.IntegerField(null=True, db_column='KundeNr', blank=True) # Field name made lowercase.
    terminal = models.IntegerField(null=True, db_column='Terminal', blank=True) # Field name made lowercase.
    prisinclmoms = models.FloatField(null=True, db_column='PrisInclMoms', blank=True) # Field name made lowercase.
    moms = models.FloatField(null=True, db_column='Moms', blank=True) # Field name made lowercase.
    prisexmoms = models.FloatField(null=True, db_column='PrisExMoms', blank=True) # Field name made lowercase.
    debiteret = models.FloatField(null=True, db_column='Debiteret', blank=True) # Field name made lowercase.
    modtaget = models.FloatField(null=True, db_column='Modtaget', blank=True) # Field name made lowercase.
    bogfort = models.DateTimeField(null=True, db_column='Bogfort', blank=True) # Field name made lowercase.
    gebyr = models.FloatField(null=True, db_column='Gebyr', blank=True) # Field name made lowercase.
    betaltil = models.DateTimeField(null=True, db_column='Betaltil', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bonkart'

class Bookingpage(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    pagetitle = models.CharField(max_length=50, db_column='PageTitle', blank=True) # Field name made lowercase.
    bgcolor = models.CharField(max_length=10, db_column='BgColor', blank=True) # Field name made lowercase.
    bgimage = models.CharField(max_length=50, db_column='BgImage', blank=True) # Field name made lowercase.
    tablealign = models.CharField(max_length=10, db_column='TableAlign', blank=True) # Field name made lowercase.
    tablewidth = models.IntegerField(null=True, db_column='TableWidth', blank=True) # Field name made lowercase.
    headerheight = models.IntegerField(null=True, db_column='HeaderHeight', blank=True) # Field name made lowercase.
    headerbgcolor = models.CharField(max_length=10, db_column='HeaderBgColor', blank=True) # Field name made lowercase.
    headerbgimage = models.CharField(max_length=50, db_column='HeaderBgImage', blank=True) # Field name made lowercase.
    headerlowheight = models.IntegerField(null=True, db_column='HeaderLowHeight', blank=True) # Field name made lowercase.
    headerlowbgcolor = models.CharField(max_length=10, db_column='HeaderLowBgColor', blank=True) # Field name made lowercase.
    headerlowbgimage = models.CharField(max_length=50, db_column='HeaderLowBgImage', blank=True) # Field name made lowercase.
    menuwidth = models.IntegerField(null=True, db_column='MenuWidth', blank=True) # Field name made lowercase.
    menubgcolor = models.CharField(max_length=10, db_column='MenuBgColor', blank=True) # Field name made lowercase.
    menubgimage = models.CharField(max_length=50, db_column='MenuBgImage', blank=True) # Field name made lowercase.
    menuheaderbgcolor = models.CharField(max_length=10, db_column='MenuHeaderBgColor', blank=True) # Field name made lowercase.
    menuheadercolor = models.CharField(max_length=10, db_column='MenuHeaderColor', blank=True) # Field name made lowercase.
    mainbgcolor = models.CharField(max_length=10, db_column='MainBgColor', blank=True) # Field name made lowercase.
    mainbgimage = models.CharField(max_length=50, db_column='MainBgImage', blank=True) # Field name made lowercase.
    rightwidth = models.IntegerField(null=True, db_column='RightWidth', blank=True) # Field name made lowercase.
    rightbgcolor = models.CharField(max_length=10, db_column='RightBgColor', blank=True) # Field name made lowercase.
    rightbgimage = models.CharField(max_length=50, db_column='RightBgImage', blank=True) # Field name made lowercase.
    rightheaderbgcolor = models.CharField(max_length=10, db_column='RightHeaderBgColor', blank=True) # Field name made lowercase.
    rightheadercolor = models.CharField(max_length=10, db_column='RightHeaderColor', blank=True) # Field name made lowercase.
    rightmainbgcolor = models.CharField(max_length=10, db_column='RightMainBgColor', blank=True) # Field name made lowercase.
    rightmaincolor = models.CharField(max_length=10, db_column='RightMainColor', blank=True) # Field name made lowercase.
    footerheight = models.IntegerField(null=True, db_column='FooterHeight', blank=True) # Field name made lowercase.
    footerbgcolor = models.CharField(max_length=10, db_column='FooterBgColor', blank=True) # Field name made lowercase.
    footerbgimage = models.CharField(max_length=50, db_column='FooterBgImage', blank=True) # Field name made lowercase.
    praesentation = models.IntegerField(null=True, db_column='Praesentation', blank=True) # Field name made lowercase.
    menucolor = models.CharField(max_length=10, db_column='Menucolor', blank=True) # Field name made lowercase.
    footercolor = models.CharField(max_length=10, db_column='FooterColor', blank=True) # Field name made lowercase.
    valuta = models.CharField(max_length=10, db_column='Valuta', blank=True) # Field name made lowercase.
    headerlowvagon = models.CharField(max_length=15, db_column='Headerlowvagon', blank=True) # Field name made lowercase.
    mainvagon = models.CharField(max_length=15, db_column='Mainvagon', blank=True) # Field name made lowercase.
    font = models.CharField(max_length=50, db_column='Font', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'bookingpage'

class Budget(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    periode = models.IntegerField(null=True, db_column='Periode', blank=True) # Field name made lowercase.
    budgettype = models.IntegerField(null=True, db_column='BudgetType', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    tusinde = models.IntegerField(null=True, db_column='Tusinde', blank=True) # Field name made lowercase.
    prafdeling = models.IntegerField(null=True, db_column='prAfdeling', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    sumbudget = models.CharField(max_length=1, db_column='Sumbudget', blank=True) # Field name made lowercase.
    budgets = models.CharField(max_length=255, db_column='Budgets', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'budget'

class Budgettal(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    budgetfunique = models.IntegerField(null=True, db_column='BudgetFUnique', blank=True) # Field name made lowercase.
    periode = models.IntegerField(null=True, db_column='Periode', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    kontotype = models.IntegerField(null=True, db_column='KontoType', blank=True) # Field name made lowercase.
    primo = models.FloatField(null=True, db_column='Primo', blank=True) # Field name made lowercase.
    belob1 = models.FloatField(null=True, db_column='Belob1', blank=True) # Field name made lowercase.
    belob2 = models.FloatField(null=True, db_column='Belob2', blank=True) # Field name made lowercase.
    belob3 = models.FloatField(null=True, db_column='Belob3', blank=True) # Field name made lowercase.
    belob4 = models.FloatField(null=True, db_column='Belob4', blank=True) # Field name made lowercase.
    belob5 = models.FloatField(null=True, db_column='Belob5', blank=True) # Field name made lowercase.
    belob6 = models.FloatField(null=True, db_column='Belob6', blank=True) # Field name made lowercase.
    belob7 = models.FloatField(null=True, db_column='Belob7', blank=True) # Field name made lowercase.
    belob8 = models.FloatField(null=True, db_column='Belob8', blank=True) # Field name made lowercase.
    belob9 = models.FloatField(null=True, db_column='Belob9', blank=True) # Field name made lowercase.
    belob10 = models.FloatField(null=True, db_column='Belob10', blank=True) # Field name made lowercase.
    belob11 = models.FloatField(null=True, db_column='Belob11', blank=True) # Field name made lowercase.
    belob12 = models.FloatField(null=True, db_column='Belob12', blank=True) # Field name made lowercase.
    belob13 = models.FloatField(null=True, db_column='Belob13', blank=True) # Field name made lowercase.
    belob14 = models.FloatField(null=True, db_column='Belob14', blank=True) # Field name made lowercase.
    belob15 = models.FloatField(null=True, db_column='Belob15', blank=True) # Field name made lowercase.
    belob16 = models.FloatField(null=True, db_column='Belob16', blank=True) # Field name made lowercase.
    belob17 = models.FloatField(null=True, db_column='Belob17', blank=True) # Field name made lowercase.
    belob18 = models.FloatField(null=True, db_column='Belob18', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'budgettal'

class Cardtime(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=50, db_column='Navn', blank=True) # Field name made lowercase.
    mansttid = models.TextField(db_column='ManSTTid', blank=True) # Field name made lowercase. This field type is a guess.
    mansltid = models.TextField(db_column='ManSLTid', blank=True) # Field name made lowercase. This field type is a guess.
    tirsttid = models.TextField(db_column='TirSTTid', blank=True) # Field name made lowercase. This field type is a guess.
    tirsltid = models.TextField(db_column='TirSLTid', blank=True) # Field name made lowercase. This field type is a guess.
    onssttid = models.TextField(db_column='OnsSTTid', blank=True) # Field name made lowercase. This field type is a guess.
    onssltid = models.TextField(db_column='OnsSLTid', blank=True) # Field name made lowercase. This field type is a guess.
    torsttid = models.TextField(db_column='TorSTTid', blank=True) # Field name made lowercase. This field type is a guess.
    torsltid = models.TextField(db_column='TorSLTid', blank=True) # Field name made lowercase. This field type is a guess.
    fresttid = models.TextField(db_column='FreSTTid', blank=True) # Field name made lowercase. This field type is a guess.
    fresltid = models.TextField(db_column='FreSLTid', blank=True) # Field name made lowercase. This field type is a guess.
    lorsttid = models.TextField(db_column='LorSTTid', blank=True) # Field name made lowercase. This field type is a guess.
    lorsltid = models.TextField(db_column='LorSLTid', blank=True) # Field name made lowercase. This field type is a guess.
    sonsttid = models.TextField(db_column='SonSTTid', blank=True) # Field name made lowercase. This field type is a guess.
    sonsltid = models.TextField(db_column='SonSLTid', blank=True) # Field name made lowercase. This field type is a guess.
    class Meta:
        db_table = 'cardtime'

class Clients(models.Model):
    stationname = models.CharField(unique=True, max_length=32, db_column='StationName') # Field name made lowercase.
    stationno = models.IntegerField(null=True, db_column='StationNo', blank=True) # Field name made lowercase.
    name = models.CharField(max_length=32, db_column='Name', blank=True) # Field name made lowercase.
    factive = models.IntegerField(null=True, db_column='FActive', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'clients'

class Comports(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    porttype = models.IntegerField(null=True, db_column='PortType', blank=True) # Field name made lowercase.
    comport = models.IntegerField(null=True, db_column='ComPort', blank=True) # Field name made lowercase.
    starttegn = models.CharField(max_length=16, db_column='StartTegn', blank=True) # Field name made lowercase.
    sluttegn = models.CharField(max_length=16, db_column='SlutTegn', blank=True) # Field name made lowercase.
    crlftegn = models.CharField(max_length=16, db_column='CRLFTegn', blank=True) # Field name made lowercase.
    start2tegn = models.CharField(max_length=16, db_column='Start2Tegn', blank=True) # Field name made lowercase.
    millisec = models.IntegerField(null=True, db_column='MilliSec', blank=True) # Field name made lowercase.
    startignore = models.CharField(max_length=1, db_column='StartIgnore', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'comports'

class Dataupdate(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    wpmodule = models.IntegerField(null=True, db_column='WPModule', blank=True) # Field name made lowercase.
    wpfelt00 = models.IntegerField(null=True, db_column='WPFelt00', blank=True) # Field name made lowercase.
    wpupdatetype = models.IntegerField(null=True, db_column='WPUpdateType', blank=True) # Field name made lowercase.
    wpfieldname = models.CharField(max_length=16, db_column='WPFieldName', blank=True) # Field name made lowercase.
    wpvalue = models.TextField(db_column='WPValue', blank=True) # Field name made lowercase.
    wptimestamp = models.DateTimeField(null=True, db_column='WPTimeStamp', blank=True) # Field name made lowercase.
    wpapproved = models.CharField(max_length=1, db_column='WPApproved', blank=True) # Field name made lowercase.
    wpapprovedby = models.CharField(max_length=50, db_column='WPApprovedby', blank=True) # Field name made lowercase.
    wpapproveddate = models.DateTimeField(null=True, db_column='WPApprovedDate', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'dataupdate'

class Debbero(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    fradato = models.DateTimeField(null=True, db_column='FraDato', blank=True) # Field name made lowercase.
    tildato = models.DateTimeField(null=True, db_column='TilDato', blank=True) # Field name made lowercase.
    antaldage = models.IntegerField(null=True, db_column='Antaldage', blank=True) # Field name made lowercase.
    belob = models.FloatField(null=True, db_column='Belob', blank=True) # Field name made lowercase.
    virkning = models.CharField(max_length=1, db_column='Virkning', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    resultat = models.TextField(db_column='Resultat', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'debbero'

class Debcard(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    cardid = models.CharField(max_length=50, db_column='CardId', blank=True) # Field name made lowercase.
    pincode = models.CharField(max_length=4, db_column='Pincode', blank=True) # Field name made lowercase.
    fradato = models.DateTimeField(null=True, db_column='FraDato', blank=True) # Field name made lowercase.
    udlobdato = models.DateTimeField(null=True, db_column='UdlobDato', blank=True) # Field name made lowercase.
    udlobklip = models.IntegerField(null=True, db_column='UdlobKlip', blank=True) # Field name made lowercase.
    foerstemanede = models.IntegerField(null=True, db_column='FoersteManede', blank=True) # Field name made lowercase.
    betaldato = models.DateTimeField(null=True, db_column='Betaldato', blank=True) # Field name made lowercase.
    kortaktiv = models.CharField(max_length=1, db_column='KortAktiv', blank=True) # Field name made lowercase.
    pulje = models.CharField(max_length=1, db_column='Pulje', blank=True) # Field name made lowercase.
    puljebelob = models.FloatField(null=True, blank=True)
    deaktivdato = models.DateTimeField(null=True, db_column='DeAktivdato', blank=True) # Field name made lowercase.
    billede = models.TextField(db_column='Billede', blank=True) # Field name made lowercase.
    cardtime = models.IntegerField(null=True, db_column='CardTime', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'debcard'

class Debfiles(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    filnavn = models.CharField(max_length=50, db_column='FilNavn', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    filstream = models.TextField(db_column='FilStream', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'debfiles'

class Diffrapp(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    lagersum = models.FloatField(null=True, db_column='LagerSum', blank=True) # Field name made lowercase.
    finanssum = models.FloatField(null=True, db_column='FinansSum', blank=True) # Field name made lowercase.
    diff = models.FloatField(null=True, db_column='Diff', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'diffrapp'

class Eksternfaktura(models.Model):
    fakturanr = models.IntegerField(unique=True, null=True, db_column='Fakturanr', blank=True) # Field name made lowercase.
    kundenr = models.IntegerField(null=True, db_column='Kundenr', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    adresse = models.TextField(db_column='Adresse', blank=True) # Field name made lowercase.
    fodselsdato = models.DateTimeField(null=True, db_column='FodselsDato', blank=True) # Field name made lowercase.
    modtagetden = models.DateTimeField(null=True, db_column='ModtagetDen', blank=True) # Field name made lowercase.
    betaltden = models.DateTimeField(null=True, db_column='BetaltDen', blank=True) # Field name made lowercase.
    ansat = models.IntegerField(null=True, db_column='Ansat', blank=True) # Field name made lowercase.
    ansatwinkas = models.IntegerField(null=True, db_column='AnsatWinKAS', blank=True) # Field name made lowercase.
    aktivitetsid = models.IntegerField(null=True, db_column='AktivitetsID', blank=True) # Field name made lowercase.
    totalbelob = models.FloatField(null=True, db_column='TotalBelob', blank=True) # Field name made lowercase.
    betaltbelob = models.FloatField(null=True, db_column='BetaltBelob', blank=True) # Field name made lowercase.
    betalt = models.IntegerField(null=True, db_column='Betalt', blank=True) # Field name made lowercase.
    betalingslog = models.TextField(db_column='BetalingsLog', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'eksternfaktura'

class Emails(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fstatus = models.IntegerField(null=True, db_column='FStatus', blank=True) # Field name made lowercase.
    emne = models.CharField(max_length=128, db_column='Emne', blank=True) # Field name made lowercase.
    afsendermail = models.CharField(max_length=64, db_column='AfsenderMail', blank=True) # Field name made lowercase.
    afsendernavn = models.CharField(max_length=32, db_column='AfsenderNavn', blank=True) # Field name made lowercase.
    modtagermail = models.CharField(max_length=64, db_column='ModtagerMail', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    filer = models.TextField(db_column='Filer', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'emails'

class Exadr2(models.Model):
    linie1 = models.CharField(max_length=96, db_column='Linie1', blank=True) # Field name made lowercase.
    linie2 = models.CharField(max_length=96, db_column='Linie2', blank=True) # Field name made lowercase.
    linie3 = models.CharField(max_length=96, db_column='Linie3', blank=True) # Field name made lowercase.
    linie4 = models.CharField(max_length=96, db_column='Linie4', blank=True) # Field name made lowercase.
    linie5 = models.CharField(max_length=96, db_column='Linie5', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'exadr2'

class Facilitet(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'facilitet'

class Fagkonttypelin(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fkunique = models.IntegerField(null=True, db_column='FKUnique', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    navn2 = models.CharField(max_length=24, db_column='Navn2', blank=True) # Field name made lowercase.
    valutafelt = models.CharField(max_length=12, db_column='ValutaFelt', blank=True) # Field name made lowercase.
    belob = models.FloatField(null=True, db_column='Belob', blank=True) # Field name made lowercase.
    belob2 = models.FloatField(null=True, db_column='Belob2', blank=True) # Field name made lowercase.
    fyear = models.IntegerField(null=True, db_column='FYear', blank=True) # Field name made lowercase.
    fmonth = models.IntegerField(null=True, db_column='FMonth', blank=True) # Field name made lowercase.
    fday = models.IntegerField(null=True, db_column='FDay', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'fagkonttypelin'

class Fagkonttyper(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=40, db_column='Navn', blank=True) # Field name made lowercase.
    artfunique = models.IntegerField(null=True, db_column='ArtFUnique', blank=True) # Field name made lowercase.
    feltstartdato = models.CharField(max_length=8, db_column='FeltStartDato', blank=True) # Field name made lowercase.
    felttrin = models.CharField(max_length=8, db_column='FeltTrin', blank=True) # Field name made lowercase.
    felttil2 = models.CharField(max_length=8, db_column='FeltTil2', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'fagkonttyper'

class Felter(models.Model):
    wpmodule = models.IntegerField(null=True, db_column='WPModule', blank=True) # Field name made lowercase.
    feltnavn = models.CharField(unique=True, max_length=24, db_column='FeltNavn', blank=True) # Field name made lowercase.
    feltetiket = models.CharField(max_length=35, db_column='FeltEtiket', blank=True) # Field name made lowercase.
    intfelttype = models.IntegerField(null=True, db_column='intFeltType', blank=True) # Field name made lowercase.
    reftype = models.IntegerField(null=True, db_column='RefType', blank=True) # Field name made lowercase.
    feltsize = models.IntegerField(null=True, db_column='FeltSize', blank=True) # Field name made lowercase.
    displaywidth = models.IntegerField(null=True, db_column='DisplayWidth', blank=True) # Field name made lowercase.
    displayheight = models.IntegerField(null=True, db_column='DisplayHeight', blank=True) # Field name made lowercase.
    taborder = models.IntegerField(null=True, db_column='TabOrder', blank=True) # Field name made lowercase.
    fleft = models.IntegerField(null=True, db_column='FLeft', blank=True) # Field name made lowercase.
    ftop = models.IntegerField(null=True, db_column='FTop', blank=True) # Field name made lowercase.
    fpage = models.IntegerField(null=True, db_column='FPage', blank=True) # Field name made lowercase.
    propflag = models.IntegerField(null=True, db_column='PropFlag', blank=True) # Field name made lowercase.
    frequired = models.CharField(max_length=1, db_column='FRequired', blank=True) # Field name made lowercase.
    freadonly = models.CharField(max_length=1, db_column='FReadOnly', blank=True) # Field name made lowercase.
    ftabstop = models.CharField(max_length=1, db_column='FTabstop', blank=True) # Field name made lowercase.
    fobs = models.CharField(max_length=1, db_column='FObs', blank=True) # Field name made lowercase.
    fnonduplicates = models.CharField(max_length=1, db_column='FNonDuplicates', blank=True) # Field name made lowercase.
    fnonsuplicates = models.CharField(max_length=1, db_column='FNonSuplicates', blank=True) # Field name made lowercase.
    beregning = models.CharField(max_length=128, db_column='Beregning', blank=True) # Field name made lowercase.
    filnavn = models.CharField(max_length=24, db_column='FilNavn', blank=True) # Field name made lowercase.
    inet = models.CharField(max_length=1, db_column='INet', blank=True) # Field name made lowercase.
    webshopfelt = models.IntegerField(null=True, db_column='Webshopfelt', blank=True) # Field name made lowercase.
    fcodefield = models.IntegerField(null=True, db_column='FCodeField', blank=True) # Field name made lowercase.
    fshowfield = models.IntegerField(null=True, db_column='FShowField', blank=True) # Field name made lowercase.
    historik = models.CharField(max_length=1, db_column='Historik', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'felter'

class Felterny(models.Model):
    wpmodule = models.IntegerField(unique=True, null=True, db_column='WPModule', blank=True) # Field name made lowercase.
    feltnavn = models.CharField(unique=True, max_length=24, db_column='FeltNavn', blank=True) # Field name made lowercase.
    feltetiket = models.CharField(max_length=35, db_column='FeltEtiket', blank=True) # Field name made lowercase.
    intfelttype = models.IntegerField(null=True, db_column='intFeltType', blank=True) # Field name made lowercase.
    reftype = models.IntegerField(null=True, db_column='RefType', blank=True) # Field name made lowercase.
    feltsize = models.IntegerField(null=True, db_column='FeltSize', blank=True) # Field name made lowercase.
    displaywidth = models.IntegerField(null=True, db_column='DisplayWidth', blank=True) # Field name made lowercase.
    displayheight = models.IntegerField(null=True, db_column='DisplayHeight', blank=True) # Field name made lowercase.
    taborder = models.IntegerField(null=True, db_column='TabOrder', blank=True) # Field name made lowercase.
    fleft = models.IntegerField(null=True, db_column='FLeft', blank=True) # Field name made lowercase.
    ftop = models.IntegerField(null=True, db_column='FTop', blank=True) # Field name made lowercase.
    fpage = models.IntegerField(null=True, db_column='FPage', blank=True) # Field name made lowercase.
    propflag = models.IntegerField(null=True, db_column='PropFlag', blank=True) # Field name made lowercase.
    frequired = models.CharField(max_length=1, db_column='FRequired', blank=True) # Field name made lowercase.
    freadonly = models.CharField(max_length=1, db_column='FReadOnly', blank=True) # Field name made lowercase.
    ftabstop = models.CharField(max_length=1, db_column='FTabstop', blank=True) # Field name made lowercase.
    fobs = models.CharField(max_length=1, db_column='FObs', blank=True) # Field name made lowercase.
    fnonduplicates = models.CharField(max_length=1, db_column='FNonDuplicates', blank=True) # Field name made lowercase.
    fnonsuplicates = models.CharField(max_length=1, db_column='FNonSuplicates', blank=True) # Field name made lowercase.
    beregning = models.CharField(max_length=128, db_column='Beregning', blank=True) # Field name made lowercase.
    filnavn = models.CharField(max_length=24, db_column='FilNavn', blank=True) # Field name made lowercase.
    inet = models.CharField(max_length=1, db_column='INet', blank=True) # Field name made lowercase.
    webshopfelt = models.IntegerField(null=True, db_column='Webshopfelt', blank=True) # Field name made lowercase.
    fcodefield = models.IntegerField(null=True, db_column='FCodeField', blank=True) # Field name made lowercase.
    fshowfield = models.IntegerField(null=True, db_column='FShowField', blank=True) # Field name made lowercase.
    historik = models.CharField(max_length=1, db_column='Historik', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'felterny'

class Finadim5(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    koncern = models.IntegerField(null=True, db_column='Koncern', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'finadim5'

class Finadim6(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    koncern = models.IntegerField(null=True, db_column='Koncern', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'finadim6'

class Firma(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    firma = models.CharField(max_length=48, db_column='Firma', blank=True) # Field name made lowercase.
    navn2 = models.CharField(max_length=48, db_column='Navn2', blank=True) # Field name made lowercase.
    adress1 = models.CharField(max_length=40, db_column='Adress1', blank=True) # Field name made lowercase.
    adress2 = models.CharField(max_length=40, db_column='Adress2', blank=True) # Field name made lowercase.
    adrnummer = models.CharField(max_length=10, db_column='AdrNummer', blank=True) # Field name made lowercase.
    post = models.CharField(max_length=10, db_column='Post', blank=True) # Field name made lowercase.
    bynavn = models.CharField(max_length=30, db_column='ByNavn', blank=True) # Field name made lowercase.
    telefon = models.CharField(max_length=11, db_column='Telefon', blank=True) # Field name made lowercase.
    mobil = models.CharField(max_length=11, db_column='Mobil', blank=True) # Field name made lowercase.
    fax = models.CharField(max_length=11, db_column='Fax', blank=True) # Field name made lowercase.
    produktid = models.CharField(max_length=20, db_column='ProduktID', blank=True) # Field name made lowercase.
    se_nr = models.CharField(max_length=12, db_column='SE_Nr', blank=True) # Field name made lowercase.
    pbs_nr = models.CharField(max_length=8, db_column='PBS_Nr', blank=True) # Field name made lowercase.
    bankkontonr = models.CharField(max_length=9, db_column='BankKontoNr', blank=True) # Field name made lowercase.
    reg_nr = models.CharField(max_length=4, db_column='Reg_Nr', blank=True) # Field name made lowercase.
    bank_konto = models.CharField(max_length=10, db_column='Bank_Konto', blank=True) # Field name made lowercase.
    debgrpnr = models.CharField(max_length=5, db_column='DebGrpNr', blank=True) # Field name made lowercase.
    delsystem = models.CharField(max_length=3, db_column='DelSystem', blank=True) # Field name made lowercase.
    email = models.CharField(max_length=48, db_column='Email', blank=True) # Field name made lowercase.
    www = models.CharField(max_length=48, blank=True)
    inetid = models.IntegerField(null=True, db_column='INETID', blank=True) # Field name made lowercase.
    inetpass = models.CharField(max_length=50, db_column='INETPASS', blank=True) # Field name made lowercase.
    dimension = models.IntegerField(null=True, db_column='Dimension', blank=True) # Field name made lowercase.
    forhnr = models.CharField(max_length=10, db_column='ForhNR', blank=True) # Field name made lowercase.
    koncernafdeling = models.CharField(max_length=32, db_column='KoncernAfdeling', blank=True) # Field name made lowercase.
    tilslutning = models.IntegerField(null=True, db_column='Tilslutning', blank=True) # Field name made lowercase.
    modtagpbshverdag = models.IntegerField(null=True, db_column='ModtagPBSHverDag', blank=True) # Field name made lowercase.
    gln = models.CharField(max_length=13, db_column='GLN', blank=True) # Field name made lowercase.
    nemidtype = models.IntegerField(null=True, db_column='NemIdType', blank=True) # Field name made lowercase.
    shopid = models.CharField(max_length=25, db_column='ShopID', blank=True) # Field name made lowercase.
    shoppassword = models.CharField(max_length=25, db_column='ShopPassword', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'firma'

class Fitsetup(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=50, db_column='Navn', blank=True) # Field name made lowercase.
    placering = models.CharField(max_length=50, db_column='Placering', blank=True) # Field name made lowercase.
    station = models.CharField(max_length=50, db_column='Station', blank=True) # Field name made lowercase.
    note = models.TextField(db_column='Note', blank=True) # Field name made lowercase.
    pbsdag = models.IntegerField(null=True, db_column='PBSDag', blank=True) # Field name made lowercase.
    bodebelob = models.FloatField(null=True, db_column='Bodebelob', blank=True) # Field name made lowercase.
    bodeoms = models.IntegerField(null=True, db_column='Bodeoms', blank=True) # Field name made lowercase.
    berooms = models.IntegerField(null=True, db_column='Berooms', blank=True) # Field name made lowercase.
    puljeoms = models.IntegerField(null=True, blank=True)
    timerespit = models.IntegerField(null=True, db_column='Timerespit', blank=True) # Field name made lowercase.
    logo = models.CharField(max_length=150, blank=True)
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    noankomreg = models.CharField(max_length=1, db_column='NoAnkomReg', blank=True) # Field name made lowercase.
    nohold = models.CharField(max_length=1, db_column='NoHold', blank=True) # Field name made lowercase.
    printankomreg = models.CharField(max_length=1, db_column='PrintAnkomReg', blank=True) # Field name made lowercase.
    autoopen = models.CharField(max_length=1, db_column='Autoopen', blank=True) # Field name made lowercase.
    printholdreg = models.CharField(max_length=1, db_column='PrintHoldReg', blank=True) # Field name made lowercase.
    printtilmeld = models.CharField(max_length=1, db_column='PrintTilmeld', blank=True) # Field name made lowercase.
    bonprinter = models.CharField(max_length=64, db_column='BonPrinter', blank=True) # Field name made lowercase.
    autologud = models.CharField(max_length=1, db_column='Autologud', blank=True) # Field name made lowercase.
    bookmax = models.IntegerField(null=True, db_column='BookMax', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'fitsetup'

class Flytkld(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    fralager = models.IntegerField(null=True, db_column='FraLager', blank=True) # Field name made lowercase.
    tillager = models.IntegerField(null=True, db_column='TilLager', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'flytkld'

class Forhandler(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kontonr = models.CharField(max_length=6, db_column='Kontonr', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=30, db_column='Navn', blank=True) # Field name made lowercase.
    ftp = models.FloatField(null=True, db_column='FTP', blank=True) # Field name made lowercase.
    booking = models.FloatField(null=True, db_column='Booking', blank=True) # Field name made lowercase.
    inet = models.FloatField(null=True, db_column='Inet', blank=True) # Field name made lowercase.
    hostet = models.FloatField(null=True, db_column='Hostet', blank=True) # Field name made lowercase.
    terminal = models.FloatField(null=True, db_column='Terminal', blank=True) # Field name made lowercase.
    webshop = models.FloatField(null=True, db_column='Webshop', blank=True) # Field name made lowercase.
    leje = models.FloatField(null=True, db_column='Leje', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'forhandler'

class Funiques(models.Model):
    thetablename = models.CharField(unique=True, max_length=20, db_column='TheTableName', blank=True) # Field name made lowercase.
    thefunique = models.IntegerField(null=True, db_column='TheFUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'funiques'

class Fydelse(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'fydelse'

class Grupperegl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=48, db_column='Navn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'grupperegl'

class Grupperegllinje(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fmastertype = models.IntegerField(null=True, db_column='FMasterType', blank=True) # Field name made lowercase.
    fmasterunique = models.IntegerField(null=True, db_column='FMasterUnique', blank=True) # Field name made lowercase.
    gruppereglunique = models.IntegerField(null=True, db_column='GruppeReglUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'grupperegllinje'

class Historik(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    konto = models.IntegerField(null=True, db_column='Konto', blank=True) # Field name made lowercase.
    bruger = models.CharField(max_length=10, db_column='BRUGER', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='DATO', blank=True) # Field name made lowercase.
    feltnavn = models.CharField(max_length=8, db_column='FELTNAVN', blank=True) # Field name made lowercase.
    ovalue = models.CharField(max_length=255, db_column='OVALUE', blank=True) # Field name made lowercase.
    nvalue = models.CharField(max_length=255, db_column='NVALUE', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'historik'

class Holdvent(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fkalenderunique = models.IntegerField(null=True, db_column='FKalenderUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'holdvent'

class Homebank(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    korttype = models.CharField(max_length=2, db_column='Korttype', blank=True) # Field name made lowercase.
    bkgrpunique = models.IntegerField(null=True, db_column='BKGrpUnique', blank=True) # Field name made lowercase.
    frakonto = models.CharField(max_length=14, db_column='FraKonto', blank=True) # Field name made lowercase.
    tilkonto = models.CharField(max_length=14, db_column='TilKonto', blank=True) # Field name made lowercase.
    modtager = models.CharField(max_length=34, db_column='Modtager', blank=True) # Field name made lowercase.
    belob = models.FloatField(null=True, db_column='Belob', blank=True) # Field name made lowercase.
    kvittering = models.CharField(max_length=1, db_column='Kvittering', blank=True) # Field name made lowercase.
    betalingsdato = models.DateTimeField(null=True, db_column='Betalingsdato', blank=True) # Field name made lowercase.
    egenref = models.CharField(max_length=30, db_column='EgenRef', blank=True) # Field name made lowercase.
    kortref = models.CharField(max_length=20, db_column='KortRef', blank=True) # Field name made lowercase.
    fritekst = models.TextField(db_column='FriTekst', blank=True) # Field name made lowercase.
    kreditor = models.IntegerField(null=True, db_column='Kreditor', blank=True) # Field name made lowercase.
    refunique = models.IntegerField(null=True, db_column='RefUnique', blank=True) # Field name made lowercase.
    fakturanr = models.CharField(max_length=15, db_column='FakturaNr', blank=True) # Field name made lowercase.
    koncern = models.IntegerField(null=True, db_column='Koncern', blank=True) # Field name made lowercase.
    udden = models.DateTimeField(null=True, db_column='UdDen', blank=True) # Field name made lowercase.
    valutakode = models.CharField(max_length=3, db_column='ValutaKode', blank=True) # Field name made lowercase.
    valutafunique = models.IntegerField(null=True, db_column='ValutaFUnique', blank=True) # Field name made lowercase.
    swiftadresse = models.CharField(max_length=11, db_column='Swiftadresse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'homebank'

class Husk(models.Model):
    dato = models.DateTimeField(unique=True, null=True, db_column='Dato', blank=True) # Field name made lowercase.
    tekst = models.TextField(db_column='Tekst', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'husk'

class Ibestskabelon(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    stedunique = models.IntegerField(null=True, db_column='StedUnique', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    medarb = models.IntegerField(null=True, db_column='Medarb', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'ibestskabelon'

class Import2(models.Model):
    kommunikationstype = models.IntegerField(null=True, db_column='KommunikationsType', blank=True) # Field name made lowercase.
    userid = models.CharField(max_length=30, db_column='UserID', blank=True) # Field name made lowercase.
    telefon1 = models.CharField(max_length=11, db_column='Telefon1', blank=True) # Field name made lowercase.
    telefon2 = models.CharField(max_length=11, db_column='Telefon2', blank=True) # Field name made lowercase.
    pathmailbox = models.CharField(max_length=64, db_column='PathMailbox', blank=True) # Field name made lowercase.
    brugkladdenr = models.IntegerField(null=True, db_column='BrugKladdeNr', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'import2'

class Inetkartfelter(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    feltnavn = models.CharField(max_length=24, db_column='Feltnavn', blank=True) # Field name made lowercase.
    webetiket = models.CharField(max_length=100, db_column='Webetiket', blank=True) # Field name made lowercase.
    forklaring = models.TextField(db_column='Forklaring', blank=True) # Field name made lowercase.
    vissom = models.IntegerField(null=True, db_column='Vissom', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'inetkartfelter'

class Inetkartfeltregeldata(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    regelunique = models.IntegerField(null=True, db_column='RegelUnique', blank=True) # Field name made lowercase.
    data = models.CharField(max_length=100, db_column='Data', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'inetkartfeltregeldata'

class Inetkartfeltregler(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    feltunique = models.IntegerField(null=True, db_column='FeltUnique', blank=True) # Field name made lowercase.
    regeltype = models.IntegerField(null=True, db_column='Regeltype', blank=True) # Field name made lowercase.
    meddelelse = models.TextField(db_column='Meddelelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'inetkartfeltregler'

class Inetkartfeltvaerdier(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    feltunique = models.IntegerField(null=True, db_column='FeltUnique', blank=True) # Field name made lowercase.
    janej = models.IntegerField(null=True, db_column='JaNej', blank=True) # Field name made lowercase.
    tekst = models.CharField(max_length=100, db_column='Tekst', blank=True) # Field name made lowercase.
    vaerdi = models.CharField(max_length=100, db_column='Vaerdi', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'inetkartfeltvaerdier'

class Inetkartmenu(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    ffunique = models.IntegerField(null=True, db_column='FFUnique', blank=True) # Field name made lowercase.
    etiket = models.CharField(max_length=255, db_column='Etiket', blank=True) # Field name made lowercase.
    tiptekst = models.CharField(max_length=255, db_column='Tiptekst', blank=True) # Field name made lowercase.
    standardmenuindeks = models.IntegerField(null=True, db_column='StandardMenuIndeks', blank=True) # Field name made lowercase.
    sideindeks = models.IntegerField(null=True, db_column='SideIndeks', blank=True) # Field name made lowercase.
    url = models.CharField(max_length=255, db_column='Url', blank=True) # Field name made lowercase.
    aktiv = models.IntegerField(null=True, db_column='Aktiv', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'inetkartmenu'

class Inetkartoteksider(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    titel = models.CharField(max_length=100, db_column='Titel', blank=True) # Field name made lowercase.
    linktitel = models.CharField(max_length=100, db_column='LinkTitel', blank=True) # Field name made lowercase.
    indhold = models.TextField(db_column='Indhold', blank=True) # Field name made lowercase.
    login = models.IntegerField(null=True, db_column='Login', blank=True) # Field name made lowercase.
    medtagimenu = models.IntegerField(null=True, db_column='MedtagIMenu', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'inetkartoteksider'

class Inetkartsidegrupper(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sideunique = models.IntegerField(null=True, db_column='SideUnique', blank=True) # Field name made lowercase.
    gruppeunique = models.IntegerField(null=True, db_column='GruppeUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'inetkartsidegrupper'

class Inetkartudsnitfelter(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    gruppeunique = models.IntegerField(null=True, db_column='GruppeUnique', blank=True) # Field name made lowercase.
    felter = models.TextField(db_column='Felter', blank=True) # Field name made lowercase.
    eksklusiv = models.IntegerField(null=True, db_column='Eksklusiv', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'inetkartudsnitfelter'

class Kalender(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    farrunique = models.IntegerField(null=True, db_column='FArrUnique', blank=True) # Field name made lowercase.
    fmunique = models.IntegerField(null=True, db_column='FMUnique', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    starttid = models.DateTimeField(null=True, db_column='Starttid', blank=True) # Field name made lowercase.
    sluttid = models.DateTimeField(null=True, db_column='Sluttid', blank=True) # Field name made lowercase.
    medarbejder = models.IntegerField(null=True, db_column='Medarbejder', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    emne = models.CharField(max_length=50, db_column='Emne', blank=True) # Field name made lowercase.
    note = models.TextField(db_column='Note', blank=True) # Field name made lowercase.
    form = models.IntegerField(null=True, db_column='Form', blank=True) # Field name made lowercase.
    blink = models.CharField(max_length=1, db_column='Blink', blank=True) # Field name made lowercase.
    allday = models.CharField(max_length=1, db_column='AllDay', blank=True) # Field name made lowercase.
    www = models.CharField(max_length=50, db_column='WWW', blank=True) # Field name made lowercase.
    mail = models.CharField(max_length=50, db_column='Mail', blank=True) # Field name made lowercase.
    slutdato = models.DateTimeField(null=True, db_column='SlutDato', blank=True) # Field name made lowercase.
    helsehold = models.CharField(max_length=1, db_column='Helsehold', blank=True) # Field name made lowercase.
    ressource = models.CharField(max_length=1, db_column='Ressource', blank=True) # Field name made lowercase.
    ressourcetype = models.IntegerField(null=True, db_column='RessourceType', blank=True) # Field name made lowercase.
    holddel = models.IntegerField(null=True, db_column='HoldDel', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    gentag = models.IntegerField(null=True, db_column='Gentag', blank=True) # Field name made lowercase.
    opgavefunique = models.IntegerField(null=True, db_column='OpgaveFUnique', blank=True) # Field name made lowercase.
    fardig = models.CharField(max_length=1, db_column='Fardig', blank=True) # Field name made lowercase.
    kategori = models.IntegerField(null=True, db_column='Kategori', blank=True) # Field name made lowercase.
    pris = models.FloatField(null=True, db_column='Pris', blank=True) # Field name made lowercase.
    venteliste = models.CharField(max_length=1, db_column='Venteliste', blank=True) # Field name made lowercase.
    tilmeldingsfrist = models.DateTimeField(null=True, db_column='Tilmeldingsfrist', blank=True) # Field name made lowercase.
    frameldingsfrist = models.DateTimeField(null=True, db_column='Frameldingsfrist', blank=True) # Field name made lowercase.
    afsluttes = models.DateTimeField(null=True, db_column='Afsluttes', blank=True) # Field name made lowercase.
    medtagbooking = models.IntegerField(null=True, db_column='Medtagbooking', blank=True) # Field name made lowercase.
    bookingomskonto = models.IntegerField(null=True, db_column='BookingOmsKonto', blank=True) # Field name made lowercase.
    maxbooking = models.IntegerField(null=True, db_column='MaxBooking', blank=True) # Field name made lowercase.
    bemark = models.CharField(max_length=128, db_column='Bemark', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kalender'

class Kalhold(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fkalenderunique = models.IntegerField(null=True, db_column='FKalenderUnique', blank=True) # Field name made lowercase.
    tilmelding = models.DateTimeField(null=True, db_column='Tilmelding', blank=True) # Field name made lowercase.
    afmelding = models.DateTimeField(null=True, db_column='Afmelding', blank=True) # Field name made lowercase.
    starttid = models.DateTimeField(null=True, db_column='Starttid', blank=True) # Field name made lowercase.
    sluttid = models.DateTimeField(null=True, db_column='Sluttid', blank=True) # Field name made lowercase.
    medarbejder = models.IntegerField(null=True, db_column='Medarbejder', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    ankom = models.CharField(max_length=1, db_column='Ankom', blank=True) # Field name made lowercase.
    bode = models.CharField(max_length=1, db_column='Bode', blank=True) # Field name made lowercase.
    web = models.CharField(max_length=1, blank=True)
    vente = models.DateTimeField(null=True, db_column='Vente', blank=True) # Field name made lowercase.
    ankomtid = models.DateTimeField(null=True, db_column='Ankomtid', blank=True) # Field name made lowercase.
    beloeb = models.FloatField(null=True, db_column='Beloeb', blank=True) # Field name made lowercase.
    webdibstrans = models.CharField(max_length=50, db_column='WebDibsTrans', blank=True) # Field name made lowercase.
    ordrenr = models.CharField(max_length=50, blank=True)
    tilpbbfunique = models.IntegerField(null=True, db_column='TilPbbFUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kalhold'

class Kalkategorier(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=35, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kalkategorier'

class Kalopg(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kalopg'

class Kalsetup(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    aktivstart = models.DateTimeField(null=True, db_column='Aktivstart', blank=True) # Field name made lowercase.
    aktivslut = models.DateTimeField(null=True, db_column='Aktivslut', blank=True) # Field name made lowercase.
    finterval = models.IntegerField(null=True, db_column='FInterval', blank=True) # Field name made lowercase.
    showday = models.CharField(max_length=1, db_column='ShowDay', blank=True) # Field name made lowercase.
    showinfo = models.CharField(max_length=1, db_column='ShowInfo', blank=True) # Field name made lowercase.
    showdeb = models.CharField(max_length=1, db_column='ShowDeb', blank=True) # Field name made lowercase.
    color1 = models.IntegerField(null=True, db_column='Color1', blank=True) # Field name made lowercase.
    color2 = models.IntegerField(null=True, db_column='Color2', blank=True) # Field name made lowercase.
    color3 = models.IntegerField(null=True, db_column='Color3', blank=True) # Field name made lowercase.
    color4 = models.IntegerField(null=True, db_column='Color4', blank=True) # Field name made lowercase.
    color5 = models.IntegerField(null=True, db_column='Color5', blank=True) # Field name made lowercase.
    color6 = models.IntegerField(null=True, db_column='Color6', blank=True) # Field name made lowercase.
    aftaledesign = models.CharField(max_length=12, db_column='Aftaledesign', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kalsetup'

class Keyscan(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    stationname = models.CharField(max_length=32, db_column='StationName', blank=True) # Field name made lowercase.
    tilsluttet = models.CharField(max_length=1, db_column='Tilsluttet', blank=True) # Field name made lowercase.
    kode = models.IntegerField(null=True, db_column='Kode', blank=True) # Field name made lowercase.
    millisec = models.IntegerField(null=True, db_column='MilliSec', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'keyscan'

class Kladder(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=48, db_column='Navn', blank=True) # Field name made lowercase.
    permanent = models.IntegerField(null=True, db_column='Permanent', blank=True) # Field name made lowercase.
    nydato = models.IntegerField(null=True, db_column='NyDato', blank=True) # Field name made lowercase.
    bilagstart = models.IntegerField(null=True, db_column='BilagStart', blank=True) # Field name made lowercase.
    bilagnext = models.IntegerField(null=True, db_column='BilagNext', blank=True) # Field name made lowercase.
    bilagslut = models.IntegerField(null=True, db_column='BilagSlut', blank=True) # Field name made lowercase.
    mindato = models.DateTimeField(null=True, db_column='MinDato', blank=True) # Field name made lowercase.
    maxdato = models.DateTimeField(null=True, db_column='MaxDato', blank=True) # Field name made lowercase.
    koncern = models.IntegerField(null=True, db_column='Koncern', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kladder'

class Kobimp(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=16, db_column='VareNr', blank=True) # Field name made lowercase.
    sfsenhunique = models.IntegerField(null=True, db_column='SFSEnhUnique', blank=True) # Field name made lowercase.
    ftype = models.IntegerField(null=True, db_column='FType', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=32, db_column='VareTekst', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    enhed = models.CharField(max_length=12, db_column='Enhed', blank=True) # Field name made lowercase.
    kostpris = models.FloatField(null=True, db_column='Kostpris', blank=True) # Field name made lowercase.
    salgspris = models.FloatField(null=True, db_column='Salgspris', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    liniesum = models.FloatField(null=True, db_column='LinieSum', blank=True) # Field name made lowercase.
    kreditor = models.IntegerField(null=True, db_column='Kreditor', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='LagerSted', blank=True) # Field name made lowercase.
    momskode = models.CharField(max_length=2, db_column='Momskode', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kobimp'

class Kommuner(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.IntegerField(null=True, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=30, db_column='Navn', blank=True) # Field name made lowercase.
    amtskode = models.IntegerField(null=True, db_column='AmtsKode', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kommuner'

class Kontop(models.Model):
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    kontonavn = models.CharField(max_length=40, db_column='KontoNavn', blank=True) # Field name made lowercase.
    kontotype = models.IntegerField(null=True, db_column='KontoType', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    sogkode = models.CharField(max_length=15, db_column='SogKode', blank=True) # Field name made lowercase.
    sumfra = models.IntegerField(null=True, db_column='SumFra', blank=True) # Field name made lowercase.
    sumtil = models.IntegerField(null=True, db_column='SumTil', blank=True) # Field name made lowercase.
    afslutkonto = models.IntegerField(null=True, db_column='AfslutKonto', blank=True) # Field name made lowercase.
    modkonto = models.IntegerField(null=True, db_column='ModKonto', blank=True) # Field name made lowercase.
    frrkonto = models.IntegerField(null=True, db_column='FRRKonto', blank=True) # Field name made lowercase.
    startdato = models.DateTimeField(null=True, db_column='StartDato', blank=True) # Field name made lowercase.
    rapportfortegn = models.CharField(max_length=1, db_column='RapportFortegn', blank=True) # Field name made lowercase.
    graf = models.IntegerField(null=True, db_column='Graf', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    tvungenafdeling = models.IntegerField(null=True, db_column='TvungenAfdeling', blank=True) # Field name made lowercase.
    tvungenprojekt = models.IntegerField(null=True, db_column='TvungenProjekt', blank=True) # Field name made lowercase.
    lukket = models.IntegerField(null=True, db_column='Lukket', blank=True) # Field name made lowercase.
    lukketmanuel = models.IntegerField(null=True, db_column='LukketManuel', blank=True) # Field name made lowercase.
    forslagafdmk = models.IntegerField(null=True, db_column='ForslagAfdMK', blank=True) # Field name made lowercase.
    budgetprimo = models.FloatField(null=True, db_column='BudgetPrimo', blank=True) # Field name made lowercase.
    budget1 = models.FloatField(null=True, db_column='Budget1', blank=True) # Field name made lowercase.
    budget2 = models.FloatField(null=True, db_column='Budget2', blank=True) # Field name made lowercase.
    budget3 = models.FloatField(null=True, db_column='Budget3', blank=True) # Field name made lowercase.
    budget4 = models.FloatField(null=True, db_column='Budget4', blank=True) # Field name made lowercase.
    budget5 = models.FloatField(null=True, db_column='Budget5', blank=True) # Field name made lowercase.
    budget6 = models.FloatField(null=True, db_column='Budget6', blank=True) # Field name made lowercase.
    budget7 = models.FloatField(null=True, db_column='Budget7', blank=True) # Field name made lowercase.
    budget8 = models.FloatField(null=True, db_column='Budget8', blank=True) # Field name made lowercase.
    budget9 = models.FloatField(null=True, db_column='Budget9', blank=True) # Field name made lowercase.
    budget10 = models.FloatField(null=True, db_column='Budget10', blank=True) # Field name made lowercase.
    budget11 = models.FloatField(null=True, db_column='Budget11', blank=True) # Field name made lowercase.
    budget12 = models.FloatField(null=True, db_column='Budget12', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kontop'

class Kortstat(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    tid = models.TextField(db_column='Tid', blank=True) # Field name made lowercase. This field type is a guess.
    afvist = models.CharField(max_length=1, db_column='Afvist', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kortstat'

class Krebetal(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    udligntable = models.IntegerField(null=True, db_column='UdlignTable', blank=True) # Field name made lowercase.
    udlignfunique = models.IntegerField(null=True, db_column='UdlignFUnique', blank=True) # Field name made lowercase.
    betaltype = models.IntegerField(null=True, db_column='BetalType', blank=True) # Field name made lowercase.
    betalfunique = models.IntegerField(null=True, db_column='BetalFUnique', blank=True) # Field name made lowercase.
    modtable = models.IntegerField(null=True, db_column='ModTable', blank=True) # Field name made lowercase.
    modfunique = models.IntegerField(null=True, db_column='ModFUnique', blank=True) # Field name made lowercase.
    belob = models.FloatField(null=True, db_column='Belob', blank=True) # Field name made lowercase.
    egenref = models.CharField(max_length=20, db_column='EgenRef', blank=True) # Field name made lowercase.
    godkendt = models.IntegerField(null=True, db_column='Godkendt', blank=True) # Field name made lowercase.
    bogfort = models.IntegerField(null=True, db_column='Bogfort', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'krebetal'

class Krebtsup(models.Model):
    bankkontofinans = models.IntegerField(null=True, db_column='BankKontoFinans', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=35, db_column='Navn', blank=True) # Field name made lowercase.
    adresse = models.CharField(max_length=35, db_column='Adresse', blank=True) # Field name made lowercase.
    postnr = models.CharField(max_length=4, db_column='Postnr', blank=True) # Field name made lowercase.
    bynavn = models.CharField(max_length=30, db_column='Bynavn', blank=True) # Field name made lowercase.
    frakonto = models.CharField(max_length=14, db_column='FraKonto', blank=True) # Field name made lowercase.
    kvittering = models.IntegerField(null=True, db_column='Kvittering', blank=True) # Field name made lowercase.
    egenreftype = models.IntegerField(null=True, db_column='EgenRefType', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'krebtsup'

class Kredgrp(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    samlekonto = models.IntegerField(null=True, db_column='SamleKonto', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kredgrp'

class Kreditor(models.Model):
    konto = models.IntegerField(unique=True, null=True, db_column='Konto', blank=True) # Field name made lowercase.
    gruppe = models.IntegerField(null=True, db_column='Gruppe', blank=True) # Field name made lowercase.
    fnavn = models.CharField(max_length=50, db_column='FNavn', blank=True) # Field name made lowercase.
    pnavn = models.CharField(max_length=30, db_column='PNavn', blank=True) # Field name made lowercase.
    adress1 = models.CharField(max_length=30, db_column='Adress1', blank=True) # Field name made lowercase.
    adress2 = models.CharField(max_length=30, db_column='Adress2', blank=True) # Field name made lowercase.
    post = models.CharField(max_length=10, db_column='Post', blank=True) # Field name made lowercase.
    bynavn = models.CharField(max_length=30, db_column='ByNavn', blank=True) # Field name made lowercase.
    tel = models.CharField(max_length=16, db_column='Tel', blank=True) # Field name made lowercase.
    fax = models.CharField(max_length=16, db_column='Fax', blank=True) # Field name made lowercase.
    mobil = models.CharField(max_length=16, db_column='Mobil', blank=True) # Field name made lowercase.
    bank = models.CharField(max_length=15, db_column='Bank', blank=True) # Field name made lowercase.
    giro = models.CharField(max_length=15, db_column='Giro', blank=True) # Field name made lowercase.
    betalbet = models.IntegerField(null=True, db_column='BetalBet', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    koncern = models.IntegerField(null=True, db_column='Koncern', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='LagerSted', blank=True) # Field name made lowercase.
    bem�rk = models.TextField(db_column='Bem\xe6rk', blank=True) # Field name made lowercase.
    opretdato = models.DateTimeField(null=True, db_column='OpretDato', blank=True) # Field name made lowercase.
    lukket = models.IntegerField(null=True, db_column='Lukket', blank=True) # Field name made lowercase.
    sogekode = models.CharField(max_length=16, db_column='SogeKode', blank=True) # Field name made lowercase.
    www = models.CharField(max_length=32, blank=True)
    kunderef = models.CharField(max_length=15, db_column='KundeRef', blank=True) # Field name made lowercase.
    email = models.CharField(max_length=48, blank=True)
    emailkob = models.CharField(max_length=48, db_column='emailKob', blank=True) # Field name made lowercase.
    gln = models.CharField(max_length=13, db_column='GLN', blank=True) # Field name made lowercase.
    cvrnummer = models.IntegerField(null=True, db_column='CVRNummer', blank=True) # Field name made lowercase.
    kreditornr = models.IntegerField(null=True, db_column='KreditorNr', blank=True) # Field name made lowercase.
    prislevrid = models.IntegerField(null=True, db_column='PrisLevrID', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    frrkonto = models.IntegerField(null=True, db_column='FRRKonto', blank=True) # Field name made lowercase.
    betaltype = models.CharField(max_length=2, db_column='BetalType', blank=True) # Field name made lowercase.
    kobsfakmetode = models.IntegerField(null=True, db_column='KobsFakMetode', blank=True) # Field name made lowercase.
    regnr = models.CharField(max_length=15, db_column='Regnr', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kreditor'

class Kvitkart(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    terminal = models.IntegerField(null=True, db_column='Terminal', blank=True) # Field name made lowercase.
    journalnr = models.IntegerField(null=True, db_column='JournalNr', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    ekspedient = models.IntegerField(null=True, db_column='Ekspedient', blank=True) # Field name made lowercase.
    brutto = models.FloatField(null=True, db_column='Brutto', blank=True) # Field name made lowercase.
    modtaget = models.FloatField(null=True, db_column='Modtaget', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    bogfort = models.DateTimeField(null=True, db_column='Bogfort', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kvitkart'

class Kvitsup(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kvittype = models.IntegerField(null=True, db_column='KvitType', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    ktype = models.CharField(max_length=1, db_column='KType', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    retkonto = models.CharField(max_length=1, db_column='RetKonto', blank=True) # Field name made lowercase.
    kontoforkort = models.CharField(max_length=1, db_column='KontoForKort', blank=True) # Field name made lowercase.
    belobtype = models.IntegerField(null=True, db_column='BelobType', blank=True) # Field name made lowercase.
    valutafelt = models.CharField(max_length=8, db_column='ValutaFelt', blank=True) # Field name made lowercase.
    brutto = models.FloatField(null=True, db_column='Brutto', blank=True) # Field name made lowercase.
    retbrutto = models.CharField(max_length=1, db_column='RetBrutto', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'kvitsup'

class Lagbehv(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    pris = models.FloatField(null=True, db_column='Pris', blank=True) # Field name made lowercase.
    sfak = models.FloatField(null=True, db_column='SFAK', blank=True) # Field name made lowercase.
    ifak = models.FloatField(null=True, db_column='IFAK', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'lagbehv'

class Lagjour(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    transaktionskode = models.IntegerField(null=True, db_column='TransaktionsKode', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    journalnr = models.IntegerField(null=True, db_column='JournalNr', blank=True) # Field name made lowercase.
    bruger = models.CharField(max_length=10, db_column='Bruger', blank=True) # Field name made lowercase.
    medarb = models.IntegerField(null=True, db_column='MedArb', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=40, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'lagjour'

class Lagkld(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kreditor = models.IntegerField(null=True, db_column='Kreditor', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    ftype = models.CharField(max_length=1, db_column='FType', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    kostpris = models.FloatField(null=True, db_column='KostPris', blank=True) # Field name made lowercase.
    salgspris = models.FloatField(null=True, db_column='SalgsPris', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='Lagerkonto', blank=True) # Field name made lowercase.
    vareforbrug = models.IntegerField(null=True, db_column='Vareforbrug', blank=True) # Field name made lowercase.
    dimskabelon = models.IntegerField(null=True, db_column='DimSkabelon', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'lagkld'

class Lagpost(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    sfegenvfunique = models.IntegerField(null=True, db_column='SFEgenVFUnique', blank=True) # Field name made lowercase.
    samleunique = models.IntegerField(null=True, db_column='SamleUnique', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    ftype = models.CharField(max_length=1, db_column='FType', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    rabat = models.FloatField(null=True, db_column='Rabat', blank=True) # Field name made lowercase.
    kostpris = models.FloatField(null=True, db_column='KostPris', blank=True) # Field name made lowercase.
    kpreg = models.FloatField(null=True, db_column='KPReg', blank=True) # Field name made lowercase.
    udlignet = models.FloatField(null=True, db_column='Udlignet', blank=True) # Field name made lowercase.
    salgspris = models.FloatField(null=True, db_column='SalgsPris', blank=True) # Field name made lowercase.
    restype = models.CharField(max_length=1, db_column='ResType', blank=True) # Field name made lowercase.
    kd = models.IntegerField(null=True, db_column='KD', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    kontraktnr = models.CharField(max_length=15, db_column='KontraktNr', blank=True) # Field name made lowercase.
    kassenr = models.IntegerField(null=True, db_column='KasseNr', blank=True) # Field name made lowercase.
    medarb = models.IntegerField(null=True, db_column='MedArb', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='LagerKonto', blank=True) # Field name made lowercase.
    vareforbrug = models.IntegerField(null=True, db_column='Vareforbrug', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'lagpost'

class Lagreg(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    ftype = models.CharField(max_length=1, db_column='FType', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    kostpris = models.FloatField(null=True, db_column='KostPris', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='Lagerkonto', blank=True) # Field name made lowercase.
    svindkonto = models.IntegerField(null=True, db_column='Svindkonto', blank=True) # Field name made lowercase.
    dimskabelon = models.IntegerField(null=True, db_column='DimSkabelon', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'lagreg'

class Lande(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    landetype = models.IntegerField(null=True, db_column='LandeType', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='Momskode', blank=True) # Field name made lowercase.
    intrastat = models.CharField(max_length=3, db_column='Intrastat', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'lande'

class Lastlb(models.Model):
    arkname = models.CharField(unique=True, max_length=30, db_column='ArkName', blank=True) # Field name made lowercase.
    antalnedad = models.FloatField(null=True, db_column='AntalNedad', blank=True) # Field name made lowercase.
    antalhenad = models.FloatField(null=True, db_column='AntalHenad', blank=True) # Field name made lowercase.
    arktype = models.CharField(max_length=30, db_column='ArkType', blank=True) # Field name made lowercase.
    sidemargen = models.FloatField(null=True, db_column='SideMargen', blank=True) # Field name made lowercase.
    topmargen = models.FloatField(null=True, db_column='TopMargen', blank=True) # Field name made lowercase.
    vandretpitch = models.FloatField(null=True, db_column='VandretPitch', blank=True) # Field name made lowercase.
    lodretpitch = models.FloatField(null=True, db_column='LodretPitch', blank=True) # Field name made lowercase.
    etiketbrede = models.FloatField(null=True, db_column='EtiketBrede', blank=True) # Field name made lowercase.
    etikethojde = models.FloatField(null=True, db_column='EtiketHojde', blank=True) # Field name made lowercase.
    label1 = models.CharField(max_length=1, db_column='Label1', blank=True) # Field name made lowercase.
    label2 = models.CharField(max_length=1, db_column='Label2', blank=True) # Field name made lowercase.
    label3 = models.CharField(max_length=1, db_column='Label3', blank=True) # Field name made lowercase.
    label4 = models.CharField(max_length=1, db_column='Label4', blank=True) # Field name made lowercase.
    brugerdef = models.CharField(max_length=1, db_column='BrugerDef', blank=True) # Field name made lowercase.
    gruppename = models.CharField(max_length=30, db_column='GruppeName', blank=True) # Field name made lowercase.
    arkside = models.CharField(max_length=10, db_column='ArkSide', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'lastlb'

class Laydetl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    lhunique = models.IntegerField(null=True, db_column='LHUnique', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=16, db_column='Navn', blank=True) # Field name made lowercase.
    x1 = models.FloatField(null=True, db_column='X1', blank=True) # Field name made lowercase.
    y1 = models.FloatField(null=True, db_column='Y1', blank=True) # Field name made lowercase.
    x2 = models.FloatField(null=True, db_column='X2', blank=True) # Field name made lowercase.
    y2 = models.FloatField(null=True, db_column='Y2', blank=True) # Field name made lowercase.
    aligndetail = models.CharField(max_length=1, db_column='AlignDetail', blank=True) # Field name made lowercase.
    alignment = models.IntegerField(null=True, db_column='Alignment', blank=True) # Field name made lowercase.
    fontname = models.CharField(max_length=32, db_column='FontName', blank=True) # Field name made lowercase.
    fontstyle = models.IntegerField(null=True, db_column='FontStyle', blank=True) # Field name made lowercase.
    fontsize = models.IntegerField(null=True, db_column='FontSize', blank=True) # Field name made lowercase.
    fontcolor = models.IntegerField(null=True, db_column='FontColor', blank=True) # Field name made lowercase.
    format = models.IntegerField(null=True, db_column='Format', blank=True) # Field name made lowercase.
    printonpage = models.IntegerField(null=True, db_column='PrintOnPage', blank=True) # Field name made lowercase.
    datatype = models.IntegerField(null=True, db_column='DataType', blank=True) # Field name made lowercase.
    tekst = models.TextField(db_column='Tekst', blank=True) # Field name made lowercase.
    wordwrap = models.CharField(max_length=1, db_column='WordWrap', blank=True) # Field name made lowercase.
    bmp = models.TextField(db_column='BMP', blank=True) # Field name made lowercase.
    sysfield = models.IntegerField(null=True, db_column='SysField', blank=True) # Field name made lowercase.
    fieldname = models.CharField(max_length=32, db_column='FieldName', blank=True) # Field name made lowercase.
    recttype = models.IntegerField(null=True, db_column='RectType', blank=True) # Field name made lowercase.
    linewidth = models.FloatField(null=True, db_column='LineWidth', blank=True) # Field name made lowercase.
    funcname = models.CharField(max_length=24, db_column='FuncName', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'laydetl'

class Layhead(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    layoutid = models.IntegerField(null=True, db_column='LayoutID', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    type = models.IntegerField(null=True, db_column='Type', blank=True) # Field name made lowercase.
    kopier = models.IntegerField(null=True, db_column='Kopier', blank=True) # Field name made lowercase.
    newpageprrec = models.CharField(max_length=1, db_column='NewPagePrRec', blank=True) # Field name made lowercase.
    pagecntprrec = models.CharField(max_length=1, db_column='PageCntPrRec', blank=True) # Field name made lowercase.
    zerodetails = models.CharField(max_length=1, db_column='ZeroDetails', blank=True) # Field name made lowercase.
    landscape = models.CharField(max_length=1, db_column='LandScape', blank=True) # Field name made lowercase.
    validrecfunc = models.IntegerField(null=True, db_column='ValidRecFunc', blank=True) # Field name made lowercase.
    mastertable = models.CharField(max_length=8, db_column='MasterTable', blank=True) # Field name made lowercase.
    detailtable = models.CharField(max_length=8, db_column='DetailTable', blank=True) # Field name made lowercase.
    detaillinkfield = models.CharField(max_length=16, db_column='DetailLinkField', blank=True) # Field name made lowercase.
    indexfield = models.CharField(max_length=32, db_column='IndexField', blank=True) # Field name made lowercase.
    phyheight = models.FloatField(null=True, db_column='PhyHeight', blank=True) # Field name made lowercase.
    phywidth = models.FloatField(null=True, db_column='PhyWidth', blank=True) # Field name made lowercase.
    detaily1 = models.FloatField(null=True, db_column='DetailY1', blank=True) # Field name made lowercase.
    detaily2 = models.FloatField(null=True, db_column='DetailY2', blank=True) # Field name made lowercase.
    lineheight = models.FloatField(null=True, db_column='LineHeight', blank=True) # Field name made lowercase.
    fontname = models.CharField(max_length=32, db_column='FontName', blank=True) # Field name made lowercase.
    fontstyle = models.IntegerField(null=True, db_column='FontStyle', blank=True) # Field name made lowercase.
    fontsize = models.IntegerField(null=True, db_column='FontSize', blank=True) # Field name made lowercase.
    fontcolor = models.IntegerField(null=True, db_column='FontColor', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'layhead'

class Laymast(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    lhunique = models.IntegerField(null=True, db_column='LHUnique', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=16, db_column='Navn', blank=True) # Field name made lowercase.
    x1 = models.FloatField(null=True, db_column='X1', blank=True) # Field name made lowercase.
    y1 = models.FloatField(null=True, db_column='Y1', blank=True) # Field name made lowercase.
    x2 = models.FloatField(null=True, db_column='X2', blank=True) # Field name made lowercase.
    y2 = models.FloatField(null=True, db_column='Y2', blank=True) # Field name made lowercase.
    aligndetail = models.CharField(max_length=1, db_column='AlignDetail', blank=True) # Field name made lowercase.
    alignment = models.IntegerField(null=True, db_column='Alignment', blank=True) # Field name made lowercase.
    fontname = models.CharField(max_length=32, db_column='FontName', blank=True) # Field name made lowercase.
    fontstyle = models.IntegerField(null=True, db_column='FontStyle', blank=True) # Field name made lowercase.
    fontsize = models.IntegerField(null=True, db_column='FontSize', blank=True) # Field name made lowercase.
    fontcolor = models.IntegerField(null=True, db_column='FontColor', blank=True) # Field name made lowercase.
    format = models.IntegerField(null=True, db_column='Format', blank=True) # Field name made lowercase.
    printonpage = models.IntegerField(null=True, db_column='PrintOnPage', blank=True) # Field name made lowercase.
    datatype = models.IntegerField(null=True, db_column='DataType', blank=True) # Field name made lowercase.
    tekst = models.TextField(db_column='Tekst', blank=True) # Field name made lowercase.
    wordwrap = models.CharField(max_length=1, db_column='WordWrap', blank=True) # Field name made lowercase.
    bmp = models.TextField(db_column='BMP', blank=True) # Field name made lowercase.
    sysfield = models.IntegerField(null=True, db_column='SysField', blank=True) # Field name made lowercase.
    fieldname = models.CharField(max_length=32, db_column='FieldName', blank=True) # Field name made lowercase.
    recttype = models.IntegerField(null=True, db_column='RectType', blank=True) # Field name made lowercase.
    linewidth = models.FloatField(null=True, db_column='LineWidth', blank=True) # Field name made lowercase.
    funcname = models.CharField(max_length=24, db_column='FuncName', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'laymast'

class Layout(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    faktura = models.IntegerField(null=True, db_column='Faktura', blank=True) # Field name made lowercase.
    kredit = models.IntegerField(null=True, db_column='Kredit', blank=True) # Field name made lowercase.
    ordre = models.IntegerField(null=True, db_column='Ordre', blank=True) # Field name made lowercase.
    folge = models.IntegerField(null=True, db_column='Folge', blank=True) # Field name made lowercase.
    tilbud = models.IntegerField(null=True, db_column='Tilbud', blank=True) # Field name made lowercase.
    vaerksted = models.IntegerField(null=True, db_column='Vaerksted', blank=True) # Field name made lowercase.
    ordrebekraft = models.IntegerField(null=True, db_column='Ordrebekraft', blank=True) # Field name made lowercase.
    klarprod = models.IntegerField(null=True, db_column='KlarProd', blank=True) # Field name made lowercase.
    iprod = models.IntegerField(null=True, db_column='IProd', blank=True) # Field name made lowercase.
    fakshow = models.CharField(max_length=1, db_column='FakShow', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'layout'

class Locks(models.Model):
    menuitem = models.IntegerField(unique=True, db_column='MenuItem') # Field name made lowercase.
    uniqueid = models.IntegerField(unique=True, null=True, db_column='UniqueId', blank=True) # Field name made lowercase.
    username = models.CharField(max_length=32, db_column='UserName', blank=True) # Field name made lowercase.
    station = models.IntegerField(null=True, db_column='Station', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'locks'

class Maalere(models.Model):
    nummer = models.IntegerField(unique=True, null=True, db_column='Nummer', blank=True) # Field name made lowercase.
    matrikelnr = models.CharField(max_length=72, db_column='MatrikelNr', blank=True) # Field name made lowercase.
    matrikelpost = models.CharField(max_length=10, db_column='MatrikelPost', blank=True) # Field name made lowercase.
    serienr = models.CharField(max_length=32, db_column='SerieNr', blank=True) # Field name made lowercase.
    gruppe = models.IntegerField(null=True, db_column='Gruppe', blank=True) # Field name made lowercase.
    opkrtype = models.IntegerField(null=True, db_column='OpkrType', blank=True) # Field name made lowercase.
    medlemnr = models.IntegerField(null=True, db_column='MedlemNr', blank=True) # Field name made lowercase.
    ejerjuridisk = models.IntegerField(null=True, db_column='EjerJuridisk', blank=True) # Field name made lowercase.
    fakturamodtager = models.IntegerField(null=True, db_column='FakturaModtager', blank=True) # Field name made lowercase.
    ejer = models.IntegerField(null=True, db_column='Ejer', blank=True) # Field name made lowercase.
    forbrug = models.FloatField(null=True, db_column='Forbrug', blank=True) # Field name made lowercase.
    noter = models.TextField(db_column='Noter', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'maalere'

class Maalgrp(models.Model):
    nummer = models.IntegerField(unique=True, null=True, db_column='Nummer', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=100, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    samlemaalere = models.CharField(max_length=1, db_column='SamleMaalere', blank=True) # Field name made lowercase.
    anvendflereydelser = models.IntegerField(null=True, db_column='AnvendFlereYdelser', blank=True) # Field name made lowercase.
    anvendtilladelser = models.IntegerField(null=True, db_column='AnvendTilladelser', blank=True) # Field name made lowercase.
    anvendinet = models.IntegerField(null=True, db_column='AnvendINet', blank=True) # Field name made lowercase.
    tilladelser = models.CharField(max_length=255, db_column='Tilladelser', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'maalgrp'

class Maalpost(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    maalernr = models.IntegerField(null=True, db_column='MaalerNr', blank=True) # Field name made lowercase.
    medlemnr = models.IntegerField(null=True, db_column='MedlemNr', blank=True) # Field name made lowercase.
    ydelsenr = models.IntegerField(null=True, db_column='YdelseNr', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    udlobdato = models.DateTimeField(null=True, db_column='UdlobDato', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    resultat = models.FloatField(null=True, db_column='Resultat', blank=True) # Field name made lowercase.
    tilstand = models.IntegerField(null=True, db_column='Tilstand', blank=True) # Field name made lowercase.
    medarbejder = models.IntegerField(null=True, db_column='Medarbejder', blank=True) # Field name made lowercase.
    den = models.DateTimeField(null=True, db_column='Den', blank=True) # Field name made lowercase.
    opkrunique = models.IntegerField(null=True, db_column='OpkrUnique', blank=True) # Field name made lowercase.
    skiftunique = models.IntegerField(null=True, db_column='SkiftUnique', blank=True) # Field name made lowercase.
    berotype = models.IntegerField(null=True, db_column='Berotype', blank=True) # Field name made lowercase.
    inet = models.IntegerField(null=True, db_column='INet', blank=True) # Field name made lowercase.
    inetden = models.DateTimeField(null=True, db_column='INetDen', blank=True) # Field name made lowercase.
    godkendt = models.IntegerField(null=True, db_column='Godkendt', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'maalpost'

class Medarb(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=50, db_column='Navn', blank=True) # Field name made lowercase.
    adresse = models.CharField(max_length=50, db_column='Adresse', blank=True) # Field name made lowercase.
    adresse2 = models.CharField(max_length=50, db_column='Adresse2', blank=True) # Field name made lowercase.
    postnr = models.CharField(max_length=10, db_column='PostNr', blank=True) # Field name made lowercase.
    bynavn = models.CharField(max_length=24, db_column='ByNavn', blank=True) # Field name made lowercase.
    telefon = models.CharField(max_length=12, db_column='Telefon', blank=True) # Field name made lowercase.
    tbcsuper = models.CharField(max_length=1, db_column='TBCSuper', blank=True) # Field name made lowercase.
    eksternnr = models.IntegerField(null=True, db_column='EksternNr', blank=True) # Field name made lowercase.
    startden = models.DateTimeField(null=True, db_column='StartDen', blank=True) # Field name made lowercase.
    slutden = models.DateTimeField(null=True, db_column='SlutDen', blank=True) # Field name made lowercase.
    mobil = models.CharField(max_length=12, db_column='Mobil', blank=True) # Field name made lowercase.
    kortkode = models.CharField(max_length=32, db_column='Kortkode', blank=True) # Field name made lowercase.
    pinkode = models.CharField(max_length=8, db_column='PinKode', blank=True) # Field name made lowercase.
    provision = models.FloatField(null=True, db_column='Provision', blank=True) # Field name made lowercase.
    provisiongruppe = models.IntegerField(null=True, db_column='ProvisionGruppe', blank=True) # Field name made lowercase.
    kalender = models.CharField(max_length=1, db_column='Kalender', blank=True) # Field name made lowercase.
    lonomk = models.FloatField(null=True, db_column='LonOmk', blank=True) # Field name made lowercase.
    sfprodtime = models.IntegerField(null=True, db_column='SFProdTime', blank=True) # Field name made lowercase.
    autologin = models.CharField(max_length=24, db_column='AutoLogin', blank=True) # Field name made lowercase.
    note = models.TextField(db_column='Note', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'medarb'

class Medgrp(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    primaer = models.CharField(max_length=1, db_column='Primaer', blank=True) # Field name made lowercase.
    samlekonto = models.IntegerField(null=True, db_column='Samlekonto', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'medgrp'

class Medkonti(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    kortnr = models.IntegerField(null=True, db_column='KortNr', blank=True) # Field name made lowercase.
    korttype = models.IntegerField(null=True, db_column='KortType', blank=True) # Field name made lowercase.
    salgsdato = models.DateTimeField(null=True, db_column='SalgsDato', blank=True) # Field name made lowercase.
    antal = models.IntegerField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    pris = models.FloatField(null=True, db_column='Pris', blank=True) # Field name made lowercase.
    udlobsdato = models.DateTimeField(null=True, db_column='UdlobsDato', blank=True) # Field name made lowercase.
    lukket = models.DateTimeField(null=True, db_column='Lukket', blank=True) # Field name made lowercase.
    kundenr = models.IntegerField(null=True, db_column='KundeNr', blank=True) # Field name made lowercase.
    foerstemanede = models.IntegerField(null=True, db_column='FoersteManede', blank=True) # Field name made lowercase.
    medarbejder = models.IntegerField(null=True, db_column='Medarbejder', blank=True) # Field name made lowercase.
    terminal = models.IntegerField(null=True, db_column='Terminal', blank=True) # Field name made lowercase.
    bonnr = models.IntegerField(null=True, db_column='BonNr', blank=True) # Field name made lowercase.
    resterendeklip = models.IntegerField(null=True, db_column='ResterendeKlip', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    betaldato = models.DateTimeField(null=True, db_column='BetalDato', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'medkonti'

class Medlem2(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fnavn = models.CharField(max_length=8, db_column='FNavn', blank=True) # Field name made lowercase.
    enavn = models.CharField(max_length=8, db_column='ENavn', blank=True) # Field name made lowercase.
    adr1 = models.CharField(max_length=8, db_column='Adr1', blank=True) # Field name made lowercase.
    adr2 = models.CharField(max_length=8, db_column='Adr2', blank=True) # Field name made lowercase.
    post = models.CharField(max_length=8, db_column='Post', blank=True) # Field name made lowercase.
    bynavn = models.CharField(max_length=8, db_column='Bynavn', blank=True) # Field name made lowercase.
    oprettet = models.CharField(max_length=8, db_column='Oprettet', blank=True) # Field name made lowercase.
    sidstrettet = models.CharField(max_length=8, db_column='SidstRettet', blank=True) # Field name made lowercase.
    kreditmax = models.CharField(max_length=8, db_column='KreditMax', blank=True) # Field name made lowercase.
    email = models.CharField(max_length=8, db_column='EMail', blank=True) # Field name made lowercase.
    pbskundenr = models.CharField(max_length=8, db_column='PBSKundeNr', blank=True) # Field name made lowercase.
    fodselsdag = models.CharField(max_length=8, db_column='FodselsDag', blank=True) # Field name made lowercase.
    cprfire = models.CharField(max_length=8, db_column='CPRFire', blank=True) # Field name made lowercase.
    cprsenr = models.CharField(max_length=8, db_column='CPRSENr', blank=True) # Field name made lowercase.
    pbssidstlevrdato = models.CharField(max_length=8, db_column='PBSSidstLevrDato', blank=True) # Field name made lowercase.
    pbssidstbehandletdato = models.CharField(max_length=8, db_column='PBSSidstBehandletDato', blank=True) # Field name made lowercase.
    pbssidstbetaldato = models.CharField(max_length=8, db_column='PBSSidstBetalDato', blank=True) # Field name made lowercase.
    pbsaftalenr = models.CharField(max_length=8, db_column='PBSAftaleNr', blank=True) # Field name made lowercase.
    bankregnr = models.CharField(max_length=8, db_column='BankRegNr', blank=True) # Field name made lowercase.
    bankkontonr = models.CharField(max_length=8, db_column='BankKontoNr', blank=True) # Field name made lowercase.
    debgrpnr = models.CharField(max_length=8, db_column='DebGrpNr', blank=True) # Field name made lowercase.
    arykker = models.CharField(max_length=8, db_column='ARykker', blank=True) # Field name made lowercase.
    drykker = models.CharField(max_length=8, db_column='DRykker', blank=True) # Field name made lowercase.
    dcard = models.CharField(max_length=8, db_column='DCard', blank=True) # Field name made lowercase.
    saldo = models.CharField(max_length=8, db_column='Saldo', blank=True) # Field name made lowercase.
    frrkonto = models.CharField(max_length=8, db_column='FRRKonto', blank=True) # Field name made lowercase.
    eannummer = models.CharField(max_length=8, db_column='EANNummer', blank=True) # Field name made lowercase.
    bygnr = models.CharField(max_length=8, db_column='BygNr', blank=True) # Field name made lowercase.
    kon = models.CharField(max_length=8, db_column='Kon', blank=True) # Field name made lowercase.
    firmakode = models.CharField(max_length=8, db_column='FirmaKode', blank=True) # Field name made lowercase.
    lukket = models.CharField(max_length=8, db_column='Lukket', blank=True) # Field name made lowercase.
    weblogin = models.CharField(max_length=8, db_column='WebLogin', blank=True) # Field name made lowercase.
    webpassword = models.CharField(max_length=8, db_column='WebPassword', blank=True) # Field name made lowercase.
    booklogin = models.CharField(max_length=8, db_column='BookLogin', blank=True) # Field name made lowercase.
    bookpassword = models.CharField(max_length=8, db_column='BookPassword', blank=True) # Field name made lowercase.
    billede = models.CharField(max_length=8, db_column='Billede', blank=True) # Field name made lowercase.
    telefon = models.CharField(max_length=8, db_column='Telefon', blank=True) # Field name made lowercase.
    mobil = models.CharField(max_length=8, db_column='Mobil', blank=True) # Field name made lowercase.
    afdeling = models.CharField(max_length=8, db_column='Afdeling', blank=True) # Field name made lowercase.
    udlob = models.CharField(max_length=8, db_column='Udlob', blank=True) # Field name made lowercase.
    notat = models.CharField(max_length=8, db_column='Notat', blank=True) # Field name made lowercase.
    egenmodule = models.IntegerField(null=True, db_column='EgenModule', blank=True) # Field name made lowercase.
    region = models.CharField(max_length=8, db_column='Region', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'medlem2'

class Medlemp(models.Model):
    felt00 = models.IntegerField(unique=True, null=True, db_column='Felt00', blank=True) # Field name made lowercase.
    felt01 = models.CharField(max_length=35, db_column='Felt01', blank=True) # Field name made lowercase.
    felt02 = models.CharField(max_length=40, db_column='Felt02', blank=True) # Field name made lowercase.
    felt03 = models.CharField(max_length=10, db_column='Felt03', blank=True) # Field name made lowercase.
    felt04 = models.CharField(max_length=25, db_column='Felt04', blank=True) # Field name made lowercase.
    felt05 = models.CharField(max_length=16, db_column='Felt05', blank=True) # Field name made lowercase.
    felt06 = models.CharField(max_length=13, db_column='Felt06', blank=True) # Field name made lowercase.
    felt07 = models.CharField(max_length=16, db_column='Felt07', blank=True) # Field name made lowercase.
    felt08 = models.TextField(db_column='Felt08', blank=True) # Field name made lowercase.
    felt09 = models.CharField(max_length=12, db_column='Felt09', blank=True) # Field name made lowercase.
    felt10 = models.CharField(max_length=50, db_column='Felt10', blank=True) # Field name made lowercase.
    felt11 = models.CharField(max_length=25, db_column='Felt11', blank=True) # Field name made lowercase.
    felt12 = models.CharField(max_length=25, db_column='Felt12', blank=True) # Field name made lowercase.
    felt13 = models.IntegerField(null=True, db_column='Felt13', blank=True) # Field name made lowercase.
    felt14 = models.CharField(max_length=25, db_column='Felt14', blank=True) # Field name made lowercase.
    felt15 = models.DateField(null=True, db_column='Felt15', blank=True) # Field name made lowercase.
    felt16 = models.DateField(null=True, db_column='Felt16', blank=True) # Field name made lowercase.
    felt17 = models.CharField(max_length=25, db_column='Felt17', blank=True) # Field name made lowercase.
    felt18 = models.FloatField(null=True, db_column='Felt18', blank=True) # Field name made lowercase.
    felt19 = models.IntegerField(null=True, db_column='Felt19', blank=True) # Field name made lowercase.
    felt20 = models.CharField(max_length=15, db_column='Felt20', blank=True) # Field name made lowercase.
    felt21 = models.IntegerField(null=True, db_column='Felt21', blank=True) # Field name made lowercase.
    felt22 = models.CharField(max_length=18, db_column='Felt22', blank=True) # Field name made lowercase.
    felt23 = models.DateField(null=True, db_column='Felt23', blank=True) # Field name made lowercase.
    felt24 = models.CharField(max_length=1, db_column='Felt24', blank=True) # Field name made lowercase.
    felt25 = models.DateField(null=True, db_column='Felt25', blank=True) # Field name made lowercase.
    felt26 = models.CharField(max_length=35, db_column='Felt26', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'medlemp'

class Medlside(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    farve = models.CharField(max_length=24, db_column='Farve', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    wpmodule = models.IntegerField(null=True, db_column='WPModule', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'medlside'

class Metkart(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=50, db_column='Navn', blank=True) # Field name made lowercase.
    antal = models.IntegerField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    tidsbeg = models.CharField(max_length=1, db_column='TidsBeg', blank=True) # Field name made lowercase.
    starttid = models.TextField(db_column='StartTid', blank=True) # Field name made lowercase. This field type is a guess.
    sluttid = models.TextField(db_column='SlutTid', blank=True) # Field name made lowercase. This field type is a guess.
    omskonti = models.IntegerField(null=True, db_column='OmsKonti', blank=True) # Field name made lowercase.
    gebyrkonti = models.IntegerField(null=True, db_column='GebyrKonti', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    udlober = models.IntegerField(null=True, db_column='Udlober', blank=True) # Field name made lowercase.
    pris = models.FloatField(null=True, db_column='Pris', blank=True) # Field name made lowercase.
    metode = models.IntegerField(null=True, db_column='Metode', blank=True) # Field name made lowercase.
    salgsdato = models.DateTimeField(null=True, db_column='SalgsDato', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    paytype = models.IntegerField(null=True, db_column='PayType', blank=True) # Field name made lowercase.
    cash = models.FloatField(null=True, db_column='Cash', blank=True) # Field name made lowercase.
    aafr = models.IntegerField(null=True, db_column='AAfr', blank=True) # Field name made lowercase.
    ikkeaktiv = models.IntegerField(null=True, db_column='IkkeAktiv', blank=True) # Field name made lowercase.
    dibsregel = models.CharField(max_length=50, db_column='DIBSRegel', blank=True) # Field name made lowercase.
    medtaginetkartotek = models.CharField(max_length=1, db_column='MedtagInetKartotek', blank=True) # Field name made lowercase.
    medtagrestdage = models.CharField(max_length=1, db_column='MedtagRestDage', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    feltnavn = models.CharField(max_length=8, db_column='FeltNavn', blank=True) # Field name made lowercase.
    fitness = models.CharField(max_length=1, db_column='Fitness', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'metkart'

class Mimport(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    filfelt = models.CharField(max_length=30, db_column='FilFelt', blank=True) # Field name made lowercase.
    winkasfelt = models.CharField(max_length=35, db_column='WinKASFelt', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'mimport'

class Modem(models.Model):
    modemname = models.CharField(unique=True, max_length=60, db_column='ModemName', blank=True) # Field name made lowercase.
    modeminit = models.CharField(max_length=60, db_column='ModemInit', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'modem'

class Nextkt(models.Model):
    nextbilag1 = models.IntegerField(null=True, db_column='NextBilag1', blank=True) # Field name made lowercase.
    nextjurnale1 = models.IntegerField(null=True, db_column='NextJurnale1', blank=True) # Field name made lowercase.
    nextbilag = models.IntegerField(null=True, db_column='NextBilag', blank=True) # Field name made lowercase.
    nextjurnale = models.IntegerField(null=True, db_column='NextJurnale', blank=True) # Field name made lowercase.
    nextbilag_1 = models.IntegerField(null=True, db_column='NextBilag_1', blank=True) # Field name made lowercase.
    nextjurnale_1 = models.IntegerField(null=True, db_column='NextJurnale_1', blank=True) # Field name made lowercase.
    sreg = models.IntegerField(null=True, db_column='SREG', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'nextkt'

class Notatlog(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    initialer = models.CharField(max_length=10, db_column='Initialer', blank=True) # Field name made lowercase.
    medarb = models.IntegerField(null=True, db_column='Medarb', blank=True) # Field name made lowercase.
    title = models.CharField(max_length=32, db_column='Title', blank=True) # Field name made lowercase.
    form = models.CharField(max_length=32, db_column='Form', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'notatlog'

class Notatlogitems(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    hunique = models.IntegerField(null=True, db_column='HUnique', blank=True) # Field name made lowercase.
    wpmodule = models.IntegerField(null=True, db_column='WPModule', blank=True) # Field name made lowercase.
    masterunique = models.IntegerField(null=True, db_column='MasterUnique', blank=True) # Field name made lowercase.
    user = models.IntegerField(null=True, db_column='User', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    notat = models.CharField(max_length=128, db_column='Notat', blank=True) # Field name made lowercase.
    component = models.CharField(max_length=32, db_column='Component', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'notatlogitems'

class Onlinebet(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    ftype = models.IntegerField(null=True, db_column='FType', blank=True) # Field name made lowercase.
    medlemid = models.IntegerField(null=True, db_column='MedlemId', blank=True) # Field name made lowercase.
    beloeb = models.FloatField(null=True, db_column='Beloeb', blank=True) # Field name made lowercase.
    betalbet = models.IntegerField(null=True, db_column='BetalBet', blank=True) # Field name made lowercase.
    webdibstrans = models.CharField(max_length=50, db_column='Webdibstrans', blank=True) # Field name made lowercase.
    webdibsordrenr = models.CharField(max_length=50, db_column='Webdibsordrenr', blank=True) # Field name made lowercase.
    mark = models.CharField(max_length=1, db_column='Mark', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='Omskonto', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'onlinebet'

class Opkr(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    opkrtype = models.IntegerField(null=True, db_column='OpkrType', blank=True) # Field name made lowercase.
    medlemflag = models.IntegerField(null=True, db_column='MedlemFlag', blank=True) # Field name made lowercase.
    fkbetalerflag = models.IntegerField(null=True, db_column='FKBetalerFlag', blank=True) # Field name made lowercase.
    datofra = models.DateTimeField(null=True, db_column='DatoFra', blank=True) # Field name made lowercase.
    datotil = models.DateTimeField(null=True, db_column='DatoTil', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    medarbejder = models.IntegerField(null=True, db_column='Medarbejder', blank=True) # Field name made lowercase.
    den = models.DateTimeField(null=True, db_column='Den', blank=True) # Field name made lowercase.
    bogfcount = models.IntegerField(null=True, db_column='BogfCount', blank=True) # Field name made lowercase.
    pbscount = models.IntegerField(null=True, db_column='PBSCount', blank=True) # Field name made lowercase.
    fakturacount = models.IntegerField(null=True, db_column='FakturaCount', blank=True) # Field name made lowercase.
    totalkr = models.FloatField(null=True, db_column='TotalKr', blank=True) # Field name made lowercase.
    antal = models.IntegerField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    antalmed = models.IntegerField(null=True, db_column='AntalMed', blank=True) # Field name made lowercase.
    lbtotalkr = models.FloatField(null=True, db_column='LbTotalKr', blank=True) # Field name made lowercase.
    lbantal = models.IntegerField(null=True, db_column='LbAntal', blank=True) # Field name made lowercase.
    lbantalmed = models.IntegerField(null=True, db_column='LbAntalMed', blank=True) # Field name made lowercase.
    medtag = models.CharField(max_length=1, db_column='Medtag', blank=True) # Field name made lowercase.
    opgorelse = models.CharField(max_length=1, db_column='Opgorelse', blank=True) # Field name made lowercase.
    alleflag = models.CharField(max_length=1, db_column='AlleFlag', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'opkr'

class Opkralle(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    opkrunique = models.IntegerField(null=True, db_column='OpkrUnique', blank=True) # Field name made lowercase.
    opkroms = models.IntegerField(null=True, db_column='OpkrOms', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    ydelsesnr = models.IntegerField(null=True, db_column='YdelsesNr', blank=True) # Field name made lowercase.
    medlemnr = models.IntegerField(null=True, db_column='MedlemNr', blank=True) # Field name made lowercase.
    vedrkonto = models.IntegerField(null=True, db_column='VedrKonto', blank=True) # Field name made lowercase.
    modtager = models.IntegerField(null=True, db_column='Modtager', blank=True) # Field name made lowercase.
    periodekode = models.IntegerField(null=True, db_column='PeriodeKode', blank=True) # Field name made lowercase.
    maalernr = models.IntegerField(null=True, db_column='MaalerNr', blank=True) # Field name made lowercase.
    vedrmaalernr = models.IntegerField(null=True, db_column='VedrMaalerNr', blank=True) # Field name made lowercase.
    maalpostunique = models.IntegerField(null=True, db_column='MaalPostUnique', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    forfald = models.DateTimeField(null=True, db_column='Forfald', blank=True) # Field name made lowercase.
    betalt = models.CharField(max_length=1, db_column='Betalt', blank=True) # Field name made lowercase.
    fvalue = models.FloatField(null=True, db_column='FValue', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    kr = models.FloatField(null=True, db_column='Kr', blank=True) # Field name made lowercase.
    momskr = models.FloatField(null=True, db_column='MomsKr', blank=True) # Field name made lowercase.
    datofra = models.DateTimeField(null=True, db_column='DatoFra', blank=True) # Field name made lowercase.
    datotil = models.DateTimeField(null=True, db_column='DatoTil', blank=True) # Field name made lowercase.
    primores = models.FloatField(null=True, db_column='PrimoRes', blank=True) # Field name made lowercase.
    ultimores = models.FloatField(null=True, db_column='UltimoRes', blank=True) # Field name made lowercase.
    betalerindent = models.FloatField(null=True, db_column='BetalerIndent', blank=True) # Field name made lowercase.
    retur = models.CharField(max_length=1, db_column='Retur', blank=True) # Field name made lowercase.
    kontooms = models.IntegerField(null=True, db_column='KontoOms', blank=True) # Field name made lowercase.
    fakturanr = models.IntegerField(null=True, db_column='FakturaNr', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, blank=True)
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    udlignetdato = models.DateTimeField(null=True, db_column='UdlignetDato', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    perhapsopkr = models.CharField(max_length=1, db_column='PerhapsOpkr', blank=True) # Field name made lowercase.
    udlignetslet = models.CharField(max_length=1, db_column='UdlignetSlet', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'opkralle'

class Opkrallevis(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    opkrunique = models.IntegerField(null=True, db_column='OpkrUnique', blank=True) # Field name made lowercase.
    opkroms = models.IntegerField(null=True, db_column='OpkrOms', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    ydelsesnr = models.IntegerField(null=True, db_column='YdelsesNr', blank=True) # Field name made lowercase.
    medlemnr = models.IntegerField(null=True, db_column='MedlemNr', blank=True) # Field name made lowercase.
    vedrkonto = models.IntegerField(null=True, db_column='VedrKonto', blank=True) # Field name made lowercase.
    modtager = models.IntegerField(null=True, db_column='Modtager', blank=True) # Field name made lowercase.
    periodekode = models.IntegerField(null=True, db_column='PeriodeKode', blank=True) # Field name made lowercase.
    maalernr = models.IntegerField(null=True, db_column='MaalerNr', blank=True) # Field name made lowercase.
    vedrmaalernr = models.IntegerField(null=True, db_column='VedrMaalerNr', blank=True) # Field name made lowercase.
    maalpostunique = models.IntegerField(null=True, db_column='MaalPostUnique', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    forfald = models.DateTimeField(null=True, db_column='Forfald', blank=True) # Field name made lowercase.
    betalt = models.CharField(max_length=1, db_column='Betalt', blank=True) # Field name made lowercase.
    fvalue = models.FloatField(null=True, db_column='FValue', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    kr = models.FloatField(null=True, db_column='Kr', blank=True) # Field name made lowercase.
    momskr = models.FloatField(null=True, db_column='MomsKr', blank=True) # Field name made lowercase.
    datofra = models.DateTimeField(null=True, db_column='DatoFra', blank=True) # Field name made lowercase.
    datotil = models.DateTimeField(null=True, db_column='DatoTil', blank=True) # Field name made lowercase.
    primores = models.FloatField(null=True, db_column='PrimoRes', blank=True) # Field name made lowercase.
    ultimores = models.FloatField(null=True, db_column='UltimoRes', blank=True) # Field name made lowercase.
    betalerindent = models.FloatField(null=True, db_column='BetalerIndent', blank=True) # Field name made lowercase.
    retur = models.CharField(max_length=1, db_column='Retur', blank=True) # Field name made lowercase.
    kontooms = models.IntegerField(null=True, db_column='KontoOms', blank=True) # Field name made lowercase.
    fakturanr = models.IntegerField(null=True, db_column='FakturaNr', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, blank=True)
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    udlignetdato = models.DateTimeField(null=True, db_column='UdlignetDato', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    perhapsopkr = models.CharField(max_length=1, db_column='PerhapsOpkr', blank=True) # Field name made lowercase.
    udlignetslet = models.CharField(max_length=1, db_column='UdlignetSlet', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'opkrallevis'

class Opkrlin(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fpsunique = models.IntegerField(null=True, db_column='FPSUnique', blank=True) # Field name made lowercase.
    ftype = models.IntegerField(null=True, db_column='FType', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    valutafelt = models.CharField(max_length=32, db_column='Valutafelt', blank=True) # Field name made lowercase.
    specadvis = models.CharField(max_length=1, db_column='SpecAdvis', blank=True) # Field name made lowercase.
    posttekst = models.CharField(max_length=50, db_column='PostTekst', blank=True) # Field name made lowercase.
    fastbelob = models.FloatField(null=True, db_column='FastBelob', blank=True) # Field name made lowercase.
    brugfastbelob = models.CharField(max_length=1, db_column='BrugFastBelob', blank=True) # Field name made lowercase.
    tekstindex = models.IntegerField(null=True, db_column='TekstIndex', blank=True) # Field name made lowercase.
    momsindex = models.IntegerField(null=True, db_column='MomsIndex', blank=True) # Field name made lowercase.
    betaltdatofelt = models.CharField(max_length=8, db_column='BetaltDatoFelt', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'opkrlin'

class Opkroms(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    opkrtype = models.IntegerField(null=True, db_column='OpkrType', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    opkart = models.IntegerField(null=True, db_column='OpkArt', blank=True) # Field name made lowercase.
    acontofelt = models.CharField(max_length=8, db_column='AcontoFelt', blank=True) # Field name made lowercase.
    forbrugfelt = models.CharField(max_length=8, db_column='ForbrugFelt', blank=True) # Field name made lowercase.
    fastkr = models.FloatField(null=True, db_column='FastKr', blank=True) # Field name made lowercase.
    prisfraydelse = models.IntegerField(null=True, db_column='PrisFraYdelse', blank=True) # Field name made lowercase.
    forfaldmdrflag = models.IntegerField(null=True, db_column='ForfaldMdrFlag', blank=True) # Field name made lowercase.
    enhedskr = models.FloatField(null=True, db_column='EnhedsKr', blank=True) # Field name made lowercase.
    beregnfast = models.CharField(max_length=1, db_column='BeregnFast', blank=True) # Field name made lowercase.
    maalertype = models.IntegerField(null=True, db_column='MaalerType', blank=True) # Field name made lowercase.
    enhedbet = models.CharField(max_length=16, db_column='EnhedBet', blank=True) # Field name made lowercase.
    specenhed = models.CharField(max_length=1, db_column='SpecEnhed', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    tekst = models.CharField(max_length=50, db_column='Tekst', blank=True) # Field name made lowercase.
    exclmoms = models.IntegerField(null=True, db_column='ExclMoms', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='Momskode', blank=True) # Field name made lowercase.
    visnul = models.CharField(max_length=1, db_column='VisNul', blank=True) # Field name made lowercase.
    autotekst = models.CharField(max_length=1, db_column='AutoTekst', blank=True) # Field name made lowercase.
    medtag = models.CharField(max_length=1, db_column='Medtag', blank=True) # Field name made lowercase.
    fkbetaler = models.IntegerField(null=True, db_column='FKBetaler', blank=True) # Field name made lowercase.
    opkravmdrflag = models.IntegerField(null=True, db_column='OpkravMdrFlag', blank=True) # Field name made lowercase.
    antalmdr = models.IntegerField(null=True, db_column='AntalMdr', blank=True) # Field name made lowercase.
    ingenrefusion = models.IntegerField(null=True, db_column='IngenRefusion', blank=True) # Field name made lowercase.
    perhapsopkr = models.CharField(max_length=1, db_column='PerhapsOpkr', blank=True) # Field name made lowercase.
    fremskriv = models.CharField(max_length=1, db_column='Fremskriv', blank=True) # Field name made lowercase.
    rabattype = models.IntegerField(null=True, db_column='RabatType', blank=True) # Field name made lowercase.
    rabatpris1 = models.FloatField(null=True, db_column='RabatPris1', blank=True) # Field name made lowercase.
    rabatantal1 = models.FloatField(null=True, db_column='RabatAntal1', blank=True) # Field name made lowercase.
    rabatpris2 = models.FloatField(null=True, db_column='RabatPris2', blank=True) # Field name made lowercase.
    rabatantal2 = models.FloatField(null=True, db_column='RabatAntal2', blank=True) # Field name made lowercase.
    rabatpris3 = models.FloatField(null=True, db_column='RabatPris3', blank=True) # Field name made lowercase.
    rabatantal3 = models.FloatField(null=True, db_column='RabatAntal3', blank=True) # Field name made lowercase.
    rabatpris4 = models.FloatField(null=True, db_column='RabatPris4', blank=True) # Field name made lowercase.
    rabatantal4 = models.FloatField(null=True, db_column='RabatAntal4', blank=True) # Field name made lowercase.
    rabatpris5 = models.FloatField(null=True, db_column='RabatPris5', blank=True) # Field name made lowercase.
    rabatantal5 = models.FloatField(null=True, db_column='RabatAntal5', blank=True) # Field name made lowercase.
    samledeforbrug = models.FloatField(null=True, db_column='SamledeForbrug', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'opkroms'

class Opkrtype(models.Model):
    nummer = models.IntegerField(unique=True, null=True, db_column='Nummer', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    note = models.CharField(max_length=50, db_column='Note', blank=True) # Field name made lowercase.
    periodekode = models.IntegerField(null=True, db_column='PeriodeKode', blank=True) # Field name made lowercase.
    udsnit = models.CharField(max_length=32, db_column='Udsnit', blank=True) # Field name made lowercase.
    wpmodule = models.IntegerField(null=True, db_column='WPModule', blank=True) # Field name made lowercase.
    udsnitsunique = models.IntegerField(null=True, db_column='UdsnitsUnique', blank=True) # Field name made lowercase.
    betalbet = models.IntegerField(null=True, db_column='BetalBet', blank=True) # Field name made lowercase.
    medtagmanglendeperiode = models.IntegerField(null=True, db_column='MedtagManglendePeriode', blank=True) # Field name made lowercase.
    ejejerskift = models.IntegerField(null=True, db_column='EjEjerSkift', blank=True) # Field name made lowercase.
    opkravflag = models.CharField(max_length=1, db_column='OpkravFlag', blank=True) # Field name made lowercase.
    udbetkonto = models.IntegerField(null=True, db_column='UdbetKonto', blank=True) # Field name made lowercase.
    gebyrsompbsrabat = models.IntegerField(null=True, db_column='GebyrSomPBSRabat', blank=True) # Field name made lowercase.
    figebyrbelob = models.FloatField(null=True, db_column='FIGebyrBelob', blank=True) # Field name made lowercase.
    figebyrkonto = models.IntegerField(null=True, db_column='FIGebyrKonto', blank=True) # Field name made lowercase.
    datofra = models.DateTimeField(null=True, db_column='DatoFra', blank=True) # Field name made lowercase.
    datotil = models.DateTimeField(null=True, db_column='DatoTil', blank=True) # Field name made lowercase.
    opkravantal = models.IntegerField(null=True, db_column='OpkravAntal', blank=True) # Field name made lowercase.
    specperiode = models.CharField(max_length=1, db_column='SpecPeriode', blank=True) # Field name made lowercase.
    specmaaler = models.IntegerField(null=True, db_column='SpecMaaler', blank=True) # Field name made lowercase.
    gennemfortopkravantal = models.IntegerField(null=True, db_column='GennemfortOpkravAntal', blank=True) # Field name made lowercase.
    opkravdato = models.DateTimeField(null=True, db_column='OpkravDato', blank=True) # Field name made lowercase.
    opkravtype = models.IntegerField(null=True, db_column='OpkravType', blank=True) # Field name made lowercase.
    samlepost = models.CharField(max_length=1, db_column='SamlePost', blank=True) # Field name made lowercase.
    specificer = models.CharField(max_length=1, db_column='Specificer', blank=True) # Field name made lowercase.
    specmoms = models.CharField(max_length=1, db_column='SpecMoms', blank=True) # Field name made lowercase.
    opkravnormalvia = models.IntegerField(null=True, db_column='OpkravNormalVia', blank=True) # Field name made lowercase.
    autobogforoms = models.CharField(max_length=1, db_column='AutoBogforOms', blank=True) # Field name made lowercase.
    fakturatitle = models.CharField(max_length=20, db_column='FakturaTitle', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    kladdenr = models.IntegerField(null=True, db_column='KladdeNr', blank=True) # Field name made lowercase.
    fakturatekst = models.TextField(db_column='FakturaTekst', blank=True) # Field name made lowercase.
    design = models.CharField(max_length=32, db_column='Design', blank=True) # Field name made lowercase.
    designkreditnota = models.CharField(max_length=32, db_column='DesignKreditnota', blank=True) # Field name made lowercase.
    autotekst = models.CharField(max_length=1, db_column='AutoTekst', blank=True) # Field name made lowercase.
    samlelinie = models.CharField(max_length=1, db_column='Samlelinie', blank=True) # Field name made lowercase.
    periodegebyr = models.CharField(max_length=1, db_column='PeriodeGebyr', blank=True) # Field name made lowercase.
    periodegebyrbelob = models.FloatField(null=True, db_column='PeriodeGebyrBelob', blank=True) # Field name made lowercase.
    periodegebyrkonto = models.IntegerField(null=True, db_column='PeriodeGebyrKonto', blank=True) # Field name made lowercase.
    periodegebyrtext = models.CharField(max_length=50, db_column='PeriodeGebyrText', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'opkrtype'

class Opkrvis(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    opkrtype = models.IntegerField(null=True, db_column='OpkrType', blank=True) # Field name made lowercase.
    medlemflag = models.IntegerField(null=True, db_column='MedlemFlag', blank=True) # Field name made lowercase.
    fkbetalerflag = models.IntegerField(null=True, db_column='FKBetalerFlag', blank=True) # Field name made lowercase.
    datofra = models.DateTimeField(null=True, db_column='DatoFra', blank=True) # Field name made lowercase.
    datotil = models.DateTimeField(null=True, db_column='DatoTil', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=50, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    medarbejder = models.IntegerField(null=True, db_column='Medarbejder', blank=True) # Field name made lowercase.
    den = models.DateTimeField(null=True, db_column='Den', blank=True) # Field name made lowercase.
    bogfcount = models.IntegerField(null=True, db_column='BogfCount', blank=True) # Field name made lowercase.
    pbscount = models.IntegerField(null=True, db_column='PBSCount', blank=True) # Field name made lowercase.
    fakturacount = models.IntegerField(null=True, db_column='FakturaCount', blank=True) # Field name made lowercase.
    totalkr = models.FloatField(null=True, db_column='TotalKr', blank=True) # Field name made lowercase.
    antal = models.IntegerField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    antalmed = models.IntegerField(null=True, db_column='AntalMed', blank=True) # Field name made lowercase.
    lbtotalkr = models.FloatField(null=True, db_column='LbTotalKr', blank=True) # Field name made lowercase.
    lbantal = models.IntegerField(null=True, db_column='LbAntal', blank=True) # Field name made lowercase.
    lbantalmed = models.IntegerField(null=True, db_column='LbAntalMed', blank=True) # Field name made lowercase.
    medtag = models.CharField(max_length=1, db_column='Medtag', blank=True) # Field name made lowercase.
    opgorelse = models.CharField(max_length=1, db_column='Opgorelse', blank=True) # Field name made lowercase.
    alleflag = models.CharField(max_length=1, db_column='AlleFlag', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'opkrvis'

class Pbsgrupper(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    regnr = models.IntegerField(null=True, db_column='RegNr', blank=True) # Field name made lowercase.
    bankkonto = models.CharField(max_length=10, db_column='BankKonto', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    delsystem = models.CharField(max_length=3, db_column='DelSystem', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'pbsgrupper'

class Pbssys(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=6, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    bankkontonr = models.IntegerField(null=True, db_column='BankKontoNr', blank=True) # Field name made lowercase.
    regnr = models.CharField(max_length=4, db_column='RegNr', blank=True) # Field name made lowercase.
    bankkonto = models.CharField(max_length=12, db_column='BankKonto', blank=True) # Field name made lowercase.
    delsystem = models.CharField(max_length=3, db_column='DelSystem', blank=True) # Field name made lowercase.
    bogfornu = models.CharField(max_length=1, db_column='BogforNu', blank=True) # Field name made lowercase.
    omsbogforbetalt = models.IntegerField(null=True, db_column='OmsBogforBetalt', blank=True) # Field name made lowercase.
    omsbogforbetaldato = models.IntegerField(null=True, db_column='OmsBogforBetalDato', blank=True) # Field name made lowercase.
    anvendejudligning = models.IntegerField(null=True, db_column='AnvendEjUdligning', blank=True) # Field name made lowercase.
    anvendfakturakonto = models.IntegerField(null=True, db_column='AnvendFakturaKonto', blank=True) # Field name made lowercase.
    losning = models.IntegerField(null=True, db_column='Losning', blank=True) # Field name made lowercase.
    debgrupnr = models.CharField(max_length=5, db_column='DebGrupNr', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    restanceadvis = models.TextField(db_column='RestanceAdvis', blank=True) # Field name made lowercase.
    udskrivmomsadvis = models.CharField(max_length=1, db_column='UdskrivMomsAdvis', blank=True) # Field name made lowercase.
    udsnit = models.CharField(max_length=32, db_column='Udsnit', blank=True) # Field name made lowercase.
    egetkartotek = models.IntegerField(null=True, db_column='EgetKartotek', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    betalbet = models.IntegerField(null=True, db_column='Betalbet', blank=True) # Field name made lowercase.
    fiknr = models.IntegerField(null=True, db_column='FIKNR', blank=True) # Field name made lowercase.
    antalopkpraar = models.IntegerField(null=True, db_column='AntalOpkPrAar', blank=True) # Field name made lowercase.
    foerstemaaned = models.IntegerField(null=True, db_column='FoersteMaaned', blank=True) # Field name made lowercase.
    forskydmdr = models.IntegerField(null=True, db_column='ForskydMdr', blank=True) # Field name made lowercase.
    periodepaaadvis = models.CharField(max_length=1, db_column='PeriodePaaAdvis', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'pbssys'

class Pck1(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=10, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=100, db_column='Navn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'pck1'

class Pck2(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=10, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=100, db_column='Navn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'pck2'

class Pck4(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=10, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=100, db_column='Navn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'pck4'

class Popkrav(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    payunique = models.IntegerField(null=True, db_column='PayUnique', blank=True) # Field name made lowercase.
    sumkontonr = models.IntegerField(null=True, db_column='SumKontoNr', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=30, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    belob = models.FloatField(null=True, db_column='Belob', blank=True) # Field name made lowercase.
    projektnr = models.IntegerField(null=True, db_column='ProjektNr', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    omsbogfornu = models.CharField(max_length=1, db_column='OmsBogforNu', blank=True) # Field name made lowercase.
    omsbogfort = models.CharField(max_length=1, db_column='OmsBogfort', blank=True) # Field name made lowercase.
    profilnr = models.IntegerField(null=True, db_column='ProfilNr', blank=True) # Field name made lowercase.
    opkraevtype = models.IntegerField(null=True, db_column='OpkraevType', blank=True) # Field name made lowercase.
    betalbogfort = models.CharField(max_length=1, db_column='BetalBogfort', blank=True) # Field name made lowercase.
    kvittering = models.CharField(max_length=1, db_column='Kvittering', blank=True) # Field name made lowercase.
    betaltdatofelt = models.CharField(max_length=8, db_column='BetaltDatoFelt', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'popkrav'

class Popkravrest(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    payunique = models.IntegerField(null=True, db_column='PayUnique', blank=True) # Field name made lowercase.
    sumkontonr = models.IntegerField(null=True, db_column='SumKontoNr', blank=True) # Field name made lowercase.
    kontonr = models.IntegerField(null=True, db_column='KontoNr', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=30, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    belob = models.FloatField(null=True, db_column='Belob', blank=True) # Field name made lowercase.
    projektnr = models.IntegerField(null=True, db_column='ProjektNr', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    omsbogfornu = models.CharField(max_length=1, db_column='OmsBogforNu', blank=True) # Field name made lowercase.
    omsbogfort = models.CharField(max_length=1, db_column='OmsBogfort', blank=True) # Field name made lowercase.
    profilnr = models.IntegerField(null=True, db_column='ProfilNr', blank=True) # Field name made lowercase.
    opkraevtype = models.IntegerField(null=True, db_column='OpkraevType', blank=True) # Field name made lowercase.
    betalbogfort = models.CharField(max_length=1, db_column='BetalBogfort', blank=True) # Field name made lowercase.
    kvittering = models.CharField(max_length=1, db_column='Kvittering', blank=True) # Field name made lowercase.
    betaltdatofelt = models.CharField(max_length=8, db_column='BetaltDatoFelt', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    art = models.IntegerField(null=True, db_column='Art', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'popkravrest'

class Postby(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    amtsnr = models.IntegerField(null=True, db_column='Amtsnr', blank=True) # Field name made lowercase.
    komnr = models.IntegerField(null=True, blank=True)
    postnr = models.CharField(max_length=10, db_column='PostNr', blank=True) # Field name made lowercase.
    bynavn = models.CharField(max_length=30, db_column='ByNavn', blank=True) # Field name made lowercase.
    region = models.CharField(max_length=50, db_column='Region', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'postby'

class Postbyp(models.Model):
    postnr = models.CharField(unique=True, max_length=10, db_column='PostNr', blank=True) # Field name made lowercase.
    gammelpostnr = models.CharField(max_length=10, db_column='GammelPostNr', blank=True) # Field name made lowercase.
    bynavn = models.CharField(max_length=30, db_column='ByNavn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'postbyp'

class Postpuvs(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    bladnr = models.IntegerField(null=True, db_column='Bladnr', blank=True) # Field name made lowercase.
    bladnavn = models.CharField(max_length=34, db_column='Bladnavn', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=64, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    udsnit = models.CharField(max_length=32, db_column='Udsnit', blank=True) # Field name made lowercase.
    email = models.CharField(max_length=128, db_column='EMail', blank=True) # Field name made lowercase.
    kundenrfelt = models.CharField(max_length=8, db_column='Kundenrfelt', blank=True) # Field name made lowercase.
    antalfelt = models.CharField(max_length=8, db_column='Antalfelt', blank=True) # Field name made lowercase.
    sti = models.CharField(max_length=255, db_column='Sti', blank=True) # Field name made lowercase.
    mailto = models.CharField(max_length=1, db_column='Mailto', blank=True) # Field name made lowercase.
    compress = models.CharField(max_length=1, db_column='Compress', blank=True) # Field name made lowercase.
    udgiv = models.CharField(max_length=1, db_column='Udgiv', blank=True) # Field name made lowercase.
    omdato = models.DateTimeField(null=True, db_column='OmDato', blank=True) # Field name made lowercase.
    fornavn = models.CharField(max_length=8, db_column='ForNavn', blank=True) # Field name made lowercase.
    efternavn = models.CharField(max_length=8, db_column='EfterNavn', blank=True) # Field name made lowercase.
    komkode = models.CharField(max_length=8, db_column='KomKode', blank=True) # Field name made lowercase.
    vejkode = models.CharField(max_length=8, db_column='VejKode', blank=True) # Field name made lowercase.
    adresse = models.CharField(max_length=8, db_column='Adresse', blank=True) # Field name made lowercase.
    husnr = models.CharField(max_length=8, db_column='HusNr', blank=True) # Field name made lowercase.
    husbog = models.CharField(max_length=8, db_column='HusBog', blank=True) # Field name made lowercase.
    etage = models.CharField(max_length=8, db_column='Etage', blank=True) # Field name made lowercase.
    placering = models.CharField(max_length=8, db_column='Placering', blank=True) # Field name made lowercase.
    stednavn = models.CharField(max_length=8, db_column='StedNavn', blank=True) # Field name made lowercase.
    postnr = models.CharField(max_length=8, db_column='Postnr', blank=True) # Field name made lowercase.
    postbox = models.CharField(max_length=8, db_column='Postbox', blank=True) # Field name made lowercase.
    conavn = models.CharField(max_length=8, db_column='Conavn', blank=True) # Field name made lowercase.
    adressenavn = models.CharField(max_length=8, db_column='Adressenavn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'postpuvs'

class Profils(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    wpmodul = models.IntegerField(null=True, db_column='WPModul', blank=True) # Field name made lowercase.
    stinavn = models.CharField(max_length=127, db_column='StiNavn', blank=True) # Field name made lowercase.
    profilnavn = models.CharField(max_length=32, db_column='ProfilNavn', blank=True) # Field name made lowercase.
    udklip = models.CharField(max_length=1, db_column='Udklip', blank=True) # Field name made lowercase.
    koloverskrift = models.CharField(max_length=1, db_column='KolOverskrift', blank=True) # Field name made lowercase.
    anforsel = models.CharField(max_length=1, db_column='Anforsel', blank=True) # Field name made lowercase.
    crlf = models.CharField(max_length=1, db_column='CRLF', blank=True) # Field name made lowercase.
    ansioem = models.CharField(max_length=1, db_column='ANSIOEM', blank=True) # Field name made lowercase.
    separator = models.IntegerField(null=True, db_column='Separator', blank=True) # Field name made lowercase.
    decimalseparator = models.IntegerField(null=True, db_column='DecimalSeparator', blank=True) # Field name made lowercase.
    shellout = models.CharField(max_length=24, db_column='ShellOut', blank=True) # Field name made lowercase.
    dateformat = models.IntegerField(null=True, db_column='DateFormat', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'profils'

class Project(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=48, db_column='Navn', blank=True) # Field name made lowercase.
    koncern = models.IntegerField(null=True, db_column='Koncern', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'project'

class Prolay(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'prolay'

class Prolayl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fprounique = models.IntegerField(null=True, db_column='FProUnique', blank=True) # Field name made lowercase.
    designnr = models.IntegerField(null=True, db_column='DesignNr', blank=True) # Field name made lowercase.
    filnavn = models.CharField(max_length=32, db_column='Filnavn', blank=True) # Field name made lowercase.
    vareprint = models.CharField(max_length=1, db_column='VarePrint', blank=True) # Field name made lowercase.
    antal = models.IntegerField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    anvendocr = models.IntegerField(null=True, db_column='AnvendOCR', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'prolayl'

class Provision(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    dummynavn = models.CharField(max_length=16, db_column='DummyNavn', blank=True) # Field name made lowercase.
    medgrp0 = models.FloatField(null=True, db_column='Medgrp0', blank=True) # Field name made lowercase.
    medgrp1 = models.FloatField(null=True, db_column='Medgrp1', blank=True) # Field name made lowercase.
    medgrp2 = models.FloatField(null=True, db_column='Medgrp2', blank=True) # Field name made lowercase.
    medgrp3 = models.FloatField(null=True, db_column='Medgrp3', blank=True) # Field name made lowercase.
    medgrp4 = models.FloatField(null=True, db_column='Medgrp4', blank=True) # Field name made lowercase.
    medgrp5 = models.FloatField(null=True, db_column='Medgrp5', blank=True) # Field name made lowercase.
    medgrp6 = models.FloatField(null=True, db_column='Medgrp6', blank=True) # Field name made lowercase.
    medgrp7 = models.FloatField(null=True, db_column='Medgrp7', blank=True) # Field name made lowercase.
    medgrp8 = models.FloatField(null=True, db_column='Medgrp8', blank=True) # Field name made lowercase.
    medgrp9 = models.FloatField(null=True, db_column='Medgrp9', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'provision'

class Rapport(models.Model):
    beskrivelse = models.CharField(unique=True, max_length=32, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    medtag1 = models.CharField(max_length=1, db_column='Medtag1', blank=True) # Field name made lowercase.
    datofra1 = models.CharField(max_length=10, db_column='DatoFra1', blank=True) # Field name made lowercase.
    datotil1 = models.CharField(max_length=10, db_column='DatoTil1', blank=True) # Field name made lowercase.
    vis1 = models.IntegerField(null=True, db_column='Vis1', blank=True) # Field name made lowercase.
    art1 = models.IntegerField(null=True, db_column='Art1', blank=True) # Field name made lowercase.
    projekt1 = models.IntegerField(null=True, db_column='Projekt1', blank=True) # Field name made lowercase.
    medtag2 = models.CharField(max_length=1, db_column='Medtag2', blank=True) # Field name made lowercase.
    datofra2 = models.CharField(max_length=10, db_column='DatoFra2', blank=True) # Field name made lowercase.
    datotil2 = models.CharField(max_length=10, db_column='DatoTil2', blank=True) # Field name made lowercase.
    vis2 = models.IntegerField(null=True, db_column='Vis2', blank=True) # Field name made lowercase.
    art2 = models.IntegerField(null=True, db_column='Art2', blank=True) # Field name made lowercase.
    projekt2 = models.IntegerField(null=True, db_column='Projekt2', blank=True) # Field name made lowercase.
    medtag3 = models.CharField(max_length=1, db_column='Medtag3', blank=True) # Field name made lowercase.
    datofra3 = models.CharField(max_length=10, db_column='DatoFra3', blank=True) # Field name made lowercase.
    datotil3 = models.CharField(max_length=10, db_column='DatoTil3', blank=True) # Field name made lowercase.
    vis3 = models.IntegerField(null=True, db_column='Vis3', blank=True) # Field name made lowercase.
    art3 = models.IntegerField(null=True, db_column='Art3', blank=True) # Field name made lowercase.
    projekt3 = models.IntegerField(null=True, db_column='Projekt3', blank=True) # Field name made lowercase.
    medtag4 = models.CharField(max_length=1, db_column='Medtag4', blank=True) # Field name made lowercase.
    datofra4 = models.CharField(max_length=10, db_column='DatoFra4', blank=True) # Field name made lowercase.
    datotil4 = models.CharField(max_length=10, db_column='DatoTil4', blank=True) # Field name made lowercase.
    vis4 = models.IntegerField(null=True, db_column='Vis4', blank=True) # Field name made lowercase.
    art4 = models.IntegerField(null=True, db_column='Art4', blank=True) # Field name made lowercase.
    projekt4 = models.IntegerField(null=True, db_column='Projekt4', blank=True) # Field name made lowercase.
    nulkonti = models.CharField(max_length=1, db_column='NulKonti', blank=True) # Field name made lowercase.
    helekroner = models.CharField(max_length=1, db_column='HeleKroner', blank=True) # Field name made lowercase.
    mknoteindex = models.IntegerField(null=True, db_column='MKNoteIndex', blank=True) # Field name made lowercase.
    medtagkontonr = models.CharField(max_length=1, db_column='MedtagKontoNr', blank=True) # Field name made lowercase.
    kundrift = models.CharField(max_length=1, db_column='KunDrift', blank=True) # Field name made lowercase.
    frakonto = models.IntegerField(null=True, db_column='FraKonto', blank=True) # Field name made lowercase.
    tilkonto = models.IntegerField(null=True, db_column='TilKonto', blank=True) # Field name made lowercase.
    tusinde = models.IntegerField(null=True, db_column='Tusinde', blank=True) # Field name made lowercase.
    graf1 = models.IntegerField(null=True, db_column='Graf1', blank=True) # Field name made lowercase.
    graf2 = models.IntegerField(null=True, db_column='Graf2', blank=True) # Field name made lowercase.
    graf3 = models.IntegerField(null=True, db_column='Graf3', blank=True) # Field name made lowercase.
    graf4 = models.IntegerField(null=True, db_column='Graf4', blank=True) # Field name made lowercase.
    afdelinghex = models.CharField(max_length=64, db_column='AfdelingHex', blank=True) # Field name made lowercase.
    fontstr = models.IntegerField(null=True, db_column='Fontstr', blank=True) # Field name made lowercase.
    budgetunique1 = models.IntegerField(null=True, db_column='BudgetUnique1', blank=True) # Field name made lowercase.
    budgetunique2 = models.IntegerField(null=True, db_column='BudgetUnique2', blank=True) # Field name made lowercase.
    budgetunique3 = models.IntegerField(null=True, db_column='BudgetUnique3', blank=True) # Field name made lowercase.
    budgetunique4 = models.IntegerField(null=True, db_column='BudgetUnique4', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'rapport'

class Regnpopl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    regnskabstart = models.DateTimeField(null=True, db_column='RegnskabStart', blank=True) # Field name made lowercase.
    regnskabstop = models.DateTimeField(null=True, db_column='RegnskabStop', blank=True) # Field name made lowercase.
    startsidsteaar = models.DateTimeField(null=True, db_column='StartSidsteAar', blank=True) # Field name made lowercase.
    start2 = models.DateTimeField(null=True, db_column='Start2', blank=True) # Field name made lowercase.
    start3 = models.DateTimeField(null=True, db_column='Start3', blank=True) # Field name made lowercase.
    start4 = models.DateTimeField(null=True, db_column='Start4', blank=True) # Field name made lowercase.
    start5 = models.DateTimeField(null=True, db_column='Start5', blank=True) # Field name made lowercase.
    start6 = models.DateTimeField(null=True, db_column='Start6', blank=True) # Field name made lowercase.
    start7 = models.DateTimeField(null=True, db_column='Start7', blank=True) # Field name made lowercase.
    start8 = models.DateTimeField(null=True, db_column='Start8', blank=True) # Field name made lowercase.
    start9 = models.DateTimeField(null=True, db_column='Start9', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'regnpopl'

class Reshold(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fkalenderunique = models.IntegerField(null=True, db_column='FKalenderUnique', blank=True) # Field name made lowercase.
    fressourceunique = models.IntegerField(null=True, db_column='FRessourceUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'reshold'

class Ressourc(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    finterval = models.IntegerField(null=True, db_column='FInterval', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    medtagbooking = models.IntegerField(null=True, db_column='Medtagbooking', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'ressourc'

class Ressourcetype(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=48, db_column='Navn', blank=True) # Field name made lowercase.
    farve = models.CharField(max_length=10, db_column='Farve', blank=True) # Field name made lowercase.
    lukket = models.CharField(max_length=1, db_column='Lukket', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'ressourcetype'

class Retkladder(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    wpmodule = models.IntegerField(null=True, db_column='WPModule', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'retkladder'

class Retkladlin(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    hunique = models.IntegerField(null=True, db_column='HUnique', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    ffeltnavn = models.CharField(max_length=8, db_column='FFeltNavn', blank=True) # Field name made lowercase.
    freadonly = models.IntegerField(null=True, db_column='FReadOnly', blank=True) # Field name made lowercase.
    fsearch = models.IntegerField(null=True, db_column='FSearch', blank=True) # Field name made lowercase.
    fsearchshow = models.CharField(max_length=48, db_column='FSearchShow', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'retkladlin'

class Rp1(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    felt1 = models.CharField(max_length=255, db_column='Felt1', blank=True) # Field name made lowercase.
    felt2 = models.CharField(max_length=255, db_column='Felt2', blank=True) # Field name made lowercase.
    felt3 = models.CharField(max_length=255, db_column='Felt3', blank=True) # Field name made lowercase.
    felt4 = models.CharField(max_length=255, db_column='Felt4', blank=True) # Field name made lowercase.
    felt5 = models.CharField(max_length=255, db_column='Felt5', blank=True) # Field name made lowercase.
    felt6 = models.CharField(max_length=255, db_column='Felt6', blank=True) # Field name made lowercase.
    felt7 = models.CharField(max_length=255, db_column='Felt7', blank=True) # Field name made lowercase.
    felt8 = models.CharField(max_length=255, db_column='Felt8', blank=True) # Field name made lowercase.
    felt9 = models.CharField(max_length=255, db_column='Felt9', blank=True) # Field name made lowercase.
    felt10 = models.CharField(max_length=255, db_column='Felt10', blank=True) # Field name made lowercase.
    felt11 = models.CharField(max_length=255, db_column='Felt11', blank=True) # Field name made lowercase.
    felt12 = models.CharField(max_length=255, db_column='Felt12', blank=True) # Field name made lowercase.
    felt13 = models.CharField(max_length=255, db_column='Felt13', blank=True) # Field name made lowercase.
    felt14 = models.CharField(max_length=255, db_column='Felt14', blank=True) # Field name made lowercase.
    felt15 = models.CharField(max_length=255, db_column='Felt15', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'rp1'

class Scancode(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    korttype = models.IntegerField(null=True, db_column='KortType', blank=True) # Field name made lowercase.
    sygesikringformatcase = models.CharField(max_length=1, db_column='SygesikringFormatCase', blank=True) # Field name made lowercase.
    maxantal = models.IntegerField(null=True, db_column='MaxAntal', blank=True) # Field name made lowercase.
    frapos = models.IntegerField(null=True, db_column='FraPos', blank=True) # Field name made lowercase.
    tilpos = models.IntegerField(null=True, db_column='TilPos', blank=True) # Field name made lowercase.
    stoptegn = models.CharField(max_length=24, db_column='StopTegn', blank=True) # Field name made lowercase.
    flerelinier = models.CharField(max_length=1, db_column='FlereLinier', blank=True) # Field name made lowercase.
    intsonly = models.CharField(max_length=1, db_column='IntsOnly', blank=True) # Field name made lowercase.
    minint = models.FloatField(null=True, db_column='MinInt', blank=True) # Field name made lowercase.
    maxint = models.FloatField(null=True, db_column='MaxInt', blank=True) # Field name made lowercase.
    validate = models.CharField(max_length=24, db_column='Validate', blank=True) # Field name made lowercase.
    validateatpos = models.IntegerField(null=True, db_column='ValidateAtPos', blank=True) # Field name made lowercase.
    fastformat = models.CharField(max_length=24, db_column='FastFormat', blank=True) # Field name made lowercase.
    faststartkode = models.CharField(max_length=2, db_column='FastStartKode', blank=True) # Field name made lowercase.
    codepage = models.IntegerField(null=True, db_column='CodePage', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'scancode'

class Scnwhere(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    stationname = models.CharField(max_length=32, db_column='StationName', blank=True) # Field name made lowercase.
    menu = models.IntegerField(null=True, db_column='Menu', blank=True) # Field name made lowercase.
    comports = models.IntegerField(null=True, db_column='Comports', blank=True) # Field name made lowercase.
    scancode = models.IntegerField(null=True, db_column='ScanCode', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'scnwhere'

class Sfbehtmp(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    primolager = models.IntegerField(null=True, db_column='Primolager', blank=True) # Field name made lowercase.
    kobsnummer = models.CharField(max_length=18, db_column='Kobsnummer', blank=True) # Field name made lowercase.
    salgsnummer = models.CharField(max_length=18, db_column='Salgsnummer', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=255, db_column='Varetekst', blank=True) # Field name made lowercase.
    lokation = models.CharField(max_length=6, db_column='Lokation', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    behold = models.FloatField(null=True, db_column='Behold', blank=True) # Field name made lowercase.
    tempbehold = models.FloatField(null=True, db_column='TempBehold', blank=True) # Field name made lowercase.
    lagerkp = models.FloatField(null=True, db_column='LagerKP', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    dimskabelon = models.IntegerField(null=True, db_column='DimSkabelon', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfbehtmp'

class Sfcombi(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    combitype = models.IntegerField(null=True, db_column='Combitype', blank=True) # Field name made lowercase.
    webshop = models.CharField(max_length=1, db_column='Webshop', blank=True) # Field name made lowercase.
    antal = models.IntegerField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    rabatbelob = models.FloatField(null=True, db_column='Rabatbelob', blank=True) # Field name made lowercase.
    konto = models.IntegerField(null=True, db_column='Konto', blank=True) # Field name made lowercase.
    gruppe = models.IntegerField(null=True, db_column='Gruppe', blank=True) # Field name made lowercase.
    startden = models.DateTimeField(null=True, db_column='StartDen', blank=True) # Field name made lowercase.
    slutden = models.DateTimeField(null=True, db_column='SlutDen', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfcombi'

class Sfcombilinie(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfcombiunique = models.IntegerField(null=True, db_column='SFCombiUnique', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=20, db_column='VareNr', blank=True) # Field name made lowercase.
    salgsnavn = models.CharField(max_length=64, db_column='SalgsNavn', blank=True) # Field name made lowercase.
    stkpris = models.FloatField(null=True, db_column='StkPris', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfcombilinie'

class Sfegenl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fegenvunique = models.IntegerField(null=True, db_column='FEgenVUnique', blank=True) # Field name made lowercase.
    farbkortunique = models.IntegerField(null=True, db_column='FArbkortUnique', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=72, db_column='VareTekst', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfegenl'

class Sfegenv(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    salgsnavn = models.CharField(max_length=64, db_column='SalgsNavn', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    skflfunique = models.IntegerField(null=True, db_column='SkflFUnique', blank=True) # Field name made lowercase.
    klar = models.CharField(max_length=1, db_column='Klar', blank=True) # Field name made lowercase.
    kreditornr = models.IntegerField(null=True, db_column='KreditorNr', blank=True) # Field name made lowercase.
    kobsdato = models.DateTimeField(null=True, db_column='Kobsdato', blank=True) # Field name made lowercase.
    kostpris = models.FloatField(null=True, db_column='Kostpris', blank=True) # Field name made lowercase.
    kundenr = models.IntegerField(null=True, db_column='KundeNr', blank=True) # Field name made lowercase.
    salgspris = models.FloatField(null=True, db_column='Salgspris', blank=True) # Field name made lowercase.
    solgtdato = models.DateTimeField(null=True, db_column='Solgtdato', blank=True) # Field name made lowercase.
    serienr = models.CharField(max_length=32, db_column='SerieNr', blank=True) # Field name made lowercase.
    bmoms = models.CharField(max_length=1, db_column='BMoms', blank=True) # Field name made lowercase.
    simlock = models.CharField(max_length=1, db_column='SimLock', blank=True) # Field name made lowercase.
    fri1 = models.CharField(max_length=64, db_column='Fri1', blank=True) # Field name made lowercase.
    fri2 = models.CharField(max_length=64, db_column='Fri2', blank=True) # Field name made lowercase.
    fri3 = models.CharField(max_length=64, db_column='Fri3', blank=True) # Field name made lowercase.
    fri4 = models.CharField(max_length=64, db_column='Fri4', blank=True) # Field name made lowercase.
    fri5 = models.CharField(max_length=64, db_column='Fri5', blank=True) # Field name made lowercase.
    fri6 = models.CharField(max_length=64, db_column='Fri6', blank=True) # Field name made lowercase.
    fri7 = models.CharField(max_length=64, db_column='Fri7', blank=True) # Field name made lowercase.
    fri8 = models.CharField(max_length=64, db_column='Fri8', blank=True) # Field name made lowercase.
    fri9 = models.CharField(max_length=64, db_column='Fri9', blank=True) # Field name made lowercase.
    sreg = models.CharField(max_length=1, db_column='SREG', blank=True) # Field name made lowercase.
    sendtsreg = models.DateTimeField(null=True, db_column='SendtSReg', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='LagerSted', blank=True) # Field name made lowercase.
    salgsnote = models.TextField(db_column='SalgsNote', blank=True) # Field name made lowercase.
    billede = models.TextField(db_column='Billede', blank=True) # Field name made lowercase.
    billede1 = models.TextField(db_column='Billede1', blank=True) # Field name made lowercase.
    billede2 = models.TextField(db_column='Billede2', blank=True) # Field name made lowercase.
    billede3 = models.TextField(db_column='Billede3', blank=True) # Field name made lowercase.
    picheight = models.IntegerField(null=True, db_column='PicHeight', blank=True) # Field name made lowercase.
    picwidth = models.IntegerField(null=True, db_column='PicWidth', blank=True) # Field name made lowercase.
    pic1height = models.IntegerField(null=True, db_column='Pic1Height', blank=True) # Field name made lowercase.
    pic1width = models.IntegerField(null=True, db_column='Pic1Width', blank=True) # Field name made lowercase.
    pic2height = models.IntegerField(null=True, db_column='Pic2Height', blank=True) # Field name made lowercase.
    pic2width = models.IntegerField(null=True, db_column='Pic2Width', blank=True) # Field name made lowercase.
    pic3height = models.IntegerField(null=True, db_column='Pic3Height', blank=True) # Field name made lowercase.
    pic3width = models.IntegerField(null=True, db_column='Pic3Width', blank=True) # Field name made lowercase.
    dimskabelon = models.IntegerField(null=True, db_column='DimSkabelon', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfegenv'

class Sffragt(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    konto = models.IntegerField(null=True, db_column='Konto', blank=True) # Field name made lowercase.
    belob = models.FloatField(null=True, db_column='Belob', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sffragt'

class Sffrbstk(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sffrbstk'

class Sffrbstl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sffrbstkfunique = models.IntegerField(null=True, db_column='SFFrbStKFUnique', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sffrbstl'

class Sfh(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    ordrenummer = models.IntegerField(null=True, db_column='OrdreNummer', blank=True) # Field name made lowercase.
    fakturanummer = models.IntegerField(null=True, db_column='FakturaNummer', blank=True) # Field name made lowercase.
    salgerid = models.IntegerField(null=True, db_column='SalgerID', blank=True) # Field name made lowercase.
    sidstrettet = models.DateTimeField(null=True, db_column='SidstRettet', blank=True) # Field name made lowercase.
    rettetaf = models.CharField(max_length=10, db_column='RettetAf', blank=True) # Field name made lowercase.
    pbsleverence = models.IntegerField(null=True, db_column='PBSLeverence', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    vaerksted = models.CharField(max_length=1, db_column='Vaerksted', blank=True) # Field name made lowercase.
    samletordre = models.IntegerField(null=True, db_column='Samletordre', blank=True) # Field name made lowercase.
    ordredato = models.DateTimeField(null=True, db_column='OrdreDato', blank=True) # Field name made lowercase.
    fakturadato = models.DateTimeField(null=True, db_column='FakturaDato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    webshopkortid = models.IntegerField(null=True, db_column='WebshopKortID', blank=True) # Field name made lowercase.
    oprettetwebshop = models.IntegerField(null=True, db_column='OprettetWebshop', blank=True) # Field name made lowercase.
    betaltdato = models.DateTimeField(null=True, db_column='BetaltDato', blank=True) # Field name made lowercase.
    behkonto = models.IntegerField(null=True, db_column='BehKonto', blank=True) # Field name made lowercase.
    altomskonto = models.IntegerField(null=True, db_column='AltOmsKonto', blank=True) # Field name made lowercase.
    kontotype = models.CharField(max_length=1, db_column='KontoType', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    fakturadebitor = models.IntegerField(null=True, db_column='FakturaDebitor', blank=True) # Field name made lowercase.
    projektnr = models.IntegerField(null=True, db_column='ProjektNr', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    permanentunique = models.IntegerField(null=True, db_column='PermanentUnique', blank=True) # Field name made lowercase.
    kategori = models.IntegerField(null=True, db_column='Kategori', blank=True) # Field name made lowercase.
    levadr1 = models.CharField(max_length=32, db_column='LevAdr1', blank=True) # Field name made lowercase.
    levadr2 = models.CharField(max_length=32, db_column='LevAdr2', blank=True) # Field name made lowercase.
    levadr3 = models.CharField(max_length=32, db_column='LevAdr3', blank=True) # Field name made lowercase.
    levadr4 = models.CharField(max_length=32, db_column='LevAdr4', blank=True) # Field name made lowercase.
    leveringskonto = models.IntegerField(null=True, db_column='LeveringsKonto', blank=True) # Field name made lowercase.
    levadr5 = models.CharField(max_length=32, db_column='LevAdr5', blank=True) # Field name made lowercase.
    levadr6 = models.CharField(max_length=32, db_column='LevAdr6', blank=True) # Field name made lowercase.
    levadr7 = models.CharField(max_length=32, db_column='LevAdr7', blank=True) # Field name made lowercase.
    levadr8 = models.CharField(max_length=32, db_column='LevAdr8', blank=True) # Field name made lowercase.
    fragtbrev = models.CharField(max_length=10, db_column='FragtBrev', blank=True) # Field name made lowercase.
    fragtcentral = models.CharField(max_length=20, db_column='FragtCentral', blank=True) # Field name made lowercase.
    note1 = models.CharField(max_length=32, db_column='Note1', blank=True) # Field name made lowercase.
    note2 = models.CharField(max_length=32, db_column='Note2', blank=True) # Field name made lowercase.
    betalbet = models.IntegerField(null=True, db_column='BetalBet', blank=True) # Field name made lowercase.
    vorref = models.CharField(max_length=40, db_column='VorRef', blank=True) # Field name made lowercase.
    deresref = models.CharField(max_length=40, db_column='DeresRef', blank=True) # Field name made lowercase.
    deresref2 = models.CharField(max_length=40, db_column='DeresRef2', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    klar = models.CharField(max_length=1, db_column='Klar', blank=True) # Field name made lowercase.
    produktionsstatus = models.IntegerField(null=True, db_column='ProduktionsStatus', blank=True) # Field name made lowercase.
    title = models.CharField(max_length=16, db_column='Title', blank=True) # Field name made lowercase.
    fakturatype = models.IntegerField(null=True, db_column='FakturaType', blank=True) # Field name made lowercase.
    udskrevet = models.CharField(max_length=1, db_column='Udskrevet', blank=True) # Field name made lowercase.
    sendt = models.CharField(max_length=1, db_column='Sendt', blank=True) # Field name made lowercase.
    bogfort = models.CharField(max_length=1, db_column='Bogfort', blank=True) # Field name made lowercase.
    exmoms = models.FloatField(null=True, db_column='ExMoms', blank=True) # Field name made lowercase.
    moms = models.FloatField(null=True, db_column='Moms', blank=True) # Field name made lowercase.
    inmoms = models.FloatField(null=True, db_column='InMoms', blank=True) # Field name made lowercase.
    momsfri = models.FloatField(null=True, db_column='MomsFri', blank=True) # Field name made lowercase.
    sumextilskud = models.FloatField(null=True, db_column='SumExTilskud', blank=True) # Field name made lowercase.
    sumfragt = models.FloatField(null=True, db_column='SumFragt', blank=True) # Field name made lowercase.
    tilskudinmoms = models.FloatField(null=True, db_column='TilskudInMoms', blank=True) # Field name made lowercase.
    tillagsum = models.FloatField(null=True, db_column='TillagSum', blank=True) # Field name made lowercase.
    kpsum = models.FloatField(null=True, db_column='KPSum', blank=True) # Field name made lowercase.
    punktafgift = models.FloatField(null=True, db_column='PunktAfgift', blank=True) # Field name made lowercase.
    db = models.FloatField(null=True, db_column='DB', blank=True) # Field name made lowercase.
    dg = models.FloatField(null=True, db_column='DG', blank=True) # Field name made lowercase.
    totalvaegt = models.FloatField(null=True, db_column='TotalVaegt', blank=True) # Field name made lowercase.
    paabegyndes = models.DateTimeField(null=True, db_column='Paabegyndes', blank=True) # Field name made lowercase.
    perlevrunique = models.IntegerField(null=True, db_column='PerLevrUnique', blank=True) # Field name made lowercase.
    perfakunique = models.IntegerField(null=True, db_column='PerFakUnique', blank=True) # Field name made lowercase.
    pergentag = models.IntegerField(null=True, db_column='PerGentag', blank=True) # Field name made lowercase.
    perudlob = models.DateTimeField(null=True, db_column='PerUdlob', blank=True) # Field name made lowercase.
    perpause = models.IntegerField(null=True, db_column='PerPause', blank=True) # Field name made lowercase.
    prodtid = models.DateTimeField(null=True, db_column='ProdTid', blank=True) # Field name made lowercase.
    prodinfo = models.CharField(max_length=22, db_column='ProdInfo', blank=True) # Field name made lowercase.
    leveringstid = models.DateTimeField(null=True, db_column='Leveringstid', blank=True) # Field name made lowercase.
    leveringsinfo = models.CharField(max_length=22, db_column='LeveringsInfo', blank=True) # Field name made lowercase.
    produktionsinfo = models.TextField(db_column='ProduktionsInfo', blank=True) # Field name made lowercase.
    prodnr = models.IntegerField(null=True, db_column='ProdNr', blank=True) # Field name made lowercase.
    note = models.TextField(db_column='Note', blank=True) # Field name made lowercase.
    ean = models.CharField(max_length=20, db_column='EAN', blank=True) # Field name made lowercase.
    webshopstatus = models.IntegerField(null=True, db_column='Webshopstatus', blank=True) # Field name made lowercase.
    webtracktrace = models.CharField(max_length=50, db_column='WebTrackTrace', blank=True) # Field name made lowercase.
    webdibstrans = models.CharField(max_length=50, db_column='WebDibsTrans', blank=True) # Field name made lowercase.
    colorstatus = models.IntegerField(null=True, db_column='Colorstatus', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfh'

class Sfhgruppe(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfhgruppe'

class Sfhlink(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfhunique = models.IntegerField(null=True, db_column='SFHUnique', blank=True) # Field name made lowercase.
    andenbetalerflag = models.IntegerField(null=True, db_column='AndenBetalerFlag', blank=True) # Field name made lowercase.
    andenbetaler = models.TextField(db_column='AndenBetaler', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfhlink'

class Sfhovedkategorier(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=48, db_column='Navn', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfhovedkategorier'

class Sfk(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fakturanr = models.CharField(max_length=15, db_column='FakturaNr', blank=True) # Field name made lowercase.
    ordrenr = models.IntegerField(null=True, db_column='OrdreNr', blank=True) # Field name made lowercase.
    bilagnr = models.IntegerField(null=True, db_column='BilagNr', blank=True) # Field name made lowercase.
    kreditor = models.IntegerField(null=True, db_column='Kreditor', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    klar = models.IntegerField(null=True, db_column='Klar', blank=True) # Field name made lowercase.
    deresref = models.CharField(max_length=24, db_column='DeresRef', blank=True) # Field name made lowercase.
    vorref = models.CharField(max_length=24, db_column='VorRef', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    fakturadato = models.DateTimeField(null=True, db_column='FakturaDato', blank=True) # Field name made lowercase.
    forfaldsdato = models.DateTimeField(null=True, db_column='ForfaldsDato', blank=True) # Field name made lowercase.
    leveresden = models.DateTimeField(null=True, db_column='LeveresDen', blank=True) # Field name made lowercase.
    inmoms = models.FloatField(null=True, db_column='InMoms', blank=True) # Field name made lowercase.
    total = models.FloatField(null=True, db_column='Total', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    oprettetden = models.DateTimeField(null=True, db_column='OprettetDen', blank=True) # Field name made lowercase.
    oprettetaf = models.IntegerField(null=True, db_column='OprettetAf', blank=True) # Field name made lowercase.
    rekvisitionden = models.DateTimeField(null=True, db_column='RekvisitionDen', blank=True) # Field name made lowercase.
    metode = models.IntegerField(null=True, db_column='Metode', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    edok = models.IntegerField(null=True, db_column='EDok', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfk'

class Sfkl(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfkfunique = models.IntegerField(null=True, db_column='SfkFUnique', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    sfvdimgruppe = models.IntegerField(null=True, db_column='SFVDimGruppe', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=20, db_column='Varenr', blank=True) # Field name made lowercase.
    betegnelse = models.CharField(max_length=64, db_column='Betegnelse', blank=True) # Field name made lowercase.
    enhed = models.CharField(max_length=8, db_column='Enhed', blank=True) # Field name made lowercase.
    kobskonto = models.IntegerField(null=True, db_column='Kobskonto', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    momsfri = models.IntegerField(null=True, db_column='Momsfri', blank=True) # Field name made lowercase.
    stkpris = models.FloatField(null=True, db_column='StkPris', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    modtagetantal = models.FloatField(null=True, db_column='ModtagetAntal', blank=True) # Field name made lowercase.
    modtagetsum = models.FloatField(null=True, db_column='ModtagetSum', blank=True) # Field name made lowercase.
    restantal = models.FloatField(null=True, db_column='RestAntal', blank=True) # Field name made lowercase.
    leveresden = models.DateTimeField(null=True, db_column='LeveresDen', blank=True) # Field name made lowercase.
    modtagetden = models.DateTimeField(null=True, db_column='ModtagetDen', blank=True) # Field name made lowercase.
    momssum = models.FloatField(null=True, db_column='MomsSum', blank=True) # Field name made lowercase.
    liniesum = models.FloatField(null=True, db_column='LinieSum', blank=True) # Field name made lowercase.
    liniesuminclmoms = models.FloatField(null=True, db_column='LinieSumInclMoms', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    sfklunique = models.IntegerField(null=True, db_column='SFKLUnique', blank=True) # Field name made lowercase.
    modtageunique = models.IntegerField(null=True, db_column='ModtageUnique', blank=True) # Field name made lowercase.
    fakturaunique = models.IntegerField(null=True, db_column='FakturaUnique', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfkl'

class Sfkobd(models.Model):
    bogfoer = models.CharField(max_length=1, db_column='Bogfoer', blank=True) # Field name made lowercase.
    stkpris = models.CharField(max_length=1, db_column='StkPris', blank=True) # Field name made lowercase.
    udgiftskonto = models.CharField(max_length=1, db_column='UdgiftsKonto', blank=True) # Field name made lowercase.
    kreditorkonto = models.CharField(max_length=1, db_column='KreditorKonto', blank=True) # Field name made lowercase.
    opretejserienr = models.CharField(max_length=1, db_column='OpretEjSerieNr', blank=True) # Field name made lowercase.
    projektnr = models.IntegerField(null=True, db_column='ProjektNr', blank=True) # Field name made lowercase.
    artnr = models.IntegerField(null=True, db_column='ArtNr', blank=True) # Field name made lowercase.
    bilagnr = models.IntegerField(null=True, db_column='BilagNr', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfkobd'

class Sfl(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfhunique = models.IntegerField(null=True, db_column='SFHUnique', blank=True) # Field name made lowercase.
    sfhperfakunique = models.IntegerField(null=True, db_column='SFHPerFakUnique', blank=True) # Field name made lowercase.
    importkode = models.IntegerField(null=True, db_column='ImportKode', blank=True) # Field name made lowercase.
    linietype = models.IntegerField(null=True, db_column='LinieType', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    sfvdimgruppe = models.IntegerField(null=True, db_column='SFVDimGruppe', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=72, db_column='VareTekst', blank=True) # Field name made lowercase.
    salgsenhed = models.CharField(max_length=12, db_column='SalgsEnhed', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    salgspris = models.FloatField(null=True, db_column='SalgsPris', blank=True) # Field name made lowercase.
    salgsprisexmoms = models.FloatField(null=True, db_column='SalgsPrisExMoms', blank=True) # Field name made lowercase.
    salgseimoms = models.IntegerField(null=True, db_column='SalgsEIMoms', blank=True) # Field name made lowercase.
    tilskud = models.FloatField(null=True, db_column='Tilskud', blank=True) # Field name made lowercase.
    tilskudtype = models.CharField(max_length=1, db_column='TilskudType', blank=True) # Field name made lowercase.
    tilskudkonto = models.IntegerField(null=True, db_column='TilskudKonto', blank=True) # Field name made lowercase.
    fragtkode = models.IntegerField(null=True, db_column='FragtKode', blank=True) # Field name made lowercase.
    kostpris = models.FloatField(null=True, db_column='KostPris', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='LagerKonto', blank=True) # Field name made lowercase.
    vareforbrug = models.IntegerField(null=True, db_column='Vareforbrug', blank=True) # Field name made lowercase.
    punktafgift = models.FloatField(null=True, db_column='PunktAfgift', blank=True) # Field name made lowercase.
    avancedel = models.FloatField(null=True, db_column='AvanceDel', blank=True) # Field name made lowercase.
    kosteimoms = models.IntegerField(null=True, db_column='KostEIMoms', blank=True) # Field name made lowercase.
    rabatkr = models.CharField(max_length=1, db_column='RabatKr', blank=True) # Field name made lowercase.
    rabat = models.FloatField(null=True, db_column='Rabat', blank=True) # Field name made lowercase.
    provision = models.FloatField(null=True, db_column='Provision', blank=True) # Field name made lowercase.
    momssum = models.FloatField(null=True, db_column='MomsSum', blank=True) # Field name made lowercase.
    liniesum = models.FloatField(null=True, db_column='LinieSum', blank=True) # Field name made lowercase.
    liniesumexmoms = models.FloatField(null=True, db_column='LinieSumExMoms', blank=True) # Field name made lowercase.
    tilskudkrsum = models.FloatField(null=True, db_column='TilskudKrSum', blank=True) # Field name made lowercase.
    liniesumextilskud = models.FloatField(null=True, db_column='LinieSumExTilskud', blank=True) # Field name made lowercase.
    tillagpct = models.FloatField(null=True, db_column='TillagPct', blank=True) # Field name made lowercase.
    tillagsum = models.FloatField(null=True, db_column='TillagSum', blank=True) # Field name made lowercase.
    punktafgiftsum = models.FloatField(null=True, db_column='PunktAfgiftSum', blank=True) # Field name made lowercase.
    levrdato = models.DateTimeField(null=True, db_column='LevrDato', blank=True) # Field name made lowercase.
    fakdato = models.DateTimeField(null=True, db_column='FakDato', blank=True) # Field name made lowercase.
    sflunique = models.IntegerField(null=True, db_column='SFLUnique', blank=True) # Field name made lowercase.
    arbkortunique = models.IntegerField(null=True, db_column='ArbkortUnique', blank=True) # Field name made lowercase.
    tilskudunique = models.IntegerField(null=True, db_column='TilskudUnique', blank=True) # Field name made lowercase.
    stykserienr = models.IntegerField(null=True, db_column='StykSerieNr', blank=True) # Field name made lowercase.
    serienrretur = models.CharField(max_length=32, db_column='SerieNrRetur', blank=True) # Field name made lowercase.
    stykvarenr = models.IntegerField(null=True, db_column='StykVareNr', blank=True) # Field name made lowercase.
    kontraktnr = models.IntegerField(null=True, db_column='KontraktNr', blank=True) # Field name made lowercase.
    udlob = models.DateTimeField(null=True, db_column='Udlob', blank=True) # Field name made lowercase.
    sogekode = models.CharField(max_length=10, db_column='SogeKode', blank=True) # Field name made lowercase.
    filnavn = models.CharField(max_length=12, db_column='FilNavn', blank=True) # Field name made lowercase.
    vareprint = models.CharField(max_length=12, db_column='VarePrint', blank=True) # Field name made lowercase.
    printantal = models.CharField(max_length=1, db_column='PrintAntal', blank=True) # Field name made lowercase.
    vaegt = models.FloatField(null=True, db_column='Vaegt', blank=True) # Field name made lowercase.
    vaegttype = models.IntegerField(null=True, db_column='VaegtType', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    statistikkode = models.IntegerField(null=True, db_column='StatistikKode', blank=True) # Field name made lowercase.
    liniekode = models.IntegerField(null=True, db_column='LinieKode', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfl'

class Sflagbe(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sflagstdfunique = models.IntegerField(null=True, db_column='SFLagStdFUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=72, db_column='VareTekst', blank=True) # Field name made lowercase.
    genbestmetode = models.IntegerField(null=True, db_column='GenBestMetode', blank=True) # Field name made lowercase.
    minbeholdning = models.FloatField(null=True, db_column='MinBeholdning', blank=True) # Field name made lowercase.
    maxbeholdning = models.FloatField(null=True, db_column='MaxBeholdning', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sflagbe'

class Sflagstd(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='Lagerkonto', blank=True) # Field name made lowercase.
    vareforbrug = models.IntegerField(null=True, db_column='Vareforbrug', blank=True) # Field name made lowercase.
    svindkonto = models.IntegerField(null=True, db_column='SvindKonto', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    genbestmetode = models.IntegerField(null=True, db_column='GenBestMetode', blank=True) # Field name made lowercase.
    minbeholdning = models.FloatField(null=True, db_column='MinBeholdning', blank=True) # Field name made lowercase.
    maxbeholdning = models.FloatField(null=True, db_column='MaxBeholdning', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sflagstd'

class Sflevlag(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    kreditor = models.IntegerField(null=True, db_column='Kreditor', blank=True) # Field name made lowercase.
    tilskud1 = models.FloatField(null=True, db_column='Tilskud1', blank=True) # Field name made lowercase.
    tilskud2 = models.FloatField(null=True, db_column='Tilskud2', blank=True) # Field name made lowercase.
    tilskud3 = models.FloatField(null=True, db_column='Tilskud3', blank=True) # Field name made lowercase.
    tilskud4 = models.FloatField(null=True, db_column='Tilskud4', blank=True) # Field name made lowercase.
    tilskud5 = models.FloatField(null=True, db_column='Tilskud5', blank=True) # Field name made lowercase.
    tilskud6 = models.FloatField(null=True, db_column='Tilskud6', blank=True) # Field name made lowercase.
    konto1 = models.IntegerField(null=True, db_column='Konto1', blank=True) # Field name made lowercase.
    konto2 = models.IntegerField(null=True, db_column='Konto2', blank=True) # Field name made lowercase.
    konto3 = models.IntegerField(null=True, db_column='Konto3', blank=True) # Field name made lowercase.
    konto4 = models.IntegerField(null=True, db_column='Konto4', blank=True) # Field name made lowercase.
    konto5 = models.IntegerField(null=True, db_column='Konto5', blank=True) # Field name made lowercase.
    konto6 = models.IntegerField(null=True, db_column='Konto6', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sflevlag'

class Sfpl(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfhunique = models.IntegerField(null=True, db_column='SFHUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    lagerkostpris = models.FloatField(null=True, db_column='LagerKostPris', blank=True) # Field name made lowercase.
    listekostpris = models.FloatField(null=True, db_column='ListeKostPris', blank=True) # Field name made lowercase.
    opgaveart = models.IntegerField(null=True, db_column='OpgaveArt', blank=True) # Field name made lowercase.
    produktionstype = models.IntegerField(null=True, db_column='ProduktionsType', blank=True) # Field name made lowercase.
    leveringstid = models.DateTimeField(null=True, db_column='LeveringsTid', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    tekst = models.CharField(max_length=24, db_column='Tekst', blank=True) # Field name made lowercase.
    delta = models.IntegerField(null=True, db_column='Delta', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfpl'

class Sfpmfaktider(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    fbagud = models.IntegerField(null=True, db_column='FBagud', blank=True) # Field name made lowercase.
    ffakturaantalmdr = models.IntegerField(null=True, db_column='FFakturaAntalMdr', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfpmfaktider'

class Sfpmlevrtider(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    fgentaghver = models.IntegerField(null=True, db_column='FGentagHver', blank=True) # Field name made lowercase.
    fgentagtype = models.IntegerField(null=True, db_column='FGentagType', blank=True) # Field name made lowercase.
    fantal = models.IntegerField(null=True, db_column='FAntal', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfpmlevrtider'

class Sfpopgl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fsfhunique = models.IntegerField(null=True, db_column='FSFHUnique', blank=True) # Field name made lowercase.
    fopgunique = models.IntegerField(null=True, db_column='FOPGUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfpopgl'

class Sfpopgst(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    genvej = models.CharField(max_length=2, db_column='Genvej', blank=True) # Field name made lowercase.
    manual = models.CharField(max_length=1, db_column='Manual', blank=True) # Field name made lowercase.
    varetype = models.IntegerField(null=True, db_column='VareType', blank=True) # Field name made lowercase.
    raekke = models.IntegerField(null=True, db_column='Raekke', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfpopgst'

class Sfprist(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    varegrp = models.IntegerField(null=True, db_column='VareGrp', blank=True) # Field name made lowercase.
    sats = models.FloatField(null=True, db_column='Sats', blank=True) # Field name made lowercase.
    omskonto = models.FloatField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfprist'

class Sfptimerh(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfptimerh'

class Sfptimerl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfhunique = models.IntegerField(null=True, db_column='SFHUnique', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=16, db_column='Navn', blank=True) # Field name made lowercase.
    normal = models.IntegerField(null=True, db_column='Normal', blank=True) # Field name made lowercase.
    normalt = models.IntegerField(null=True, db_column='NormalT', blank=True) # Field name made lowercase.
    normallkode = models.IntegerField(null=True, db_column='NormalLKode', blank=True) # Field name made lowercase.
    over = models.IntegerField(null=True, db_column='Over', blank=True) # Field name made lowercase.
    overt = models.IntegerField(null=True, db_column='OverT', blank=True) # Field name made lowercase.
    overlkode = models.IntegerField(null=True, db_column='OverLKode', blank=True) # Field name made lowercase.
    over2lkode = models.IntegerField(null=True, db_column='Over2LKode', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfptimerl'

class Sfsenh(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=18, db_column='Kode', blank=True) # Field name made lowercase.
    enhed = models.CharField(max_length=12, db_column='Enhed', blank=True) # Field name made lowercase.
    tekst = models.CharField(max_length=48, db_column='Tekst', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    kobspris = models.FloatField(null=True, db_column='KobsPris', blank=True) # Field name made lowercase.
    kobseimoms = models.IntegerField(null=True, db_column='KobsEIMoms', blank=True) # Field name made lowercase.
    salgspris = models.FloatField(null=True, db_column='SalgsPris', blank=True) # Field name made lowercase.
    salgseimoms = models.IntegerField(null=True, db_column='SalgsEIMoms', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfsenh'

class Sfstklin(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfstklstfunique = models.IntegerField(null=True, db_column='SFStkLstFUnique', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    stkpris = models.FloatField(null=True, db_column='StkPris', blank=True) # Field name made lowercase.
    paaordre = models.CharField(max_length=1, db_column='PaaOrdre', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfstklin'

class Sftillag(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    konto = models.IntegerField(null=True, db_column='Konto', blank=True) # Field name made lowercase.
    pctkr = models.IntegerField(null=True, db_column='PctKr', blank=True) # Field name made lowercase.
    pct = models.FloatField(null=True, db_column='Pct', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sftillag'

class Sfvarekategorier(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=48, db_column='Navn', blank=True) # Field name made lowercase.
    hovedkategorier = models.CharField(max_length=48, db_column='HovedKategorier', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfvarekategorier'

class Sfvareproducent(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=48, db_column='Navn', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    billede = models.TextField(db_column='Billede', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfvareproducent'

class Sfvargrp(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=48, db_column='Navn', blank=True) # Field name made lowercase.
    kategorier = models.CharField(max_length=48, db_column='Kategorier', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    vareforbrugkonto = models.IntegerField(null=True, db_column='VareforbrugKonto', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='LagerKonto', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfvargrp'

class Sfvdimension(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    dimension = models.IntegerField(null=True, db_column='Dimension', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfvdimension'

class Sfvdimensionsetup(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    tvungenlager = models.IntegerField(null=True, db_column='TvungenLager', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfvdimensionsetup'

class Sfvdimskabelon(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfvdimskabelon'

class Sfvdimvalues(models.Model):
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfvdimvalues'

class Sfvkart(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    salgsnummer = models.CharField(max_length=20, db_column='SalgsNummer', blank=True) # Field name made lowercase.
    erstatningsnr = models.CharField(max_length=18, db_column='ErstatningsNr', blank=True) # Field name made lowercase.
    sogekode = models.CharField(max_length=18, db_column='SogeKode', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=4, db_column='Kode', blank=True) # Field name made lowercase.
    varetype = models.IntegerField(null=True, db_column='VareType', blank=True) # Field name made lowercase.
    fragtkode = models.IntegerField(null=True, db_column='FragtKode', blank=True) # Field name made lowercase.
    lagertilvareforbrug = models.CharField(max_length=1, db_column='LagerTilVareforbrug', blank=True) # Field name made lowercase.
    flyttevareforbrug = models.CharField(max_length=1, db_column='FlytteVareforbrug', blank=True) # Field name made lowercase.
    produktionstype = models.IntegerField(null=True, db_column='ProduktionsType', blank=True) # Field name made lowercase.
    serievare = models.CharField(max_length=1, db_column='SerieVare', blank=True) # Field name made lowercase.
    salgsnavn = models.CharField(max_length=64, db_column='SalgsNavn', blank=True) # Field name made lowercase.
    salgsnote = models.TextField(db_column='SalgsNote', blank=True) # Field name made lowercase.
    salgsenhed = models.CharField(max_length=12, db_column='SalgsEnhed', blank=True) # Field name made lowercase.
    maxantal = models.FloatField(null=True, db_column='MaxAntal', blank=True) # Field name made lowercase.
    salgspris = models.FloatField(null=True, db_column='SalgsPris', blank=True) # Field name made lowercase.
    foerpris = models.FloatField(null=True, db_column='FoerPris', blank=True) # Field name made lowercase.
    webpris = models.FloatField(null=True, db_column='WEBPris', blank=True) # Field name made lowercase.
    rabatpris = models.FloatField(null=True, db_column='RabatPris', blank=True) # Field name made lowercase.
    avancedel = models.FloatField(null=True, db_column='AvanceDel', blank=True) # Field name made lowercase.
    salgsvaluta = models.IntegerField(null=True, db_column='SalgsValuta', blank=True) # Field name made lowercase.
    salgseimoms = models.IntegerField(null=True, db_column='SalgsEIMoms', blank=True) # Field name made lowercase.
    modtagsombrugt = models.CharField(max_length=1, db_column='ModtagSomBrugt', blank=True) # Field name made lowercase.
    eksternkontrakt = models.CharField(max_length=1, db_column='EksternKontrakt', blank=True) # Field name made lowercase.
    salgskonto = models.IntegerField(null=True, db_column='SalgsKonto', blank=True) # Field name made lowercase.
    tilskud = models.FloatField(null=True, db_column='Tilskud', blank=True) # Field name made lowercase.
    tilskudtype = models.CharField(max_length=1, db_column='TilskudType', blank=True) # Field name made lowercase.
    tilskudkonto = models.IntegerField(null=True, db_column='TilskudKonto', blank=True) # Field name made lowercase.
    kreditornr = models.IntegerField(null=True, db_column='KreditorNr', blank=True) # Field name made lowercase.
    producentnr = models.IntegerField(null=True, db_column='ProducentNr', blank=True) # Field name made lowercase.
    kobsnummer = models.CharField(max_length=18, db_column='KobsNummer', blank=True) # Field name made lowercase.
    kobsnavn = models.CharField(max_length=64, db_column='KobsNavn', blank=True) # Field name made lowercase.
    kobsenhed = models.CharField(max_length=12, db_column='KobsEnhed', blank=True) # Field name made lowercase.
    kobspris = models.FloatField(null=True, db_column='KobsPris', blank=True) # Field name made lowercase.
    kobsvaluta = models.IntegerField(null=True, db_column='KobsValuta', blank=True) # Field name made lowercase.
    kobseimoms = models.IntegerField(null=True, db_column='KobsEIMoms', blank=True) # Field name made lowercase.
    punktafgift = models.FloatField(null=True, db_column='PunktAfgift', blank=True) # Field name made lowercase.
    kobskonto = models.IntegerField(null=True, db_column='KobsKonto', blank=True) # Field name made lowercase.
    vareforbrugkonto = models.IntegerField(null=True, db_column='VareForbrugKonto', blank=True) # Field name made lowercase.
    enhedvisesi = models.IntegerField(null=True, db_column='EnhedVisesi', blank=True) # Field name made lowercase.
    defsalgenh = models.IntegerField(null=True, db_column='DefSalgEnh', blank=True) # Field name made lowercase.
    defkobenh = models.IntegerField(null=True, db_column='DefKobEnh', blank=True) # Field name made lowercase.
    gruppe = models.IntegerField(null=True, db_column='Gruppe', blank=True) # Field name made lowercase.
    tillag1 = models.IntegerField(null=True, db_column='Tillag1', blank=True) # Field name made lowercase.
    tillag2 = models.IntegerField(null=True, db_column='Tillag2', blank=True) # Field name made lowercase.
    tillag3 = models.IntegerField(null=True, db_column='Tillag3', blank=True) # Field name made lowercase.
    levtilskud = models.IntegerField(null=True, db_column='LevTilSkud', blank=True) # Field name made lowercase.
    sfvdimgruppe = models.IntegerField(null=True, db_column='SFVDimGruppe', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    forbrugstat = models.CharField(max_length=1, db_column='ForbrugStat', blank=True) # Field name made lowercase.
    pulje = models.CharField(max_length=1, db_column='Pulje', blank=True) # Field name made lowercase.
    provisiongruppe = models.IntegerField(null=True, db_column='ProvisionGruppe', blank=True) # Field name made lowercase.
    genbestmetode = models.IntegerField(null=True, db_column='GenBestMetode', blank=True) # Field name made lowercase.
    minbeholdning = models.FloatField(null=True, db_column='MinBeholdning', blank=True) # Field name made lowercase.
    maxbeholdning = models.FloatField(null=True, db_column='MaxBeholdning', blank=True) # Field name made lowercase.
    lokation = models.CharField(max_length=6, db_column='Lokation', blank=True) # Field name made lowercase.
    spaeretden = models.DateTimeField(null=True, db_column='SpaeretDen', blank=True) # Field name made lowercase.
    spaeretsalg = models.DateTimeField(null=True, db_column='SpaeretSalg', blank=True) # Field name made lowercase.
    udgaetden = models.DateTimeField(null=True, db_column='UdgaetDen', blank=True) # Field name made lowercase.
    andretden = models.DateTimeField(null=True, db_column='AndretDen', blank=True) # Field name made lowercase.
    skalikkeoptalles = models.CharField(max_length=1, db_column='SkalIkkeOptalles', blank=True) # Field name made lowercase.
    optaltden = models.DateTimeField(null=True, db_column='OptaltDen', blank=True) # Field name made lowercase.
    optalttil = models.FloatField(null=True, db_column='OptaltTil', blank=True) # Field name made lowercase.
    notepaabon = models.CharField(max_length=1, db_column='NotePaaBon', blank=True) # Field name made lowercase.
    filnavn = models.CharField(max_length=12, db_column='FilNavn', blank=True) # Field name made lowercase.
    vareprint = models.CharField(max_length=12, db_column='VarePrint', blank=True) # Field name made lowercase.
    printantal = models.CharField(max_length=1, db_column='PrintAntal', blank=True) # Field name made lowercase.
    vaegt = models.FloatField(null=True, db_column='Vaegt', blank=True) # Field name made lowercase.
    vaegttype = models.IntegerField(null=True, db_column='VaegtType', blank=True) # Field name made lowercase.
    ejpaalager = models.CharField(max_length=1, db_column='EjPaaLager', blank=True) # Field name made lowercase.
    opgaveart = models.IntegerField(null=True, db_column='OpgaveArt', blank=True) # Field name made lowercase.
    notepaaproduktionsinfo = models.CharField(max_length=1, db_column='NotePaaProduktionsInfo', blank=True) # Field name made lowercase.
    sregliste = models.CharField(max_length=1, db_column='SregListe', blank=True) # Field name made lowercase.
    medtagwebshop = models.IntegerField(null=True, db_column='MedtagWebShop', blank=True) # Field name made lowercase.
    webshoptilbud = models.IntegerField(null=True, db_column='WebShopTilbud', blank=True) # Field name made lowercase.
    webshopanmeld = models.IntegerField(null=True, db_column='WebshopAnmeld', blank=True) # Field name made lowercase.
    webshopvisrelatvarer = models.IntegerField(null=True, db_column='WebshopVisRelatVarer', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    exposedcount = models.IntegerField(null=True, db_column='ExposedCount', blank=True) # Field name made lowercase.
    webrelatede = models.TextField(db_column='WebRelatede', blank=True) # Field name made lowercase.
    webtekstkort = models.TextField(db_column='WebTekstKort', blank=True) # Field name made lowercase.
    webtekstlang = models.TextField(db_column='WebTekstLang', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfvkart'

class Sfvkartmore(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfvkartfunique = models.IntegerField(null=True, db_column='SFVKartFUnique', blank=True) # Field name made lowercase.
    fri1 = models.CharField(max_length=64, db_column='Fri1', blank=True) # Field name made lowercase.
    fri2 = models.CharField(max_length=64, db_column='Fri2', blank=True) # Field name made lowercase.
    fri3 = models.CharField(max_length=64, db_column='Fri3', blank=True) # Field name made lowercase.
    fri4 = models.CharField(max_length=64, db_column='Fri4', blank=True) # Field name made lowercase.
    fri5 = models.CharField(max_length=64, db_column='Fri5', blank=True) # Field name made lowercase.
    fri6 = models.CharField(max_length=64, db_column='Fri6', blank=True) # Field name made lowercase.
    fri7 = models.CharField(max_length=64, db_column='Fri7', blank=True) # Field name made lowercase.
    fri8 = models.CharField(max_length=64, db_column='Fri8', blank=True) # Field name made lowercase.
    fri9 = models.CharField(max_length=64, db_column='Fri9', blank=True) # Field name made lowercase.
    metatagskeywords = models.CharField(max_length=128, db_column='MetaTagsKeyWords', blank=True) # Field name made lowercase.
    metatagsdescription = models.TextField(db_column='MetaTagsDescription', blank=True) # Field name made lowercase.
    webpris = models.CharField(max_length=50, db_column='WebPris', blank=True) # Field name made lowercase.
    billede = models.TextField(db_column='Billede', blank=True) # Field name made lowercase.
    billede1 = models.TextField(db_column='Billede1', blank=True) # Field name made lowercase.
    billede2 = models.TextField(db_column='Billede2', blank=True) # Field name made lowercase.
    billede3 = models.TextField(db_column='Billede3', blank=True) # Field name made lowercase.
    picheight = models.IntegerField(null=True, db_column='PicHeight', blank=True) # Field name made lowercase.
    picwidth = models.IntegerField(null=True, db_column='PicWidth', blank=True) # Field name made lowercase.
    pic1height = models.IntegerField(null=True, db_column='Pic1Height', blank=True) # Field name made lowercase.
    pic1width = models.IntegerField(null=True, db_column='Pic1Width', blank=True) # Field name made lowercase.
    pic2height = models.IntegerField(null=True, db_column='Pic2Height', blank=True) # Field name made lowercase.
    pic2width = models.IntegerField(null=True, db_column='Pic2Width', blank=True) # Field name made lowercase.
    pic3height = models.IntegerField(null=True, db_column='Pic3Height', blank=True) # Field name made lowercase.
    pic3width = models.IntegerField(null=True, db_column='Pic3Width', blank=True) # Field name made lowercase.
    kanslettes = models.IntegerField(null=True, db_column='KanSlettes', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sfvkartmore'

class Sqlvisdef(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=50, db_column='Navn', blank=True) # Field name made lowercase.
    oprettet = models.DateTimeField(null=True, db_column='Oprettet', blank=True) # Field name made lowercase.
    oprettetaf = models.CharField(max_length=32, db_column='OprettetAf', blank=True) # Field name made lowercase.
    vistype = models.IntegerField(null=True, db_column='VisType', blank=True) # Field name made lowercase.
    design = models.CharField(max_length=32, db_column='Design', blank=True) # Field name made lowercase.
    super = models.IntegerField(null=True, db_column='Super', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    fsql = models.TextField(db_column='FSql', blank=True) # Field name made lowercase.
    fsqldetail = models.TextField(db_column='FSqlDetail', blank=True) # Field name made lowercase.
    ignorelf = models.IntegerField(null=True, db_column='IgnoreLF', blank=True) # Field name made lowercase.
    fformat = models.TextField(db_column='FFormat', blank=True) # Field name made lowercase.
    lastupdate = models.DateTimeField(null=True, db_column='LastUpdate', blank=True) # Field name made lowercase.
    mustupdate = models.CharField(max_length=1, db_column='MustUpdate', blank=True) # Field name made lowercase.
    updateonly = models.IntegerField(null=True, db_column='UpdateOnly', blank=True) # Field name made lowercase.
    proreg = models.IntegerField(null=True, db_column='ProReg', blank=True) # Field name made lowercase.
    kategori = models.IntegerField(null=True, db_column='Kategori', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sqlvisdef'

class Sysinfo(models.Model):
    kode = models.CharField(unique=True, max_length=32, db_column='Kode', blank=True) # Field name made lowercase.
    recsize = models.IntegerField(null=True, db_column='RecSize', blank=True) # Field name made lowercase.
    lastcode = models.IntegerField(null=True, db_column='LastCode', blank=True) # Field name made lowercase.
    info = models.TextField(db_column='Info', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'sysinfo'

class Tbc2000(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    artikelnr = models.IntegerField(null=True, db_column='ArtikelNr', blank=True) # Field name made lowercase.
    antal = models.IntegerField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    prisniv = models.IntegerField(null=True, db_column='Prisniv', blank=True) # Field name made lowercase.
    happy = models.IntegerField(null=True, db_column='Happy', blank=True) # Field name made lowercase.
    ekspedient = models.IntegerField(null=True, db_column='Ekspedient', blank=True) # Field name made lowercase.
    kassenr = models.IntegerField(null=True, db_column='Kassenr', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbc2000'

class Tbcartikel(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    tbcvarerfunique = models.IntegerField(null=True, db_column='TBCVarerFUnique', blank=True) # Field name made lowercase.
    ekstno = models.IntegerField(null=True, db_column='EkstNo', blank=True) # Field name made lowercase.
    ekstniv = models.IntegerField(null=True, db_column='EkstNiv', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbcartikel'

class Tbcbh(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    bonnr = models.IntegerField(null=True, db_column='BonNr', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=20, db_column='Kode', blank=True) # Field name made lowercase.
    ekspedient = models.IntegerField(null=True, db_column='Ekspedient', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    delivertime = models.DateTimeField(null=True, db_column='DeliverTime', blank=True) # Field name made lowercase.
    sttid = models.DateTimeField(null=True, db_column='STTid', blank=True) # Field name made lowercase.
    sltid = models.DateTimeField(null=True, db_column='SLTid', blank=True) # Field name made lowercase.
    kassenr = models.IntegerField(null=True, db_column='KasseNr', blank=True) # Field name made lowercase.
    journalnr = models.IntegerField(null=True, db_column='JournalNr', blank=True) # Field name made lowercase.
    bundrabat = models.FloatField(null=True, db_column='Bundrabat', blank=True) # Field name made lowercase.
    bundrabatikr = models.CharField(max_length=1, db_column='BundrabatiKr', blank=True) # Field name made lowercase.
    rabatbel = models.FloatField(null=True, db_column='RabatBel', blank=True) # Field name made lowercase.
    linierabatsum = models.FloatField(null=True, db_column='LinieRabatSum', blank=True) # Field name made lowercase.
    exmoms = models.FloatField(null=True, db_column='ExMoms', blank=True) # Field name made lowercase.
    moms = models.FloatField(null=True, db_column='Moms', blank=True) # Field name made lowercase.
    inmoms = models.FloatField(null=True, db_column='InMoms', blank=True) # Field name made lowercase.
    sumbetalinger = models.FloatField(null=True, db_column='SumBetalinger', blank=True) # Field name made lowercase.
    atbetale = models.FloatField(null=True, db_column='Atbetale', blank=True) # Field name made lowercase.
    modtaget = models.FloatField(null=True, db_column='Modtaget', blank=True) # Field name made lowercase.
    retur = models.FloatField(null=True, db_column='Retur', blank=True) # Field name made lowercase.
    bogfort = models.DateTimeField(null=True, db_column='Bogfort', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbcbh'

class Tbcbl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    bhunique = models.IntegerField(null=True, db_column='BHUnique', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    varegruppe = models.IntegerField(null=True, db_column='VareGruppe', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    sfvdimgruppe = models.IntegerField(null=True, db_column='SFVDimGruppe', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=64, db_column='VareTekst', blank=True) # Field name made lowercase.
    enhed = models.CharField(max_length=8, db_column='Enhed', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='LagerKonto', blank=True) # Field name made lowercase.
    vareforbrug = models.IntegerField(null=True, db_column='VareForbrug', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    lagerkp = models.FloatField(null=True, db_column='LagerKP', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    salgseimoms = models.IntegerField(null=True, db_column='SalgsEIMoms', blank=True) # Field name made lowercase.
    stkpris = models.FloatField(null=True, db_column='StkPris', blank=True) # Field name made lowercase.
    rabatikr = models.CharField(max_length=1, db_column='RabatiKr', blank=True) # Field name made lowercase.
    rabat = models.FloatField(null=True, db_column='Rabat', blank=True) # Field name made lowercase.
    bundrabatbel = models.FloatField(null=True, db_column='BundRabatBel', blank=True) # Field name made lowercase.
    liniesum = models.FloatField(null=True, db_column='LinieSum', blank=True) # Field name made lowercase.
    momssum = models.FloatField(null=True, db_column='MomsSum', blank=True) # Field name made lowercase.
    tilskudunique = models.IntegerField(null=True, db_column='TilskudUnique', blank=True) # Field name made lowercase.
    stykserienr = models.IntegerField(null=True, db_column='StykSerieNr', blank=True) # Field name made lowercase.
    serienrretur = models.CharField(max_length=32, db_column='SerieNrRetur', blank=True) # Field name made lowercase.
    kontraktnr = models.IntegerField(null=True, db_column='KontraktNr', blank=True) # Field name made lowercase.
    statistikkode = models.IntegerField(null=True, db_column='StatistikKode', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbcbl'

class Tbcbplan(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    gruppe = models.IntegerField(null=True, db_column='Gruppe', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbcbplan'

class Tbcbtl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kassenr = models.IntegerField(null=True, db_column='KasseNr', blank=True) # Field name made lowercase.
    bettype = models.IntegerField(null=True, db_column='BetType', blank=True) # Field name made lowercase.
    betunique = models.IntegerField(null=True, db_column='BetUnique', blank=True) # Field name made lowercase.
    linktype = models.IntegerField(null=True, db_column='LinkType', blank=True) # Field name made lowercase.
    linkunique = models.IntegerField(null=True, db_column='LinkUnique', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    belob = models.FloatField(null=True, db_column='Belob', blank=True) # Field name made lowercase.
    bogfort = models.DateTimeField(null=True, db_column='Bogfort', blank=True) # Field name made lowercase.
    kurs = models.FloatField(null=True, db_column='Kurs', blank=True) # Field name made lowercase.
    bidragsats = models.FloatField(null=True, db_column='BidragSats', blank=True) # Field name made lowercase.
    prefix = models.CharField(max_length=18, db_column='Prefix', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbcbtl'

class Tbcdplan(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    tableunique = models.IntegerField(null=True, db_column='TableUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=10, db_column='Kode', blank=True) # Field name made lowercase.
    selected = models.CharField(max_length=1, db_column='Selected', blank=True) # Field name made lowercase.
    fleft = models.IntegerField(null=True, db_column='FLeft', blank=True) # Field name made lowercase.
    ftop = models.IntegerField(null=True, db_column='FTop', blank=True) # Field name made lowercase.
    width = models.IntegerField(null=True, db_column='Width', blank=True) # Field name made lowercase.
    height = models.IntegerField(null=True, db_column='Height', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbcdplan'

class Tbckat(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    aktiv = models.CharField(max_length=1, db_column='Aktiv', blank=True) # Field name made lowercase.
    color = models.IntegerField(null=True, db_column='Color', blank=True) # Field name made lowercase.
    kodepaknap = models.CharField(max_length=1, db_column='KodePaKnap', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbckat'

class Tbckslog(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    hovedkassenr = models.IntegerField(null=True, db_column='HovedKasseNr', blank=True) # Field name made lowercase.
    kassenr = models.IntegerField(null=True, db_column='KasseNr', blank=True) # Field name made lowercase.
    ekspedient = models.IntegerField(null=True, db_column='Ekspedient', blank=True) # Field name made lowercase.
    starttid = models.DateTimeField(null=True, db_column='StartTid', blank=True) # Field name made lowercase.
    sluttid = models.DateTimeField(null=True, db_column='SlutTid', blank=True) # Field name made lowercase.
    startbeh = models.FloatField(null=True, db_column='StartBeh', blank=True) # Field name made lowercase.
    afsluttet = models.IntegerField(null=True, db_column='Afsluttet', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbckslog'

class Tbcsider(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=16, db_column='Navn', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbcsider'

class Tbcsupks(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    bonnummer = models.IntegerField(null=True, db_column='Bonnummer', blank=True) # Field name made lowercase.
    afslutover = models.IntegerField(null=True, db_column='AfslutOver', blank=True) # Field name made lowercase.
    projektnr = models.IntegerField(null=True, db_column='ProjektNr', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='LagerSted', blank=True) # Field name made lowercase.
    afslutprmedarb = models.CharField(max_length=1, db_column='AfslutPrMedArb', blank=True) # Field name made lowercase.
    korttype = models.IntegerField(null=True, db_column='Korttype', blank=True) # Field name made lowercase.
    station = models.CharField(max_length=32, db_column='Station', blank=True) # Field name made lowercase.
    defomskonto = models.IntegerField(null=True, db_column='DefOmsKonto', blank=True) # Field name made lowercase.
    afrundkonto = models.IntegerField(null=True, db_column='AfrundKonto', blank=True) # Field name made lowercase.
    kassekonto = models.IntegerField(null=True, db_column='KasseKonto', blank=True) # Field name made lowercase.
    eksterndankortvaluta = models.IntegerField(null=True, db_column='EksternDankortValuta', blank=True) # Field name made lowercase.
    dafstemningkonto = models.IntegerField(null=True, db_column='DAfstemningKonto', blank=True) # Field name made lowercase.
    kafstemningkonto = models.IntegerField(null=True, db_column='KAfstemningKonto', blank=True) # Field name made lowercase.
    gulvbokskonto = models.IntegerField(null=True, db_column='GulvboksKonto', blank=True) # Field name made lowercase.
    forbrugskonto = models.IntegerField(null=True, db_column='ForbrugsKonto', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='LagerKonto', blank=True) # Field name made lowercase.
    mindstemont = models.FloatField(null=True, db_column='MindsteMont', blank=True) # Field name made lowercase.
    specoms = models.IntegerField(null=True, db_column='SpecOms', blank=True) # Field name made lowercase.
    bonhoved = models.CharField(max_length=48, db_column='BonHoved', blank=True) # Field name made lowercase.
    bonfod = models.TextField(db_column='BonFod', blank=True) # Field name made lowercase.
    skuffekode = models.CharField(max_length=4, db_column='SkuffeKode', blank=True) # Field name made lowercase.
    bonprinter = models.CharField(max_length=128, db_column='BonPrinter', blank=True) # Field name made lowercase.
    eksprinter = models.CharField(max_length=128, db_column='EksPrinter', blank=True) # Field name made lowercase.
    eksprinter2 = models.CharField(max_length=128, db_column='EksPrinter2', blank=True) # Field name made lowercase.
    eksprinter3 = models.CharField(max_length=128, db_column='EksPrinter3', blank=True) # Field name made lowercase.
    bitmaptop = models.CharField(max_length=128, db_column='BitmapTop', blank=True) # Field name made lowercase.
    bitmaptopfirmaopl = models.IntegerField(null=True, db_column='BitmapTopFirmaOpl', blank=True) # Field name made lowercase.
    ingenfirmaopl = models.IntegerField(null=True, db_column='IngenFirmaOpl', blank=True) # Field name made lowercase.
    startettid = models.DateTimeField(null=True, db_column='StartetTid', blank=True) # Field name made lowercase.
    startetaf = models.IntegerField(null=True, db_column='StartetAf', blank=True) # Field name made lowercase.
    startbeh = models.FloatField(null=True, db_column='StartBeh', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    journalnr = models.IntegerField(null=True, db_column='JournalNr', blank=True) # Field name made lowercase.
    autostartaf = models.IntegerField(null=True, db_column='AutoStartAf', blank=True) # Field name made lowercase.
    fastprimo = models.IntegerField(null=True, db_column='FastPrimo', blank=True) # Field name made lowercase.
    visdebitor = models.IntegerField(null=True, db_column='VisDebitor', blank=True) # Field name made lowercase.
    omregnvaluta = models.IntegerField(null=True, db_column='OmregnValuta', blank=True) # Field name made lowercase.
    aktiv = models.CharField(max_length=1, db_column='Aktiv', blank=True) # Field name made lowercase.
    samllinier = models.CharField(max_length=1, db_column='SamlLinier', blank=True) # Field name made lowercase.
    sprog = models.IntegerField(null=True, db_column='Sprog', blank=True) # Field name made lowercase.
    aktivkode = models.CharField(max_length=1, db_column='AktivKode', blank=True) # Field name made lowercase.
    kodenavn = models.CharField(max_length=16, db_column='KodeNavn', blank=True) # Field name made lowercase.
    printopg = models.CharField(max_length=1, db_column='PrintOpg', blank=True) # Field name made lowercase.
    pinkode = models.CharField(max_length=1, db_column='PinKode', blank=True) # Field name made lowercase.
    gemparkerede = models.IntegerField(null=True, db_column='GemParkerede', blank=True) # Field name made lowercase.
    ejfinans = models.IntegerField(null=True, db_column='EjFinans', blank=True) # Field name made lowercase.
    tvungengulvboks = models.IntegerField(null=True, db_column='TvungenGulvBoks', blank=True) # Field name made lowercase.
    varenrpabon = models.IntegerField(null=True, db_column='VareNrPaBon', blank=True) # Field name made lowercase.
    afslutudenbetaling = models.IntegerField(null=True, db_column='AfslutUdenBetaling', blank=True) # Field name made lowercase.
    logudsalgsliste = models.CharField(max_length=1, db_column='LogudSalgsListe', blank=True) # Field name made lowercase.
    gavekortvare = models.IntegerField(null=True, db_column='GavekortVare', blank=True) # Field name made lowercase.
    tilgodevare = models.IntegerField(null=True, db_column='TilgodeVare', blank=True) # Field name made lowercase.
    gkkontoaktiv = models.IntegerField(null=True, db_column='GKKontoAktiv', blank=True) # Field name made lowercase.
    gkkontopassiv = models.IntegerField(null=True, db_column='GKKontoPassiv', blank=True) # Field name made lowercase.
    medarbrabatfelt00 = models.IntegerField(null=True, db_column='MedarbRabatFelt00', blank=True) # Field name made lowercase.
    medarbrabatsporg = models.CharField(max_length=24, db_column='MedarbRabatSporg', blank=True) # Field name made lowercase.
    smartfaktura = models.CharField(max_length=1, db_column='SmartFaktura', blank=True) # Field name made lowercase.
    displaycolor = models.IntegerField(null=True, db_column='DisplayColor', blank=True) # Field name made lowercase.
    antalcolor = models.IntegerField(null=True, db_column='AntalColor', blank=True) # Field name made lowercase.
    buttoncolor = models.IntegerField(null=True, db_column='ButtonColor', blank=True) # Field name made lowercase.
    fastholdkategori = models.CharField(max_length=1, db_column='FastholdKategori', blank=True) # Field name made lowercase.
    hurtigvarereftersek = models.IntegerField(null=True, db_column='HurtigVarerEfterSek', blank=True) # Field name made lowercase.
    autostart = models.CharField(max_length=1, db_column='Autostart', blank=True) # Field name made lowercase.
    touchscreen = models.CharField(max_length=1, db_column='TouchScreen', blank=True) # Field name made lowercase.
    cashdrawer = models.IntegerField(null=True, db_column='CashDrawer', blank=True) # Field name made lowercase.
    cashport = models.IntegerField(null=True, db_column='CashPort', blank=True) # Field name made lowercase.
    displayport = models.IntegerField(null=True, db_column='DisplayPort', blank=True) # Field name made lowercase.
    tilladdk = models.CharField(max_length=1, db_column='TilladDK', blank=True) # Field name made lowercase.
    dankortipaddr = models.IntegerField(null=True, db_column='DankortIPAddr', blank=True) # Field name made lowercase.
    dankortport = models.IntegerField(null=True, db_column='DankortPort', blank=True) # Field name made lowercase.
    maxdkvalue = models.FloatField(null=True, db_column='MaxDKValue', blank=True) # Field name made lowercase.
    dkbeskrivelse = models.CharField(max_length=12, db_column='DKBeskrivelse', blank=True) # Field name made lowercase.
    plan = models.IntegerField(null=True, db_column='Plan', blank=True) # Field name made lowercase.
    warnbeh = models.FloatField(null=True, db_column='Warnbeh', blank=True) # Field name made lowercase.
    warnmat = models.IntegerField(null=True, db_column='WarnMat', blank=True) # Field name made lowercase.
    totalkr = models.FloatField(null=True, db_column='Totalkr', blank=True) # Field name made lowercase.
    tilladlinierabat = models.CharField(max_length=1, db_column='TilladLinieRabat', blank=True) # Field name made lowercase.
    tilladbundrabat = models.CharField(max_length=1, db_column='TilladBundRabat', blank=True) # Field name made lowercase.
    rabatikr = models.CharField(max_length=1, db_column='RabatiKr', blank=True) # Field name made lowercase.
    anvendkunderabat = models.CharField(max_length=1, db_column='AnvendKundeRabat', blank=True) # Field name made lowercase.
    rabatknapper = models.CharField(max_length=32, db_column='RabatKnapper', blank=True) # Field name made lowercase.
    godkendreturpris = models.IntegerField(null=True, db_column='GodkendReturPris', blank=True) # Field name made lowercase.
    anvendexadr = models.CharField(max_length=1, db_column='AnvendExAdr', blank=True) # Field name made lowercase.
    startaktuelle = models.IntegerField(null=True, db_column='StartAktuelle', blank=True) # Field name made lowercase.
    barcodebon = models.IntegerField(null=True, db_column='BarcodeBon', blank=True) # Field name made lowercase.
    forretningskode = models.CharField(max_length=4, db_column='ForretningsKode', blank=True) # Field name made lowercase.
    tilladafstemning = models.IntegerField(null=True, db_column='TilladAfstemning', blank=True) # Field name made lowercase.
    usedesign = models.CharField(max_length=1, db_column='Usedesign', blank=True) # Field name made lowercase.
    anvendvarer = models.CharField(max_length=1, db_column='Anvendvarer', blank=True) # Field name made lowercase.
    bondesign = models.CharField(max_length=12, db_column='Bondesign', blank=True) # Field name made lowercase.
    eks1design = models.CharField(max_length=12, db_column='Eks1design', blank=True) # Field name made lowercase.
    eks2design = models.CharField(max_length=12, db_column='Eks2design', blank=True) # Field name made lowercase.
    eks3design = models.CharField(max_length=12, db_column='Eks3design', blank=True) # Field name made lowercase.
    anvendvaredesign = models.IntegerField(null=True, db_column='AnvendVareDesign', blank=True) # Field name made lowercase.
    bonloop = models.CharField(max_length=1, db_column='BonLoop', blank=True) # Field name made lowercase.
    tvungenbon = models.CharField(max_length=1, db_column='TvungenBon', blank=True) # Field name made lowercase.
    eurototalbon = models.CharField(max_length=1, db_column='EuroTotalBon', blank=True) # Field name made lowercase.
    udvidetboninfo = models.CharField(max_length=1, db_column='UdvidetBonInfo', blank=True) # Field name made lowercase.
    displayvelkommen1 = models.CharField(max_length=20, db_column='DisplayVelkommen1', blank=True) # Field name made lowercase.
    displayvelkommen2 = models.CharField(max_length=20, db_column='DisplayVelkommen2', blank=True) # Field name made lowercase.
    displayalignment = models.IntegerField(null=True, db_column='DisplayAlignment', blank=True) # Field name made lowercase.
    displayansitooem = models.CharField(max_length=1, db_column='DisplayAnsiToOem', blank=True) # Field name made lowercase.
    displayvaluta = models.CharField(max_length=4, db_column='DisplayValuta', blank=True) # Field name made lowercase.
    autostartbeh = models.CharField(max_length=1, db_column='AutoStartBeh', blank=True) # Field name made lowercase.
    activepage = models.IntegerField(null=True, db_column='ActivePage', blank=True) # Field name made lowercase.
    brugkladdenr = models.IntegerField(null=True, db_column='BrugKladdeNr', blank=True) # Field name made lowercase.
    varereturunderskrift = models.IntegerField(null=True, db_column='VareReturUnderskrift', blank=True) # Field name made lowercase.
    bonreturekspedient = models.IntegerField(null=True, db_column='BonReturEkspedient', blank=True) # Field name made lowercase.
    anvendretpris = models.IntegerField(null=True, db_column='AnvendRetPris', blank=True) # Field name made lowercase.
    matname = models.CharField(max_length=30, db_column='MatName', blank=True) # Field name made lowercase.
    debitkvit = models.CharField(max_length=1, db_column='Debitkvit', blank=True) # Field name made lowercase.
    kassetype = models.IntegerField(null=True, db_column='KasseType', blank=True) # Field name made lowercase.
    ingenbonmargin = models.IntegerField(null=True, db_column='Ingenbonmargin', blank=True) # Field name made lowercase.
    anvenda5 = models.IntegerField(null=True, db_column='AnvendA5', blank=True) # Field name made lowercase.
    useveksle = models.IntegerField(null=True, db_column='Useveksle', blank=True) # Field name made lowercase.
    logox1 = models.FloatField(null=True, db_column='LogoX1', blank=True) # Field name made lowercase.
    logox2 = models.FloatField(null=True, db_column='LogoX2', blank=True) # Field name made lowercase.
    logoy1 = models.FloatField(null=True, db_column='LogoY1', blank=True) # Field name made lowercase.
    logoy2 = models.FloatField(null=True, db_column='LogoY2', blank=True) # Field name made lowercase.
    valgmedarb = models.CharField(max_length=1, db_column='ValgMedarb', blank=True) # Field name made lowercase.
    spaeringpark = models.IntegerField(null=True, db_column='SpaeringPark', blank=True) # Field name made lowercase.
    lcd = models.CharField(max_length=1, db_column='LCD', blank=True) # Field name made lowercase.
    visvarebilledelcd = models.CharField(max_length=1, db_column='VisVarebilledeLCD', blank=True) # Field name made lowercase.
    vistekstbilledelcd = models.CharField(max_length=1, db_column='VisTekstbilledeLCD', blank=True) # Field name made lowercase.
    billede = models.TextField(db_column='Billede', blank=True) # Field name made lowercase.
    defbillede = models.TextField(db_column='DefBillede', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbcsupks'

class Tbctilp(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    cardtype = models.IntegerField(null=True, db_column='Cardtype', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbctilp'

class Tbcvarer(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=25, db_column='Kode', blank=True) # Field name made lowercase.
    bontekst = models.CharField(max_length=64, db_column='Bontekst', blank=True) # Field name made lowercase.
    kategori = models.IntegerField(null=True, db_column='Kategori', blank=True) # Field name made lowercase.
    enhed = models.CharField(max_length=8, db_column='Enhed', blank=True) # Field name made lowercase.
    salgspris = models.FloatField(null=True, db_column='SalgsPris', blank=True) # Field name made lowercase.
    rabatpris = models.FloatField(null=True, db_column='RabatPris', blank=True) # Field name made lowercase.
    varetype = models.IntegerField(null=True, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='LagerKonto', blank=True) # Field name made lowercase.
    vareforbrugkonto = models.IntegerField(null=True, db_column='VareforbrugKonto', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    color = models.IntegerField(null=True, db_column='Color', blank=True) # Field name made lowercase.
    sumoptalt = models.IntegerField(null=True, db_column='SumOptalt', blank=True) # Field name made lowercase.
    ekstno = models.IntegerField(null=True, db_column='EkstNo', blank=True) # Field name made lowercase.
    ekstniv = models.IntegerField(null=True, db_column='EkstNiv', blank=True) # Field name made lowercase.
    ekstprint = models.IntegerField(null=True, db_column='EkstPrint', blank=True) # Field name made lowercase.
    bontekstpaknap = models.CharField(max_length=1, db_column='BonTekstPaKnap', blank=True) # Field name made lowercase.
    angivkreditor = models.IntegerField(null=True, db_column='AngivKreditor', blank=True) # Field name made lowercase.
    popup = models.CharField(max_length=1, db_column='Popup', blank=True) # Field name made lowercase.
    debitor = models.CharField(max_length=1, db_column='Debitor', blank=True) # Field name made lowercase.
    nulbon = models.CharField(max_length=1, db_column='NulBon', blank=True) # Field name made lowercase.
    aktivepage = models.IntegerField(null=True, db_column='AktivePage', blank=True) # Field name made lowercase.
    showfast = models.CharField(max_length=1, db_column='ShowFast', blank=True) # Field name made lowercase.
    ffastindex = models.IntegerField(null=True, db_column='FFastIndex', blank=True) # Field name made lowercase.
    onlytext = models.CharField(max_length=1, db_column='OnLyText', blank=True) # Field name made lowercase.
    inform = models.CharField(max_length=1, db_column='Inform', blank=True) # Field name made lowercase.
    informtext = models.CharField(max_length=40, db_column='InformText', blank=True) # Field name made lowercase.
    isclosed = models.IntegerField(null=True, db_column='IsClosed', blank=True) # Field name made lowercase.
    iskontingent = models.CharField(max_length=1, db_column='IsKontingent', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tbcvarer'

class Terminal(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=50, db_column='Navn', blank=True) # Field name made lowercase.
    kassekonto = models.IntegerField(null=True, db_column='KasseKonto', blank=True) # Field name made lowercase.
    placering = models.CharField(max_length=50, db_column='Placering', blank=True) # Field name made lowercase.
    bonprinter = models.CharField(max_length=64, db_column='BonPrinter', blank=True) # Field name made lowercase.
    labelprinter = models.CharField(max_length=64, db_column='LabelPrinter', blank=True) # Field name made lowercase.
    bonnr = models.IntegerField(null=True, db_column='BonNr', blank=True) # Field name made lowercase.
    oreafrundingkonto = models.IntegerField(null=True, db_column='OreAfrundingKonto', blank=True) # Field name made lowercase.
    station = models.CharField(max_length=32, db_column='Station', blank=True) # Field name made lowercase.
    bontop = models.CharField(max_length=50, db_column='BonTop', blank=True) # Field name made lowercase.
    bonbottom = models.CharField(max_length=50, db_column='BonBottom', blank=True) # Field name made lowercase.
    loginterminal = models.CharField(max_length=1, db_column='LoginTerminal', blank=True) # Field name made lowercase.
    udskrivadgangbon = models.CharField(max_length=1, db_column='Udskrivadgangbon', blank=True) # Field name made lowercase.
    udskrivlabel = models.CharField(max_length=1, db_column='UdskrivLabel', blank=True) # Field name made lowercase.
    labeltype = models.CharField(max_length=20, db_column='LabelType', blank=True) # Field name made lowercase.
    labelunique = models.IntegerField(null=True, db_column='LabelUnique', blank=True) # Field name made lowercase.
    bitmaptop = models.CharField(max_length=64, db_column='BitMapTop', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'terminal'

class Text(models.Model):
    kode = models.CharField(unique=True, max_length=24, db_column='Kode', blank=True) # Field name made lowercase.
    tekst = models.CharField(max_length=50, db_column='Tekst', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'text'

class Tilpafgi(models.Model):
    oliekonto = models.IntegerField(null=True, db_column='OlieKonto', blank=True) # Field name made lowercase.
    elkonto = models.IntegerField(null=True, db_column='ElKonto', blank=True) # Field name made lowercase.
    gaskonto = models.IntegerField(null=True, db_column='GasKonto', blank=True) # Field name made lowercase.
    kulkonto = models.IntegerField(null=True, db_column='KulKonto', blank=True) # Field name made lowercase.
    co2konto = models.IntegerField(null=True, db_column='CO2Konto', blank=True) # Field name made lowercase.
    vandkonto = models.IntegerField(null=True, db_column='VandKonto', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpafgi'

class Tilpbank(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    ibank = models.IntegerField(null=True, db_column='IBank', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    projekt = models.IntegerField(null=True, db_column='Projekt', blank=True) # Field name made lowercase.
    samleposterbank = models.IntegerField(null=True, db_column='SamlePosterBank', blank=True) # Field name made lowercase.
    igebyr = models.IntegerField(null=True, db_column='IGebyr', blank=True) # Field name made lowercase.
    ikladdenr = models.IntegerField(null=True, db_column='IKladdeNr', blank=True) # Field name made lowercase.
    ukladdenr = models.IntegerField(null=True, db_column='UKladdeNr', blank=True) # Field name made lowercase.
    landocr = models.IntegerField(null=True, db_column='LandOCR', blank=True) # Field name made lowercase.
    overforselkontoud = models.IntegerField(null=True, db_column='OverforselKontoUd', blank=True) # Field name made lowercase.
    iindbetal = models.CharField(max_length=1, db_column='IIndbetal', blank=True) # Field name made lowercase.
    uextype = models.IntegerField(null=True, db_column='UExType', blank=True) # Field name made lowercase.
    advarantaldage = models.IntegerField(null=True, db_column='AdvarAntalDage', blank=True) # Field name made lowercase.
    anvendkreditoriegenref = models.IntegerField(null=True, db_column='AnvendKreditorIEgenRef', blank=True) # Field name made lowercase.
    uplace = models.CharField(max_length=80, db_column='UPlace', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpbank'

class Tilpbb(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    metode = models.IntegerField(null=True, db_column='Metode', blank=True) # Field name made lowercase.
    antaldage = models.IntegerField(null=True, db_column='AntalDage', blank=True) # Field name made lowercase.
    medtagwebshop = models.IntegerField(null=True, db_column='MedtagWebshop', blank=True) # Field name made lowercase.
    medtagbooking = models.IntegerField(null=True, db_column='Medtagbooking', blank=True) # Field name made lowercase.
    kreditkort = models.IntegerField(null=True, db_column='Kreditkort', blank=True) # Field name made lowercase.
    dibscode = models.CharField(max_length=20, db_column='DIBSCode', blank=True) # Field name made lowercase.
    kostvarenr = models.IntegerField(null=True, db_column='KostVareNr', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    gebyr = models.FloatField(null=True, db_column='Gebyr', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='Omskonto', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpbb'

class Tilpbooking(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    dibsbankkonto = models.IntegerField(null=True, db_column='DibsBankKonto', blank=True) # Field name made lowercase.
    dibskladdenr = models.IntegerField(null=True, db_column='DibsKladdenr', blank=True) # Field name made lowercase.
    dibssamle = models.CharField(max_length=1, db_column='DibsSamle', blank=True) # Field name made lowercase.
    bookingomskonto = models.IntegerField(null=True, db_column='BookingOmsKonto', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpbooking'

class Tilpfigi(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    kreditornr = models.CharField(max_length=8, db_column='KreditorNr', blank=True) # Field name made lowercase.
    girokontonr = models.CharField(max_length=8, db_column='GiroKontoNr', blank=True) # Field name made lowercase.
    vertical = models.IntegerField(null=True, db_column='Vertical', blank=True) # Field name made lowercase.
    horisontal = models.IntegerField(null=True, db_column='Horisontal', blank=True) # Field name made lowercase.
    fifortrykt = models.IntegerField(null=True, db_column='FIFortrykt', blank=True) # Field name made lowercase.
    fikorttype = models.IntegerField(null=True, db_column='FIKortType', blank=True) # Field name made lowercase.
    girofortrykt = models.IntegerField(null=True, db_column='GiroFortrykt', blank=True) # Field name made lowercase.
    ocrfontname = models.CharField(max_length=16, db_column='OCRFontName', blank=True) # Field name made lowercase.
    trimgiro = models.CharField(max_length=1, db_column='TrimGiro', blank=True) # Field name made lowercase.
    trimfi = models.CharField(max_length=1, db_column='TrimFI', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpfigi'

class Tilpfina(models.Model):
    beholdninger = models.IntegerField(null=True, db_column='Beholdninger', blank=True) # Field name made lowercase.
    statusstart = models.IntegerField(null=True, db_column='StatusStart', blank=True) # Field name made lowercase.
    medlemsamle = models.IntegerField(null=True, db_column='MedlemSamle', blank=True) # Field name made lowercase.
    kreditorsamle = models.IntegerField(null=True, db_column='KreditorSamle', blank=True) # Field name made lowercase.
    aarsresultat = models.IntegerField(null=True, db_column='AarsResultat', blank=True) # Field name made lowercase.
    formue = models.IntegerField(null=True, db_column='Formue', blank=True) # Field name made lowercase.
    fejlkonto = models.IntegerField(null=True, db_column='FejlKonto', blank=True) # Field name made lowercase.
    slettededebitorer = models.IntegerField(null=True, db_column='SlettedeDebitorer', blank=True) # Field name made lowercase.
    skyldigmomsprimo = models.IntegerField(null=True, db_column='SkyldigMomsPrimo', blank=True) # Field name made lowercase.
    momsafregning = models.IntegerField(null=True, db_column='MomsAfregning', blank=True) # Field name made lowercase.
    sumoms = models.IntegerField(null=True, db_column='SumOms', blank=True) # Field name made lowercase.
    sumaktiver = models.IntegerField(null=True, db_column='SumAktiver', blank=True) # Field name made lowercase.
    sumpassiver = models.IntegerField(null=True, db_column='SumPassiver', blank=True) # Field name made lowercase.
    anvendprivat = models.CharField(max_length=1, db_column='AnvendPrivat', blank=True) # Field name made lowercase.
    anvendprivatkone = models.CharField(max_length=1, db_column='AnvendPrivatKone', blank=True) # Field name made lowercase.
    kontantkontoud = models.IntegerField(null=True, db_column='KontantKontoUd', blank=True) # Field name made lowercase.
    checkkontoud = models.IntegerField(null=True, db_column='CheckKontoUd', blank=True) # Field name made lowercase.
    betalkladdenr = models.IntegerField(null=True, db_column='BetalKladdeNr', blank=True) # Field name made lowercase.
    privatstartejer = models.IntegerField(null=True, db_column='PrivatStartEjer', blank=True) # Field name made lowercase.
    privatstartkone = models.IntegerField(null=True, db_column='PrivatStartKone', blank=True) # Field name made lowercase.
    privatstartkapitalejer = models.IntegerField(null=True, db_column='PrivatStartKapitalEjer', blank=True) # Field name made lowercase.
    privatstartkapitalkone = models.IntegerField(null=True, db_column='PrivatStartKapitalKone', blank=True) # Field name made lowercase.
    privatformue = models.IntegerField(null=True, db_column='PrivatFormue', blank=True) # Field name made lowercase.
    privatkoneformue = models.IntegerField(null=True, db_column='PrivatKoneFormue', blank=True) # Field name made lowercase.
    privatresejer = models.IntegerField(null=True, db_column='PrivatResEjer', blank=True) # Field name made lowercase.
    privatreskone = models.IntegerField(null=True, db_column='PrivatResKone', blank=True) # Field name made lowercase.
    privatstatus = models.IntegerField(null=True, db_column='PrivatStatus', blank=True) # Field name made lowercase.
    privatkonestatus = models.IntegerField(null=True, db_column='PrivatKoneStatus', blank=True) # Field name made lowercase.
    privatreskapitalejer = models.IntegerField(null=True, db_column='PrivatResKapitalEjer', blank=True) # Field name made lowercase.
    privatreskapitalkone = models.IntegerField(null=True, db_column='PrivatResKapitalKone', blank=True) # Field name made lowercase.
    mellemregning = models.IntegerField(null=True, db_column='MellemRegning', blank=True) # Field name made lowercase.
    mellemprivat = models.IntegerField(null=True, db_column='MellemPrivat', blank=True) # Field name made lowercase.
    debnultabskonto = models.IntegerField(null=True, db_column='DebNulTabsKonto', blank=True) # Field name made lowercase.
    debnulminsaldo = models.FloatField(null=True, db_column='DebNulMinSaldo', blank=True) # Field name made lowercase.
    rapportfortegn = models.IntegerField(null=True, db_column='RapportFortegn', blank=True) # Field name made lowercase.
    anvendafdeling = models.IntegerField(null=True, db_column='AnvendAfdeling', blank=True) # Field name made lowercase.
    afstemafdeling = models.IntegerField(null=True, db_column='AfstemAfdeling', blank=True) # Field name made lowercase.
    primoafdeling = models.IntegerField(null=True, db_column='PrimoAfdeling', blank=True) # Field name made lowercase.
    primoprojekt = models.IntegerField(null=True, db_column='PrimoProjekt', blank=True) # Field name made lowercase.
    primofinadim5 = models.IntegerField(null=True, db_column='PrimoFinaDim5', blank=True) # Field name made lowercase.
    lukketsidstear = models.CharField(max_length=18, db_column='LukketSidsteAr', blank=True) # Field name made lowercase.
    anvendkladder = models.IntegerField(null=True, db_column='AnvendKladder', blank=True) # Field name made lowercase.
    anvendudligning = models.IntegerField(null=True, db_column='AnvendUdligning', blank=True) # Field name made lowercase.
    lukketiar = models.CharField(max_length=24, db_column='LukketIAr', blank=True) # Field name made lowercase.
    finadim5 = models.CharField(max_length=24, db_column='FinaDim5', blank=True) # Field name made lowercase.
    finadim6 = models.CharField(max_length=24, db_column='FinaDim6', blank=True) # Field name made lowercase.
    gemfinans = models.IntegerField(null=True, db_column='GemFinans', blank=True) # Field name made lowercase.
    manuelaabning = models.IntegerField(null=True, db_column='ManuelAabning', blank=True) # Field name made lowercase.
    anvendbilagserier = models.IntegerField(null=True, db_column='AnvendBilagSerier', blank=True) # Field name made lowercase.
    tilladbilagdiff = models.IntegerField(null=True, db_column='TilladBilagDiff', blank=True) # Field name made lowercase.
    tilladdatodiffprbilag = models.IntegerField(null=True, db_column='TilladDatoDiffPrBilag', blank=True) # Field name made lowercase.
    kreditorudbetalinger = models.IntegerField(null=True, db_column='KreditorUdbetalinger', blank=True) # Field name made lowercase.
    enjournal = models.IntegerField(null=True, db_column='EnJournal', blank=True) # Field name made lowercase.
    anvendlukket = models.IntegerField(null=True, db_column='AnvendLukket', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpfina'

class Tilpgbla(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    blanketnr = models.IntegerField(null=True, db_column='BlanketNr', blank=True) # Field name made lowercase.
    blanketa5 = models.CharField(max_length=1, db_column='BlanketA5', blank=True) # Field name made lowercase.
    udskrivocr = models.IntegerField(null=True, db_column='UdskrivOCR', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpgbla'

class Tilpgfel(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    pfunique = models.IntegerField(null=True, db_column='PFUnique', blank=True) # Field name made lowercase.
    name = models.CharField(max_length=32, db_column='Name', blank=True) # Field name made lowercase.
    datafield = models.IntegerField(null=True, db_column='DataField', blank=True) # Field name made lowercase.
    fontname = models.CharField(max_length=32, db_column='FontName', blank=True) # Field name made lowercase.
    fontbold = models.CharField(max_length=1, db_column='FontBold', blank=True) # Field name made lowercase.
    fontsize = models.IntegerField(null=True, db_column='FontSize', blank=True) # Field name made lowercase.
    alignment = models.IntegerField(null=True, db_column='Alignment', blank=True) # Field name made lowercase.
    format = models.IntegerField(null=True, db_column='Format', blank=True) # Field name made lowercase.
    medkartfield = models.CharField(max_length=35, db_column='MedKartField', blank=True) # Field name made lowercase.
    y1 = models.FloatField(null=True, db_column='Y1', blank=True) # Field name made lowercase.
    x1 = models.FloatField(null=True, db_column='X1', blank=True) # Field name made lowercase.
    y2 = models.FloatField(null=True, db_column='Y2', blank=True) # Field name made lowercase.
    x2 = models.FloatField(null=True, db_column='X2', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpgfel'

class Tilpkartotek(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    dibsbankkonto = models.IntegerField(null=True, db_column='DibsBankKonto', blank=True) # Field name made lowercase.
    dibskladdenr = models.IntegerField(null=True, db_column='DibsKladdenr', blank=True) # Field name made lowercase.
    dibssamle = models.CharField(max_length=1, db_column='DibsSamle', blank=True) # Field name made lowercase.
    tilmeldomskonto = models.IntegerField(null=True, db_column='TilmeldOmsKonto', blank=True) # Field name made lowercase.
    tilmeld = models.FloatField(null=True, db_column='Tilmeld', blank=True) # Field name made lowercase.
    tilmeldkontingent = models.CharField(max_length=1, db_column='TilmeldKontingent', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpkartotek'

class Tilpkgvg(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    varegrpfra = models.IntegerField(null=True, db_column='VareGrpFra', blank=True) # Field name made lowercase.
    varegrptil = models.IntegerField(null=True, db_column='VareGrpTil', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpkgvg'

class Tilpklad(models.Model):
    nummer = models.IntegerField(null=True, db_column='Nummer', blank=True) # Field name made lowercase.
    nytbilagnr = models.CharField(max_length=1, db_column='NytBilagNr', blank=True) # Field name made lowercase.
    afslutudenbogf = models.CharField(max_length=1, db_column='AfslutUdenBogf', blank=True) # Field name made lowercase.
    autosavepost = models.CharField(max_length=1, db_column='AutoSavePost', blank=True) # Field name made lowercase.
    visbehold1 = models.IntegerField(null=True, db_column='VisBehold1', blank=True) # Field name made lowercase.
    visbehold2 = models.IntegerField(null=True, db_column='VisBehold2', blank=True) # Field name made lowercase.
    visbehold3 = models.IntegerField(null=True, db_column='VisBehold3', blank=True) # Field name made lowercase.
    visbehold4 = models.IntegerField(null=True, db_column='VisBehold4', blank=True) # Field name made lowercase.
    visbehold5 = models.IntegerField(null=True, db_column='VisBehold5', blank=True) # Field name made lowercase.
    debetfirst = models.CharField(max_length=1, db_column='DebetFirst', blank=True) # Field name made lowercase.
    valutaflag = models.CharField(max_length=1, db_column='ValutaFlag', blank=True) # Field name made lowercase.
    benytforfald = models.CharField(max_length=1, db_column='BenytForfald', blank=True) # Field name made lowercase.
    gemfinans = models.IntegerField(null=True, db_column='GemFinans', blank=True) # Field name made lowercase.
    momssats = models.FloatField(null=True, db_column='MomsSats', blank=True) # Field name made lowercase.
    tilladbogforsamle = models.CharField(max_length=1, db_column='TilladBogforSamle', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpklad'

class Tilpkobmand(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    ftpsite = models.CharField(max_length=100, db_column='FTPSite', blank=True) # Field name made lowercase.
    ftpuser = models.CharField(max_length=100, db_column='FTPUser', blank=True) # Field name made lowercase.
    ftppassword = models.CharField(max_length=100, db_column='FTPPassword', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpkobmand'

class Tilplon(models.Model):
    filename = models.CharField(max_length=100, db_column='FileName', blank=True) # Field name made lowercase.
    lonplac = models.CharField(max_length=200, db_column='LonPlac', blank=True) # Field name made lowercase.
    delfile = models.CharField(max_length=1, db_column='DelFile', blank=True) # Field name made lowercase.
    lontype = models.IntegerField(null=True, db_column='LonType', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilplon'

class Tilpmail(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fuser = models.CharField(max_length=40, db_column='FUser', blank=True) # Field name made lowercase.
    fmapi = models.CharField(max_length=1, db_column='FMAPI', blank=True) # Field name made lowercase.
    fstdmapi = models.CharField(max_length=1, db_column='FSTDMAPI', blank=True) # Field name made lowercase.
    fserver = models.CharField(max_length=64, db_column='FServer', blank=True) # Field name made lowercase.
    fusername = models.CharField(max_length=64, db_column='FUsername', blank=True) # Field name made lowercase.
    fpassword = models.CharField(max_length=64, db_column='FPassword', blank=True) # Field name made lowercase.
    fsecure = models.CharField(max_length=1, db_column='FSecure', blank=True) # Field name made lowercase.
    fport = models.IntegerField(null=True, db_column='FPort', blank=True) # Field name made lowercase.
    ffromname = models.CharField(max_length=64, db_column='FFromName', blank=True) # Field name made lowercase.
    ffromadress = models.CharField(max_length=64, db_column='FFromAdress', blank=True) # Field name made lowercase.
    fsignatur = models.TextField(db_column='FSignatur', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpmail'

class Tilpmedl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    stkonto = models.CharField(max_length=1, db_column='STKonto', blank=True) # Field name made lowercase.
    warnsaldo = models.CharField(max_length=1, db_column='WarnSaldo', blank=True) # Field name made lowercase.
    autofelt00 = models.CharField(max_length=1, db_column='AutoFelt00', blank=True) # Field name made lowercase.
    inetfelt00 = models.CharField(max_length=1, db_column='InetFelt00', blank=True) # Field name made lowercase.
    noinetvalidering = models.CharField(max_length=1, db_column='NoInetValidering', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpmedl'

class Tilpmoms(models.Model):
    nummer = models.IntegerField(unique=True, null=True, db_column='Nummer', blank=True) # Field name made lowercase.
    sats = models.FloatField(null=True, db_column='Sats', blank=True) # Field name made lowercase.
    kontoind = models.IntegerField(null=True, db_column='KontoInd', blank=True) # Field name made lowercase.
    kontoud = models.IntegerField(null=True, db_column='KontoUd', blank=True) # Field name made lowercase.
    kontoafslut = models.IntegerField(null=True, db_column='KontoAfslut', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpmoms'

class Tilpnemhandel(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sti = models.CharField(max_length=150, db_column='Sti', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpnemhandel'

class Tilpomsk(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    medgrp = models.IntegerField(null=True, db_column='MedGrp', blank=True) # Field name made lowercase.
    varegrp = models.IntegerField(null=True, db_column='VareGrp', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpomsk'

class Tilpryk(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    respit = models.IntegerField(null=True, db_column='Respit', blank=True) # Field name made lowercase.
    gebyr = models.IntegerField(null=True, db_column='Gebyr', blank=True) # Field name made lowercase.
    kladdenr = models.IntegerField(null=True, db_column='KladdeNr', blank=True) # Field name made lowercase.
    ryk1 = models.FloatField(null=True, db_column='Ryk1', blank=True) # Field name made lowercase.
    ryk2 = models.FloatField(null=True, db_column='Ryk2', blank=True) # Field name made lowercase.
    ryk3 = models.FloatField(null=True, db_column='Ryk3', blank=True) # Field name made lowercase.
    minbel = models.FloatField(null=True, db_column='MinBel', blank=True) # Field name made lowercase.
    brev1 = models.CharField(max_length=12, db_column='Brev1', blank=True) # Field name made lowercase.
    brev2 = models.CharField(max_length=12, db_column='Brev2', blank=True) # Field name made lowercase.
    brev3 = models.CharField(max_length=12, db_column='Brev3', blank=True) # Field name made lowercase.
    rentesats = models.FloatField(null=True, db_column='RenteSats', blank=True) # Field name made lowercase.
    rentekonto = models.IntegerField(null=True, db_column='RenteKonto', blank=True) # Field name made lowercase.
    useocr = models.IntegerField(null=True, db_column='USEOCR', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpryk'

class Tilpsf(models.Model):
    fakturanr = models.IntegerField(null=True, db_column='FakturaNr', blank=True) # Field name made lowercase.
    kreditnotanr = models.IntegerField(null=True, db_column='KreditNotaNr', blank=True) # Field name made lowercase.
    ordrenr = models.IntegerField(null=True, db_column='OrdreNr', blank=True) # Field name made lowercase.
    autoklar = models.CharField(max_length=1, db_column='AutoKlar', blank=True) # Field name made lowercase.
    exadr2flag = models.CharField(max_length=1, db_column='ExAdr2Flag', blank=True) # Field name made lowercase.
    altomskonto = models.IntegerField(null=True, db_column='AltOmsKonto', blank=True) # Field name made lowercase.
    defkobskonto = models.IntegerField(null=True, db_column='DefKobsKonto', blank=True) # Field name made lowercase.
    defsvindkonto = models.IntegerField(null=True, db_column='DefSvindKonto', blank=True) # Field name made lowercase.
    tilbydkopier = models.CharField(max_length=1, db_column='TilbydKopier', blank=True) # Field name made lowercase.
    arbfakvalidering = models.CharField(max_length=1, db_column='ArbFakValidering', blank=True) # Field name made lowercase.
    defrabatpct = models.CharField(max_length=1, db_column='DefRabatPct', blank=True) # Field name made lowercase.
    anvendocr = models.IntegerField(null=True, db_column='AnvendOCR', blank=True) # Field name made lowercase.
    anvendocrkontoudtog = models.IntegerField(null=True, db_column='AnvendOCRKontoUdtog', blank=True) # Field name made lowercase.
    anvendocrordre = models.IntegerField(null=True, db_column='AnvendOCROrdre', blank=True) # Field name made lowercase.
    betalbet = models.IntegerField(null=True, db_column='BetalBet', blank=True) # Field name made lowercase.
    layoutfaktura = models.IntegerField(null=True, db_column='LayoutFaktura', blank=True) # Field name made lowercase.
    layoutkreditnota = models.IntegerField(null=True, db_column='LayoutKreditNota', blank=True) # Field name made lowercase.
    layoutordre = models.IntegerField(null=True, db_column='LayoutOrdre', blank=True) # Field name made lowercase.
    layoutfolgeseddel = models.IntegerField(null=True, db_column='LayoutFolgeSeddel', blank=True) # Field name made lowercase.
    layouttilbud = models.IntegerField(null=True, db_column='LayoutTilbud', blank=True) # Field name made lowercase.
    layoutvaerksted = models.IntegerField(null=True, db_column='LayoutVaerksted', blank=True) # Field name made lowercase.
    stdfakturatype = models.IntegerField(null=True, db_column='StdFakturaType', blank=True) # Field name made lowercase.
    maxfaklinewidth = models.IntegerField(null=True, db_column='MaxFakLineWidth', blank=True) # Field name made lowercase.
    prisimplevrindex = models.IntegerField(null=True, db_column='PrisImpLevrIndex', blank=True) # Field name made lowercase.
    qkundenr = models.CharField(max_length=8, db_column='QKundeNr', blank=True) # Field name made lowercase.
    kontantkunde = models.IntegerField(null=True, db_column='KontantKunde', blank=True) # Field name made lowercase.
    kontantkonto = models.IntegerField(null=True, db_column='KontantKonto', blank=True) # Field name made lowercase.
    defavancedel = models.FloatField(null=True, db_column='DefAvanceDel', blank=True) # Field name made lowercase.
    prisimpfilename = models.CharField(max_length=127, db_column='PrisImpFileName', blank=True) # Field name made lowercase.
    ordreimpfilename = models.CharField(max_length=127, db_column='OrdreImpFileName', blank=True) # Field name made lowercase.
    lagertilforbrug = models.CharField(max_length=1, db_column='LagerTilForbrug', blank=True) # Field name made lowercase.
    lagertilforbrugydelser = models.IntegerField(null=True, db_column='LagerTilForbrugYdelser', blank=True) # Field name made lowercase.
    vareforbrugkonto = models.IntegerField(null=True, db_column='VareForbrugKonto', blank=True) # Field name made lowercase.
    vareforbrugmodel = models.IntegerField(null=True, db_column='VareforbrugModel', blank=True) # Field name made lowercase.
    omsgruppe10 = models.CharField(max_length=1, db_column='OmsGruppe10', blank=True) # Field name made lowercase.
    tilladinternfaktura = models.CharField(max_length=1, db_column='TilladInternFaktura', blank=True) # Field name made lowercase.
    anvendtbcvarer = models.CharField(max_length=1, db_column='AnvendTBCVarer', blank=True) # Field name made lowercase.
    advarejkp0 = models.CharField(max_length=1, db_column='AdvarEjKP0', blank=True) # Field name made lowercase.
    historik = models.CharField(max_length=1, db_column='Historik', blank=True) # Field name made lowercase.
    anvendindkob = models.IntegerField(null=True, db_column='AnvendIndkob', blank=True) # Field name made lowercase.
    ordrenrindkob = models.IntegerField(null=True, db_column='OrdreNrIndkob', blank=True) # Field name made lowercase.
    bilagnrfrafinans = models.CharField(max_length=1, db_column='BilagNrFraFinans', blank=True) # Field name made lowercase.
    ipo = models.FloatField(null=True, db_column='IPO', blank=True) # Field name made lowercase.
    anvendproduger = models.IntegerField(null=True, db_column='AnvendProdUger', blank=True) # Field name made lowercase.
    permanentordre = models.IntegerField(null=True, db_column='PermanentOrdre', blank=True) # Field name made lowercase.
    andvenddebitorocr = models.IntegerField(null=True, db_column='AndvendDebitorOCR', blank=True) # Field name made lowercase.
    anvendvaregrupper = models.IntegerField(null=True, db_column='AnvendVareGrupper', blank=True) # Field name made lowercase.
    anvendmedarb = models.IntegerField(null=True, db_column='AnvendMedarb', blank=True) # Field name made lowercase.
    webshopkunder = models.IntegerField(null=True, db_column='WebshopKunder', blank=True) # Field name made lowercase.
    webshopmedarbejdere = models.IntegerField(null=True, db_column='WebshopMedarbejdere', blank=True) # Field name made lowercase.
    webshopvalidering = models.CharField(max_length=1, db_column='WebshopValidering', blank=True) # Field name made lowercase.
    webshopbetingelser = models.TextField(db_column='WebshopBetingelser', blank=True) # Field name made lowercase.
    webshoplevering = models.TextField(db_column='WebshopLevering', blank=True) # Field name made lowercase.
    webshopreklamation = models.TextField(db_column='WebshopReklamation', blank=True) # Field name made lowercase.
    webshopforsidetekst = models.TextField(db_column='WebshopForsideTekst', blank=True) # Field name made lowercase.
    webshopmestsolgtvist = models.IntegerField(null=True, db_column='WebshopMestSolgtVist', blank=True) # Field name made lowercase.
    webshopantalkategorier = models.IntegerField(null=True, db_column='WebshopAntalKategorier', blank=True) # Field name made lowercase.
    webshopgrpsort = models.CharField(max_length=8, db_column='WebshopGrpSort', blank=True) # Field name made lowercase.
    webshopdibsimportkonto = models.IntegerField(null=True, db_column='WebshopDibsimportkonto', blank=True) # Field name made lowercase.
    webshopdibssamle = models.CharField(max_length=1, db_column='WebshopDibsSamle', blank=True) # Field name made lowercase.
    webshopdibskladdenr = models.IntegerField(null=True, db_column='WebshopDibsKladdeNr', blank=True) # Field name made lowercase.
    sqlvalgfield = models.IntegerField(null=True, db_column='SQLValgField', blank=True) # Field name made lowercase.
    anvendkassesalg = models.IntegerField(null=True, db_column='AnvendKasseSalg', blank=True) # Field name made lowercase.
    forcefakturachange = models.IntegerField(null=True, db_column='ForceFakturaChange', blank=True) # Field name made lowercase.
    defratiodesign = models.IntegerField(null=True, db_column='DefRatioDesign', blank=True) # Field name made lowercase.
    defratioboner = models.IntegerField(null=True, db_column='DefRatioBoner', blank=True) # Field name made lowercase.
    defdesigntype = models.IntegerField(null=True, db_column='DefDesignType', blank=True) # Field name made lowercase.
    defbontype = models.IntegerField(null=True, db_column='DefBonType', blank=True) # Field name made lowercase.
    defratioheight = models.IntegerField(null=True, db_column='DefRatioHeight', blank=True) # Field name made lowercase.
    skrabetvarekartotek = models.CharField(max_length=1, db_column='SkrabetVareKartotek', blank=True) # Field name made lowercase.
    udskrivegetnr = models.CharField(max_length=1, db_column='UdskrivEgetNr', blank=True) # Field name made lowercase.
    udskrivserienr = models.CharField(max_length=1, db_column='UdskrivSerieNr', blank=True) # Field name made lowercase.
    autoklaregetnr = models.CharField(max_length=1, db_column='AutoKlarEgetNr', blank=True) # Field name made lowercase.
    defsimlock = models.CharField(max_length=1, db_column='DefSimLock', blank=True) # Field name made lowercase.
    advarvarenr = models.IntegerField(null=True, db_column='AdvarVareNr', blank=True) # Field name made lowercase.
    egetnr = models.IntegerField(null=True, db_column='EgetNr', blank=True) # Field name made lowercase.
    labelegetnr = models.IntegerField(null=True, db_column='LabelEgetNr', blank=True) # Field name made lowercase.
    labeltype = models.CharField(max_length=20, db_column='LabelType', blank=True) # Field name made lowercase.
    labelunique = models.IntegerField(null=True, db_column='LabelUnique', blank=True) # Field name made lowercase.
    serienrnavn = models.CharField(max_length=32, db_column='SerieNrNavn', blank=True) # Field name made lowercase.
    labelfri1 = models.CharField(max_length=32, db_column='LabelFri1', blank=True) # Field name made lowercase.
    labelfri2 = models.CharField(max_length=32, db_column='LabelFri2', blank=True) # Field name made lowercase.
    labelfri3 = models.CharField(max_length=32, db_column='LabelFri3', blank=True) # Field name made lowercase.
    labelfri4 = models.CharField(max_length=32, db_column='LabelFri4', blank=True) # Field name made lowercase.
    labelfri5 = models.CharField(max_length=32, db_column='LabelFri5', blank=True) # Field name made lowercase.
    labelfri6 = models.CharField(max_length=32, db_column='LabelFri6', blank=True) # Field name made lowercase.
    labelfri7 = models.CharField(max_length=32, db_column='LabelFri7', blank=True) # Field name made lowercase.
    labelfri8 = models.CharField(max_length=32, db_column='LabelFri8', blank=True) # Field name made lowercase.
    labelfri9 = models.CharField(max_length=32, db_column='LabelFri9', blank=True) # Field name made lowercase.
    checkfri1 = models.CharField(max_length=1, db_column='CheckFri1', blank=True) # Field name made lowercase.
    checkfri2 = models.CharField(max_length=1, db_column='CheckFri2', blank=True) # Field name made lowercase.
    checkfri3 = models.CharField(max_length=1, db_column='CheckFri3', blank=True) # Field name made lowercase.
    checkfri4 = models.CharField(max_length=1, db_column='CheckFri4', blank=True) # Field name made lowercase.
    checkfri5 = models.CharField(max_length=1, db_column='CheckFri5', blank=True) # Field name made lowercase.
    checkfri6 = models.CharField(max_length=1, db_column='CheckFri6', blank=True) # Field name made lowercase.
    checkfri7 = models.CharField(max_length=1, db_column='CheckFri7', blank=True) # Field name made lowercase.
    checkfri8 = models.CharField(max_length=1, db_column='CheckFri8', blank=True) # Field name made lowercase.
    checkfri9 = models.CharField(max_length=1, db_column='CheckFri9', blank=True) # Field name made lowercase.
    fakfri1 = models.CharField(max_length=1, db_column='FakFri1', blank=True) # Field name made lowercase.
    fakfri2 = models.CharField(max_length=1, db_column='FakFri2', blank=True) # Field name made lowercase.
    fakfri3 = models.CharField(max_length=1, db_column='FakFri3', blank=True) # Field name made lowercase.
    fakfri4 = models.CharField(max_length=1, db_column='FakFri4', blank=True) # Field name made lowercase.
    fakfri5 = models.CharField(max_length=1, db_column='FakFri5', blank=True) # Field name made lowercase.
    fakfri6 = models.CharField(max_length=1, db_column='FakFri6', blank=True) # Field name made lowercase.
    fakfri7 = models.CharField(max_length=1, db_column='FakFri7', blank=True) # Field name made lowercase.
    fakfri8 = models.CharField(max_length=1, db_column='FakFri8', blank=True) # Field name made lowercase.
    fakfri9 = models.CharField(max_length=1, db_column='FakFri9', blank=True) # Field name made lowercase.
    viskunsalgspriser = models.IntegerField(null=True, db_column='VisKunSalgsPriser', blank=True) # Field name made lowercase.
    sortmodsat = models.IntegerField(null=True, db_column='SortModsat', blank=True) # Field name made lowercase.
    viscalcfields = models.CharField(max_length=1, db_column='VisCalcFields', blank=True) # Field name made lowercase.
    anvendejprisliste = models.CharField(max_length=1, db_column='AnvendEjPrisListe', blank=True) # Field name made lowercase.
    anvendsogekode = models.CharField(max_length=1, db_column='AnvendSogeKode', blank=True) # Field name made lowercase.
    sogekodenavn = models.CharField(max_length=16, db_column='SogeKodeNavn', blank=True) # Field name made lowercase.
    brugkladdenr = models.IntegerField(null=True, db_column='BrugKladdeNr', blank=True) # Field name made lowercase.
    brugkladdenrvarekob = models.IntegerField(null=True, db_column='BrugKladdeNrVareKob', blank=True) # Field name made lowercase.
    integrationfinans = models.IntegerField(null=True, db_column='IntegrationFinans', blank=True) # Field name made lowercase.
    brugkladdenrlager = models.IntegerField(null=True, db_column='BrugKladdeNrLager', blank=True) # Field name made lowercase.
    brugkladdenrregulering = models.IntegerField(null=True, db_column='BrugKladdeNrRegulering', blank=True) # Field name made lowercase.
    primolagerbogfsomsvind = models.IntegerField(null=True, db_column='PrimoLagerBogfSomSvind', blank=True) # Field name made lowercase.
    ejgenbrugordrenr = models.CharField(max_length=1, db_column='EjGenbrugOrdreNr', blank=True) # Field name made lowercase.
    fakturanerordre = models.CharField(max_length=1, db_column='Fakturanerordre', blank=True) # Field name made lowercase.
    tilskud1navn = models.CharField(max_length=16, db_column='Tilskud1Navn', blank=True) # Field name made lowercase.
    tilskud2navn = models.CharField(max_length=16, db_column='Tilskud2Navn', blank=True) # Field name made lowercase.
    tilskud3navn = models.CharField(max_length=16, db_column='Tilskud3Navn', blank=True) # Field name made lowercase.
    tilskud4navn = models.CharField(max_length=16, db_column='Tilskud4Navn', blank=True) # Field name made lowercase.
    tilskud5navn = models.CharField(max_length=16, db_column='Tilskud5Navn', blank=True) # Field name made lowercase.
    tilskud6navn = models.CharField(max_length=16, db_column='Tilskud6Navn', blank=True) # Field name made lowercase.
    labeldesign = models.CharField(max_length=12, db_column='Labeldesign', blank=True) # Field name made lowercase.
    cap = models.CharField(max_length=10, db_column='CAP', blank=True) # Field name made lowercase.
    splitdebpost = models.IntegerField(null=True, db_column='SplitDebpost', blank=True) # Field name made lowercase.
    fragtkode = models.IntegerField(null=True, db_column='Fragtkode', blank=True) # Field name made lowercase.
    anvendfragt = models.CharField(max_length=1, db_column='AnvendFragt', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpsf'

class Tilptilslutning(models.Model):
    inetforsidetekst = models.TextField(db_column='INetForsideTekst', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilptilslutning'

class Tilpweb(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    filedelete = models.CharField(max_length=1, db_column='FileDelete', blank=True) # Field name made lowercase.
    autotreat = models.CharField(max_length=1, db_column='AutoTreat', blank=True) # Field name made lowercase.
    cashcust = models.CharField(max_length=1, db_column='CashCust', blank=True) # Field name made lowercase.
    useorderno = models.CharField(max_length=1, db_column='UseOrderNo', blank=True) # Field name made lowercase.
    seekfield = models.CharField(max_length=8, db_column='SeekField', blank=True) # Field name made lowercase.
    navn1 = models.CharField(max_length=10, db_column='NAVN1', blank=True) # Field name made lowercase.
    navn2 = models.CharField(max_length=10, db_column='NAVN2', blank=True) # Field name made lowercase.
    adr1 = models.CharField(max_length=10, db_column='ADR1', blank=True) # Field name made lowercase.
    adr2 = models.CharField(max_length=10, db_column='ADR2', blank=True) # Field name made lowercase.
    postnr = models.CharField(max_length=10, db_column='POSTNR', blank=True) # Field name made lowercase.
    city = models.CharField(max_length=10, db_column='CITY', blank=True) # Field name made lowercase.
    tlf1 = models.CharField(max_length=10, db_column='TLF1', blank=True) # Field name made lowercase.
    tlf2 = models.CharField(max_length=10, db_column='TLF2', blank=True) # Field name made lowercase.
    email = models.CharField(max_length=10, db_column='EMAIL', blank=True) # Field name made lowercase.
    betmet = models.CharField(max_length=10, db_column='BETMET', blank=True) # Field name made lowercase.
    komtar = models.CharField(max_length=10, db_column='KOMTAR', blank=True) # Field name made lowercase.
    levmet = models.CharField(max_length=10, db_column='LEVMET', blank=True) # Field name made lowercase.
    levdat = models.CharField(max_length=10, db_column='LEVDAT', blank=True) # Field name made lowercase.
    afhent = models.CharField(max_length=10, db_column='AFHENT', blank=True) # Field name made lowercase.
    fri1 = models.CharField(max_length=10, db_column='FRI1', blank=True) # Field name made lowercase.
    fri2 = models.CharField(max_length=10, db_column='FRI2', blank=True) # Field name made lowercase.
    fri3 = models.CharField(max_length=10, db_column='FRI3', blank=True) # Field name made lowercase.
    fri4 = models.CharField(max_length=10, db_column='FRI4', blank=True) # Field name made lowercase.
    fri5 = models.CharField(max_length=10, db_column='FRI5', blank=True) # Field name made lowercase.
    fri6 = models.CharField(max_length=10, db_column='FRI6', blank=True) # Field name made lowercase.
    fri7 = models.CharField(max_length=10, db_column='FRI7', blank=True) # Field name made lowercase.
    fri8 = models.CharField(max_length=10, db_column='FRI8', blank=True) # Field name made lowercase.
    fri9 = models.CharField(max_length=10, db_column='FRI9', blank=True) # Field name made lowercase.
    fri10 = models.CharField(max_length=10, db_column='FRI10', blank=True) # Field name made lowercase.
    inclmoms = models.CharField(max_length=1, db_column='InclMoms', blank=True) # Field name made lowercase.
    uniquename = models.IntegerField(null=True, db_column='UniqueName', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpweb'

class Tilpwebshop(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    dibsbankkonto = models.IntegerField(null=True, db_column='DibsBankKonto', blank=True) # Field name made lowercase.
    dibskladdenr = models.IntegerField(null=True, db_column='DibsKladdenr', blank=True) # Field name made lowercase.
    dibssamle = models.CharField(max_length=1, db_column='DibsSamle', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tilpwebshop'

class Timeopg(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=24, db_column='Navn', blank=True) # Field name made lowercase.
    lonkode = models.IntegerField(null=True, db_column='Lonkode', blank=True) # Field name made lowercase.
    vareflag = models.CharField(max_length=1, db_column='VareFlag', blank=True) # Field name made lowercase.
    enprmedarb = models.CharField(max_length=1, db_column='EnPrMedarb', blank=True) # Field name made lowercase.
    longivende = models.CharField(max_length=1, db_column='LonGivende', blank=True) # Field name made lowercase.
    picindex = models.IntegerField(null=True, db_column='PicIndex', blank=True) # Field name made lowercase.
    brugnote = models.IntegerField(null=True, db_column='BrugNote', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'timeopg'

class Timereg(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    medarb = models.IntegerField(null=True, db_column='Medarb', blank=True) # Field name made lowercase.
    uniquetype = models.CharField(max_length=1, db_column='UniqueType', blank=True) # Field name made lowercase.
    linkunique = models.IntegerField(null=True, db_column='LinkUnique', blank=True) # Field name made lowercase.
    opgaveunique = models.IntegerField(null=True, db_column='OpgaveUnique', blank=True) # Field name made lowercase.
    note = models.TextField(db_column='Note', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'timereg'

class Timeregl(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    timeregunique = models.IntegerField(null=True, db_column='TimeRegUnique', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    tidsforbrug = models.IntegerField(null=True, db_column='TidsForbrug', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'timeregl'

class Tmbestil(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    ordrenummer = models.IntegerField(null=True, db_column='OrdreNummer', blank=True) # Field name made lowercase.
    medarbejder = models.CharField(max_length=32, db_column='Medarbejder', blank=True) # Field name made lowercase.
    kobsnummer = models.CharField(max_length=32, db_column='Kobsnummer', blank=True) # Field name made lowercase.
    kobsnavn = models.CharField(max_length=32, db_column='Kobsnavn', blank=True) # Field name made lowercase.
    antal = models.IntegerField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    bestilt = models.CharField(max_length=16, db_column='Bestilt', blank=True) # Field name made lowercase.
    kobsenhed = models.CharField(max_length=16, db_column='KobsEnhed', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tmbestil'

class Tmfaerdig(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    ordrenummer = models.CharField(max_length=8, db_column='OrdreNummer', blank=True) # Field name made lowercase.
    faerdig = models.CharField(max_length=4, db_column='Faerdig', blank=True) # Field name made lowercase.
    beskrivelse = models.CharField(max_length=250, db_column='Beskrivelse', blank=True) # Field name made lowercase.
    ordredato = models.CharField(max_length=40, db_column='OrdreDato', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tmfaerdig'

class Tmhalvfabrika(models.Model):
    sfhunique = models.IntegerField(unique=True, null=True, db_column='SFHUnique', blank=True) # Field name made lowercase.
    tekst = models.CharField(max_length=64, db_column='Tekst', blank=True) # Field name made lowercase.
    udfoert = models.CharField(max_length=16, db_column='Udfoert', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'tmhalvfabrika'

class Todo(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    oprettet = models.DateTimeField(null=True, db_column='Oprettet', blank=True) # Field name made lowercase.
    udlob = models.DateTimeField(null=True, db_column='Udlob', blank=True) # Field name made lowercase.
    udtime = models.DateTimeField(null=True, db_column='UdTime', blank=True) # Field name made lowercase.
    formedarb = models.IntegerField(null=True, db_column='ForMedarb', blank=True) # Field name made lowercase.
    afmedarb = models.CharField(max_length=50, db_column='AfMedarb', blank=True) # Field name made lowercase.
    sfhunique = models.IntegerField(null=True, db_column='SFHUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    emne = models.CharField(max_length=50, db_column='Emne', blank=True) # Field name made lowercase.
    markdone = models.CharField(max_length=1, db_column='MarkDone', blank=True) # Field name made lowercase.
    note = models.TextField(db_column='Note', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'todo'

class Udligning(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    udlignespos = models.IntegerField(null=True, db_column='Udlignespos', blank=True) # Field name made lowercase.
    udlignesfunique = models.IntegerField(null=True, db_column='UdlignesFUnique', blank=True) # Field name made lowercase.
    udlignetmedpos = models.IntegerField(null=True, db_column='UdlignetmedPos', blank=True) # Field name made lowercase.
    udlignetmedfunique = models.IntegerField(null=True, db_column='UdlignetmedFUnique', blank=True) # Field name made lowercase.
    udlignetkr = models.FloatField(null=True, db_column='UdlignetKr', blank=True) # Field name made lowercase.
    samlefunique = models.IntegerField(null=True, db_column='SamleFUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'udligning'

class Udsnits(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    wpmodule = models.IntegerField(null=True, db_column='WPModule', blank=True) # Field name made lowercase.
    sqlrapport = models.IntegerField(null=True, db_column='SQLRapport', blank=True) # Field name made lowercase.
    fcaption = models.CharField(max_length=32, db_column='FCaption', blank=True) # Field name made lowercase.
    fwhere = models.CharField(max_length=255, db_column='FWhere', blank=True) # Field name made lowercase.
    fwhere1 = models.CharField(max_length=255, db_column='FWhere1', blank=True) # Field name made lowercase.
    fwhere2 = models.CharField(max_length=255, db_column='FWhere2', blank=True) # Field name made lowercase.
    fwhere3 = models.CharField(max_length=255, db_column='FWhere3', blank=True) # Field name made lowercase.
    fwhere4 = models.CharField(max_length=255, db_column='FWhere4', blank=True) # Field name made lowercase.
    forderby = models.CharField(max_length=32, db_column='FOrderBy', blank=True) # Field name made lowercase.
    fnonduplicates = models.CharField(max_length=1, db_column='FNonDuplicates', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'udsnits'

class Valuta(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    land = models.CharField(max_length=30, db_column='Land', blank=True) # Field name made lowercase.
    kurs = models.FloatField(null=True, db_column='Kurs', blank=True) # Field name made lowercase.
    omregnbidragsats = models.FloatField(null=True, db_column='OmregnBidragSats', blank=True) # Field name made lowercase.
    symbol = models.CharField(max_length=8, db_column='Symbol', blank=True) # Field name made lowercase.
    kursregkonto = models.IntegerField(null=True, db_column='KursRegKonto', blank=True) # Field name made lowercase.
    salggebyrkonto = models.IntegerField(null=True, db_column='SalgGebyrKonto', blank=True) # Field name made lowercase.
    color = models.IntegerField(null=True, db_column='Color', blank=True) # Field name made lowercase.
    bankkode = models.CharField(max_length=3, db_column='BankKode', blank=True) # Field name made lowercase.
    webshop = models.CharField(max_length=1, db_column='Webshop', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'valuta'

class Webfelt(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fid = models.IntegerField(null=True, db_column='FID', blank=True) # Field name made lowercase.
    fornavn = models.CharField(max_length=10, db_column='FORNAVN', blank=True) # Field name made lowercase.
    efternavn = models.CharField(max_length=10, db_column='EFTERNAVN', blank=True) # Field name made lowercase.
    adresse1 = models.CharField(max_length=10, db_column='ADRESSE1', blank=True) # Field name made lowercase.
    adresse2 = models.CharField(max_length=10, db_column='ADRESSE2', blank=True) # Field name made lowercase.
    postnr = models.CharField(max_length=10, db_column='POSTNR', blank=True) # Field name made lowercase.
    bynavn = models.CharField(max_length=10, db_column='BYNAVN', blank=True) # Field name made lowercase.
    username = models.CharField(max_length=10, db_column='USERNAME', blank=True) # Field name made lowercase.
    password = models.CharField(max_length=10, db_column='PASSWORD', blank=True) # Field name made lowercase.
    super = models.CharField(max_length=10, db_column='SUPER', blank=True) # Field name made lowercase.
    aktiv = models.CharField(max_length=10, db_column='AKTIV', blank=True) # Field name made lowercase.
    telefon = models.CharField(max_length=10, db_column='TELEFON', blank=True) # Field name made lowercase.
    email = models.CharField(max_length=10, db_column='EMAIL', blank=True) # Field name made lowercase.
    hold = models.CharField(max_length=10, db_column='HOLD', blank=True) # Field name made lowercase.
    fodselsdag = models.CharField(max_length=10, db_column='FODSELSDAG', blank=True) # Field name made lowercase.
    mobil = models.CharField(max_length=10, db_column='MOBIL', blank=True) # Field name made lowercase.
    note = models.CharField(max_length=255, db_column='NOTE', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webfelt'

class Websfh(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    ordrenummer = models.IntegerField(null=True, db_column='OrdreNummer', blank=True) # Field name made lowercase.
    fakturanummer = models.IntegerField(null=True, db_column='FakturaNummer', blank=True) # Field name made lowercase.
    salgerid = models.IntegerField(null=True, db_column='SalgerID', blank=True) # Field name made lowercase.
    sidstrettet = models.DateTimeField(null=True, db_column='SidstRettet', blank=True) # Field name made lowercase.
    rettetaf = models.CharField(max_length=10, db_column='RettetAf', blank=True) # Field name made lowercase.
    pbsleverence = models.IntegerField(null=True, db_column='PBSLeverence', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    samletordre = models.IntegerField(null=True, db_column='Samletordre', blank=True) # Field name made lowercase.
    ordredato = models.DateTimeField(null=True, db_column='OrdreDato', blank=True) # Field name made lowercase.
    fakturadato = models.DateTimeField(null=True, db_column='FakturaDato', blank=True) # Field name made lowercase.
    forfalddato = models.DateTimeField(null=True, db_column='ForfaldDato', blank=True) # Field name made lowercase.
    webshopkortid = models.IntegerField(null=True, db_column='WebshopKortID', blank=True) # Field name made lowercase.
    oprettetwebshop = models.IntegerField(null=True, db_column='OprettetWebshop', blank=True) # Field name made lowercase.
    betaltdato = models.DateTimeField(null=True, db_column='BetaltDato', blank=True) # Field name made lowercase.
    behkonto = models.IntegerField(null=True, db_column='BehKonto', blank=True) # Field name made lowercase.
    altomskonto = models.IntegerField(null=True, db_column='AltOmsKonto', blank=True) # Field name made lowercase.
    kontotype = models.CharField(max_length=1, db_column='KontoType', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    fakturadebitor = models.IntegerField(null=True, db_column='FakturaDebitor', blank=True) # Field name made lowercase.
    projektnr = models.IntegerField(null=True, db_column='ProjektNr', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    finadim5 = models.IntegerField(null=True, db_column='FinaDim5', blank=True) # Field name made lowercase.
    transaktion = models.IntegerField(null=True, db_column='Transaktion', blank=True) # Field name made lowercase.
    permanentunique = models.IntegerField(null=True, db_column='PermanentUnique', blank=True) # Field name made lowercase.
    kategori = models.IntegerField(null=True, db_column='Kategori', blank=True) # Field name made lowercase.
    levadr1 = models.CharField(max_length=32, db_column='LevAdr1', blank=True) # Field name made lowercase.
    levadr2 = models.CharField(max_length=32, db_column='LevAdr2', blank=True) # Field name made lowercase.
    levadr3 = models.CharField(max_length=32, db_column='LevAdr3', blank=True) # Field name made lowercase.
    levadr4 = models.CharField(max_length=32, db_column='LevAdr4', blank=True) # Field name made lowercase.
    leveringskonto = models.IntegerField(null=True, db_column='LeveringsKonto', blank=True) # Field name made lowercase.
    levadr5 = models.CharField(max_length=32, db_column='LevAdr5', blank=True) # Field name made lowercase.
    levadr6 = models.CharField(max_length=32, db_column='LevAdr6', blank=True) # Field name made lowercase.
    levadr7 = models.CharField(max_length=32, db_column='LevAdr7', blank=True) # Field name made lowercase.
    levadr8 = models.CharField(max_length=32, db_column='LevAdr8', blank=True) # Field name made lowercase.
    fragtbrev = models.CharField(max_length=10, db_column='FragtBrev', blank=True) # Field name made lowercase.
    fragtcentral = models.CharField(max_length=20, db_column='FragtCentral', blank=True) # Field name made lowercase.
    note1 = models.CharField(max_length=32, db_column='Note1', blank=True) # Field name made lowercase.
    note2 = models.CharField(max_length=32, db_column='Note2', blank=True) # Field name made lowercase.
    betalbet = models.IntegerField(null=True, db_column='BetalBet', blank=True) # Field name made lowercase.
    vorref = models.CharField(max_length=40, db_column='VorRef', blank=True) # Field name made lowercase.
    deresref = models.CharField(max_length=40, db_column='DeresRef', blank=True) # Field name made lowercase.
    deresref2 = models.CharField(max_length=40, db_column='DeresRef2', blank=True) # Field name made lowercase.
    valuta = models.IntegerField(null=True, db_column='Valuta', blank=True) # Field name made lowercase.
    klar = models.CharField(max_length=1, db_column='Klar', blank=True) # Field name made lowercase.
    produktionsstatus = models.IntegerField(null=True, db_column='ProduktionsStatus', blank=True) # Field name made lowercase.
    title = models.CharField(max_length=16, db_column='Title', blank=True) # Field name made lowercase.
    fakturatype = models.IntegerField(null=True, db_column='FakturaType', blank=True) # Field name made lowercase.
    udskrevet = models.CharField(max_length=1, db_column='Udskrevet', blank=True) # Field name made lowercase.
    bogfort = models.CharField(max_length=1, db_column='Bogfort', blank=True) # Field name made lowercase.
    exmoms = models.FloatField(null=True, db_column='ExMoms', blank=True) # Field name made lowercase.
    moms = models.FloatField(null=True, db_column='Moms', blank=True) # Field name made lowercase.
    inmoms = models.FloatField(null=True, db_column='InMoms', blank=True) # Field name made lowercase.
    momsfri = models.FloatField(null=True, db_column='MomsFri', blank=True) # Field name made lowercase.
    sumextilskud = models.FloatField(null=True, db_column='SumExTilskud', blank=True) # Field name made lowercase.
    sumfragt = models.FloatField(null=True, db_column='SumFragt', blank=True) # Field name made lowercase.
    tilskudinmoms = models.FloatField(null=True, db_column='TilskudInMoms', blank=True) # Field name made lowercase.
    tillagsum = models.FloatField(null=True, db_column='TillagSum', blank=True) # Field name made lowercase.
    kpsum = models.FloatField(null=True, db_column='KPSum', blank=True) # Field name made lowercase.
    punktafgift = models.FloatField(null=True, db_column='PunktAfgift', blank=True) # Field name made lowercase.
    db = models.FloatField(null=True, db_column='DB', blank=True) # Field name made lowercase.
    dg = models.FloatField(null=True, db_column='DG', blank=True) # Field name made lowercase.
    totalvaegt = models.FloatField(null=True, db_column='TotalVaegt', blank=True) # Field name made lowercase.
    paabegyndes = models.DateTimeField(null=True, db_column='Paabegyndes', blank=True) # Field name made lowercase.
    perlevrunique = models.IntegerField(null=True, db_column='PerLevrUnique', blank=True) # Field name made lowercase.
    perfakunique = models.IntegerField(null=True, db_column='PerFakUnique', blank=True) # Field name made lowercase.
    pergentag = models.IntegerField(null=True, db_column='PerGentag', blank=True) # Field name made lowercase.
    perudlob = models.DateTimeField(null=True, db_column='PerUdlob', blank=True) # Field name made lowercase.
    perpause = models.IntegerField(null=True, db_column='PerPause', blank=True) # Field name made lowercase.
    prodtid = models.DateTimeField(null=True, db_column='ProdTid', blank=True) # Field name made lowercase.
    prodinfo = models.CharField(max_length=22, db_column='ProdInfo', blank=True) # Field name made lowercase.
    leveringstid = models.DateTimeField(null=True, db_column='Leveringstid', blank=True) # Field name made lowercase.
    leveringsinfo = models.CharField(max_length=22, db_column='LeveringsInfo', blank=True) # Field name made lowercase.
    produktionsinfo = models.TextField(db_column='ProduktionsInfo', blank=True) # Field name made lowercase.
    prodnr = models.IntegerField(null=True, db_column='ProdNr', blank=True) # Field name made lowercase.
    note = models.TextField(db_column='Note', blank=True) # Field name made lowercase.
    ean = models.CharField(max_length=20, db_column='EAN', blank=True) # Field name made lowercase.
    vaerksted = models.CharField(max_length=1, db_column='Vaerksted', blank=True) # Field name made lowercase.
    webshopstatus = models.IntegerField(null=True, db_column='Webshopstatus', blank=True) # Field name made lowercase.
    webtracktrace = models.CharField(max_length=50, db_column='WebTrackTrace', blank=True) # Field name made lowercase.
    webdibstrans = models.CharField(max_length=50, db_column='WebDibsTrans', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'websfh'

class Websfl(models.Model):
    funique = models.IntegerField(null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    sfhunique = models.IntegerField(null=True, db_column='SFHUnique', blank=True) # Field name made lowercase.
    sfhperfakunique = models.IntegerField(null=True, db_column='SFHPerFakUnique', blank=True) # Field name made lowercase.
    importkode = models.IntegerField(null=True, db_column='ImportKode', blank=True) # Field name made lowercase.
    linietype = models.IntegerField(null=True, db_column='LinieType', blank=True) # Field name made lowercase.
    omskonto = models.IntegerField(null=True, db_column='OmsKonto', blank=True) # Field name made lowercase.
    afdeling = models.IntegerField(null=True, db_column='Afdeling', blank=True) # Field name made lowercase.
    momskode = models.IntegerField(null=True, db_column='MomsKode', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    sfvdimgruppe = models.IntegerField(null=True, db_column='SFVDimGruppe', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=72, db_column='VareTekst', blank=True) # Field name made lowercase.
    salgsenhed = models.CharField(max_length=12, db_column='SalgsEnhed', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    salgspris = models.FloatField(null=True, db_column='SalgsPris', blank=True) # Field name made lowercase.
    salgseimoms = models.IntegerField(null=True, db_column='SalgsEIMoms', blank=True) # Field name made lowercase.
    tilskud = models.FloatField(null=True, db_column='Tilskud', blank=True) # Field name made lowercase.
    tilskudtype = models.CharField(max_length=1, db_column='TilskudType', blank=True) # Field name made lowercase.
    tilskudkonto = models.IntegerField(null=True, db_column='TilskudKonto', blank=True) # Field name made lowercase.
    fragtkode = models.IntegerField(null=True, db_column='FragtKode', blank=True) # Field name made lowercase.
    kostpris = models.FloatField(null=True, db_column='KostPris', blank=True) # Field name made lowercase.
    lagerkonto = models.IntegerField(null=True, db_column='LagerKonto', blank=True) # Field name made lowercase.
    vareforbrug = models.IntegerField(null=True, db_column='Vareforbrug', blank=True) # Field name made lowercase.
    punktafgift = models.FloatField(null=True, db_column='PunktAfgift', blank=True) # Field name made lowercase.
    avancedel = models.FloatField(null=True, db_column='AvanceDel', blank=True) # Field name made lowercase.
    kosteimoms = models.IntegerField(null=True, db_column='KostEIMoms', blank=True) # Field name made lowercase.
    rabatkr = models.CharField(max_length=1, db_column='RabatKr', blank=True) # Field name made lowercase.
    rabat = models.FloatField(null=True, db_column='Rabat', blank=True) # Field name made lowercase.
    provision = models.FloatField(null=True, db_column='Provision', blank=True) # Field name made lowercase.
    momssum = models.FloatField(null=True, db_column='MomsSum', blank=True) # Field name made lowercase.
    liniesum = models.FloatField(null=True, db_column='LinieSum', blank=True) # Field name made lowercase.
    tilskudkrsum = models.FloatField(null=True, db_column='TilskudKrSum', blank=True) # Field name made lowercase.
    liniesumextilskud = models.FloatField(null=True, db_column='LinieSumExTilskud', blank=True) # Field name made lowercase.
    tillagpct = models.FloatField(null=True, db_column='TillagPct', blank=True) # Field name made lowercase.
    tillagsum = models.FloatField(null=True, db_column='TillagSum', blank=True) # Field name made lowercase.
    punktafgiftsum = models.FloatField(null=True, db_column='PunktAfgiftSum', blank=True) # Field name made lowercase.
    levrdato = models.DateTimeField(null=True, db_column='LevrDato', blank=True) # Field name made lowercase.
    fakdato = models.DateTimeField(null=True, db_column='FakDato', blank=True) # Field name made lowercase.
    sflunique = models.IntegerField(null=True, db_column='SFLUnique', blank=True) # Field name made lowercase.
    arbkortunique = models.IntegerField(null=True, db_column='ArbkortUnique', blank=True) # Field name made lowercase.
    tilskudunique = models.IntegerField(null=True, db_column='TilskudUnique', blank=True) # Field name made lowercase.
    stykserienr = models.IntegerField(null=True, db_column='StykSerieNr', blank=True) # Field name made lowercase.
    serienrretur = models.CharField(max_length=32, db_column='SerieNrRetur', blank=True) # Field name made lowercase.
    stykvarenr = models.IntegerField(null=True, db_column='StykVareNr', blank=True) # Field name made lowercase.
    kontraktnr = models.IntegerField(null=True, db_column='KontraktNr', blank=True) # Field name made lowercase.
    udlob = models.DateTimeField(null=True, db_column='Udlob', blank=True) # Field name made lowercase.
    sogekode = models.CharField(max_length=10, db_column='SogeKode', blank=True) # Field name made lowercase.
    filnavn = models.CharField(max_length=12, db_column='FilNavn', blank=True) # Field name made lowercase.
    vareprint = models.CharField(max_length=12, db_column='VarePrint', blank=True) # Field name made lowercase.
    printantal = models.CharField(max_length=1, db_column='PrintAntal', blank=True) # Field name made lowercase.
    vaegt = models.FloatField(null=True, db_column='Vaegt', blank=True) # Field name made lowercase.
    vaegttype = models.IntegerField(null=True, db_column='VaegtType', blank=True) # Field name made lowercase.
    lagersted = models.IntegerField(null=True, db_column='Lagersted', blank=True) # Field name made lowercase.
    statistikkode = models.IntegerField(null=True, db_column='StatistikKode', blank=True) # Field name made lowercase.
    liniekode = models.IntegerField(null=True, db_column='LinieKode', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'websfl'

class Webshopbasket(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    sessionid = models.CharField(max_length=30, db_column='SessionId', blank=True) # Field name made lowercase.
    sfvkart = models.IntegerField(null=True, db_column='SFVKart', blank=True) # Field name made lowercase.
    antal = models.IntegerField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    exmoms = models.FloatField(null=True, db_column='ExMoms', blank=True) # Field name made lowercase.
    moms = models.FloatField(null=True, db_column='Moms', blank=True) # Field name made lowercase.
    inmoms = models.FloatField(null=True, db_column='InMoms', blank=True) # Field name made lowercase.
    momsfri = models.FloatField(null=True, db_column='MomsFri', blank=True) # Field name made lowercase.
    note = models.CharField(max_length=255, db_column='NOTE', blank=True) # Field name made lowercase.
    status = models.IntegerField(null=True, db_column='Status', blank=True) # Field name made lowercase.
    rabat = models.FloatField(null=True, db_column='Rabat', blank=True) # Field name made lowercase.
    dim1 = models.IntegerField(null=True, db_column='Dim1', blank=True) # Field name made lowercase.
    dim2 = models.IntegerField(null=True, db_column='Dim2', blank=True) # Field name made lowercase.
    dim3 = models.IntegerField(null=True, db_column='Dim3', blank=True) # Field name made lowercase.
    tilpbbunique = models.IntegerField(null=True, db_column='TilpbbUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopbasket'

class Webshopfelter(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    feltnavn = models.CharField(max_length=24, db_column='Feltnavn', blank=True) # Field name made lowercase.
    webetiket = models.CharField(max_length=100, db_column='Webetiket', blank=True) # Field name made lowercase.
    forklaring = models.TextField(db_column='Forklaring', blank=True) # Field name made lowercase.
    vissom = models.IntegerField(null=True, db_column='Vissom', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopfelter'

class Webshopfeltregeldata(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    regelunique = models.IntegerField(null=True, db_column='RegelUnique', blank=True) # Field name made lowercase.
    data = models.CharField(max_length=100, db_column='Data', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopfeltregeldata'

class Webshopfeltregler(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    feltunique = models.IntegerField(null=True, db_column='FeltUnique', blank=True) # Field name made lowercase.
    regeltype = models.IntegerField(null=True, db_column='RegelType', blank=True) # Field name made lowercase.
    meddelelse = models.TextField(db_column='Meddelelse', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopfeltregler'

class Webshopfeltvaerdier(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    feltunique = models.IntegerField(null=True, db_column='FeltUnique', blank=True) # Field name made lowercase.
    janej = models.IntegerField(null=True, db_column='JaNej', blank=True) # Field name made lowercase.
    tekst = models.CharField(max_length=100, db_column='Tekst', blank=True) # Field name made lowercase.
    vaerdi = models.CharField(max_length=100, db_column='Vaerdi', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopfeltvaerdier'

class Webshopfields(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    hunique = models.IntegerField(null=True, db_column='HUnique', blank=True) # Field name made lowercase.
    ftable = models.CharField(max_length=20, db_column='FTable', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    ffield = models.CharField(max_length=20, db_column='FField', blank=True) # Field name made lowercase.
    fdisplayname = models.CharField(max_length=20, db_column='FDisplayName', blank=True) # Field name made lowercase.
    fshow = models.IntegerField(null=True, db_column='FShow', blank=True) # Field name made lowercase.
    fedit = models.IntegerField(null=True, db_column='FEdit', blank=True) # Field name made lowercase.
    frequired = models.IntegerField(null=True, db_column='FRequired', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopfields'

class Webshopfieldshead(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=32, db_column='Navn', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    tilladtilbud = models.IntegerField(null=True, db_column='TilladTilbud', blank=True) # Field name made lowercase.
    tilladordre = models.IntegerField(null=True, db_column='TilladOrdre', blank=True) # Field name made lowercase.
    tilladpermanent = models.IntegerField(null=True, db_column='TilladPermanent', blank=True) # Field name made lowercase.
    klarmeld = models.IntegerField(null=True, db_column='Klarmeld', blank=True) # Field name made lowercase.
    anvendemail = models.IntegerField(null=True, db_column='AnvendEmail', blank=True) # Field name made lowercase.
    tilladandenbetaler = models.IntegerField(null=True, db_column='TilladAndenBetaler', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopfieldshead'

class Webshopmenu(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    ffunique = models.IntegerField(null=True, db_column='FFUnique', blank=True) # Field name made lowercase.
    etiket = models.CharField(max_length=255, db_column='Etiket', blank=True) # Field name made lowercase.
    tiptekst = models.CharField(max_length=255, db_column='Tiptekst', blank=True) # Field name made lowercase.
    standardmenuindeks = models.IntegerField(null=True, db_column='StandardMenuIndeks', blank=True) # Field name made lowercase.
    sideindeks = models.IntegerField(null=True, db_column='SideIndeks', blank=True) # Field name made lowercase.
    url = models.CharField(max_length=255, db_column='Url', blank=True) # Field name made lowercase.
    aktiv = models.IntegerField(null=True, db_column='Aktiv', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopmenu'

class Webshopnews(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fsfvkartunique = models.IntegerField(null=True, db_column='FSfvkartUnique', blank=True) # Field name made lowercase.
    title = models.CharField(max_length=100, db_column='Title', blank=True) # Field name made lowercase.
    startdate = models.DateTimeField(null=True, db_column='StartDate', blank=True) # Field name made lowercase.
    enddate = models.DateTimeField(null=True, db_column='EndDate', blank=True) # Field name made lowercase.
    showimage = models.TextField(db_column='ShowImage', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopnews'

class Webshoppage(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    pagetitle = models.CharField(max_length=50, db_column='PageTitle', blank=True) # Field name made lowercase.
    bgcolor = models.CharField(max_length=10, db_column='BgColor', blank=True) # Field name made lowercase.
    bgimage = models.CharField(max_length=50, db_column='BgImage', blank=True) # Field name made lowercase.
    tablealign = models.CharField(max_length=10, db_column='TableAlign', blank=True) # Field name made lowercase.
    tablewidth = models.IntegerField(null=True, db_column='TableWidth', blank=True) # Field name made lowercase.
    headerheight = models.IntegerField(null=True, db_column='HeaderHeight', blank=True) # Field name made lowercase.
    headerbgcolor = models.CharField(max_length=10, db_column='HeaderBgColor', blank=True) # Field name made lowercase.
    headerbgimage = models.CharField(max_length=50, db_column='HeaderBgImage', blank=True) # Field name made lowercase.
    headerlowheight = models.IntegerField(null=True, db_column='HeaderLowHeight', blank=True) # Field name made lowercase.
    headerlowbgcolor = models.CharField(max_length=10, db_column='HeaderLowBgColor', blank=True) # Field name made lowercase.
    headerlowbgimage = models.CharField(max_length=50, db_column='HeaderLowBgImage', blank=True) # Field name made lowercase.
    menuwidth = models.IntegerField(null=True, db_column='MenuWidth', blank=True) # Field name made lowercase.
    menubgcolor = models.CharField(max_length=10, db_column='MenuBgColor', blank=True) # Field name made lowercase.
    menubgimage = models.CharField(max_length=50, db_column='MenuBgImage', blank=True) # Field name made lowercase.
    menuheaderbgcolor = models.CharField(max_length=10, db_column='MenuHeaderBgColor', blank=True) # Field name made lowercase.
    menuheadercolor = models.CharField(max_length=10, db_column='MenuHeaderColor', blank=True) # Field name made lowercase.
    mainbgcolor = models.CharField(max_length=10, db_column='MainBgColor', blank=True) # Field name made lowercase.
    mainbgimage = models.CharField(max_length=50, db_column='MainBgImage', blank=True) # Field name made lowercase.
    rightwidth = models.IntegerField(null=True, db_column='RightWidth', blank=True) # Field name made lowercase.
    rightbgcolor = models.CharField(max_length=10, db_column='RightBgColor', blank=True) # Field name made lowercase.
    rightbgimage = models.CharField(max_length=50, db_column='RightBgImage', blank=True) # Field name made lowercase.
    rightheaderbgcolor = models.CharField(max_length=10, db_column='RightHeaderBgColor', blank=True) # Field name made lowercase.
    rightheadercolor = models.CharField(max_length=10, db_column='RightHeaderColor', blank=True) # Field name made lowercase.
    rightmainbgcolor = models.CharField(max_length=10, db_column='RightMainBgColor', blank=True) # Field name made lowercase.
    rightmaincolor = models.CharField(max_length=10, db_column='RightMainColor', blank=True) # Field name made lowercase.
    footerheight = models.IntegerField(null=True, db_column='FooterHeight', blank=True) # Field name made lowercase.
    footerbgcolor = models.CharField(max_length=10, db_column='FooterBgColor', blank=True) # Field name made lowercase.
    footerbgimage = models.CharField(max_length=50, db_column='FooterBgImage', blank=True) # Field name made lowercase.
    praesentation = models.IntegerField(null=True, db_column='Praesentation', blank=True) # Field name made lowercase.
    menucolor = models.CharField(max_length=10, db_column='Menucolor', blank=True) # Field name made lowercase.
    footercolor = models.CharField(max_length=10, db_column='FooterColor', blank=True) # Field name made lowercase.
    valuta = models.CharField(max_length=10, db_column='Valuta', blank=True) # Field name made lowercase.
    headerlowvagon = models.CharField(max_length=15, db_column='Headerlowvagon', blank=True) # Field name made lowercase.
    mainvagon = models.CharField(max_length=15, db_column='Mainvagon', blank=True) # Field name made lowercase.
    font = models.CharField(max_length=50, db_column='Font', blank=True) # Field name made lowercase.
    varegruppetekst = models.CharField(max_length=15, db_column='VareGruppeTekst', blank=True) # Field name made lowercase.
    ikkepaalagertekst = models.CharField(max_length=25, db_column='IkkePaaLagerTekst', blank=True) # Field name made lowercase.
    visikkemestsolgte = models.IntegerField(null=True, db_column='VisIkkeMestSolgte', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshoppage'

class Webshopreviews(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    vare = models.IntegerField(null=True, db_column='Vare', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    stjerner = models.IntegerField(null=True, db_column='Stjerner', blank=True) # Field name made lowercase.
    tid = models.DateTimeField(null=True, db_column='Tid', blank=True) # Field name made lowercase.
    overskrift = models.CharField(max_length=255, db_column='Overskrift', blank=True) # Field name made lowercase.
    tekst = models.TextField(db_column='Tekst', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopreviews'

class Webshopsider(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    findex = models.IntegerField(null=True, db_column='FIndex', blank=True) # Field name made lowercase.
    title = models.CharField(max_length=100, db_column='Title', blank=True) # Field name made lowercase.
    beskrivelse = models.TextField(db_column='Beskrivelse', blank=True) # Field name made lowercase.
    indeholderhtml = models.CharField(max_length=1, db_column='IndeholderHtml', blank=True) # Field name made lowercase.
    forside = models.CharField(max_length=1, db_column='Forside', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopsider'

class Webshopwebstatus(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=50, db_column='Navn', blank=True) # Field name made lowercase.
    sortering = models.IntegerField(null=True, db_column='Sortering', blank=True) # Field name made lowercase.
    sendkunde = models.CharField(max_length=1, db_column='SendKunde', blank=True) # Field name made lowercase.
    sendanden = models.CharField(max_length=1, db_column='SendAnden', blank=True) # Field name made lowercase.
    mailadranden = models.CharField(max_length=50, db_column='MailadrAnden', blank=True) # Field name made lowercase.
    kundeemne = models.CharField(max_length=50, db_column='Kundeemne', blank=True) # Field name made lowercase.
    andenemne = models.CharField(max_length=50, db_column='Andenemne', blank=True) # Field name made lowercase.
    kundemail = models.TextField(db_column='Kundemail', blank=True) # Field name made lowercase.
    andenmail = models.TextField(db_column='AndenMail', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webshopwebstatus'

class Webstat(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    debitor = models.IntegerField(null=True, db_column='Debitor', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    tid = models.TextField(db_column='Tid', blank=True) # Field name made lowercase. This field type is a guess.
    afvist = models.CharField(max_length=1, db_column='Afvist', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'webstat'

class Webstatshop(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    dato = models.DateTimeField(null=True, db_column='Dato', blank=True) # Field name made lowercase.
    tid = models.TextField(db_column='Tid', blank=True) # Field name made lowercase. This field type is a guess.
    sessionid = models.CharField(max_length=50, db_column='Sessionid', blank=True) # Field name made lowercase.
    page = models.CharField(max_length=50, db_column='Page', blank=True) # Field name made lowercase.
    vare = models.IntegerField(null=True, db_column='Vare', blank=True) # Field name made lowercase.
    referrer = models.CharField(max_length=255, blank=True)
    class Meta:
        db_table = 'webstatshop'

class Winkas(models.Model):
    dbversion = models.DateTimeField(null=True, db_column='DBVersion', blank=True) # Field name made lowercase.
    etiketter = models.CharField(max_length=4, db_column='Etiketter', blank=True) # Field name made lowercase.
    afslutaaret = models.CharField(max_length=1, db_column='AfslutAaret', blank=True) # Field name made lowercase.
    bogforkladde = models.CharField(max_length=1, db_column='BogforKladde', blank=True) # Field name made lowercase.
    visejkomigang = models.CharField(max_length=1, db_column='VisEjKomIGang', blank=True) # Field name made lowercase.
    xsfvkart = models.IntegerField(null=True, blank=True)
    lastbackup = models.DateTimeField(null=True, db_column='LastBackup', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'winkas'

class Xpck4(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fmasterunique = models.IntegerField(null=True, db_column='FMasterUnique', blank=True) # Field name made lowercase.
    pckunique = models.IntegerField(null=True, db_column='PCKUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'xpck4'

class Xudsnits(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fmasterunique = models.IntegerField(null=True, db_column='FMasterUnique', blank=True) # Field name made lowercase.
    fudsnitunique = models.IntegerField(null=True, db_column='FUdsnitUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'xudsnits'

class XudsnitsCopy(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fmasterunique = models.IntegerField(null=True, db_column='FMasterUnique', blank=True) # Field name made lowercase.
    fudsnitunique = models.IntegerField(null=True, db_column='FUdsnitUnique', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'xudsnits_copy'

class Ydelser(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    kode = models.CharField(max_length=8, db_column='Kode', blank=True) # Field name made lowercase.
    navn = models.CharField(max_length=50, db_column='Navn', blank=True) # Field name made lowercase.
    fakturatekst = models.CharField(max_length=50, db_column='FakturaTekst', blank=True) # Field name made lowercase.
    engangs = models.IntegerField(null=True, db_column='Engangs', blank=True) # Field name made lowercase.
    engangspris = models.FloatField(null=True, db_column='EngangsPris', blank=True) # Field name made lowercase.
    prisangives = models.IntegerField(null=True, db_column='PrisAngives', blank=True) # Field name made lowercase.
    kategori = models.CharField(max_length=8, db_column='Kategori', blank=True) # Field name made lowercase.
    medtaginet = models.IntegerField(null=True, db_column='MedtagINet', blank=True) # Field name made lowercase.
    provision = models.FloatField(null=True, db_column='Provision', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'ydelser'

class Ydlinie(models.Model):
    funique = models.IntegerField(unique=True, null=True, db_column='FUnique', blank=True) # Field name made lowercase.
    fydunique = models.IntegerField(null=True, db_column='FYdUnique', blank=True) # Field name made lowercase.
    varenr = models.CharField(max_length=18, db_column='VareNr', blank=True) # Field name made lowercase.
    varetype = models.CharField(max_length=1, db_column='VareType', blank=True) # Field name made lowercase.
    vareunique = models.IntegerField(null=True, db_column='VareUnique', blank=True) # Field name made lowercase.
    varetekst = models.CharField(max_length=72, db_column='VareTekst', blank=True) # Field name made lowercase.
    antal = models.FloatField(null=True, db_column='Antal', blank=True) # Field name made lowercase.
    pris = models.FloatField(null=True, db_column='Pris', blank=True) # Field name made lowercase.
    afregnet = models.CharField(max_length=1, db_column='Afregnet', blank=True) # Field name made lowercase.
    class Meta:
        db_table = 'ydlinie'

