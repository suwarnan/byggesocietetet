from django.contrib import admin

from winkas.models import *

class MemberAdmin(admin.ModelAdmin):
    def user_active(self, obj):
        return obj.active == "Y"
    user_active.boolean = True

    list_display = ('company', 'name', 'email', 'phone', 'mobile', 'city', 'created', 'user_active')
    list_display_links = ('company', 'name')
    search_fields = ('company', 'name',)
    list_filter = ("active",)
admin.site.register(Member, MemberAdmin)
