# -*- coding: utf-8 -*-
from django.db import models

class Member(models.Model):
    id = models.AutoField(primary_key=True, unique=True, db_column='Felt00', blank=True)
    company = models.CharField(max_length=35, db_column='Felt01', blank=True, help_text=u'Firmanavn')
    address = models.CharField(max_length=40, db_column='Felt02', blank=True, help_text=u'Adresse')
    zipcode = models.CharField(max_length=10, db_column='Felt03', blank=True, help_text=u'Postnr')
    city = models.CharField(max_length=25, db_column='Felt04', blank=True, help_text=u'Bynavn')
    mobile = models.CharField(max_length=16, db_column='Felt05', blank=True, help_text=u'Mobiltelefon')
    ean = models.CharField(max_length=13, db_column='Felt06', blank=True, help_text=u'EAN')
    phone = models.CharField(max_length=16, db_column='Felt07', blank=True, help_text=u'Telefon')
    comment = models.TextField(db_column='Felt08', blank=True, help_text=u'Bemærkninger')
    fax = models.CharField(max_length=12, db_column='Felt09', blank=True, help_text=u'Fax')
    email = models.CharField(max_length=50, db_column='Felt10', blank=True, help_text=u'Email')
    name = models.CharField(max_length=25, db_column='Felt11', blank=True, help_text=u'Efternavn')
    community = models.CharField(max_length=25, db_column='Felt12', blank=True, help_text=u'Kreds')
    catalog_permissions = models.IntegerField(null=True, db_column='Felt13', blank=True, help_text=u'Rettigheder Internet Kartoteket')
    username = models.CharField(max_length=25, db_column='Felt14', blank=True, help_text=u'Brugernavn')
    created = models.DateField(null=True, db_column='Felt15', blank=True, help_text=u'Oprettet den')
    updated = models.DateField(null=True, db_column='Felt16', blank=True, help_text=u'Sidst rettet')
    password = models.CharField(max_length=25, db_column='Felt17', blank=True, help_text=u'Adgangskode')
    half_yearly_fee = models.FloatField(null=True, db_column='Felt18', blank=True, help_text=u'Halvårskontingent')
    member_type =  models.IntegerField(null=True, db_column='Felt19', blank=True, help_text=u'Medlemstype')
    payed = models.CharField(max_length=15, db_column='Felt20', blank=True, help_text=u'Betalt')
    committee =  models.IntegerField(null=True, db_column='Felt21', blank=True, help_text=u'Udvalg')
    industry = models.CharField(max_length=18, db_column='Felt22', blank=True, help_text=u'Branche')
    resigned = models.DateField(null=True, db_column='Felt23', blank=True, help_text=u'Udmeldt')
    active =  models.CharField(max_length=1, db_column='Felt24', blank=True, help_text=u'Aktiv')
    birthday = models.DateField(null=True, db_column='Felt25', blank=True, help_text=u'Fødselsdato')
    requisition_number = models.CharField(max_length=35, db_column='Felt26', blank=True, help_text=u'Rekvnr')

    class Meta:
        db_table = 'medlemp'
        ordering = ("company",)

    def __unicode__(self):
        return "%s (%s) [%s]" % (self.company, self.name, self.community)


class Felter(models.Model):
    wpmodule = models.IntegerField(null=True, db_column='WPModule', blank=True)
    feltnavn = models.CharField(primary_key=True, unique=True, max_length=24, db_column='FeltNavn', blank=True)
    feltetiket = models.CharField(max_length=35, db_column='FeltEtiket', blank=True)
    intfelttype = models.IntegerField(null=True, db_column='intFeltType', blank=True)
    reftype = models.IntegerField(null=True, db_column='RefType', blank=True)
    feltsize = models.IntegerField(null=True, db_column='FeltSize', blank=True)
    displaywidth = models.IntegerField(null=True, db_column='DisplayWidth', blank=True)
    displayheight = models.IntegerField(null=True, db_column='DisplayHeight', blank=True)
    taborder = models.IntegerField(null=True, db_column='TabOrder', blank=True)
    fleft = models.IntegerField(null=True, db_column='FLeft', blank=True)
    ftop = models.IntegerField(null=True, db_column='FTop', blank=True)
    fpage = models.IntegerField(null=True, db_column='FPage', blank=True)
    propflag = models.IntegerField(null=True, db_column='PropFlag', blank=True)
    frequired = models.CharField(max_length=1, db_column='FRequired', blank=True)
    freadonly = models.CharField(max_length=1, db_column='FReadOnly', blank=True)
    ftabstop = models.CharField(max_length=1, db_column='FTabstop', blank=True)
    fobs = models.CharField(max_length=1, db_column='FObs', blank=True)
    fnonduplicates = models.CharField(max_length=1, db_column='FNonDuplicates', blank=True)
    fnonsuplicates = models.CharField(max_length=1, db_column='FNonSuplicates', blank=True)
    beregning = models.CharField(max_length=128, db_column='Beregning', blank=True)
    filnavn = models.CharField(max_length=24, db_column='FilNavn', blank=True)
    inet = models.CharField(max_length=1, db_column='INet', blank=True)
    webshopfelt = models.IntegerField(null=True, db_column='Webshopfelt', blank=True)
    fcodefield = models.IntegerField(null=True, db_column='FCodeField', blank=True)
    fshowfield = models.IntegerField(null=True, db_column='FShowField', blank=True)
    historik = models.CharField(max_length=1, db_column='Historik', blank=True)
    class Meta:
        db_table = 'felter'
