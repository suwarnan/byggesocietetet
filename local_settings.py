DEBUG = True
TEMPLATE_DEBUG = DEBUG
RUNNING_ON_LIVE = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'byg',
        'USER': 'root',    
        'PASSWORD': '',
        'HOST': '',    
        'PORT': '3306',    
        # 'OPTIONS': {
        #     'init_command': 'SET storage_engine=INNODB',
        #     },
    },

    'winkas': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'DB21108',
        'USER': 'bygsoc',
        'PASSWORD': '2013github',
        'HOST': '195.249.85.50',
        'PORT': '3307',
        'OPTIONS': {
            'charset': 'latin1',
            }
    # 'winkas': {
    #     'ENGINE': 'django.db.backends.mysql',
    #     'NAME': 'DB21108',
    #     'USER': 'root',
    #     'PASSWORD': '20Proudwing15!',
    #     'HOST': '',
    #     'PORT': '3306',
        # 'OPTIONS': {
        #     'init_command': 'SET storage_engine=INNODB',
        #     },
     }
}

SOUTH_DATABASE_ADAPTERS = {
    'default': "south.db.mysql",
    'winkas': "south.db.mysql",
}

SERVER_URL = "http://127.0.0.1:8000/"

# USE_L10N = True
# USE_I18N = True
# LANGUAGE_CODE = 'da'
