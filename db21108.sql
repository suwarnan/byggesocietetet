-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2016 at 07:19 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db21108`
--

-- --------------------------------------------------------

--
-- Table structure for table `felter`
--

CREATE TABLE `felter` (
  `WPModule` int(11) DEFAULT NULL,
  `FeltNavn` varchar(24) NOT NULL,
  `FeltEtiket` varchar(35) NOT NULL,
  `intFeltType` int(11) DEFAULT NULL,
  `RefType` int(11) DEFAULT NULL,
  `FeltSize` int(11) DEFAULT NULL,
  `DisplayWidth` int(11) DEFAULT NULL,
  `DisplayHeight` int(11) DEFAULT NULL,
  `TabOrder` int(11) DEFAULT NULL,
  `FLeft` int(11) DEFAULT NULL,
  `FTop` int(11) DEFAULT NULL,
  `FPage` int(11) DEFAULT NULL,
  `PropFlag` int(11) DEFAULT NULL,
  `FRequired` varchar(1) NOT NULL,
  `FReadOnly` varchar(1) NOT NULL,
  `FTabstop` varchar(1) NOT NULL,
  `FObs` varchar(1) NOT NULL,
  `FNonDuplicates` varchar(1) NOT NULL,
  `FNonSuplicates` varchar(1) NOT NULL,
  `Beregning` varchar(128) NOT NULL,
  `FilNavn` varchar(24) NOT NULL,
  `INet` varchar(1) NOT NULL,
  `Webshopfelt` int(11) DEFAULT NULL,
  `FCodeField` int(11) DEFAULT NULL,
  `FShowField` int(11) DEFAULT NULL,
  `Historik` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medlemp`
--

CREATE TABLE `medlemp` (
  `Felt00` int(11) NOT NULL,
  `Felt01` varchar(35) NOT NULL,
  `Felt02` varchar(40) NOT NULL,
  `Felt03` varchar(10) NOT NULL,
  `Felt04` varchar(25) NOT NULL,
  `Felt05` varchar(16) NOT NULL,
  `Felt06` varchar(13) NOT NULL,
  `Felt07` varchar(16) NOT NULL,
  `Felt08` longtext NOT NULL,
  `Felt09` varchar(12) NOT NULL,
  `Felt10` varchar(50) NOT NULL,
  `Felt11` varchar(25) NOT NULL,
  `Felt12` varchar(25) NOT NULL,
  `Felt13` int(11) DEFAULT NULL,
  `Felt14` varchar(25) NOT NULL,
  `Felt15` date DEFAULT NULL,
  `Felt16` date DEFAULT NULL,
  `Felt17` varchar(25) NOT NULL,
  `Felt18` double DEFAULT NULL,
  `Felt19` int(11) DEFAULT NULL,
  `Felt20` varchar(15) NOT NULL,
  `Felt21` int(11) DEFAULT NULL,
  `Felt22` varchar(18) NOT NULL,
  `Felt23` date DEFAULT NULL,
  `Felt24` varchar(1) NOT NULL,
  `Felt25` date DEFAULT NULL,
  `Felt26` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `felter`
--
ALTER TABLE `felter`
  ADD PRIMARY KEY (`FeltNavn`);

--
-- Indexes for table `medlemp`
--
ALTER TABLE `medlemp`
  ADD PRIMARY KEY (`Felt00`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `medlemp`
--
ALTER TABLE `medlemp`
  MODIFY `Felt00` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
