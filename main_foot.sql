-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2016 at 12:14 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `byggesocietetet`
--

-- --------------------------------------------------------

--
-- Table structure for table `main_foot`
--

CREATE TABLE `main_foot` (
  `id` int(11) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(100) NOT NULL,
  `text` varchar(300) NOT NULL,
  `url` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_foot`
--

INSERT INTO `main_foot` (`id`, `content_type_id`, `object_id`, `image`, `text`, `url`) VALUES
(1, 0, 1, 'dyn/foot/icon-facebook.png', 'facebook', ''),
(2, 0, 1, 'dyn/foot/icon-twitter.png', 'twitter', ''),
(3, 0, 2, 'dyn/foot/logo-cowi.png', '', 'http://www.cowi.dk/'),
(4, 0, 2, 'dyn/foot/logo-kpmg.png', '', 'http://ey.com/dk'),
(5, 0, 1, 'dyn/foot/icon-linkedin.png', 'linkedin', 'http://www.linkedin.com/groups?gid=1129647'),
(6, 0, 2, 'dyn/foot/logo-nordea.png', '', 'http://www.nordeaejendomme.dk/');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `main_foot`
--
ALTER TABLE `main_foot`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `main_foot`
--
ALTER TABLE `main_foot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
