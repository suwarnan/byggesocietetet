Ny tilmelding til arrangementet {{ signup.event.name }} den {{ signup.event.date }}:

  Firmanavn: {{ signup.company }}
  Navn: {{ signup.name }}
  Adresse: {{ signup.address }}
  Postnummer: {{ signup.zipcode }}
  By: {{ signup.city }}
  Telefonnr.: {{ signup.phone }}
  Fax: {{ signup.fax }}
  E-mail-adresse: {{ signup.email }}
  Branche: {{ signup.industry }}
  EAN-nummer: {{ signup.ean }}
  Medlem af Byggesocietetet: {% if signup.member %}ja{% else %}nej{% endif %}
  Antal deltagere: {{ signup.amount }}
  Navn på øvrige deltagere:
{{ signup.others }}

Afsendt fra byggesocietetet.dk
