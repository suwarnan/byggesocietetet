from django.conf import settings
import sys, traceback

class APIDebugMiddleware(object):
    def process_request(self, request):
        if False and hasattr(settings, "API_DEBUG") and settings.API_DEBUG and request.path.startswith("/api/"):
            pass #Log.objects.create(text=unicode(request), device="Webserver DEBUG")

    def process_exception(self, request, exception):
        if request.path.startswith("/shop/"):
            exception = '\n'.join(traceback.format_exception(*sys.exc_info()))
            text = "%s\n\n%s" % (exception, request)
            #Log.objects.create(text=text, type="callback")

            if settings.API_DEBUG:
                print exception


class ViewNameMiddleware(object):
    def process_view(self, request, view_func, view_args, view_kwargs):  
        request.view_name = ".".join((view_func.__module__, view_func.__name__))  

