from django.conf import settings
from django.http import HttpResponseRedirect
import re
class HardcodeAdminLanguage:
    def process_request(self, request):
        p = request.path
        try:
            lang = settings.LANGUAGES[settings.DEFAULT_LANGUAGE][0]
        except IndexError:
            lang = "en"
        if p.startswith("/admin") or (re.match("/(.+)/admin/", p) and not p.startswith("/%s/admin" % lang)):
            rest = ""
            i = p.find("admin")
            if i != -1 and len(p) > i + 5:
                rest = p[p.find("admin")+5:]
            return HttpResponseRedirect("/%s/admin%s" % (lang, rest))
