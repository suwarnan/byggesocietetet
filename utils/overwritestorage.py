from django.db import models
from django.core.files.storage import FileSystemStorage
from django.db.models.signals import post_save
from django.conf import settings
from south.modelsinspector import add_introspection_rules

import os
import tempfile

class OverwriteStorage(FileSystemStorage):
    def _save(self, name, content):
        if self.exists(name):
            self.delete(name)
        return super(OverwriteStorage, self)._save(name, content)
    def get_available_name(self, name):
        return name

def filename_from_db_id_in_path(field, path, instance, filename):
    ext = os.path.splitext(filename)[1]
    if not instance.id:
        folder = os.path.join(settings.MEDIA_ROOT, path)
        if not os.path.exists(folder):
            os.mkdir(folder)
        # we need to return a name, and since the id won't be there
        # until in the post save signal, we just generate a temporary
        # one
        _,tmppath = tempfile.mkstemp(suffix=ext.lower(), dir=folder)

        def rename_after_save(sender, instance, **kwargs):
            post_save.disconnect(sender=instance.__class__, weak=False, dispatch_uid=tmppath)
            # check whether we need to rename the field now
            if instance.id:
                frompath = getattr(instance, field.name).path
                prefix = settings.MEDIA_ROOT
                if frompath.startswith(prefix):
                    frompath = frompath[len(prefix) + 1:]
                name = unicode(instance.id) + os.path.splitext(frompath)[1]
                p = os.path.join(path, name.lower())
                if frompath != p:
                    setattr(instance, field.name, p)
                    os.rename(os.path.join(settings.MEDIA_ROOT, frompath), os.path.join(settings.MEDIA_ROOT, p))
                    instance.save()
        
        post_save.connect(rename_after_save, sender=instance.__class__, weak=False, dispatch_uid=tmppath)

        return tmppath
    else:
        name = unicode(instance.id) + ext
        return os.path.join(path, name.lower())

def db_id_path(field, path):
    from django.utils.functional import curry

    return curry(filename_from_db_id_in_path, field, path)


class DbIdFileField(models.FileField):
    def __init__(self, *args, **kwargs):
        if 'storage' not in kwargs:
            kwargs['storage'] = OverwriteStorage()
            
        if 'upload_to' not in kwargs:
            kwargs['upload_to'] = ""            

        kwargs['upload_to'] = db_id_path(self, kwargs['upload_to'])
        super(DbIdFileField, self).__init__(*args, **kwargs)

class DbIdImageField(models.ImageField):
    def __init__(self, *args, **kwargs):
        if 'storage' not in kwargs:
            kwargs['storage'] = OverwriteStorage()
            
        if 'upload_to' not in kwargs:
            kwargs['upload_to'] = ""            

        kwargs['upload_to'] = db_id_path(self, kwargs['upload_to'])
        super(DbIdImageField, self).__init__(*args, **kwargs)

add_introspection_rules([], ["^utils\.overwritestorage\.*"])
