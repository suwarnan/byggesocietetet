from django import template
from main.models import TextSnippet
from django.core import urlresolvers

register = template.Library()

@register.simple_tag(takes_context=True)
def textsnippet(context, key, option="with_edit_link"):
    key = key.strip()
    if not key:
        return ""

    snippet,_ = TextSnippet.objects.get_or_create(key=key)
    admin_link = ""
    if False and context['request'].user.is_superuser and option == "with_edit_link":
        admin_link = '<span class="adminEdit" onClick="window.location = \'%s\'; return false;">&nbsp;</span>' % urlresolvers.reverse('admin:main_textsnippet_change', args=(snippet.id,))
    return u'%s%s' % (admin_link, snippet.text or "")
