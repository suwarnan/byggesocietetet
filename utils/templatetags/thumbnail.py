import os

from django import template
from django.conf import settings
from utils.imaging import fit_image_with_cropping, fit_image_without_cropping#, crop_image

register = template.Library()

@register.filter
def thumbnail(file, size="100x150"):
    # defining the size
    w, h = [int(x) for x in size.split('x')]
    return get_thumbnail(file, w, h)

NO_CROPPING = 0
SCALE_THEN_CROP = 1
CROP_ONLY = 2

def get_thumbnail(file_url, w, h, crop=SCALE_THEN_CROP, alignment=None, quality=85):
    if not file_url:
        return ""

    suffix = str(w) + 'x' + str(h)
    if alignment:
        suffix += "x" + alignment

    # find out whether it's already in a "generated" subdir
    components = file_url.split('/')
    if len(components) > 1 and components[-2] == "generated":
        already_in_generated = True
    else:
        already_in_generated = False
    
    # get name
    name = components[-1]
    basename, ext = os.path.splitext(name)
    if not ext:
        ext = '.png'
        
    if already_in_generated:
        prefix = ""
    else:
        prefix = 'generated/'
    generated_name = prefix + basename + '_' + suffix + ext

    # get url
    generated_url = file_url[:-len(name)] + generated_name

    # get filename
    filename = os.path.join(settings.MEDIA_ROOT, file_url[len(settings.MEDIA_URL):])
    generated_filename = os.path.join(filename[:-len(name)], generated_name)

    # if the image wasn't already resized, resize it
    if not os.path.exists(generated_filename) or os.path.getmtime(filename) > os.path.getmtime(generated_filename):
        
        # get generated dir
        if already_in_generated:
            generated_dir = filename[:-len(name)]
        else:
            generated_dir = os.path.join(filename[:-len(name)], "generated")
            
        if not os.path.exists(generated_dir):
            os.makedirs(generated_dir)

        if crop == SCALE_THEN_CROP:
            fit_image_with_cropping(filename, w, h, generated_filename, quality=quality)
        elif crop == NO_CROPPING:
            fit_image_without_cropping(filename, w, h, generated_filename, alignment=alignment, quality=quality)
        else:
            print "not implemented"
            #crop_image(filename, w, h, generated_filename)
    
    return generated_url

@register.filter
def croppedthumbnail(file, size="100x150"):
    # args: alignment=<align>,quality=85
    args = size.split(",")
    w, h = [int(x) for x in args[0].split('x')]
    kwargs = dict([map(str, a.split("=")) for a in args[1:]])
    if 'quality' in kwargs:
        kwargs['quality'] = int(kwargs['quality'])
    kwargs['crop'] = SCALE_THEN_CROP #CROP_ONLY
    return get_thumbnail(file, w, h, **kwargs)

@register.filter
def noncroppedthumbnail(file, size="100x150"):
    # args: alignment=<align>,quality=85
    args = size.split(",")
    w, h = [int(x) for x in args[0].split('x')]
    kwargs = dict([map(str, a.split("=")) for a in args[1:]])
    if 'quality' in kwargs:
        kwargs['quality'] = int(kwargs['quality'])
    kwargs['crop'] = NO_CROPPING
    return get_thumbnail(file, w, h, **kwargs)

from django.utils.safestring import mark_safe

@register.filter
def thumbnaillinkwithcrop(file, size="200x150"):
    w, h = [int(x) for x in size.split('x')]
    url = get_thumbnail(file, w, h)
    
    return mark_safe(u'<a href="%s"><img src="%s" width="%s" height="%s" /></a>' % (file, url, w, h))

@register.filter
def thumbnaillinknocrop(file, size="200x150"):
    w, h = [int(x) for x in size.split('x')]
    url = get_thumbnail(file, w, h, crop=NO_CROPPING)
    
    return mark_safe(u'<a href="%s"><img src="%s" /></a>' % (file, url))
