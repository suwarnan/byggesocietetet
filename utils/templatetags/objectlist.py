from django.template import Library, Node, TemplateSyntaxError
from django.db.models import get_model
     
register = Library()

"""
Usage:
  get_list(model, amount, order_field, "as", template_variable)

  {% get_list main.News 2 published as news_items %}

"""

class LatestContentNode(Node):
    def __init__(self, model, num, order, varname):
        self.num, self.varname, self.order = int(num), varname, order
        self.model = get_model(*model.split('.'))
    
    def render(self, context):
        objs = self.model._default_manager.order_by(self.order)[:self.num]
        if self.num == 1:
            objs = objs[0] if objs else None
        context[self.varname] = objs
        return ''
 
def get_list(parser, token):
    bits = token.contents.split()
    if len(bits) != 6:
        raise TemplateSyntaxError, "get_latest tag takes exactly five arguments"
    if bits[4] != 'as':
        raise TemplateSyntaxError, "fourth argument to get_latest tag must be 'as'"
    return LatestContentNode(bits[1], bits[2], bits[3], bits[5])
    
get_list = register.tag(get_list)
