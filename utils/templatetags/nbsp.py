from django import template
from django.utils import simplejson
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter
def nbsp(value):
    return mark_safe(value.replace(" ", "&nbsp;") if value else "")
