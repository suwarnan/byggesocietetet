from django import template
from django.template import Node, Token, TemplateSyntaxError
from django.template import resolve_variable, defaulttags
from django.template.defaultfilters import stringfilter
from django.utils.functional import wraps

from localeurl import utils

register = template.Library()



from django import template
from main.models import TextSnippet
from django.core import urlresolvers

register = template.Library()

@register.simple_tag(takes_context=True)
def localize_current_url(context, language):
    key = key.strip()
    if not key:
        return ""




def localized_url(parser, token):
    locale_url_wrapper(parser, "%s %s" % (token, request.view_name))
    """
    Renders the url for the view with another locale prefix. The syntax is
    like the 'url' tag, only with a locale before the view.

    Examples:
      {% locale_url "de" cal.views.day day %}
      {% locale_url "nl" cal.views.home %}
      {% locale_url "en-gb" cal.views.month month as month_url %}
    """
    bits = token.split_contents()
    if len(bits) < 3:
        raise TemplateSyntaxError("'%s' takes at least two arguments:"
                " the locale and a view" % bits[0])
    urltoken = Token(token.token_type, bits[0] + ' ' + ' '.join(bits[2:]))
    urlnode = django_url_tag(parser, urltoken)
    return LocaleURLNode(bits[1], urlnode)
