from django.db import models
import datetime
from django.utils.encoding import smart_str
import decimal

class CharDateField(models.DateField):
    default_error_messages = {
        'invalid': 'Enter a valid date in %s format.'
    }
    description = "Date (without time)"

    def __init__(self, verbose_name=None, name=None, auto_now=False, auto_now_add=False, date_format="%d/%m/%Y", **kwargs):
        self.date_format = date_format
        self.default_error_messages['invalid'] = self.default_error_messages['invalid'] % self.date_format
        models.DateField.__init__(self, verbose_name, name, **kwargs)

    def get_internal_type(self):
        return "DateField"

    def to_python(self, value):
        if value is None:
            return value
        if not value.strip():
            return None
        if isinstance(value, datetime.date):
            return value
        if isinstance(value, datetime.datetime):
            return value.date()

        # Attempt to parse a datetime:
        value = smart_str(value)
        return datetime.datetime(value, self.date_format).date()

    def get_prep_value(self, value):
        return self.to_python(value)

    def get_db_prep_value(self, value, connection, prepared=False):
        # Casts dates into the format expected by the backend
        if not prepared:
            value = self.get_prep_value(value)
        return value.strftime(self.date_format)


class CharDateTimeField(models.DateTimeField):
    default_error_messages = {
        'invalid': 'Enter a valid date/time in %s format.'
    }
    description = "Date (with time)"

    def __init__(self, verbose_name=None, name=None, auto_now=False, auto_now_add=False, date_format="%d/%m/%Y", **kwargs):
        self.date_format = date_format
        self.default_error_messages['invalid'] = self.default_error_messages['invalid'] % self.date_format
        models.DateTimeField.__init__(self, verbose_name, name, **kwargs)

    def get_internal_type(self):
        return "DateTimeField"

    def to_python(self, value):
        if value is None:
            return value
        if isinstance(value, datetime.datetime):
            return value
        if isinstance(value, datetime.date):
            return datetime.datetime(value.year, value.month, value.day)

        # Attempt to parse a datetime:
        value = smart_str(value)
        return datetime.datetime(value, self.date_format)

    def get_prep_value(self, value):
        return self.to_python(value)

    def get_db_prep_value(self, value, connection, prepared=False):
        # Casts dates into the format expected by the backend
        if not prepared:
            value = self.get_prep_value(value)
        return value.strftime(self.date_format)

class DecimalDateField(models.DateField):
    default_error_messages = {
        'invalid': 'Enter a valid date.'
    }
    description = "Date (without time)"

    __metaclass__ = models.SubfieldBase

    def __init__(self, verbose_name=None, name=None, auto_now=False, auto_now_add=False, date_format="%Y%m%d", **kwargs):
        self.date_format = date_format
        self.date_width = len(datetime.date.today().strftime(date_format))
        models.DateField.__init__(self, verbose_name, name, **kwargs)

    def get_internal_type(self):
        return "DateField"

    def to_python(self, value):
        if value is None:
            return value
        if not value:
            return None
        if isinstance(value, datetime.date):
            return value
        if isinstance(value, datetime.datetime):
            return value.date()

        date = "".join([str(i) for i in value.as_tuple()[1]])
        try:
            return datetime.datetime.strptime(date.rjust(self.date_width, "0"), self.date_format).date()
        except Exception:
            return None

    def get_db_prep_value(self, value, connection, prepared=False):
        if not prepared:
            value = self.get_prep_value(value)

        if value:
            return decimal.Decimal(value.strftime(self.date_format))
        else:
            return decimal.Decimal(0)
