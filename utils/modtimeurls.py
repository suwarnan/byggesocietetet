import re, os

from django.conf import settings

def human_readable_modtime(modtime):
    import datetime, time
    return datetime.datetime(*time.gmtime(modtime)[:6]).strftime("%Y%m%d-%H%M%S")

#MODTIME_CACHING_FORMATTER = human_readable_modtime

# adapted from http://code.activestate.com/recipes/65212/
def rebase(num, numerals="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"):
    "Return integer num as string in another base using numerals."
    num = int(num)
    if num == 0:
        return numerals[0]

    res = ""
    if num < 0:
        res = '-'
        num = -num
        
    while num > 0:
        res = numerals[num % len(numerals)] + res
        num //= len(numerals)
        
    return res

MODTIME_CACHING_FORMATTER = rebase

if hasattr(settings, 'MODTIME_CACHING_FORMATTER'):
    MODTIME_CACHING_FORMATTER = settings.MODTIME_CACHING_FORMATTER

url_attributes = ['src', 'href']

# stop matching when we hit <>" to guard against erratic markup
link_matcher = re.compile('((?:%s)="%s[^<>"]*")' % ("|".join(url_attributes), re.escape(settings.MEDIA_URL)))

def append_modtime_to_url(url):
    """Append the file modification time to URL if it's a file in MEDIA_ROOT."""
    if not url.startswith(settings.MEDIA_URL):
        return url
    filename = os.path.join(settings.MEDIA_ROOT, url[len(settings.MEDIA_URL):])
    index = filename.rfind('?')
    if index != -1:
        filename = filename[:index]
    try:
        stat = os.stat(filename)

        timestamp = MODTIME_CACHING_FORMATTER(stat.st_mtime)
        if '?' in url: # .split('/')[-1]
            timestamp = '&_=' + timestamp
        else:
            timestamp = '?_=' + timestamp

        return url + timestamp
    except OSError:
        pass

    return url

def append_modtime_to_urls_in_html(html):
    """Add modtime GET parameter to each media URL in input."""
    def replace_urls(m):
        before, url, after = m.group(1).split('"')

        return before + '"' + append_modtime_to_url(url) + '"' + after

    return link_matcher.sub(replace_urls, html)

class ModTimeUrlsMiddleware:
    """Middleware for adding modtime GET parameter to each media URL in responses."""
    
    def process_response(self, request, response):
        if 'text/html' in response['content-type'].lower():
            response.content = append_modtime_to_urls_in_html(response.content)
        return response
