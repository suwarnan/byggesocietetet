import os

from django.conf import settings

from PIL import Image, ImageDraw, ImageFont

def get_file_path_with_valid_extension(file_path):
    file_path = file_path.lower()
    base, ext = os.path.splitext(file_path)
    if ext not in ('.jpg', '.jpeg', '.png', '.tiff', '.tif', '.gif'):
        return base + '.jpg'
    else:
        return file_path

def fit_image_with_cropping(file_path, max_width=None, max_height=None, save_as=None, quality=85):
    img = Image.open(file_path)
  
    w, h = float(img.size[0]), float(img.size[1])
  
    max_width = float(max_width or w)
    max_height = float(max_height or h)
  
    # Find the closest bigger proportion to the maximum size
    scale = max(max_width / w, max_height / h)
  
    # Image bigger than maximum size?
    if (scale < 1):
        # Calculate proportions and resize
        w = int(round(w * scale))
        h = int(round(h * scale))
        img = img.resize((w, h), Image.ANTIALIAS)
  
    # Avoid enlarging the image
    max_width = min(max_width, w)
    max_height = min(max_height, h)
    
    # Define the cropping box
    left = int((w - max_width) / 2)
    top = int((h - max_height) / 2)
  
    # Crop to fit the desired size
    if w != max_width or h != max_height:
        right = left + int(round(max_width))
        bottom = top + int(round(max_height))

        img = img.crop((left, top, right, bottom))
        
    save_as = get_file_path_with_valid_extension(save_as or file_path)
    if save_as.endswith(".gif"):
        img.save(save_as, transparency=img.info.get('transparency'))
    else:
        img.save(save_as, quality=quality)

    
def fit_image_without_cropping(file_path, max_width=None, max_height=None, save_as=None, alignment=None, quality=85):
    img = Image.open(file_path)
    w, h = img.size
    w = int(max_width or w)
    h = int(max_height or h)
    img.thumbnail((w, h), Image.ANTIALIAS)
    if alignment != None and (img.size[0] != max_width or img.size[1] != max_height):
        new_img = Image.new("RGBA", (max_width, max_height), (0, 0, 0, 0))
        if alignment == "right":
            x = (max_width - img.size[0])
        else:
            x = (max_width - img.size[0]) // 2
        y = (max_height - img.size[1]) // 2
        new_img.paste(img, (x,y))
        img = new_img
        
    save_as = get_file_path_with_valid_extension(save_as or file_path)
    img.save(save_as, quality=quality)

def fit_image_without_cropping_to_grayscale(file_path, max_width=None, max_height=None, save_as=None, quality=85):
    img = Image.open(file_path)
    w, h = img.size
    w = int(max_width or w)
    h = int(max_height or h)
    img.thumbnail((w, h), Image.ANTIALIAS)
    img = img.convert('L')
    
    save_as = get_file_path_with_valid_extension(save_as or file_path)
    img.save(save_as, quality=quality)
