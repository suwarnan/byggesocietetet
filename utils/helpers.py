from django.http import Http404, HttpResponse
from django.utils import simplejson

from calendar import timegm
from datetime import datetime, timedelta
import time

def get_parameter_or_404(dictionary, parameter, as_type=None):
    # actually it's more appropriate with 400 as error code, but
    # Django hasn't got that
    if not parameter in dictionary:
        raise Http404("missing required parameter %s" % parameter)

    v = dictionary[parameter]
    if as_type:
        try:
            return as_type(v)
        except ValueError:
            raise Http404("couldn't cast parameter %s to %s" % (parameter, as_type))
    else:
        return v

class JsonResponse(HttpResponse):
    def __init__(self, obj):
        content = simplejson.dumps(obj)
        super(JsonResponse, self).__init__(content, mimetype="text/plain")

def to_python_time(js):
    return datetime.utcfromtimestamp(js/1000)

def to_javascript_time(dt):
    try:
        js_time = timegm(dt.utctimetuple())
    except ValueError:
        # Some EV files have been found to contain '00/00/00 00:00:00' which
        # correctly raises a ValueError. In this case we will return timegm
        # from a default value.
        js_time = timegm(datetime(year=2000, month=01, day=01).utctimetuple())
    return js_time * 1000
