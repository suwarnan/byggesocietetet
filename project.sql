BEGIN;
CREATE TABLE `main_photo` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `content_type_id` integer NOT NULL,
    `object_id` integer UNSIGNED NOT NULL,
    `image` varchar(100) NOT NULL,
    `text` varchar(300) NOT NULL
)
;
CREATE TABLE `main_foot` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `object_id` integer UNSIGNED NOT NULL,
    `image` varchar(100) NOT NULL,
    `text` varchar(300) NOT NULL,
    `url` varchar(200) NOT NULL
)
;
CREATE TABLE `main_file` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `content_type_id` integer NOT NULL,
    `object_id` integer UNSIGNED NOT NULL,
    `name` varchar(100) NOT NULL,
    `file` varchar(100) NOT NULL
)
;
CREATE TABLE `main_uploadedfile` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name` varchar(100) NOT NULL,
    `file` varchar(100) NOT NULL
)
;
CREATE TABLE `main_frontimage` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `position` varchar(30) NOT NULL,
    `image` varchar(100) NOT NULL,
    `order` integer NOT NULL,
    `link` varchar(200) NOT NULL,
    `active` bool NOT NULL
)
;
CREATE TABLE `main_news` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `title` varchar(200) NOT NULL,
    `url` varchar(200) NOT NULL UNIQUE,
    `text` longtext NOT NULL,
    `featured` bool NOT NULL,
    `front_image` varchar(100) NOT NULL,
    `image` varchar(100) NOT NULL,
    `published` datetime NOT NULL
)
;
CREATE TABLE `main_event` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `type` varchar(30) NOT NULL,
    `name` varchar(200) NOT NULL,
    `url` varchar(200) NOT NULL UNIQUE,
    `date` date NOT NULL,
    `time` varchar(100) NOT NULL,
    `image` varchar(100) NOT NULL,
    `image_detail` varchar(100) NOT NULL,
    `intro` longtext NOT NULL,
    `text` longtext NOT NULL,
    `organizer` varchar(200) NOT NULL,
    `email` varchar(75) NOT NULL,
    `signup_link` varchar(200) NOT NULL,
    `event_number` integer UNSIGNED NOT NULL,
    `place` varchar(200) NOT NULL,
    `price_member` integer UNSIGNED NOT NULL,
    `price_nonmember` integer UNSIGNED NOT NULL,
    `group_id` integer
)
;
CREATE TABLE `main_memberproxy` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `number` integer UNSIGNED NOT NULL
)
;
CREATE TABLE `main_membergroup_alternates` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `membergroup_id` integer NOT NULL,
    `memberproxy_id` integer NOT NULL,
    UNIQUE (`membergroup_id`, `memberproxy_id`)
)
;
ALTER TABLE `main_membergroup_alternates` ADD CONSTRAINT `memberproxy_id_refs_id_605821d9` FOREIGN KEY (`memberproxy_id`) REFERENCES `main_memberproxy` (`id`);
CREATE TABLE `main_membergroup_board` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `membergroup_id` integer NOT NULL,
    `memberproxy_id` integer NOT NULL,
    UNIQUE (`membergroup_id`, `memberproxy_id`)
)
;
ALTER TABLE `main_membergroup_board` ADD CONSTRAINT `memberproxy_id_refs_id_8c34ba21` FOREIGN KEY (`memberproxy_id`) REFERENCES `main_memberproxy` (`id`);
CREATE TABLE `main_membergroup_treasurer` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `membergroup_id` integer NOT NULL,
    `memberproxy_id` integer NOT NULL,
    UNIQUE (`membergroup_id`, `memberproxy_id`)
)
;
ALTER TABLE `main_membergroup_treasurer` ADD CONSTRAINT `memberproxy_id_refs_id_f689447a` FOREIGN KEY (`memberproxy_id`) REFERENCES `main_memberproxy` (`id`);
CREATE TABLE `main_membergroup_members` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `membergroup_id` integer NOT NULL,
    `memberproxy_id` integer NOT NULL,
    UNIQUE (`membergroup_id`, `memberproxy_id`)
)
;
ALTER TABLE `main_membergroup_members` ADD CONSTRAINT `memberproxy_id_refs_id_c531a70e` FOREIGN KEY (`memberproxy_id`) REFERENCES `main_memberproxy` (`id`);
CREATE TABLE `main_membergroup_president` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `membergroup_id` integer NOT NULL,
    `memberproxy_id` integer NOT NULL,
    UNIQUE (`membergroup_id`, `memberproxy_id`)
)
;
ALTER TABLE `main_membergroup_president` ADD CONSTRAINT `memberproxy_id_refs_id_ae6cd71d` FOREIGN KEY (`memberproxy_id`) REFERENCES `main_memberproxy` (`id`);
CREATE TABLE `main_membergroup_vicepresident` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `membergroup_id` integer NOT NULL,
    `memberproxy_id` integer NOT NULL,
    UNIQUE (`membergroup_id`, `memberproxy_id`)
)
;
ALTER TABLE `main_membergroup_vicepresident` ADD CONSTRAINT `memberproxy_id_refs_id_8643c6e8` FOREIGN KEY (`memberproxy_id`) REFERENCES `main_memberproxy` (`id`);
CREATE TABLE `main_membergroup` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `type` varchar(30) NOT NULL,
    `name` varchar(200) NOT NULL,
    `menu` varchar(200) NOT NULL,
    `url` varchar(200) NOT NULL UNIQUE,
    `order` integer NOT NULL,
    `image` varchar(100) NOT NULL,
    `intro` longtext NOT NULL,
    `text` longtext NOT NULL,
    `extra_header` varchar(200) NOT NULL,
    `extra_text` longtext NOT NULL,
    `extra_image` varchar(100) NOT NULL
)
;
ALTER TABLE `main_event` ADD CONSTRAINT `group_id_refs_id_6ccb1454` FOREIGN KEY (`group_id`) REFERENCES `main_membergroup` (`id`);
ALTER TABLE `main_membergroup_alternates` ADD CONSTRAINT `membergroup_id_refs_id_774430c6` FOREIGN KEY (`membergroup_id`) REFERENCES `main_membergroup` (`id`);
ALTER TABLE `main_membergroup_board` ADD CONSTRAINT `membergroup_id_refs_id_76c1edb2` FOREIGN KEY (`membergroup_id`) REFERENCES `main_membergroup` (`id`);
ALTER TABLE `main_membergroup_treasurer` ADD CONSTRAINT `membergroup_id_refs_id_66d0d5e7` FOREIGN KEY (`membergroup_id`) REFERENCES `main_membergroup` (`id`);
ALTER TABLE `main_membergroup_members` ADD CONSTRAINT `membergroup_id_refs_id_637fd79f` FOREIGN KEY (`membergroup_id`) REFERENCES `main_membergroup` (`id`);
ALTER TABLE `main_membergroup_president` ADD CONSTRAINT `membergroup_id_refs_id_f2655c96` FOREIGN KEY (`membergroup_id`) REFERENCES `main_membergroup` (`id`);
ALTER TABLE `main_membergroup_vicepresident` ADD CONSTRAINT `membergroup_id_refs_id_f286dba5` FOREIGN KEY (`membergroup_id`) REFERENCES `main_membergroup` (`id`);
CREATE TABLE `main_page` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `section` varchar(50) NOT NULL,
    `parent_id` integer,
    `name` varchar(200) NOT NULL,
    `menu` varchar(200) NOT NULL,
    `url` varchar(200) NOT NULL UNIQUE,
    `primary` bool NOT NULL,
    `order` integer NOT NULL,
    `image` varchar(100) NOT NULL,
    `intro` longtext NOT NULL,
    `text` longtext NOT NULL
)
;
ALTER TABLE `main_page` ADD CONSTRAINT `parent_id_refs_id_665c2d87` FOREIGN KEY (`parent_id`) REFERENCES `main_page` (`id`);
CREATE TABLE `main_eventsignup` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `event_id` integer NOT NULL,
    `company` varchar(100) NOT NULL,
    `name` varchar(100) NOT NULL,
    `address` varchar(200) NOT NULL,
    `zipcode` varchar(30) NOT NULL,
    `city` varchar(100) NOT NULL,
    `phone` varchar(30) NOT NULL,
    `fax` varchar(30) NOT NULL,
    `email` varchar(75) NOT NULL,
    `industry` varchar(100) NOT NULL,
    `ean` varchar(50) NOT NULL,
    `amount` integer UNSIGNED NOT NULL,
    `others` longtext NOT NULL,
    `member` bool NOT NULL
)
;
ALTER TABLE `main_eventsignup` ADD CONSTRAINT `event_id_refs_id_88e67d0f` FOREIGN KEY (`event_id`) REFERENCES `main_event` (`id`);
CREATE TABLE `main_textsnippet` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `key` varchar(100) NOT NULL UNIQUE,
    `text` longtext NOT NULL,
    `last_modified` datetime NOT NULL,
    `use_visual_editor` bool NOT NULL
)
;
CREATE TABLE `main_stylesheet` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `text` longtext NOT NULL
)
;
CREATE TABLE `main_industry` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `name` varchar(100) NOT NULL
)
;
CREATE TABLE `main_signup` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `membership` varchar(30) NOT NULL,
    `accept_visibility` bool NOT NULL,
    `company` varchar(100) NOT NULL,
    `name` varchar(100) NOT NULL,
    `address` varchar(200) NOT NULL,
    `zipcode` varchar(30) NOT NULL,
    `city` varchar(100) NOT NULL,
    `phone` varchar(30) NOT NULL,
    `fax` varchar(30) NOT NULL,
    `email` varchar(75) NOT NULL,
    `industry_id` integer NOT NULL,
    `ean` varchar(50) NOT NULL,
    `dob` varchar(50) NOT NULL
)
;
ALTER TABLE `main_signup` ADD CONSTRAINT `industry_id_refs_id_fe2758c1` FOREIGN KEY (`industry_id`) REFERENCES `main_industry` (`id`);
-- The following references should be added but depend on non-existent tables:
-- ALTER TABLE `main_photo` ADD CONSTRAINT `content_type_id_refs_id_1261fcbc` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);
-- ALTER TABLE `main_file` ADD CONSTRAINT `content_type_id_refs_id_40e1d499` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);
CREATE INDEX `main_photo_e4470c6e` ON `main_photo` (`content_type_id`);
CREATE INDEX `main_photo_829e37fd` ON `main_photo` (`object_id`);
CREATE INDEX `main_file_e4470c6e` ON `main_file` (`content_type_id`);
CREATE INDEX `main_file_829e37fd` ON `main_file` (`object_id`);
CREATE INDEX `main_event_bda51c3c` ON `main_event` (`group_id`);
CREATE INDEX `main_page_63f17a16` ON `main_page` (`parent_id`);
CREATE INDEX `main_eventsignup_e9b82f95` ON `main_eventsignup` (`event_id`);
CREATE INDEX `main_signup_d28c39ae` ON `main_signup` (`industry_id`);
COMMIT;