from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.conf import settings
from main.feeds import NewsFeed, EventFeed
import os

admin.autodiscover()

FEEDS = {
    'news': NewsFeed,
    'events': EventFeed,
    }

urlpatterns = patterns('',
     # url(r'^admin/', include(admin.site.urls),namespace='admin'),
    # url(r'^static/(?P<path>.*)$', 'django.views.static.serve',{'document_root': 'settings.MEDIA_ROOT'}),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'^adminmedia/(?P<path>.*)$', 'django.views.static.serve', {'document_root': os.path.join(settings.BASE_DIR, "/root/byg/myprojectenv/lib/python2.7/site-packages/django/contrib/admin/media/"), 'show_indexes': True}),
    url(r'^$', 'main.views.home', name='home'),

    url(r'^(?P<group_type>(lokalomraade|udvalg|bs-gruppe))/$', 'main.views.member_group_page', name='member_group_page_default'),
    url(r'^(?P<group_type>(lokalomraade|udvalg|bs-gruppe))/information/(?P<page_url>[\w-]+)/$', 'main.views.member_group_page', name='member_group_page'),
    url(r'^(?P<group_type>(lokalomraade|udvalg|bs-gruppe))/(?P<group_url>[\w-]+)/$', 'main.views.member_group', name='member_group'),

    url(r'^arrangementer/$', 'main.views.event_overview', name='event_overview'),
    url(r'^arrangementer/aktuelle/$', 'main.views.event_overview', {'mode': 'current'}, name='event_overview_current'),
    url(r'^arrangementer/arkiv/$', 'main.views.event_overview', {'mode': 'past'}, name='event_overview_past_current'),
    url(r'^arrangementer/arkiv/(?P<year>\d+)/$', 'main.views.event_overview', {'mode': 'past'}, name='event_overview_past'),
    url(r'^arrangementer/alle/$', 'main.views.event_overview', {'mode': 'all'}, name='event_overview_all'),
    url(r'^arrangementer/arkiv/(?P<year>\d+)/$', 'main.views.event_overview', {'mode': 'past'}, name='event_overview_past'),
    url(r'^arrangementer/information/(?P<page_url>[\w-]+)/$', 'main.views.event_page', name='event_page'),
    url(r'^arrangementer/(?P<event_url>[\w-]+)/$', 'main.views.event_detail', name='event_detail'),

    url(r'^medlemmer/detail$', 'main.views.members_page', name='details'),
    # url(r'^medlemmer/$', 'main.views.member_registration', name='members_page'),
    url(r'^medlemmer/$', 'main.views.members_page', name='members_page'),
    url(r'^medlemmer/indmelding/$', 'main.views.member_registration', name='member_registration'),
    url(r'^medlemmer/medlemskab/$', 'main.views.member_contact', name='member_contact'),
    url(r'^medlemmer/find/$', 'main.views.member_section', {'member_type': 'a'}, name='member_section_find'),
    url(r'^medlemmer/(?P<member_type>[\w-]+)/$', 'main.views.member_section', name='member_section'),

    url(r'^nyheder/$', 'main.views.news_overview', name='news_overview'),
    url(r'^nyheder/(?P<year>\d+)/$', 'main.views.news_overview', name='news_overview'),
    url(r'^nyheder/(?P<news_url>[\w-]+)/$', 'main.views.news_detail', name='news_detail'),

    url(r'^om/$', 'main.views.page', {'page_url': 'byggesocietetet'}, name='about'),
    url(r'^om/kontakt/$', 'main.views.page1',{'page_url': 'kontakt'}, name='about_contact'),
    url(r'^om/(?P<page_url>[\w-]+)/$', 'main.views.page', name='about_page'),

    url(r'^feeds/(?P<url>.*)/$', 'django.contrib.syndication.views.feed', {'feed_dict': FEEDS}, name="feeds"),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': "main/login.html"}, name="login"),
    url(r'^admin/', include(admin.site.urls)),
)

from django.conf.urls.static import static
urlpatterns += static(settings.STATIC_URL, document_root=settings.MEDIA_ROOT)
