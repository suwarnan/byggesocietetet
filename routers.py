class WinkasRouter(object):
    """A router to control all database operations on models in
    the winkas application"""

    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'winkas':
            return 'winkas'
        return None

    def db_for_write(self, model, **hints):
#        if model._meta.app_label == 'winkas':
#            return 'winkas'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        "Only allow relations inside the same db"
        if obj1._meta.app_label == 'winkas' or obj2._meta.app_label == 'winkas':
            if obj1._meta.app_label == 'winkas' and obj2._meta.app_label == 'winkas':
                return True
            else:
                return False
        else:
            return None

    def allow_syncdb(self, db, model):
        "Never sync to winkas"
        if model._meta.app_label == 'winkas':
            return False
        return None
