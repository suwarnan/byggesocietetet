# -*- coding: utf-8 -*-
from django.db import models
from django.utils.html import strip_tags
from django.utils.safestring import SafeUnicode, mark_safe
from django.contrib.auth.models import User
# from django.contrib.auth import get_user_model
# User = get_user_model()
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from datetime import datetime
from utils.overwritestorage import *
from winkas import models as winkas

def uploaded_file_path(instance, filename):
    name = filename.encode('ascii', 'ignore')
    return os.path.join("dyn/%s/files" % instance._meta.module_name, name.lower())

class Photo(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField(db_index=True)
    object = generic.GenericForeignKey()

    image = DbIdImageField("billede", upload_to="dyn/photo")
    text = models.CharField("tekst", max_length=300, blank=True)

    class Meta:
        verbose_name = "billede"
        verbose_name_plural = "billeder"

    def get_absolute_url(self):
        return os.path.join(self.object.get_absolute_url(), "%s/" % self.id)

    def __unicode__(self):
        return unicode(self.id)

class foot(models.Model):
    object_id = models.PositiveIntegerField('id', default=0)
    image = DbIdImageField("billede", upload_to="dyn/foot")
    text = models.CharField("tekst", max_length=300, blank=True)
    url = models.URLField("link til tilmelding", blank=True)

    class Meta:
        verbose_name = "footer"
        verbose_name_plural = "footer" 
    def __unicode__(self):
        return unicode(self.object_id)      

class File(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField(db_index=True)
    object = generic.GenericForeignKey()

    name = models.CharField('navn', max_length=100)
    file = models.FileField('fil', upload_to="dyn/files")

    class Meta:
        verbose_name = 'fil'
        verbose_name_plural = 'filer'

    def __unicode__(self):
        return unicode(self.id)

class UploadedFile(models.Model):
    name = models.CharField('navn', max_length=100, blank=True)
    file = models.FileField('fil', upload_to="dyn/link")

    class Meta:
        verbose_name = 'fil'
        verbose_name_plural = 'filer'

    def __unicode__(self):
        return self.file.url

class FrontImage(models.Model):
    POSITIONS = (
        ('top', 'Top'),
        ('bottom', 'Bund'),
        )

    position = models.CharField("placering", max_length=30, choices=POSITIONS, default="top")
    image = DbIdImageField("billede", upload_to="dyn/frontpage")
    order = models.IntegerField("rækkefølge", default=0)
    link = models.CharField(max_length=200, blank=True)
    active = models.BooleanField("aktiv", default=True)

    class Meta:
        ordering = ("-position", "order")
        verbose_name = "forsidebillede"
        verbose_name_plural = "forsidebilleder"

    def __unicode__(self):
        return "billede %s" % self.id


class News(models.Model):
    title = models.CharField("titel", max_length=200)
    url = models.SlugField('url', max_length=200, unique=True, help_text="Autoudfyldes")
    text = models.TextField('tekst')

    featured = models.BooleanField('primær', help_text=u"Skal nyheden vises i slideshowet?")
    front_image = DbIdImageField("slideshowbillede", upload_to="dyn/blog", blank=True, help_text="Hvis det en primær nyhed skal der være et slideshowbillede")
    image = DbIdImageField("billede", upload_to="dyn/news", blank=True)
    published = models.DateTimeField("udgivet", default=datetime.now)

    files = generic.GenericRelation('File', verbose_name="filer")

    class Meta:
        ordering = ("-published",)
        verbose_name = u"nyhedsindlæg"
        verbose_name_plural = u"nyhedsindlæg"

    def __unicode__(self):
        return self.title


class Event(models.Model):
    TYPES = (
        ('bs', 'BS-arrangement'),
        ('external', 'Eksternt arrangement'),
        )
    type = models.CharField(max_length=30, choices=TYPES, default="bs")
    name = models.CharField("navn", max_length=200)
    url = models.SlugField('url', max_length=200, unique=True, help_text="Autoudfyldes")
    date = models.DateField("dato")
    time = models.CharField("tid", max_length=100, blank=True)
    image = DbIdImageField("forsidebillede", upload_to="dyn/event", blank=True, editable=False)
    image_detail = DbIdImageField("undersidebillede", upload_to="dyn/eventdetail", blank=True)
    intro = models.TextField("introduktion")
    text = models.TextField("beskrivelse")
    organizer = models.CharField(u"arrangør", max_length=200)
    email = models.EmailField(u'email tilmeldinger til', blank=True, help_text=u"Email-adressen tilmeldinger sendes til. Hvis den ikke udfyldes sendes tilmeldingerne til sekretariatet.")
    signup_link = models.URLField("link til tilmelding", blank=True, help_text=u"Udfyldes hvis der er et eksternt arrangement")

    event_number = models.PositiveIntegerField("arrangement nummer")
    place = models.CharField("sted", max_length=200)
    price_member = models.PositiveIntegerField("medlemspris")
    price_nonmember = models.PositiveIntegerField("pris for ikke-medlemmer")
    group = models.ForeignKey('MemberGroup', blank=True, null=True)

    files = generic.GenericRelation('File', verbose_name="filer")

    class Meta:
        ordering = ("-date",)
        verbose_name = u"arrangement"
        verbose_name_plural = "arrangementer"

    def __unicode__(self):
        return self.name

class MemberProxy(models.Model):
    number = models.PositiveIntegerField()

class MemberGroup(models.Model):
    TYPES = (
        ("udvalg", "Udvalg"),
        ("bs-gruppe", "BS-Gruppe"),
        ("lokalomraade", "Lokalområde"),
        )
    type = models.CharField('type', max_length=30, choices=TYPES)
    name = models.CharField('navn', max_length=200)
    menu = models.CharField('menupunkt', max_length=200)
    url = models.SlugField('url', max_length=200, unique=True, help_text="Autoudfyldes")
    order = models.IntegerField(u'rækkefølge', default=0)
    image = DbIdImageField("billede", upload_to="dyn/group/main", blank=True)
    intro = models.TextField("introduktion", blank=True)
    text = models.TextField("tekst")

    extra_header = models.CharField("ekstra-sektion overskrift", max_length=200, blank=True)
    extra_text = models.TextField("ekstra-sektion tekst", blank=True)
    extra_image = DbIdImageField("ekstra-sektion billede", upload_to="dyn/group/extra", blank=True)
    extra_image1= DbIdImageField("ekstra-sektion billede1", upload_to="dyn/blog", blank=True)
    
    president = models.ManyToManyField(MemberProxy, verbose_name="formand", related_name="%(class)s_president", blank=True)
    vicepresident = models.ManyToManyField(MemberProxy, verbose_name=u"næstformand", related_name="%(class)s_vicepresident", blank=True)
    treasurer = models.ManyToManyField(MemberProxy, verbose_name="kasserer", related_name="%(class)s_treasurer", blank=True)
    alternates = models.ManyToManyField(MemberProxy, verbose_name=u"sekretær", related_name="%(class)s_alternates", blank=True)
    board = models.ManyToManyField(MemberProxy, verbose_name="bestyrelsesmedlemmer", related_name="%(class)s_board", blank=True)

    members = models.ManyToManyField(MemberProxy, verbose_name="medlemmer", related_name="%(class)s_members", blank=True, help_text=u"Vises ikke på gruppens side, men bruges i søgningen under medlemmer, derfor skal alle gruppens medlemmer tilføjes her")

    files = generic.GenericRelation('File', verbose_name="filer")

    class Meta:
        ordering = ("type", "order", "name")
        verbose_name = "Gruppe"
        verbose_name_plural = "Grupper"

    # helper for relations spanning databases
    def winkas_members(self, field="members"):
        return winkas.Member.objects.filter(id__in=list(getattr(self, field).values_list("number", flat=True)))

    def winkas_president(self):
        return self.winkas_members("president")
    def winkas_vicepresident(self):
        return self.winkas_members("vicepresident")
    def winkas_treasurer(self):
        return self.winkas_members("treasurer")
    def winkas_alternates(self):
        return self.winkas_members("alternates")
    def winkas_board(self):
        return self.winkas_members("board")

    def __unicode__(self):
        return "%s: %s" % (self.get_type_display(), self.name)


class Page(models.Model):
    SECTIONS = (
        ('lokalomraade', u'Lokalområder'),
        ('udvalg', 'Udvalg'),
        ('bs-gruppe', 'BS-Grupper'),
        ('event', 'Arrangementer'),
        ('member', 'Medlemmer'),
        ('about', 'Om Byggesocietetet'),
        )
    section = models.CharField(max_length=50, choices=SECTIONS)
    parent = models.ForeignKey('self', null=True, blank=True, verbose_name="Ovenliggende side")
    name = models.CharField('navn', max_length=200)
    menu = models.CharField('menupunkt', max_length=200)
    url = models.SlugField('url', max_length=200, unique=True, help_text="Autoudfyldes")
    primary = models.BooleanField('hovedside', help_text="Denne side vises når sektionen klikkes")
    order = models.IntegerField(u'rækkefølge', default=0)
    image = DbIdImageField("billede", upload_to="dyn/page", blank=True)
    front_image=DbIdImageField("billedes", upload_to="dyn/blog", blank=True)
    intro = models.TextField("introduktion", blank=True)
    text = models.TextField("tekst")

#    photos = generic.GenericRelation('Photo', verbose_name="billeder")
    files = generic.GenericRelation('File', verbose_name="filer")

    class Meta:
        ordering = ("section", "order")
        verbose_name = "side"
        verbose_name_plural = "sider"        

    def __unicode__(self):
        if self.parent:
            return "%s > %s" % (unicode(self.parent), self.name)
        else:
            return "%s: %s" % (self.get_section_display(), self.name)






class EventSignup(models.Model):
    event = models.ForeignKey(Event)
    company = models.CharField("firmanavn", max_length=100, blank=True)
    name = models.CharField("navn", max_length=100)
    address = models.CharField("adresse", max_length=200)
    zipcode = models.CharField("postnr.", max_length=30)
    city = models.CharField("by", max_length=100)
    phone = models.CharField("telefonnr.", max_length=30, blank=True)
    fax = models.CharField("telefaxnr.", max_length=30, blank=True)
    email = models.EmailField("e-mail-adresse")
    industry = models.CharField("branche", max_length=100, blank=True)
    ean = models.CharField("EAN-nummer", max_length=50, blank=True)
    amount = models.PositiveIntegerField("antal deltagere", default=1)
    others = models.TextField("navn på øvrige deltagere", blank=True)
    member = models.BooleanField("medlem af Byggesocietetet")

    class Meta:
        verbose_name = "arrangement tilmelding"
        verbose_name_plural = "arrangement tilmeldinger"

    def __unicode__(self):
        return self.name

    def get_total(self):
        if self.member:
            return self.event.price_member * self.amount
        else:
            return self.event.price_nonmember * self.amount

    def get_total_excluding_tax(self):
        return self.get_total() * 0.8

    def get_tax(self):
        return self.get_total() * 0.2


class TextSnippet(models.Model):
    key = models.CharField(u"placering", max_length=100, unique=True)
    text = models.TextField(u'tekst', blank=True)

    last_modified = models.DateTimeField(u'sidst ændret', auto_now=True)
    use_visual_editor = models.BooleanField(default=False, editable=False)

    def __unicode__(self):
        return self.key
    
    class Meta:
        ordering = ('key',)
        verbose_name = "tekststump"
        verbose_name_plural = "tekststumper"

    @classmethod
    def get_snippet(cls, key):
        snippet, _ = cls.objects.get_or_create(key=key)
        return snippet.text or ""

class StyleSheet(models.Model):
    text = models.TextField('styles', blank=True, help_text='See current style sheets: <a target="_blank" href="%scss/layout.css">layout.css</a> and <a target="_blank" href="%scss/grid.css">grid.css</a>' % (settings.MEDIA_URL, settings.MEDIA_URL))

    def __unicode__(self):
        return "Custom style sheet"

    def save(self):
        super(StyleSheet, self).save()

        f = open(os.path.join(settings.MEDIA_ROOT, "dyn/custom.css"), "w")
        f.write(self.text.encode("utf8"))

class Industry(models.Model):
    name = models.CharField("navn", max_length=100, blank=True)
    
    class Meta:
        verbose_name = "Branche"
        verbose_name_plural = "Branches"
        ordering = ['name']
    def __unicode__(self):
        return self.name

class Signup(models.Model):
    MEMBERSHIPS = (
        ('a', 'A-medlem'),
        ('b', 'B-medlem'),
        )

    
    membership = models.CharField("medlemskab", max_length=30, choices=MEMBERSHIPS)
    company = models.CharField("firmanavn", max_length=100, blank=True)
    name = models.CharField("navn", max_length=100)
    industry=models.ForeignKey(Industry,verbose_name="Branche")
    address = models.CharField("adresse", max_length=200)
    zipcode = models.CharField("postnr.", max_length=30)
    city = models.CharField("by", max_length=100)
    phone = models.CharField("telefonnr.", max_length=30, blank=True)
    # fax = models.CharField("telefaxnr.", max_length=30, blank=True)
    email = models.EmailField("e-mail-adresse")
    ean = models.CharField("EAN-nummer", max_length=50, blank=True)
    dob = models.CharField(u"fødselsdato", max_length=50)
    accept_visibility = models.BooleanField("jeg tillader at mine data bliver vist på internettet")
    accept_visibility1 = models.BooleanField("er interesseret i at blive medlem af en BS-gruppe og vil gerne kontaktes herom")
    

    class Meta:
        verbose_name = "tilmelding"
        verbose_name_plural = "tilmeldinger"

    def __unicode__(self):
        return self.name

class contact(models.Model):
    name = models.CharField("navn", max_length=100)
    email = models.EmailField("e-mail-adresse")
    comment= models.TextField("ønsket ændring", max_length=500)

    class Meta:
        verbose_name = "kontakt"
        verbose_name_plural = "kontakter"

    def __unicode__(self):
        return self.name

class mail(models.Model):
    name = models.CharField("navn", max_length=100)
    email = models.EmailField("e-mail-adresse")
    subject= models.CharField("emne", max_length=500)
    message= models.TextField("besked", max_length=500)

    class Meta:
        verbose_name = "mail"
        verbose_name_plural = "mails"

    def __unicode__(self):
        return self.email        

class maillist(models.Model):
    email = models.EmailField("e-mail-adresse",unique=True)
    class Meta:
        verbose_name = "mailing list"
        verbose_name_plural = "mailing lists"

    def __unicode__(self):
        return self.email  