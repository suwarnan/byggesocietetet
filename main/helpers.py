# -*- coding: utf-8 -*-
from django.conf import settings
from django.core.urlresolvers import reverse as urlreverse
from django.template import RequestContext
from django.http import HttpResponse
from django.utils.http import http_date
from main.models import *
from datetime import *
from main.forms import *
def get_context(request):
    javascript = [
        "lib/jquery-1.7.1.min.js",
        ]
    if request.method == "POST":
        form2= maillistForm(request.POST)
        if form2.is_valid():
            form2.save()
    else:
        form2= maillistForm()

    pages_menu = []
    for page in Page.objects.filter(parent=None, section="about"):
        pages_menu.append({'name': page.menu, 'url': urlreverse('about_page', args=(page.url,)),
                           'children': [{'name': p.menu, 'url': urlreverse('about_page', args=(p.url,)),
                                         'children': [{'name': p.menu, 'url': urlreverse('about_page', args=(p.url,))}
                                                      for q in p.page_set.all()]}
                                        for p in page.page_set.all()]})
        
    event_years = sorted(set(unicode(e['date'].year) for e in Event.objects.filter(date__lte=date.today()).values("date")), reverse=True)
    news_years = sorted(set(unicode(n['published'].year) for n in News.objects.values("published")), reverse=True)

    submenu = [
        
        {'name': TextSnippet.get_snippet("menupunkt: om byggesocietetet"), 'url': urlreverse('about'),
         'children': pages_menu},


        {'name': TextSnippet.get_snippet("menupunkt: lokalområder"), 'url': urlreverse('member_group_page_default', args=("lokalomraade",)),
         'children': [{'name': m.menu, 'url': urlreverse('member_group', args=(m.type, m.url))}
                      for m in MemberGroup.objects.filter(type="lokalomraade")] +
         [{'name': p.menu, 'url': urlreverse('member_group_page', args=("lokalomraade", p.url))} for p in Page.objects.filter(section="lokalomraade", primary=False)]},


        {'name': TextSnippet.get_snippet("menupunkt: udvalg"), 'url': urlreverse('member_group_page_default', args=("udvalg",)),
         'children': [{'name': m.menu, 'url': urlreverse('member_group', args=(m.type, m.url))}
                      for m in MemberGroup.objects.filter(type="udvalg")] +
         [{'name': p.menu, 'url': urlreverse('member_group_page', args=("udvalg", p.url))} for p in Page.objects.filter(section="udvalg", primary=False)]
         },

      
        {'name': TextSnippet.get_snippet("menupunkt: bs-grupper"), 'url': urlreverse('member_group_page_default', args=("bs-gruppe",)),
         'children': [{'name': m.menu, 'url': urlreverse('member_group', args=(m.type, m.url))}
                      for m in MemberGroup.objects.filter(type="bs-gruppe")] +
         [{'name': p.menu, 'url': urlreverse('member_group_page', args=("bs-gruppe", p.url))} for p in Page.objects.filter(section="bs-gruppe", primary=False)]},

        # {'name': TextSnippet.get_snippet("menupunkt: arrangementer"), 'url': urlreverse('event_overview'),
        #  'children': [{'name': TextSnippet.get_snippet("menupunkt: arrangementer aktuelle"),
        #                'url': urlreverse('event_overview_current')},
        #               {'name': TextSnippet.get_snippet("menupunkt: arrangementer ældre"),
        #                'url': urlreverse('event_overview_past_current'),
        #                'children': [{'name': y,
        #                              'url': urlreverse('event_overview_past', args=(y,))} for y in event_years]}
        #               ] + [{'name': p.menu, 'url': urlreverse('event_page', args=(p.url,))} for p in Page.objects.filter(section="event", primary=False)]},

        {'name': TextSnippet.get_snippet("menupunkt: medlemmer"), 'url': urlreverse('members_page'),
         'children': [
             {'name': TextSnippet.get_snippet("menupunkt: medlemmer søg"),
              'url': urlreverse('member_section_find')},
             # {'name': TextSnippet.get_snippet("menupunkt: medlemmer a"),
             #  'url': urlreverse('member_section', args=("a",))},
             # {'name': TextSnippet.get_snippet("menupunkt: medlemmer b"),
             #  'url': urlreverse('member_section', args=("b",))},
             {'name': TextSnippet.get_snippet("menupunkt: medlemmer indmelding"),
              'url': urlreverse('member_registration')},
             {'name': TextSnippet.get_snippet("menupunkt: medlemmer Ændringe"),
              'url': urlreverse('member_contact')},]},

        # {'name': TextSnippet.get_snippet("menupunkt: nyheder"), 'url': urlreverse('news_overview'),
        #  'children': [{'name': y,
        #                'url': urlreverse('news_overview', args=(y,))} for y in news_years]},

        
        ]

    if hasattr(request, "javascript"):
        javascript.extend(request.javascript)

    includes = []
    
    # transform javascript urls
    for js in javascript:
        if js.startswith("http"):
            includes.append(js)
        else:
            includes.append("%sjs/%s" % (settings.MEDIA_URL, js))
    
    # form2= maillistForm()
    context = {
        'submenu': submenu,
        'request': request,
        'settings': settings,
        'javascripts': includes,
        'upcoming_events': Event.objects.filter(date__gte=date.today()).order_by("date")[:5],
        'foot1':foot.objects.filter(object_id="1"),
        'foot2':foot.objects.filter(object_id="2"),
        'form2':form2,
        # 'man':Page.objects.all(),
        }

    if os.path.exists(os.path.join(settings.MEDIA_ROOT, "dyn/custom.css")):
        context['custom_style_sheet'] = "dyn/custom.css"

    return RequestContext(request, context)

class ExcelResponse(HttpResponse):
    def __init__(self, *args, **kwargs):
        kwargs['mimetype'] = "application/vnd.ms-excel, charset=utf-8; encoding=utf-8"
        super(ExcelResponse, self).__init__(*args, **kwargs)

    def set_excel_headers(self, name):
        self['Content-Disposition'] = 'attachment; filename="%s"' % name
        self["Last-Modified"] = http_date()
        self['Content-Length'] = str(len(self.content))
        self['Content-Type'] = "application/vnd.ms-excel"
