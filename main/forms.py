# -*- coding: utf-8 
from django import forms
from django.utils import simplejson
from main.models import Signup, EventSignup, Event,contact,mail,maillist
from winkas.models import Member

class SignupForm(forms.Form):
    name = forms.CharField(label="Navn", max_length=100)
    address = forms.CharField(label="Adresse", max_length=100)
    zipcode = forms.CharField(label="Postnummer", max_length=20)
    city = forms.CharField(label="By", max_length=100)
    phone = forms.CharField(label="Telefon", max_length=20)
    comment = forms.CharField(label="Bemærkninger", widget=forms.Textarea(), required=False)

    terms = forms.BooleanField(label="Accepteret handelsbetingelserne", required=False)

    def clean_terms(self):
        terms = self.cleaned_data["terms"]
        if not terms:
            raise forms.ValidationError("Du skal acceptere handlesbetingelserne")
        else:
            return terms

class maillistForm(forms.ModelForm) :
    class Meta:
        model = maillist
        widgets = {
            'email': forms.TextInput(attrs={'placeholder': 'mail@example.com'}),
            
        }

class SignupForm(forms.ModelForm):
    # industry = forms.ModelChoiceField(queryset=Industry.objects.all(),widget=forms.HiddenInput())
    # accept_visibility=forms.BooleanField()
    # accept_visibility1 = forms.BooleanField()
    class Meta:
        model = Signup


     # def __init__(self, *args, **kwargs):
     #     super(SignupForm, self).__init__(*args, **kwargs)
     #      self.fields['industry'].queryset =Industry.objects.name()
class ContactForm(forms.ModelForm):
    class Meta:
        model = contact 


class MailForm(forms.ModelForm):
    class Meta:
        model = mail 
        # widgets = {
        #     'email': forms.TextInput(attrs={'placeholder': 'mail@example.com'}),
            
        # }

        
class EventSignupForm(forms.ModelForm):
    event = forms.ModelChoiceField(queryset=Event.objects.all(), widget=forms.HiddenInput())
    class Meta:
        model = EventSignup

from main.models import MemberGroup
class MemberSearchForm(forms.Form):
    query = forms.CharField(max_length=200, required=False)
    group = forms.ChoiceField(choices=[], required=False)

    def __init__(self, *args, **kwargs):
        super(MemberSearchForm, self).__init__(*args, **kwargs)
        areas = set()
        for a in Member.objects.exclude(community="").order_by("community").values_list("community", flat=True).distinct():
            area = a.strip().replace("  ", " ")
            try:
                area=area# area = " ".join(area.split(" ")[1:])
            except:
                pass
            if area:
                areas.add((area, area))
        self.fields['group'].choices = [('', u"Alle lokalområder")] + sorted(list(areas))
