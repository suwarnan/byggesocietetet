# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Photo'
        db.create_table('main_photo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=300, blank=True)),
        ))
        db.send_create_signal('main', ['Photo'])

        # Adding model 'File'
        db.create_table('main_file', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('main', ['File'])

        # Adding model 'FrontImage'
        db.create_table('main_frontimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('main', ['FrontImage'])

        # Adding model 'News'
        db.create_table('main_news', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, db_index=True)),
            ('text', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('featured', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('front_image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('published', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal('main', ['News'])

        # Adding model 'Event'
        db.create_table('main_event', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, db_index=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('time', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('organizer', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('main', ['Event'])

        # Adding model 'MemberProxy'
        db.create_table('main_memberproxy', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('main', ['MemberProxy'])

        # Adding model 'Area'
        db.create_table('main_area', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('menu', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, db_index=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('chairman_description', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('main', ['Area'])

        # Adding M2M table for field members on 'Area'
        db.create_table('main_area_members', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('area', models.ForeignKey(orm['main.area'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique('main_area_members', ['area_id', 'memberproxy_id'])

        # Adding M2M table for field board on 'Area'
        db.create_table('main_area_board', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('area', models.ForeignKey(orm['main.area'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique('main_area_board', ['area_id', 'memberproxy_id'])

        # Adding model 'Committee'
        db.create_table('main_committee', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('menu', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, db_index=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('main', ['Committee'])

        # Adding M2M table for field members on 'Committee'
        db.create_table('main_committee_members', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('committee', models.ForeignKey(orm['main.committee'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique('main_committee_members', ['committee_id', 'memberproxy_id'])

        # Adding model 'BSGroup'
        db.create_table('main_bsgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('menu', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, db_index=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('main', ['BSGroup'])

        # Adding M2M table for field members on 'BSGroup'
        db.create_table('main_bsgroup_members', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('bsgroup', models.ForeignKey(orm['main.bsgroup'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique('main_bsgroup_members', ['bsgroup_id', 'memberproxy_id'])

        # Adding model 'Page'
        db.create_table('main_page', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('section', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Page'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('menu', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200, db_index=True)),
            ('primary', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('main', ['Page'])

        # Adding model 'TextSnippet'
        db.create_table('main_textsnippet', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('text', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('use_visual_editor', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('main', ['TextSnippet'])


    def backwards(self, orm):
        
        # Deleting model 'Photo'
        db.delete_table('main_photo')

        # Deleting model 'File'
        db.delete_table('main_file')

        # Deleting model 'FrontImage'
        db.delete_table('main_frontimage')

        # Deleting model 'News'
        db.delete_table('main_news')

        # Deleting model 'Event'
        db.delete_table('main_event')

        # Deleting model 'MemberProxy'
        db.delete_table('main_memberproxy')

        # Deleting model 'Area'
        db.delete_table('main_area')

        # Removing M2M table for field members on 'Area'
        db.delete_table('main_area_members')

        # Removing M2M table for field board on 'Area'
        db.delete_table('main_area_board')

        # Deleting model 'Committee'
        db.delete_table('main_committee')

        # Removing M2M table for field members on 'Committee'
        db.delete_table('main_committee_members')

        # Deleting model 'BSGroup'
        db.delete_table('main_bsgroup')

        # Removing M2M table for field members on 'BSGroup'
        db.delete_table('main_bsgroup_members')

        # Deleting model 'Page'
        db.delete_table('main_page')

        # Deleting model 'TextSnippet'
        db.delete_table('main_textsnippet')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'main.area': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'Area'},
            'board': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'area_board_members'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'chairman_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_area_related'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.bsgroup': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'BSGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_bsgroup_related'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.committee': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'Committee'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_committee_related'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.event': {
            'Meta': {'ordering': "('date',)", 'object_name': 'Event'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'organizer': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'time': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.file': {
            'Meta': {'object_name': 'File'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'main.frontimage': {
            'Meta': {'ordering': "('order',)", 'object_name': 'FrontImage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'main.memberproxy': {
            'Meta': {'object_name': 'MemberProxy'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'main.news': {
            'Meta': {'ordering': "('-published',)", 'object_name': 'News'},
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'front_image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'published': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.page': {
            'Meta': {'ordering': "('section', 'order')", 'object_name': 'Page'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Page']", 'null': 'True', 'blank': 'True'}),
            'primary': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.photo': {
            'Meta': {'object_name': 'Photo'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'})
        },
        'main.textsnippet': {
            'Meta': {'ordering': "('key',)", 'object_name': 'TextSnippet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'use_visual_editor': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['main']
