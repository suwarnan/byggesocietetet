# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Photo'
        db.create_table('main_photo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=300, blank=True)),
        ))
        db.send_create_signal('main', ['Photo'])

        # Adding model 'foot'
        db.create_table('main_foot', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100)),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=300, blank=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('main', ['foot'])

        # Adding model 'File'
        db.create_table('main_file', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
            ('object_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('main', ['File'])

        # Adding model 'UploadedFile'
        db.create_table('main_uploadedfile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('main', ['UploadedFile'])

        # Adding model 'FrontImage'
        db.create_table('main_frontimage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('position', self.gf('django.db.models.fields.CharField')(default='top', max_length=30)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('main', ['FrontImage'])

        # Adding model 'News'
        db.create_table('main_news', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('featured', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('front_image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('published', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal('main', ['News'])

        # Adding model 'Event'
        db.create_table('main_event', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.CharField')(default='bs', max_length=30)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('time', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('image_detail', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')()),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('organizer', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('signup_link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('event_number', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('place', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('price_member', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('price_nonmember', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.MemberGroup'], null=True, blank=True)),
        ))
        db.send_create_signal('main', ['Event'])

        # Adding model 'MemberProxy'
        db.create_table('main_memberproxy', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('main', ['MemberProxy'])

        # Adding model 'MemberGroup'
        db.create_table('main_membergroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('menu', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('extra_header', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('extra_text', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('extra_image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('main', ['MemberGroup'])

        # Adding M2M table for field president on 'MemberGroup'
        m2m_table_name = db.shorten_name('main_membergroup_president')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('membergroup', models.ForeignKey(orm['main.membergroup'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique(m2m_table_name, ['membergroup_id', 'memberproxy_id'])

        # Adding M2M table for field vicepresident on 'MemberGroup'
        m2m_table_name = db.shorten_name('main_membergroup_vicepresident')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('membergroup', models.ForeignKey(orm['main.membergroup'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique(m2m_table_name, ['membergroup_id', 'memberproxy_id'])

        # Adding M2M table for field treasurer on 'MemberGroup'
        m2m_table_name = db.shorten_name('main_membergroup_treasurer')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('membergroup', models.ForeignKey(orm['main.membergroup'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique(m2m_table_name, ['membergroup_id', 'memberproxy_id'])

        # Adding M2M table for field alternates on 'MemberGroup'
        m2m_table_name = db.shorten_name('main_membergroup_alternates')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('membergroup', models.ForeignKey(orm['main.membergroup'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique(m2m_table_name, ['membergroup_id', 'memberproxy_id'])

        # Adding M2M table for field board on 'MemberGroup'
        m2m_table_name = db.shorten_name('main_membergroup_board')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('membergroup', models.ForeignKey(orm['main.membergroup'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique(m2m_table_name, ['membergroup_id', 'memberproxy_id'])

        # Adding M2M table for field members on 'MemberGroup'
        m2m_table_name = db.shorten_name('main_membergroup_members')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('membergroup', models.ForeignKey(orm['main.membergroup'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique(m2m_table_name, ['membergroup_id', 'memberproxy_id'])

        # Adding model 'Page'
        db.create_table('main_page', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('section', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Page'], null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('menu', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=200)),
            ('primary', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('main', ['Page'])

        # Adding model 'EventSignup'
        db.create_table('main_eventsignup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Event'])),
            ('company', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('zipcode', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('industry', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('ean', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('amount', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('others', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('member', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('main', ['EventSignup'])

        # Adding model 'TextSnippet'
        db.create_table('main_textsnippet', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('text', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('last_modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('use_visual_editor', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('main', ['TextSnippet'])

        # Adding model 'StyleSheet'
        db.create_table('main_stylesheet', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('main', ['StyleSheet'])

        # Adding model 'Industry'
        db.create_table('main_industry', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('main', ['Industry'])

        # Adding model 'Signup'
        db.create_table('main_signup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('membership', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('company', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('industry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Industry'])),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('zipcode', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('ean', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('dob', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('accept_visibility', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('accept_visibility1', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('main', ['Signup'])

        # Adding model 'contact'
        db.create_table('main_contact', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('comment', self.gf('django.db.models.fields.TextField')(max_length=500)),
        ))
        db.send_create_signal('main', ['contact'])


    def backwards(self, orm):
        # Deleting model 'Photo'
        db.delete_table('main_photo')

        # Deleting model 'foot'
        db.delete_table('main_foot')

        # Deleting model 'File'
        db.delete_table('main_file')

        # Deleting model 'UploadedFile'
        db.delete_table('main_uploadedfile')

        # Deleting model 'FrontImage'
        db.delete_table('main_frontimage')

        # Deleting model 'News'
        db.delete_table('main_news')

        # Deleting model 'Event'
        db.delete_table('main_event')

        # Deleting model 'MemberProxy'
        db.delete_table('main_memberproxy')

        # Deleting model 'MemberGroup'
        db.delete_table('main_membergroup')

        # Removing M2M table for field president on 'MemberGroup'
        db.delete_table(db.shorten_name('main_membergroup_president'))

        # Removing M2M table for field vicepresident on 'MemberGroup'
        db.delete_table(db.shorten_name('main_membergroup_vicepresident'))

        # Removing M2M table for field treasurer on 'MemberGroup'
        db.delete_table(db.shorten_name('main_membergroup_treasurer'))

        # Removing M2M table for field alternates on 'MemberGroup'
        db.delete_table(db.shorten_name('main_membergroup_alternates'))

        # Removing M2M table for field board on 'MemberGroup'
        db.delete_table(db.shorten_name('main_membergroup_board'))

        # Removing M2M table for field members on 'MemberGroup'
        db.delete_table(db.shorten_name('main_membergroup_members'))

        # Deleting model 'Page'
        db.delete_table('main_page')

        # Deleting model 'EventSignup'
        db.delete_table('main_eventsignup')

        # Deleting model 'TextSnippet'
        db.delete_table('main_textsnippet')

        # Deleting model 'StyleSheet'
        db.delete_table('main_stylesheet')

        # Deleting model 'Industry'
        db.delete_table('main_industry')

        # Deleting model 'Signup'
        db.delete_table('main_signup')

        # Deleting model 'contact'
        db.delete_table('main_contact')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'main.contact': {
            'Meta': {'object_name': 'contact'},
            'comment': ('django.db.models.fields.TextField', [], {'max_length': '500'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'main.event': {
            'Meta': {'ordering': "('-date',)", 'object_name': 'Event'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'event_number': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.MemberGroup']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'image_detail': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'organizer': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'place': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'price_member': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'price_nonmember': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'signup_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'time': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'bs'", 'max_length': '30'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'})
        },
        'main.eventsignup': {
            'Meta': {'object_name': 'EventSignup'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'amount': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ean': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Event']"}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'member': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'others': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'zipcode': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'main.file': {
            'Meta': {'object_name': 'File'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'main.foot': {
            'Meta': {'object_name': 'foot'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        'main.frontimage': {
            'Meta': {'ordering': "('-position', 'order')", 'object_name': 'FrontImage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'position': ('django.db.models.fields.CharField', [], {'default': "'top'", 'max_length': '30'})
        },
        'main.industry': {
            'Meta': {'object_name': 'Industry'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'main.membergroup': {
            'Meta': {'ordering': "('type', 'order', 'name')", 'object_name': 'MemberGroup'},
            'alternates': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'membergroup_alternates'", 'blank': 'True', 'to': "orm['main.MemberProxy']"}),
            'board': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'membergroup_board'", 'blank': 'True', 'to': "orm['main.MemberProxy']"}),
            'extra_header': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'extra_image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'extra_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'membergroup_members'", 'blank': 'True', 'to': "orm['main.MemberProxy']"}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'president': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'membergroup_president'", 'blank': 'True', 'to': "orm['main.MemberProxy']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'treasurer': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'membergroup_treasurer'", 'blank': 'True', 'to': "orm['main.MemberProxy']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'}),
            'vicepresident': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'membergroup_vicepresident'", 'blank': 'True', 'to': "orm['main.MemberProxy']"})
        },
        'main.memberproxy': {
            'Meta': {'object_name': 'MemberProxy'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'main.news': {
            'Meta': {'ordering': "('-published',)", 'object_name': 'News'},
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'front_image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'published': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'})
        },
        'main.page': {
            'Meta': {'ordering': "('section', 'order')", 'object_name': 'Page'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Page']", 'null': 'True', 'blank': 'True'}),
            'primary': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200'})
        },
        'main.photo': {
            'Meta': {'object_name': 'Photo'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'})
        },
        'main.signup': {
            'Meta': {'object_name': 'Signup'},
            'accept_visibility': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'accept_visibility1': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'company': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'dob': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'ean': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Industry']"}),
            'membership': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'zipcode': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'main.stylesheet': {
            'Meta': {'object_name': 'StyleSheet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'main.textsnippet': {
            'Meta': {'ordering': "('key',)", 'object_name': 'TextSnippet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'use_visual_editor': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'main.uploadedfile': {
            'Meta': {'object_name': 'UploadedFile'},
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        }
    }

    complete_apps = ['main']