# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting model 'Committee'
        db.delete_table('main_committee')

        # Removing M2M table for field members on 'Committee'
        db.delete_table('main_committee_members')

        # Deleting model 'Area'
        db.delete_table('main_area')

        # Removing M2M table for field members on 'Area'
        db.delete_table('main_area_members')

        # Removing M2M table for field board on 'Area'
        db.delete_table('main_area_board')

        # Deleting model 'BSGroup'
        db.delete_table('main_bsgroup')

        # Removing M2M table for field members on 'BSGroup'
        db.delete_table('main_bsgroup_members')


    def backwards(self, orm):
        
        # Adding model 'Committee'
        db.create_table('main_committee', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('url', self.gf('django.db.models.fields.SlugField')(max_length=200, unique=True, db_index=True)),
            ('menu', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('main', ['Committee'])

        # Adding M2M table for field members on 'Committee'
        db.create_table('main_committee_members', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('committee', models.ForeignKey(orm['main.committee'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique('main_committee_members', ['committee_id', 'memberproxy_id'])

        # Adding model 'Area'
        db.create_table('main_area', (
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('url', self.gf('django.db.models.fields.SlugField')(max_length=200, unique=True, db_index=True)),
            ('menu', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('chairman_description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('main', ['Area'])

        # Adding M2M table for field members on 'Area'
        db.create_table('main_area_members', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('area', models.ForeignKey(orm['main.area'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique('main_area_members', ['area_id', 'memberproxy_id'])

        # Adding M2M table for field board on 'Area'
        db.create_table('main_area_board', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('area', models.ForeignKey(orm['main.area'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique('main_area_board', ['area_id', 'memberproxy_id'])

        # Adding model 'BSGroup'
        db.create_table('main_bsgroup', (
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('url', self.gf('django.db.models.fields.SlugField')(max_length=200, unique=True, db_index=True)),
            ('menu', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('image', self.gf('utils.overwritestorage.DbIdImageField')(max_length=100, blank=True)),
            ('intro', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('main', ['BSGroup'])

        # Adding M2M table for field members on 'BSGroup'
        db.create_table('main_bsgroup_members', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('bsgroup', models.ForeignKey(orm['main.bsgroup'], null=False)),
            ('memberproxy', models.ForeignKey(orm['main.memberproxy'], null=False))
        ))
        db.create_unique('main_bsgroup_members', ['bsgroup_id', 'memberproxy_id'])


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'main.event': {
            'Meta': {'ordering': "('date',)", 'object_name': 'Event'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'organizer': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'time': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.file': {
            'Meta': {'object_name': 'File'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'main.frontimage': {
            'Meta': {'ordering': "('order',)", 'object_name': 'FrontImage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'main.membergroup': {
            'Meta': {'ordering': "('type', 'order', 'name')", 'object_name': 'MemberGroup'},
            'alternates': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_alternates'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'extra_header': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'extra_image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'extra_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_related'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'president': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_president'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'treasurer': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_treasurer'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'}),
            'vicepresident': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_vicepresident'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"})
        },
        'main.memberproxy': {
            'Meta': {'object_name': 'MemberProxy'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'main.news': {
            'Meta': {'ordering': "('-published',)", 'object_name': 'News'},
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'front_image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'published': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.page': {
            'Meta': {'ordering': "('section', 'order')", 'object_name': 'Page'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Page']", 'null': 'True', 'blank': 'True'}),
            'primary': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.photo': {
            'Meta': {'object_name': 'Photo'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'})
        },
        'main.textsnippet': {
            'Meta': {'ordering': "('key',)", 'object_name': 'TextSnippet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'use_visual_editor': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['main']
