# encoding: utf-8
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):


    def forwards(self, orm):
        fields = [
            'name',
            'menu',
            'url',
            'order',
            'image',
            'intro',
            'text',
            ]

        for area in orm.Area.objects.all():
            m = orm.MemberGroup()
            m.type = "lokalomraade"
            for f in fields:
                setattr(m, f, getattr(area, f))
            m.extra_text = area.chairman_description
            m.save()
            m.members = area.members.all()

        for committee in orm.Committee.objects.all():
            m = orm.MemberGroup()
            m.type = "udvalg"
            for f in fields:
                setattr(m, f, getattr(committee, f))
            m.save()
            m.members = committee.members.all()

        for bsgroup in orm.BSGroup.objects.all():
            m = orm.MemberGroup()
            m.type = "bs-gruppe"
            for f in fields:
                setattr(m, f, getattr(bsgroup, f))
            m.save()
            m.members = bsgroup.members.all()


    def backwards(self, orm):
        pass


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'main.area': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'Area'},
            'board': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'area_board_members'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'chairman_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_area_related'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.bsgroup': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'BSGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_bsgroup_related'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.committee': {
            'Meta': {'ordering': "('order', 'name')", 'object_name': 'Committee'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_committee_related'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.event': {
            'Meta': {'ordering': "('date',)", 'object_name': 'Event'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'organizer': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'time': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.file': {
            'Meta': {'object_name': 'File'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'main.frontimage': {
            'Meta': {'ordering': "('order',)", 'object_name': 'FrontImage'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'main.membergroup': {
            'Meta': {'ordering': "('type', 'order', 'name')", 'object_name': 'MemberGroup'},
            'alternates': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_alternates'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'extra_header': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'extra_image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'extra_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_related'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'president': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_president'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'treasurer': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_treasurer'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'}),
            'vicepresident': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'main_membergroup_vicepresident'", 'symmetrical': 'False', 'to': "orm['main.MemberProxy']"})
        },
        'main.memberproxy': {
            'Meta': {'object_name': 'MemberProxy'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'main.news': {
            'Meta': {'ordering': "('-published',)", 'object_name': 'News'},
            'featured': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'front_image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'published': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.page': {
            'Meta': {'ordering': "('section', 'order')", 'object_name': 'Page'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100', 'blank': 'True'}),
            'intro': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'menu': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['main.Page']", 'null': 'True', 'blank': 'True'}),
            'primary': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        'main.photo': {
            'Meta': {'object_name': 'Photo'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('utils.overwritestorage.DbIdImageField', [], {'max_length': '100'}),
            'object_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '300', 'blank': 'True'})
        },
        'main.textsnippet': {
            'Meta': {'ordering': "('key',)", 'object_name': 'TextSnippet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'last_modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'use_visual_editor': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['main']
