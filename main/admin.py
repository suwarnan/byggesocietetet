# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django import forms
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from django.utils.encoding import force_unicode, smart_unicode

from main.models import *
from main.helpers import *
from winkas import models as winkas
from utils.templatetags import thumbnail

TINY_MCE_LIST = ('adminmedia/tiny_mce/tiny_mce.js','adminmedia/js/admin-tinymce.js')



class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, "url", None):
            image_url = value.url
            file_name=str(value)
            output.append(u' <a href="%s" target="_blank"><img src="%s" alt="%s" /></a> %s ' % \
                              (image_url, thumbnail.get_thumbnail(image_url, 113, 87, crop=thumbnail.NO_CROPPING), file_name, _('Change:')))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))



class PhotoInline(generic.GenericTabularInline):
    model = Photo
    extra = 3

class FilesInline(generic.GenericTabularInline):
    model = File
    extra = 3

class FrontImageAdmin(admin.ModelAdmin):
    list_display = ('preview_thumb', 'image', 'position', 'order')
    list_display_links = list_display
    list_filter = ('position',)

    def preview_thumb(self, obj):
        return mark_safe(u'<img src="%s">' % thumbnail.get_thumbnail(obj.image.url, 300, 300, crop=thumbnail.NO_CROPPING))
    preview_thumb.allow_tags = True
    
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'image':
            request = kwargs.pop("request", None)
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(FrontImageAdmin,self).formfield_for_dbfield(db_field, **kwargs)       
admin.site.register(FrontImage, FrontImageAdmin)

class UploadedFileAdmin(admin.ModelAdmin):
    list_display = ('id', 'link', 'name')
    #list_display_links = list_display

    def link(self, obj):
        return mark_safe(u'<a target="_blank" href="%s">%s</a>' % (obj.file.url, obj.file.url))
    link.allow_tags = True

admin.site.register(UploadedFile, UploadedFileAdmin)

from functools import partial
class MemberRelationAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(MemberRelationAdminForm, self).__init__(*args, **kwargs)

        # convert the winkas Member objects back to MemberProxy objects
        def winkas_clean(self, field):
            return [MemberProxy.objects.get_or_create(number=m.id)[0] for m in self.cleaned_data[field]]

        for name, field in self.fields.items():
            if type(field) == forms.ModelMultipleChoiceField and field.queryset.model._meta.object_name == 'Member':
                setattr(self, "clean_%s" % name, partial(winkas_clean, self, name))


class MemberGroupAdmin(admin.ModelAdmin):
    def member_count(self, obj):
        return obj.members.count()
    member_count.short_description = "antal medlemmer"
    
    list_display = ('type', 'name', 'menu', 'order', 'member_count')
    list_display_links = ('type', 'name')
    list_filter = ('type',)
    search_fields = ('name', )
    filter_horizontal = ('members', 'president' ,'vicepresident', 'treasurer', 'alternates', 'board')
    prepopulated_fields = {"url": ("name",)}
    list_editable = ('order',)
    inlines = [FilesInline]

    class Media:
        js = TINY_MCE_LIST 
        css = {
            "all": ("css/admin-member.css",)
            }

    form = MemberRelationAdminForm

    def get_object(self, request, object_id):
        self.obj = super(MemberGroupAdmin, self).get_object(request, object_id)
        return self.obj

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.rel.to._meta.object_name == 'MemberProxy':
            instance = getattr(self, "obj", None)
            kwargs['queryset'] = winkas.Member.objects.all()
            if instance and instance.id:
                ids = list(getattr(instance, db_field.name).values_list("number", flat=True))

                def fix(obj):
                    return winkas.Member.objects.filter(id__in=ids)
                db_field.value_from_object = fix

        return super(MemberGroupAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
admin.site.register(MemberGroup, MemberGroupAdmin)


class PageAdminForm(forms.ModelForm):
    def clean_parent(self):
        parent = self.cleaned_data["parent"]
        if parent == self.instance:
            raise forms.ValidationError("'Ovenliggende side' må ikke pege på sigselv")
        return parent

class PageAdmin(admin.ModelAdmin):
    list_display = ('name', 'menu', 'section', 'parent', 'primary', 'order')
    search_fields = ('name',)
    inlines = [FilesInline]
    prepopulated_fields = {"url": ("name",)}
#    use_fieldsets = ((None, {
#        'fields': ('title', 'text', 'menu', 'video_ids')
#        }),)
    
    class Media:
        js = TINY_MCE_LIST

    form = PageAdminForm
admin.site.register(Page, PageAdmin)

class EventAdmin(admin.ModelAdmin):

    list_display = ('name', 'date', 'time', 'organizer')
    search_fields = ('name',)
    inlines = [FilesInline]
    prepopulated_fields = {"url": ("name",)}
   
#    use_fieldsets = ((None, {
#        'fields': ('title', 'text', 'menu', 'video_ids')
#        }),)
    
    class Media:
        js = TINY_MCE_LIST
admin.site.register(Event, EventAdmin)

class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'published', 'featured')
    search_fields = ('title',)
    inlines = [FilesInline]
    prepopulated_fields = {"url": ("title",)}
    
    class Media:
        js = TINY_MCE_LIST
admin.site.register(News, NewsAdmin)


class TextSnippetAdminForm(forms.ModelForm):
    class Meta:
        model = TextSnippet

    def _media(self):
        if self.instance.use_visual_editor:
            js = TINY_MCE_LIST
        else:
            js = []
        return forms.Media(js=js)

    media = property(_media)

class TextSnippetAdmin(admin.ModelAdmin):
    form = TextSnippetAdminForm

    search_fields = ('key', 'text')
    list_display = ('key', 'snip', 'last_modified')
    readonly_fields = ('key',)

    def snip(self, instance):
        if instance.text:
            if len(instance.text) > 100:
                return instance.text[:100] + " ..."
            else:
                return instance.text
        else:
            return ""
    snip.short_description = 'tekststump'
admin.site.register(TextSnippet, TextSnippetAdmin)


class SignupAdmin(admin.ModelAdmin):
    list_display = ('name', 'company')
    list_display_links = ('name', 'company')
    search_fields = ('name', 'company')
admin.site.register(Signup, SignupAdmin)

class EventSignupAdmin(admin.ModelAdmin):
    list_display = ('name', 'company', 'address', 'email', 'event', 'amount')
    list_display_links = ('name', 'company')
    list_filter = ('event',)
    search_fields = ('name', 'company', 'event__name')
    actions = ['export']

    def export(self, request, queryset):
        import xlwt
        wb = xlwt.Workbook()
        sheet = wb.add_sheet("Tilmeldinger")

        for col, cell in enumerate(["Navn", "Firma", "Email", "Antal", "Andre deltagere"]):
            sheet.write(0, col, cell)

        for row, s in enumerate(queryset, 1):
            for col, cell in enumerate([s.name, s.company, s.email, s.amount, s.others]):
                sheet.write(row, col, cell)

        for i in range(3):
            sheet.col(i).width = 10000
        sheet.col(4).width = 20000

        #self.message_user(request, "%s drinks created as templates." % queryset.count())

        response = ExcelResponse()

        wb.save(response)
        response.set_excel_headers(u'Tilmeldinger.xls')

        return response
    export.short_description = u"Eksportér tilmeldinger"

class MyModelAdmin(admin.ModelAdmin):
    actions = ['export_csv']

    def export_csv(modeladmin, request, queryset):
	    import csv
	    from django.utils.encoding import smart_str
	    response = HttpResponse(mimetype='text/csv')
	    response['Content-Disposition'] = 'attachment; filename=kontakts.csv'
	    writer = csv.writer(response, csv.excel)
	    response.write(u'\ufeff'.encode('utf8')) # BOM (optional...Excel needs it to open UTF-8 file properly)
	    writer.writerow([
	    	smart_str(u"id"),
	        smart_str(u"name"),
	        smart_str(u"email"),
	        smart_str(u"comment"),
	    ])
	    for obj in queryset:
	        writer.writerow([
	            smart_str(obj.pk),
	            smart_str(obj.name),
	            smart_str(obj.email),
	            smart_str(obj.comment),
	        ])
	    return response
		# export_csv.short_description = u"Export CSV"
class ExportmailAdmin(admin.ModelAdmin):
    actions = ['export_csv']

    def export_csv(modeladmin, request, queryset):
        import csv
        from django.utils.encoding import smart_str
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=mailing list.csv'
        writer = csv.writer(response, csv.excel)
        response.write(u'\ufeff'.encode('utf8')) # BOM (optional...Excel needs it to open UTF-8 file properly)
        writer.writerow([
            smart_str(u"id"),
            smart_str(u"email"),
        ])
        for obj in queryset:
            writer.writerow([
                smart_str(obj.pk),
                smart_str(obj.email),
            ])
        return response

admin.site.register(EventSignup, EventSignupAdmin)
admin.site.register(StyleSheet)
admin.site.register(foot)
admin.site.register(Industry)
admin.site.register(contact, MyModelAdmin)
admin.site.register(mail)
admin.site.register(maillist,ExportmailAdmin)