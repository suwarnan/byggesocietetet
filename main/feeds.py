from django.contrib.syndication import feeds
from main.models import News, Event
import datetime
from django.core.urlresolvers import reverse as urlreverse

class NewsFeed(feeds.Feed):
    title = "Byggesocietetet - Nyheder"
    link = "/nyheder/"
    description = "Seneste nyheder fra Byggesocietetet"
    description_template = 'feed/news.html'

    def items(self):
        return News.objects.filter()[:10]

    def item_title(self, item):
        return item.title

    def item_pubdate(self, item):
        return item.published

    def item_link(self, item):
        return urlreverse('news_detail', args=(item.url,))

class EventFeed(feeds.Feed):
    title = "Byggesocietetet - Arrangementer"
    link = "/arrangementer/"
    description = "Kommende aktiviterer hos Byggesocietetet"
    description_template = 'feed/events.html'

    def items(self):
        return Event.objects.filter(date__gte=datetime.date.today()).order_by("-date")[:10]

    def item_title(self, item):
        return item.name

    def item_pubdate(self, item):
        return datetime.datetime.combine(item.date, datetime.time(0,0))

    def item_link(self, item):
        return urlreverse('event_detail', args=(item.url,))
