# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.conf import settings
from django.core.urlresolvers import reverse as urlreverse
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail, mail_managers, EmailMessage
from django.template.loader import render_to_string

from main.helpers import *
from main.models import *
from main.forms import *
from winkas.models import *
from feedparser import feedparser
from datetime import *

def mail_byggesocietetet(subject, message, extra_email=None):
    # get email addresses separated by newline or comma
    addresses = TextSnippet.get_snippet("email: administrator email-adresse").split("\n")
    addresses = [e.strip() for l in addresses for e in l.split(",") if e.strip()]

    if extra_email:
        addresses.append(extra_email)

    mail = EmailMessage(u'%s%s' % (settings.EMAIL_SUBJECT_PREFIX, subject),
                        message, settings.SERVER_EMAIL, addresses)
    mail.send()

def home(request):
    request.javascript = ['lib/jquery.cycle.all.min.js', 'home.js']

    featured = News.objects.filter(featured=True)
    top_images = FrontImage.objects.filter(active=True, position="top")
    bottom_images = FrontImage.objects.filter(active=True, position="bottom")[:3]
    feed = feedparser.parse(settings.NEWS_FEED)
    events = Event.objects.filter(date__gte=date.today()).order_by("date")[:2]

    return render_to_response("main/home.html", {
        'featured': featured[0] if featured else None,
        'top_images': top_images,
        'bottom_images': bottom_images,
        'news': News.objects.exclude(id__in=featured)[:3],
        'feed': feed.entries[:5],
        'events': events,
        }, context_instance=get_context(request))

def member_group_page(request, group_type, page_url=None):
    if page_url:
        return render_to_response("main/page.html", {
            'page': get_object_or_404(Page, section=group_type, url=page_url),
            }, context_instance=get_context(request))
    else:
        page = Page.objects.filter(section=group_type, primary=True)
        if not page:
            return HttpResponseNotFound()
        else:
            page = page[0]

    return render_to_response("main/member-group-page.html", {
        'group': group_type,
         'page': page,
        'box': MemberGroup.objects.filter(type='udvalg')
        }, context_instance=get_context(request))

def member_group(request, group_type, group_url):
    group = get_object_or_404(MemberGroup, type=group_type, url=group_url)

    events = Event.objects.all()
    # if group_type == "lokalomraade":
    #     events = events.filter(group=group)

    return render_to_response("main/member-group.html", {
        'group': group,
        # 'events': events[:10],
        'events': events.filter(date__gte=date.today()),
        }, context_instance=get_context(request))

def event_overview(request, mode=None, year=None):
    if not mode:
        return HttpResponseRedirect(urlreverse("event_overview_current"))
        
    events = Event.objects.all()

    if mode == "current":
        events = events.filter(date__year=2017, date__gte=date.today()).order_by("date")
    elif mode == "past":
        if year:
            events = events.filter(date__year=year, date__lt=date.today()).order_by("date")
        else:
            return HttpResponseRedirect(urlreverse('event_overview_past', args=(datetime.now().year,)))

    return render_to_response("main/event-overview.html", {
        'events': events,
        }, context_instance=get_context(request))

def event_page(request, page_url=None):
    return render_to_response("main/page.html", {
        'page': get_object_or_404(Page, section="event", url=page_url),
        }, context_instance=get_context(request))

def event_detail(request, event_url):
    event = get_object_or_404(Event, url=event_url)

    if request.method == "POST":
        form = EventSignupForm(request.POST)
        if form.is_valid():
            signup = form.save()

            if (signup.get_total()):
                recipient = u'"%s" <%s>' % (signup.name, signup.email)
                subject = u"Byggesocietetet - Faktura"

                message = render_to_string("email/invoice.html", {
                    'signup': signup,
                    'settings': settings,
                    'date': date.today(),
                    })

                if event.email:
                    from_email = u"%s <%s>" % (event.organizer, event.email)
                else:
                    from_email = settings.DEFAULT_FROM_EMAIL

                msg = EmailMessage(subject, message, from_email, [recipient])
                msg.content_subtype = "html"
                msg.send()


            message = render_to_string("email/event-signup.txt", {
                    'signup': signup,
                    })

            mail_byggesocietetet("Ny tilmelding til %s" % event.name, message, event.email)

            return render_to_response("main/event-signup-complete.html", {
                'form': form,
                }, context_instance=get_context(request))
    else:
        form = EventSignupForm(initial={ 'event': event })

    return render_to_response("main/event-detail.html", {
        'form': form,
        'past': event.date < date.today(),
        'event': event,
        }, context_instance=get_context(request))

def members_page(request):
    page = Page.objects.filter(section="member")
    if not page:
        return HttpResponseNotFound()
    else:
        page = page[0]

    return render_to_response("main/members-page.html", {
        'page': page,
        }, context_instance=get_context(request))

def member_registration(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            signup = form.save()
       
             # send_mail('Subject here','Here is the message.', 'from@example.com', ['skan@irstha.com'],fail_silently=False,)
            # message="hai"
#            message = """Hej
      
# Et nyt medlem har oprettet sig, se deres information her:

#   %s

# """ % (request.build_absolute_uri(urlreverse("admin:main_signup_change", args=[signup.id])))
            # mail_byggesocietetet("Nyt medlem", message)

            # subject = TextSnippet.get_snippet("email: tilmelding modtaget overskrift")
            # message = TextSnippet.get_snippet("email: tilmelding modtaget text")
            # mail = EmailMessage(subject, message, settings.SERVER_EMAIL, [signup.email])
            # mail.send()

            return render_to_response("main/member-signup-complete.html", {
                'form': form,
                }, context_instance=get_context(request))
    else:
        form = SignupForm()
       
    return render_to_response("main/member-signup.html", {
        'form': form,
        }, context_instance=get_context(request))
def member_contact(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            contact = form.save()
            
            return render_to_response("main/member-contact-complete.html", {
                'form': form,
                }, context_instance=get_context(request))
    else:
        form = ContactForm()

    return render_to_response("main/member-contact.html", {
        'form': form,
        }, context_instance=get_context(request))

@login_required
def member_section(request, member_type):
    form = MemberSearchForm(request.GET)
    if form.is_valid():
        data = form.cleaned_data
    else:
        data = {}

    if data['group']:
        members = Member.objects.filter(community__contains=data['group'])
    else:
        members = Member.objects.all()

    if data['query']:
        for word in data['query'].split():
            members = members.filter(models.Q(name__icontains=word) |
                                     models.Q(email__icontains=word) |
                                     models.Q(address__icontains=word) |
                                     models.Q(zipcode__icontains=word) |
                                     models.Q(city__icontains=word) |
                                     models.Q(company__icontains=word))

    return render_to_response("main/members.html", {
        'form': form,
        'members': members,
        }, context_instance=get_context(request))

def news_overview(request, year=None):
    if not year:
        return HttpResponseRedirect(urlreverse("news_overview", args=(datetime.now().year,)))

    return render_to_response("main/news-overview.html", {
        'news_items': News.objects.filter(published__year=year),
        }, context_instance=get_context(request))

def news_detail(request, news_url):
    return render_to_response("main/news-detail.html", {
        'news': get_object_or_404(News, url=news_url),
        }, context_instance=get_context(request))

def page(request, page_url):
    return render_to_response("main/page.html", {
        'page': get_object_or_404(Page, url=page_url),
        }, context_instance=get_context(request))

def page1(request, page_url):
    # return render_to_response("main/page1.html", {
    #     'page': get_object_or_404(Page, url=page_url),
    #     }, context_instance=get_context(request))
    if request.method == "POST":
        form = MailForm(request.POST)
        if form.is_valid():
            mail = form.save()
            
            # return render_to_response("main/member-contact-complete.html", {
            #     'form': form,
            #     }, context_instance=get_context(request))
    else:
        form = MailForm()

    return render_to_response("main/page1.html", {
        'form': form,'page': get_object_or_404(Page, url=page_url),
        }, context_instance=get_context(request))
