# Django settings for byggesocietetet project.
import os.path
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Thilan Irshad', 'thilan.irshad@gmail.com'),
)

MANAGERS = (
    ('Thilan Irshad', 'thilan.irshad@gmail.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'test.db'),    
        'USER': '',    
        'PASSWORD': '',
        'HOST': '',    
        'PORT': '',    
    },
    
    'winkas': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'bygge',
        'USER': 'byggem',
        'PASSWORD': 'mediabyg',
        'HOST': '195.249.85.62',
        'PORT': '3306',
        'OPTIONS': {
            'charset': 'latin1',
            },
    }
}




DATABASE_ROUTERS = (
    "routers.WinkasRouter",
)

SOUTH_DATABASE_ADAPTERS = {
    'default': "south.db.sqlite3",
    'winkas': "south.db.mysql",
}

TIME_ZONE = 'Europe/Copenhagen'
LANGUAGE_CODE = 'da'
SITE_ID = 1
# LANGUAGES = (('da', _('Danish')))

USE_L10N = True
USE_I18N = True
# LANGUAGE_CODE = 'da'


MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
ADMIN_MEDIA_PREFIX = '/adminmedia/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

SECRET_KEY = 'spbg-t-7z80gi!zmk%zmc+k)&!=umqlhn!sdjpeg@=vv@fu^6z'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
   # 'django.middleware.csrf.CsrfViewMiddleware',
    'winkas.middleware.WinkasExceptionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'utils.modtimeurls.ModTimeUrlsMiddleware',
    'django.middleware.locale.LocaleMiddleware'
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates/'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.admin',
    'utils',
    'south',
    'winkas',
    'main',
   # 'django.contrib.staticfiles',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'winkas.auth.WinkasAuthBackend',
    )

# AUTH_PROFILE_MODULE = 'accounts.UserProfile'
# LOGIN_REDIRECT_URL = '/'
# LOGIN_URL = '/account/login/'

RUNNING_ON_LIVE = False
SERVER_URL = "http://www.byg.dk"

EMAIL_SUBJECT_PREFIX = "[byg.dk] "
DEFAULT_FROM_EMAIL = "byg <info@byg.dk>"
SERVER_EMAIL = "byg <noreply@byg.dk>"

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# start debug server with python -m smtpd -n -c DebuggingServer localhost:1025
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25

NEWS_FEED = "http://estatemedia.dk/dk/feed/"

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# import local settings overriding the defaults
try:
    from local_settings import *
except ImportError:
    try:
        from mod_python import apache
        apache.log_error( "local_settings.py not set; using default settings", apache.APLOG_NOTICE )
    except ImportError:
        import sys
        sys.stderr.write( "local_settings.py not set; using default settings\n" )

